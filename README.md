#-----------------------------------------------------------------------------------
#                             3D Quadrupole field study                             
#-----------------------------------------------------------------------------------

Macros and utilities to create simulation inputs and to run analysis of impact 
of 3D field distribution on beam dynamics.


#    - Theory
#-----------------------------------------------------------------------------------

This directory contains the jupyter-lab script develloping the theory of Amplitude 
Detuning and Amplitude Beta-Beating. Those code in Symbolic can be use as long as 
jupyter-lab or an equivalent is installed. 

A procedure to install Anaconda (and jupyter-lab) has explain in "Install_conda.txt".




#    - detuning_analysis
#-----------------------------------------------------------------------------------

This directory contains two differents python class: 

In "detuning_predition", the class case_Detuning_Theoric will predict the Amplitude 
Detuning and Amplitude Beta-Beating given an optics and an error file .tfs from MADX. 
This file must contain the strength of the errors as well as the twiss parameters. 
It is also possible to comput the correctors strength in order to correct the effect 
predicted. The fringe field file can also be taken into consideration for the 
analysis.

In "detuning_analysis", the class Case_Detuning_Simulated is used to analysed SixTrack
Output (using the "DUMP" option). An home-made FFT is done (not using NAFF or SUSSIX),
in order to identify the tunes and measure the actions (2Ju, linear interpretation).




#    - example
#-----------------------------------------------------------------------------------

This directory contains different example of analysis that have been done using the 
python class mentionned previously.

#          * example/DA_plot_stat/
It contains python scripts to compute and plot Dynamics Aperture results given by the 
./sixdb namedatabase.db da and ./sixdb namedatabase.db da_vs_turns commands

#          * example/Tracking_simulation_inputs/ 
It contains general scripts to create the vector potential inputs files using the 
vec_pot code and the scripts to change the fort.2 and fort.3 input files to run 
simulations with Sixtrack. 

For DA simulation sixdesk enviroment (https://github.com/SixTrack/SixDesk) is
used so the fort.3.mother2(.tmp) needs to be modified.

#          * 
