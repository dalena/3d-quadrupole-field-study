#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt


sys.path.insert(0, '/local/home/tpugnat/Documents/CEA/SixTrack/These_Workplace/Combin/NAFF/python/')
from class_case_Detuning_Simulation import Case_Detuning_Simulated as cas_DT_Sm


sys.path.insert(0, '/local/home/tpugnat/Documents/CEA/SixTrack/These_Workplace/Combin/detuning_prediction/')
from class_case_Detuning_Theoric    import case_Detuning_Theoric   as cas_DT_Th


plt.rcParams.update({'font.size': 14})
plt.rc('xtick', labelsize=14) 
plt.rc('ytick', labelsize=14) 



#Q1/Q3
#l_MQXFA   =8.004/2;
#Q2a/Q2b
#l_MQXFB   =6.792;
#FL connector side
#l_MQXFcs  = 0.620;
#FL non connector side
#l_MQXFnc  = 0.581;
#l_TOT_FL=4.*( 4.*l_MQXFA+2.*l_MQXFB + 6.*(l_MQXFcs+l_MQXFnc) )
#l_TOT_NM=4.*( 4.*l_MQXFA+2.*l_MQXFB )
#l_TOT_FL=l_TOT_NM




# --------------------------------------------------------------------------------
# Data analytic
# --------------------------------------------------------------------------------
Analytic_mctx1=[]
drt="/local/home/tpugnat/Documents/CEA/SixTrack/These_Workplace/SixTrack/Test_Dump2/"
#         * Nominal
#             > Define case
c=cas_DT_Th(case_name="HE Analytic",case_col="g", \
#            path_opt=drt+"six_inp_flatbeam_nominal_corupto6_hllhcv1.0_triponly_withb4/", \
            path_opt=drt+"six_inp_cases/", \
#            path_err=drt+"six_inp_flatbeam_nominal_corupto6_hllhcv1.0_triponly_withb4/temp_1/")
            path_err=drt+"six_inp_flatbeam_nominal_corupto6_hllhcv1.0_onlyb6/temp_1/")
#             > Add Quad
#             > Add Skip
#             > Add File
#             > Save case
Analytic_mctx1.append(c)

#         * Riccardo
#             > Define case
c=cas_DT_Th(case_name="HE+Heads Analytic",case_col="r", \
#            path_opt=drt+"six_inp_flatbeam_riccardo_corupto6_hllhcv1.0_triponly_withb4/", \
            path_opt=drt+"six_inp_cases/", \
#            path_err=drt+"six_inp_flatbeam_riccardo_corupto6_hllhcv1.0_triponly_withb4/temp_1/")
            path_err=drt+"six_inp_flatbeam_riccardo_corupto6_hllhcv1.0_onlyb6/temp_1/")
#             > Add Quad
#             > Add Skip
#             > Add File
#             > Save case
#Analytic_mctx1.append(c)

#         * Lie2 ND0
#             > Define case
c=cas_DT_Th(case_name="Lie2 ND0 Analytic",case_col="b", \
#            path_opt=drt+"six_inp_flatbeam_Lie2ND0_corupto6_hllhcv1.0_triponly_withb4/", \
            path_opt=drt+"six_inp_cases/", \
#            path_err=drt+"six_inp_flatbeam_Lie2ND0_corupto6_hllhcv1.0_triponly_withb4/temp_1/")
            path_err=drt+"six_inp_flatbeam_Lie2ND0_corupto6_hllhcv1.0_onlyb6/temp_1/")
#             > Add Quad
c.add_Quad("MQXFA.A1L5..1" ,5 , 1); c.add_Quad("MQXFA.A1L5..16",10, 0)
c.add_Quad("MQXFA.B1L5..1" ,4 , 1); c.add_Quad("MQXFA.B1L5..16",12, 0)
c.add_Quad("MQXFB.A2L5..1" ,3 , 1); c.add_Quad("MQXFB.A2L5..16",8 , 0)
c.add_Quad("MQXFB.B2L5..1" ,1 , 1); c.add_Quad("MQXFB.B2L5..16",7 , 0)
c.add_Quad("MQXFA.A3L5..1" ,5 , 1); c.add_Quad("MQXFA.A3L5..16",10, 0)
c.add_Quad("MQXFA.B3L5..1" ,4 , 1); c.add_Quad("MQXFA.B3L5..16",12, 0)
c.add_Quad("MQXFA.A3R5..1" ,3 , 1); c.add_Quad("MQXFA.A3R5..16",9 , 0)
c.add_Quad("MQXFA.B3R5..1" ,2 , 1); c.add_Quad("MQXFA.B3R5..16",7 , 0)
c.add_Quad("MQXFB.A2R5..1" ,6 , 1); c.add_Quad("MQXFB.A2R5..16",10, 0)
c.add_Quad("MQXFB.B2R5..1" ,4 , 1); c.add_Quad("MQXFB.B2R5..16",11, 0)
c.add_Quad("MQXFA.A1R5..1" ,3 , 1); c.add_Quad("MQXFA.A1R5..16",9 , 0)
c.add_Quad("MQXFA.B1R5..1" ,2 , 1); c.add_Quad("MQXFA.B1R5..16",7 , 0)
c.add_Quad("MQXFA.A1L1..1" ,5 , 1); c.add_Quad("MQXFA.A1L1..16",10, 0)
c.add_Quad("MQXFA.B1L1..1" ,4 , 1); c.add_Quad("MQXFA.B1L1..16",12, 0)
c.add_Quad("MQXFB.A2L1..1" ,3 , 1); c.add_Quad("MQXFB.A2L1..16",8 , 0)
c.add_Quad("MQXFB.B2L1..1" ,1 , 1); c.add_Quad("MQXFB.B2L1..16",7 , 0)
c.add_Quad("MQXFA.A3L1..1" ,5 , 1); c.add_Quad("MQXFA.A3L1..16",10, 0)
c.add_Quad("MQXFA.B3L1..1" ,4 , 1); c.add_Quad("MQXFA.B3L1..16",12, 0)
c.add_Quad("MQXFA.A3R1..1" ,3 , 1); c.add_Quad("MQXFA.A3R1..16",9 , 0)
c.add_Quad("MQXFA.B3R1..1" ,2 , 1); c.add_Quad("MQXFA.B3R1..16",7 , 0)
c.add_Quad("MQXFB.A2R1..1" ,6 , 1); c.add_Quad("MQXFB.A2R1..16",10, 0)
c.add_Quad("MQXFB.B2R1..1" ,4 , 1); c.add_Quad("MQXFB.B2R1..16",11, 0)
c.add_Quad("MQXFA.A1R1..1" ,3 , 1); c.add_Quad("MQXFA.A1R1..16",9 , 0)
c.add_Quad("MQXFA.B1R1..1" ,2 , 1); c.add_Quad("MQXFA.B1R1..16",7 , 0)
#             > Add Skip
c.add_Skip("MQXFA.B3L5..FL"); c.add_Skip("MQXFA.A3L5..FL"); c.add_Skip("MQXFB.B2L5..FL")
c.add_Skip("MQXFB.A2L5..FL"); c.add_Skip("MQXFA.B1L5..FL"); c.add_Skip("MQXFA.A1L5..FL")
c.add_Skip("MQXFA.A1R5..FL"); c.add_Skip("MQXFA.B1R5..FL"); c.add_Skip("MQXFB.A2R5..FL")
c.add_Skip("MQXFB.B2R5..FL"); c.add_Skip("MQXFA.A3R5..FL"); c.add_Skip("MQXFA.B3R5..FL")
c.add_Skip("MQXFA.B3L1..FL"); c.add_Skip("MQXFA.A3L1..FL"); c.add_Skip("MQXFB.B2L1..FL")
c.add_Skip("MQXFB.A2L1..FL"); c.add_Skip("MQXFA.B1L1..FL"); c.add_Skip("MQXFA.A1L1..FL")
c.add_Skip("MQXFA.A1R1..FL"); c.add_Skip("MQXFA.B1R1..FL"); c.add_Skip("MQXFB.A2R1..FL")
c.add_Skip("MQXFB.B2R1..FL"); c.add_Skip("MQXFA.A3R1..FL"); c.add_Skip("MQXFA.B3R1..FL")
c.add_Skip("MQXFA.B3L5..FR"); c.add_Skip("MQXFA.A3L5..FR"); c.add_Skip("MQXFB.B2L5..FR")
c.add_Skip("MQXFB.A2L5..FR"); c.add_Skip("MQXFA.B1L5..FR"); c.add_Skip("MQXFA.A1L5..FR")
c.add_Skip("MQXFA.A1R5..FR"); c.add_Skip("MQXFA.B1R5..FR"); c.add_Skip("MQXFB.A2R5..FR")
c.add_Skip("MQXFB.B2R5..FR"); c.add_Skip("MQXFA.A3R5..FR"); c.add_Skip("MQXFA.B3R5..FR")
c.add_Skip("MQXFA.B3L1..FR"); c.add_Skip("MQXFA.A3L1..FR"); c.add_Skip("MQXFB.B2L1..FR")
c.add_Skip("MQXFB.A2L1..FR"); c.add_Skip("MQXFA.B1L1..FR"); c.add_Skip("MQXFA.A1L1..FR")
c.add_Skip("MQXFA.A1R1..FR"); c.add_Skip("MQXFA.B1R1..FR"); c.add_Skip("MQXFB.A2R1..FR")
c.add_Skip("MQXFB.B2R1..FR"); c.add_Skip("MQXFA.A3R1..FR"); c.add_Skip("MQXFA.B3R1..FR")
#             > Add File
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_in_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m.out"             ,5.91783358704972118e-01,True ) #,1.420
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_in_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_mid.out"         ,5.91783358704972118e-01,True ) #,1.420
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_in_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_inv.out"         ,6.29915713814027667e-01,True ) #,1.620
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_in_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_inv_opp.out"     ,6.29915713814027667e-01,True ) #,1.620
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_in_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_opp_mid.out"     ,5.91783358704972118e-01,True ) #,1.420
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_in_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_opp.out"         ,5.91783358704972118e-01,True ) #,1.420
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_out_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m.out"            ,6.09915713246783286e-01,False) #,1.620
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_out_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_inv.out"        ,5.71783359230295996e-01,False) #,1.420
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_out_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_inv_mid.out"    ,5.71783359230295996e-01,False) #,1.420
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_out_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_opp.out"        ,6.09915713246783286e-01,False) #,1.620
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_out_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_inv_opp.out"    ,5.71783359230295996e-01,False) #,1.420
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_out_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_inv_opp_mid.out",5.71783359230295996e-01,False) #,1.420
#             > Save case
#Analytic_mctx1.append(c)

#         * Lie2 ND6
#             > Define case
c=cas_DT_Th(case_name="Lie2 ND6 Analytic",case_col="k", \
#            path_opt=drt+"six_inp_flatbeam_Lie2ND6_corupto6_hllhcv1.0_triponly_withb4/", \
            path_opt=drt+"six_inp_cases/", \
#            path_err=drt+"six_inp_flatbeam_Lie2ND6_corupto6_hllhcv1.0_triponly_withb4/temp_1/")
            path_err=drt+"six_inp_flatbeam_Lie2ND6_corupto6_hllhcv1.0_onlyb6/temp_1/")
#             > Add Quad
c.add_Quad("MQXFA.A1L5..1" ,5 , 1); c.add_Quad("MQXFA.A1L5..16",10, 0)
c.add_Quad("MQXFA.B1L5..1" ,4 , 1); c.add_Quad("MQXFA.B1L5..16",12, 0)
c.add_Quad("MQXFB.A2L5..1" ,3 , 1); c.add_Quad("MQXFB.A2L5..16",8 , 0)
c.add_Quad("MQXFB.B2L5..1" ,1 , 1); c.add_Quad("MQXFB.B2L5..16",7 , 0)
c.add_Quad("MQXFA.A3L5..1" ,5 , 1); c.add_Quad("MQXFA.A3L5..16",10, 0)
c.add_Quad("MQXFA.B3L5..1" ,4 , 1); c.add_Quad("MQXFA.B3L5..16",12, 0)
c.add_Quad("MQXFA.A3R5..1" ,3 , 1); c.add_Quad("MQXFA.A3R5..16",9 , 0)
c.add_Quad("MQXFA.B3R5..1" ,2 , 1); c.add_Quad("MQXFA.B3R5..16",7 , 0)
c.add_Quad("MQXFB.A2R5..1" ,6 , 1); c.add_Quad("MQXFB.A2R5..16",10, 0)
c.add_Quad("MQXFB.B2R5..1" ,4 , 1); c.add_Quad("MQXFB.B2R5..16",11, 0)
c.add_Quad("MQXFA.A1R5..1" ,3 , 1); c.add_Quad("MQXFA.A1R5..16",9 , 0)
c.add_Quad("MQXFA.B1R5..1" ,2 , 1); c.add_Quad("MQXFA.B1R5..16",7 , 0)
c.add_Quad("MQXFA.A1L1..1" ,5 , 1); c.add_Quad("MQXFA.A1L1..16",10, 0)
c.add_Quad("MQXFA.B1L1..1" ,4 , 1); c.add_Quad("MQXFA.B1L1..16",12, 0)
c.add_Quad("MQXFB.A2L1..1" ,3 , 1); c.add_Quad("MQXFB.A2L1..16",8 , 0)
c.add_Quad("MQXFB.B2L1..1" ,1 , 1); c.add_Quad("MQXFB.B2L1..16",7 , 0)
c.add_Quad("MQXFA.A3L1..1" ,5 , 1); c.add_Quad("MQXFA.A3L1..16",10, 0)
c.add_Quad("MQXFA.B3L1..1" ,4 , 1); c.add_Quad("MQXFA.B3L1..16",12, 0)
c.add_Quad("MQXFA.A3R1..1" ,3 , 1); c.add_Quad("MQXFA.A3R1..16",9 , 0)
c.add_Quad("MQXFA.B3R1..1" ,2 , 1); c.add_Quad("MQXFA.B3R1..16",7 , 0)
c.add_Quad("MQXFB.A2R1..1" ,6 , 1); c.add_Quad("MQXFB.A2R1..16",10, 0)
c.add_Quad("MQXFB.B2R1..1" ,4 , 1); c.add_Quad("MQXFB.B2R1..16",11, 0)
c.add_Quad("MQXFA.A1R1..1" ,3 , 1); c.add_Quad("MQXFA.A1R1..16",9 , 0)
c.add_Quad("MQXFA.B1R1..1" ,2 , 1); c.add_Quad("MQXFA.B1R1..16",7 , 0)
#             > Add Skip
c.add_Skip("MQXFA.B3L5..FL"); c.add_Skip("MQXFA.A3L5..FL"); c.add_Skip("MQXFB.B2L5..FL")
c.add_Skip("MQXFB.A2L5..FL"); c.add_Skip("MQXFA.B1L5..FL"); c.add_Skip("MQXFA.A1L5..FL")
c.add_Skip("MQXFA.A1R5..FL"); c.add_Skip("MQXFA.B1R5..FL"); c.add_Skip("MQXFB.A2R5..FL")
c.add_Skip("MQXFB.B2R5..FL"); c.add_Skip("MQXFA.A3R5..FL"); c.add_Skip("MQXFA.B3R5..FL")
c.add_Skip("MQXFA.B3L1..FL"); c.add_Skip("MQXFA.A3L1..FL"); c.add_Skip("MQXFB.B2L1..FL")
c.add_Skip("MQXFB.A2L1..FL"); c.add_Skip("MQXFA.B1L1..FL"); c.add_Skip("MQXFA.A1L1..FL")
c.add_Skip("MQXFA.A1R1..FL"); c.add_Skip("MQXFA.B1R1..FL"); c.add_Skip("MQXFB.A2R1..FL")
c.add_Skip("MQXFB.B2R1..FL"); c.add_Skip("MQXFA.A3R1..FL"); c.add_Skip("MQXFA.B3R1..FL")
c.add_Skip("MQXFA.B3L5..FR"); c.add_Skip("MQXFA.A3L5..FR"); c.add_Skip("MQXFB.B2L5..FR")
c.add_Skip("MQXFB.A2L5..FR"); c.add_Skip("MQXFA.B1L5..FR"); c.add_Skip("MQXFA.A1L5..FR")
c.add_Skip("MQXFA.A1R5..FR"); c.add_Skip("MQXFA.B1R5..FR"); c.add_Skip("MQXFB.A2R5..FR")
c.add_Skip("MQXFB.B2R5..FR"); c.add_Skip("MQXFA.A3R5..FR"); c.add_Skip("MQXFA.B3R5..FR")
c.add_Skip("MQXFA.B3L1..FR"); c.add_Skip("MQXFA.A3L1..FR"); c.add_Skip("MQXFB.B2L1..FR")
c.add_Skip("MQXFB.A2L1..FR"); c.add_Skip("MQXFA.B1L1..FR"); c.add_Skip("MQXFA.A1L1..FR")
c.add_Skip("MQXFA.A1R1..FR"); c.add_Skip("MQXFA.B1R1..FR"); c.add_Skip("MQXFB.A2R1..FR")
c.add_Skip("MQXFB.B2R1..FR"); c.add_Skip("MQXFA.A3R1..FR"); c.add_Skip("MQXFA.B3R1..FR")
#             > Add File
c.add_File(drt+"C2-6-10-14_ND6_dz02_AF/coeff_in_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m.out"             ,5.91783358704972118e-01,True ) #,1.420
c.add_File(drt+"C2-6-10-14_ND6_dz02_AF/coeff_in_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m_mid.out"         ,5.91783358704972118e-01,True ) #,1.420
c.add_File(drt+"C2-6-10-14_ND6_dz02_AF/coeff_in_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m_inv.out"         ,6.29915713814027667e-01,True ) #,1.620
c.add_File(drt+"C2-6-10-14_ND6_dz02_AF/coeff_in_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m_inv_opp.out"     ,6.29915713814027667e-01,True ) #,1.620
c.add_File(drt+"C2-6-10-14_ND6_dz02_AF/coeff_in_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m_opp_mid.out"     ,5.91783358704972118e-01,True ) #,1.420
c.add_File(drt+"C2-6-10-14_ND6_dz02_AF/coeff_in_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m_opp.out"         ,5.91783358704972118e-01,True ) #,1.420
c.add_File(drt+"C2-6-10-14_ND6_dz02_AF/coeff_out_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m.out"            ,6.09915713246783286e-01,False) #,1.620
c.add_File(drt+"C2-6-10-14_ND6_dz02_AF/coeff_out_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m_inv.out"        ,5.71783359230295996e-01,False) #,1.420
c.add_File(drt+"C2-6-10-14_ND6_dz02_AF/coeff_out_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m_inv_mid.out"    ,5.71783359230295996e-01,False) #,1.420
c.add_File(drt+"C2-6-10-14_ND6_dz02_AF/coeff_out_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m_opp.out"        ,6.09915713246783286e-01,False) #,1.620
c.add_File(drt+"C2-6-10-14_ND6_dz02_AF/coeff_out_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m_inv_opp.out"    ,5.71783359230295996e-01,False) #,1.420
c.add_File(drt+"C2-6-10-14_ND6_dz02_AF/coeff_out_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m_inv_opp_mid.out",5.71783359230295996e-01,False) #,1.420
#             > Save case
#Analytic_mctx1.append(c)




Analytic_mctx0=[]
drt="/local/home/tpugnat/Documents/CEA/SixTrack/These_Workplace/SixTrack/Test_Dump2/"
#         * Nominal
#             > Define case
c=cas_DT_Th(case_name="HE Analytic",case_col="g", \
#            path_opt=drt+"six_inp_flatbeam_nominal_corupto6_hllhcv1.0_triponly_withb4/", \
            path_opt=drt+"six_inp_cases/", \
#            path_err=drt+"six_inp_flatbeam_nominal_corupto6_hllhcv1.0_triponly_withb4/temp_1/")
            path_err=drt+"six_inp_flatbeam_nominal_corupto6_hllhcv1.0_onlyb6/temp_1/")
#             > Add Quad
#             > Add Skip

c.add_Skip("MCTX.3L5"); c.add_Skip("MCTX.3R5"); c.add_Skip("MCTX.3L1"); c.add_Skip("MCTX.3R1");
c.add_Skip("MCTX.3L8"); c.add_Skip("MCTX.3R8"); c.add_Skip("MCTX.3L2"); c.add_Skip("MCTX.3R2");
#             > Add File
#             > Save case
Analytic_mctx0.append(c)

#         * Riccardo
#             > Define case
c=cas_DT_Th(case_name="HE+Head Analytic",case_col="r", \
#            path_opt=drt+"six_inp_flatbeam_riccardo_corupto6_hllhcv1.0_triponly_withb4/", \
            path_opt=drt+"six_inp_cases/", \
#            path_err=drt+"six_inp_flatbeam_riccardo_corupto6_hllhcv1.0_triponly_withb4/temp_1/")
            path_err=drt+"six_inp_flatbeam_riccardo_corupto6_hllhcv1.0_onlyb6/temp_1/")
#             > Add Quad
#             > Add Skip

c.add_Skip("MCTX.3L5"); c.add_Skip("MCTX.3R5"); c.add_Skip("MCTX.3L1"); c.add_Skip("MCTX.3R1");
c.add_Skip("MCTX.3L8"); c.add_Skip("MCTX.3R8"); c.add_Skip("MCTX.3L2"); c.add_Skip("MCTX.3R2");
#             > Add File
#             > Save case
#Analytic_mctx0.append(c)

#         * Lie2 ND0
#             > Define case
c=cas_DT_Th(case_name="Lie2 ND0 Analytic",case_col="b", \
#            path_opt=drt+"six_inp_flatbeam_Lie2ND0_corupto6_hllhcv1.0_triponly_withb4/", \
            path_opt=drt+"six_inp_cases/", \
#            path_err=drt+"six_inp_flatbeam_Lie2ND0_corupto6_hllhcv1.0_triponly_withb4/temp_1/")
            path_err=drt+"six_inp_flatbeam_Lie2ND0_corupto6_hllhcv1.0_onlyb6/temp_1/")
#             > Add Quad
c.add_Quad("MQXFA.A1L5..1" ,      5,       1);   c.add_Quad("MQXFA.A1L5..16",      10,      0)
c.add_Quad("MQXFA.B1L5..1" ,      4  ,     1);   c.add_Quad("MQXFA.B1L5..16",      12,      0)
c.add_Quad("MQXFB.A2L5..1" ,      3 ,      1);   c.add_Quad("MQXFB.A2L5..16",      8 ,      0)
c.add_Quad("MQXFB.B2L5..1" ,      1 ,      1);   c.add_Quad("MQXFB.B2L5..16",      7 ,      0)
c.add_Quad("MQXFA.A3L5..1" ,      5 ,      1);   c.add_Quad("MQXFA.A3L5..16",      10,      0)
c.add_Quad("MQXFA.B3L5..1" ,      4 ,      1);   c.add_Quad("MQXFA.B3L5..16",      12,      0)
c.add_Quad("MQXFA.A3R5..1" ,      3 ,      1);   c.add_Quad("MQXFA.A3R5..16",      9 ,      0)
c.add_Quad("MQXFA.B3R5..1" ,      2 ,      1);   c.add_Quad("MQXFA.B3R5..16",      7 ,      0)
c.add_Quad("MQXFB.A2R5..1" ,      6 ,      1);   c.add_Quad("MQXFB.A2R5..16",      10,      0)
c.add_Quad("MQXFB.B2R5..1" ,      4 ,      1);   c.add_Quad("MQXFB.B2R5..16",      11,      0)
c.add_Quad("MQXFA.A1R5..1" ,      3,       1);   c.add_Quad("MQXFA.A1R5..16",      9,       0)
c.add_Quad("MQXFA.B1R5..1" ,      2,       1);   c.add_Quad("MQXFA.B1R5..16",      7,       0)
c.add_Quad("MQXFA.A1L1..1" ,      5,       1);   c.add_Quad("MQXFA.A1L1..16",      10,      0)
c.add_Quad("MQXFA.B1L1..1" ,      4  ,     1);   c.add_Quad("MQXFA.B1L1..16",      12,      0)
c.add_Quad("MQXFB.A2L1..1" ,      3 ,      1);   c.add_Quad("MQXFB.A2L1..16",      8 ,      0)
c.add_Quad("MQXFB.B2L1..1" ,      1 ,      1);   c.add_Quad("MQXFB.B2L1..16",      7 ,      0)
c.add_Quad("MQXFA.A3L1..1" ,      5 ,      1);   c.add_Quad("MQXFA.A3L1..16",      10,      0)
c.add_Quad("MQXFA.B3L1..1" ,      4 ,      1);   c.add_Quad("MQXFA.B3L1..16",      12,      0)
c.add_Quad("MQXFA.A3R1..1" ,      3 ,      1);   c.add_Quad("MQXFA.A3R1..16",      9 ,      0)
c.add_Quad("MQXFA.B3R1..1" ,      2 ,      1);   c.add_Quad("MQXFA.B3R1..16",      7 ,      0)
c.add_Quad("MQXFB.A2R1..1" ,      6 ,      1);   c.add_Quad("MQXFB.A2R1..16",      10,      0)
c.add_Quad("MQXFB.B2R1..1" ,      4 ,      1);   c.add_Quad("MQXFB.B2R1..16",      11,      0)
c.add_Quad("MQXFA.A1R1..1" ,      3,       1);   c.add_Quad("MQXFA.A1R1..16",      9,       0)
c.add_Quad("MQXFA.B1R1..1" ,      2,       1);   c.add_Quad("MQXFA.B1R1..16",      7,       0)
#             > Add Skip
c.add_Skip("MQXFA.B3L5..FL");   c.add_Skip("MQXFA.A3L5..FL")
c.add_Skip("MQXFB.B2L5..FL");   c.add_Skip("MQXFB.A2L5..FL")
c.add_Skip("MQXFA.B1L5..FL");   c.add_Skip("MQXFA.A1L5..FL")
c.add_Skip("MQXFA.A1R5..FL");   c.add_Skip("MQXFA.B1R5..FL")
c.add_Skip("MQXFB.A2R5..FL");   c.add_Skip("MQXFB.B2R5..FL")
c.add_Skip("MQXFA.A3R5..FL");   c.add_Skip("MQXFA.B3R5..FL")
c.add_Skip("MQXFA.B3L1..FL");   c.add_Skip("MQXFA.A3L1..FL")
c.add_Skip("MQXFB.B2L1..FL");   c.add_Skip("MQXFB.A2L1..FL")
c.add_Skip("MQXFA.B1L1..FL");   c.add_Skip("MQXFA.A1L1..FL")
c.add_Skip("MQXFA.A1R1..FL");   c.add_Skip("MQXFA.B1R1..FL")
c.add_Skip("MQXFB.A2R1..FL");   c.add_Skip("MQXFB.B2R1..FL")
c.add_Skip("MQXFA.A3R1..FL");   c.add_Skip("MQXFA.B3R1..FL")
c.add_Skip("MQXFA.B3L5..FR");   c.add_Skip("MQXFA.A3L5..FR")
c.add_Skip("MQXFB.B2L5..FR");   c.add_Skip("MQXFB.A2L5..FR")
c.add_Skip("MQXFA.B1L5..FR");   c.add_Skip("MQXFA.A1L5..FR")
c.add_Skip("MQXFA.A1R5..FR");   c.add_Skip("MQXFA.B1R5..FR")
c.add_Skip("MQXFB.A2R5..FR");   c.add_Skip("MQXFB.B2R5..FR")
c.add_Skip("MQXFA.A3R5..FR");   c.add_Skip("MQXFA.B3R5..FR")
c.add_Skip("MQXFA.B3L1..FR");   c.add_Skip("MQXFA.A3L1..FR")
c.add_Skip("MQXFB.B2L1..FR");   c.add_Skip("MQXFB.A2L1..FR")
c.add_Skip("MQXFA.B1L1..FR");   c.add_Skip("MQXFA.A1L1..FR")
c.add_Skip("MQXFA.A1R1..FR");   c.add_Skip("MQXFA.B1R1..FR")
c.add_Skip("MQXFB.A2R1..FR");   c.add_Skip("MQXFB.B2R1..FR")
c.add_Skip("MQXFA.A3R1..FR");   c.add_Skip("MQXFA.B3R1..FR")
c.add_Skip("MQXFB.B2R1..FR"); c.add_Skip("MQXFA.A3R1..FR"); c.add_Skip("MQXFA.B3R1..FR")

c.add_Skip("MCTX.3L5"); c.add_Skip("MCTX.3R5"); c.add_Skip("MCTX.3L1"); c.add_Skip("MCTX.3R1");
c.add_Skip("MCTX.3L8"); c.add_Skip("MCTX.3R8"); c.add_Skip("MCTX.3L2"); c.add_Skip("MCTX.3R2");
#             > Add File
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_in_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m.out"             ,5.91783358704972118e-01,True ) #,1.420
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_in_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_mid.out"         ,5.91783358704972118e-01,True ) #,1.420
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_in_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_inv.out"         ,6.29915713814027667e-01,True ) #,1.620
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_in_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_inv_opp.out"     ,6.29915713814027667e-01,True ) #,1.620
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_in_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_opp_mid.out"     ,5.91783358704972118e-01,True ) #,1.420
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_in_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_opp.out"         ,5.91783358704972118e-01,True ) #,1.420
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_out_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m.out"            ,6.09915713246783286e-01,False) #,1.620
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_out_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_inv.out"        ,5.71783359230295996e-01,False) #,1.420
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_out_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_inv_mid.out"    ,5.71783359230295996e-01,False) #,1.420
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_out_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_opp.out"        ,6.09915713246783286e-01,False) #,1.620
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_out_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_inv_opp.out"    ,5.71783359230295996e-01,False) #,1.420
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_out_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_inv_opp_mid.out",5.71783359230295996e-01,False) #,1.420
#             > Save case
#Analytic_mctx0.append(c)

#         * Lie2 ND6
#             > Define case
c=cas_DT_Th(case_name="Lie2 ND6 Analytic",case_col="k", \
#            path_opt=drt+"six_inp_flatbeam_Lie2ND6_corupto6_hllhcv1.0_triponly_withb4/", \
            path_opt=drt+"six_inp_cases/", \
#            path_err=drt+"six_inp_flatbeam_Lie2ND6_corupto6_hllhcv1.0_triponly_withb4/temp_1/")
            path_err=drt+"six_inp_flatbeam_Lie2ND6_corupto6_hllhcv1.0_onlyb6/temp_1/")
#             > Add Quad
c.add_Quad("MQXFA.A1L5..1" ,      5,       1);   c.add_Quad("MQXFA.A1L5..16",      10,      0)
c.add_Quad("MQXFA.B1L5..1" ,      4  ,     1);   c.add_Quad("MQXFA.B1L5..16",      12,      0)
c.add_Quad("MQXFB.A2L5..1" ,      3 ,      1);   c.add_Quad("MQXFB.A2L5..16",      8 ,      0)
c.add_Quad("MQXFB.B2L5..1" ,      1 ,      1);   c.add_Quad("MQXFB.B2L5..16",      7 ,      0)
c.add_Quad("MQXFA.A3L5..1" ,      5 ,      1);   c.add_Quad("MQXFA.A3L5..16",      10,      0)
c.add_Quad("MQXFA.B3L5..1" ,      4 ,      1);   c.add_Quad("MQXFA.B3L5..16",      12,      0)
c.add_Quad("MQXFA.A3R5..1" ,      3 ,      1);   c.add_Quad("MQXFA.A3R5..16",      9 ,      0)
c.add_Quad("MQXFA.B3R5..1" ,      2 ,      1);   c.add_Quad("MQXFA.B3R5..16",      7 ,      0)
c.add_Quad("MQXFB.A2R5..1" ,      6 ,      1);   c.add_Quad("MQXFB.A2R5..16",      10,      0)
c.add_Quad("MQXFB.B2R5..1" ,      4 ,      1);   c.add_Quad("MQXFB.B2R5..16",      11,      0)
c.add_Quad("MQXFA.A1R5..1" ,      3,       1);   c.add_Quad("MQXFA.A1R5..16",      9,       0)
c.add_Quad("MQXFA.B1R5..1" ,      2,       1);   c.add_Quad("MQXFA.B1R5..16",      7,       0)
c.add_Quad("MQXFA.A1L1..1" ,      5,       1);   c.add_Quad("MQXFA.A1L1..16",      10,      0)
c.add_Quad("MQXFA.B1L1..1" ,      4  ,     1);   c.add_Quad("MQXFA.B1L1..16",      12,      0)
c.add_Quad("MQXFB.A2L1..1" ,      3 ,      1);   c.add_Quad("MQXFB.A2L1..16",      8 ,      0)
c.add_Quad("MQXFB.B2L1..1" ,      1 ,      1);   c.add_Quad("MQXFB.B2L1..16",      7 ,      0)
c.add_Quad("MQXFA.A3L1..1" ,      5 ,      1);   c.add_Quad("MQXFA.A3L1..16",      10,      0)
c.add_Quad("MQXFA.B3L1..1" ,      4 ,      1);   c.add_Quad("MQXFA.B3L1..16",      12,      0)
c.add_Quad("MQXFA.A3R1..1" ,      3 ,      1);   c.add_Quad("MQXFA.A3R1..16",      9 ,      0)
c.add_Quad("MQXFA.B3R1..1" ,      2 ,      1);   c.add_Quad("MQXFA.B3R1..16",      7 ,      0)
c.add_Quad("MQXFB.A2R1..1" ,      6 ,      1);   c.add_Quad("MQXFB.A2R1..16",      10,      0)
c.add_Quad("MQXFB.B2R1..1" ,      4 ,      1);   c.add_Quad("MQXFB.B2R1..16",      11,      0)
c.add_Quad("MQXFA.A1R1..1" ,      3,       1);   c.add_Quad("MQXFA.A1R1..16",      9,       0)
c.add_Quad("MQXFA.B1R1..1" ,      2,       1);   c.add_Quad("MQXFA.B1R1..16",      7,       0)
#             > Add Skip
c.add_Skip("MQXFA.B3L5..FL");   c.add_Skip("MQXFA.A3L5..FL")
c.add_Skip("MQXFB.B2L5..FL");   c.add_Skip("MQXFB.A2L5..FL")
c.add_Skip("MQXFA.B1L5..FL");   c.add_Skip("MQXFA.A1L5..FL")
c.add_Skip("MQXFA.A1R5..FL");   c.add_Skip("MQXFA.B1R5..FL")
c.add_Skip("MQXFB.A2R5..FL");   c.add_Skip("MQXFB.B2R5..FL")
c.add_Skip("MQXFA.A3R5..FL");   c.add_Skip("MQXFA.B3R5..FL")
c.add_Skip("MQXFA.B3L1..FL");   c.add_Skip("MQXFA.A3L1..FL")
c.add_Skip("MQXFB.B2L1..FL");   c.add_Skip("MQXFB.A2L1..FL")
c.add_Skip("MQXFA.B1L1..FL");   c.add_Skip("MQXFA.A1L1..FL")
c.add_Skip("MQXFA.A1R1..FL");   c.add_Skip("MQXFA.B1R1..FL")
c.add_Skip("MQXFB.A2R1..FL");   c.add_Skip("MQXFB.B2R1..FL")
c.add_Skip("MQXFA.A3R1..FL");   c.add_Skip("MQXFA.B3R1..FL")
c.add_Skip("MQXFA.B3L5..FR");   c.add_Skip("MQXFA.A3L5..FR")
c.add_Skip("MQXFB.B2L5..FR");   c.add_Skip("MQXFB.A2L5..FR")
c.add_Skip("MQXFA.B1L5..FR");   c.add_Skip("MQXFA.A1L5..FR")
c.add_Skip("MQXFA.A1R5..FR");   c.add_Skip("MQXFA.B1R5..FR")
c.add_Skip("MQXFB.A2R5..FR");   c.add_Skip("MQXFB.B2R5..FR")
c.add_Skip("MQXFA.A3R5..FR");   c.add_Skip("MQXFA.B3R5..FR")
c.add_Skip("MQXFA.B3L1..FR");   c.add_Skip("MQXFA.A3L1..FR")
c.add_Skip("MQXFB.B2L1..FR");   c.add_Skip("MQXFB.A2L1..FR")
c.add_Skip("MQXFA.B1L1..FR");   c.add_Skip("MQXFA.A1L1..FR")
c.add_Skip("MQXFA.A1R1..FR");   c.add_Skip("MQXFA.B1R1..FR")
c.add_Skip("MQXFB.A2R1..FR");   c.add_Skip("MQXFB.B2R1..FR")
c.add_Skip("MQXFA.A3R1..FR");   c.add_Skip("MQXFA.B3R1..FR")

c.add_Skip("MCTX.3L5"); c.add_Skip("MCTX.3R5"); c.add_Skip("MCTX.3L1"); c.add_Skip("MCTX.3R1");
c.add_Skip("MCTX.3L8"); c.add_Skip("MCTX.3R8"); c.add_Skip("MCTX.3L2"); c.add_Skip("MCTX.3R2");
#             > Add File
c.add_File(drt+"C2-6-10-14_ND6_dz02_AF/coeff_in_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m.out"             ,5.91783358704972118e-01,True ) #,1.420
c.add_File(drt+"C2-6-10-14_ND6_dz02_AF/coeff_in_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m_mid.out"         ,5.91783358704972118e-01,True ) #,1.420
c.add_File(drt+"C2-6-10-14_ND6_dz02_AF/coeff_in_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m_inv.out"         ,6.29915713814027667e-01,True ) #,1.620
c.add_File(drt+"C2-6-10-14_ND6_dz02_AF/coeff_in_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m_inv_opp.out"     ,6.29915713814027667e-01,True ) #,1.620
c.add_File(drt+"C2-6-10-14_ND6_dz02_AF/coeff_in_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m_opp_mid.out"     ,5.91783358704972118e-01,True ) #,1.420
c.add_File(drt+"C2-6-10-14_ND6_dz02_AF/coeff_in_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m_opp.out"         ,5.91783358704972118e-01,True ) #,1.420
c.add_File(drt+"C2-6-10-14_ND6_dz02_AF/coeff_out_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m.out"            ,6.09915713246783286e-01,False) #,1.620
c.add_File(drt+"C2-6-10-14_ND6_dz02_AF/coeff_out_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m_inv.out"        ,5.71783359230295996e-01,False) #,1.420
c.add_File(drt+"C2-6-10-14_ND6_dz02_AF/coeff_out_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m_inv_mid.out"    ,5.71783359230295996e-01,False) #,1.420
c.add_File(drt+"C2-6-10-14_ND6_dz02_AF/coeff_out_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m_opp.out"        ,6.09915713246783286e-01,False) #,1.620
c.add_File(drt+"C2-6-10-14_ND6_dz02_AF/coeff_out_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m_inv_opp.out"    ,5.71783359230295996e-01,False) #,1.420
c.add_File(drt+"C2-6-10-14_ND6_dz02_AF/coeff_out_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m_inv_opp_mid.out",5.71783359230295996e-01,False) #,1.420
#             > Save case
#Analytic_mctx0.append(c)





# --------------------------------------------------------------------------------
# Data tracking
# --------------------------------------------------------------------------------
drt="/local/home/tpugnat/Documents/CEA/SixTrack/These_Workplace/SixTrack/Test_Dump2/"
#     - Case x
# --------------------------------------------------------------------------------
Trackingx_mctx0=[]
#Q2_mctx0={'x':0.0185, 'y':0.0175}
Q2_mctx0={'x':0.00, 'y':0.00}
#         * nosextnoerr
#             > Define case
c=cas_DT_Sm("HE Tracking",case_col="g",path=drt+"track_nominal/",   \
            file_type="onlyb6/ip3_Data_Part_Rat0_mctx0.txt")
#             > Save case
Trackingx_mctx0.append(c)

#         * sextnoerr
#             > Define case
c=cas_DT_Sm("HE+Heads Tracking",case_col="r",path=drt+"track_riccardo/",   \
            file_type="onlyb6/ip3_Data_Part_Rat0_mctx0.txt")
#             > Save case
#Trackingx_mctx0.append(c)

#         * b3Ran_1
#             > Define case
c=cas_DT_Sm("Lie2 ND0 Tracking",case_col="b",path=drt+"track_Lie2_ND0/",   \
#            file_type="onlyb6/ip3_Data_Part_Rat0_C2-6_mctx0.txt")
            file_type="onlyb6/ip3_Data_Part_Rat0_C6_mctx0.txt")
#             > Save case
#Trackingx_mctx0.append(c)

#         * b4Ran_1
#             > Define case
c=cas_DT_Sm("Lie2 ND6 Tracking",case_col="k",path=drt+"track_Lie2_ND6/",   \
#            file_type="onlyb6/ip3_Data_Part_Rat0_C2-6_mctx0.txt")
            file_type="onlyb6/ip3_Data_Part_Rat0_C2-6_mctx0.txt")
#             > Save case
#Trackingx_mctx0.append(c)



Trackingx_mctx1=[]
#Q2_mctx1={'x':0.0185, 'y':0.0175}
Q2_mctx1={'x':0.00, 'y':0.00}
#         * nosextnoerr
#             > Define case
c=cas_DT_Sm("HE Tracking",case_col="g",path=drt+"track_nominal/",   \
            file_type="onlyb6/ip3_Data_Part_Rat0_mctx1.txt")
#             > Save case
Trackingx_mctx1.append(c)

#         * sextnoerr
#             > Define case
c=cas_DT_Sm("HE+Heads Tracking",case_col="r",path=drt+"track_riccardo/",   \
            file_type="onlyb6/ip3_Data_Part_Rat0_mctx1.txt")
#             > Save case
#Trackingx_mctx1.append(c)

#         * b3Ran_1
#             > Define case
c=cas_DT_Sm("Lie2 ND0 Tracking",case_col="b",path=drt+"track_Lie2_ND0/",   \
#            file_type="onlyb6/ip3_Data_Part_Rat0_C2-6_mctx1.txt")
            file_type="onlyb6/ip3_Data_Part_Rat0_C6_mctx1.txt")
#             > Save case
#Trackingx_mctx1.append(c)

#         * b4Ran_1
#             > Define case
c=cas_DT_Sm("Lie2 ND6 Tracking",case_col="k",path=drt+"track_Lie2_ND6/",   \
#            file_type="onlyb6/ip3_Data_Part_Rat0_C2-6_mctx1.txt")
            file_type="onlyb6/ip3_Data_Part_Rat0_C2-6_mctx1.txt")
#             > Save case
#Trackingx_mctx1.append(c)





# Compute detuning from analytic data
# --------------------------------------------------------------------------------
for t in Analytic_mctx0:
  print(t.case_name)
  
  t.load_File()
  
  t.load_FFFile()
  #t.DTune_Andr()
  t.DTune_Chance()




# Compute detuning from tracking data
# --------------------------------------------------------------------------------
for f in Trackingx_mctx0:
  print(f.case_name)
  
  # Set parameter
  f.set_refBPara(mx=62.3099997999436,my=0.32,                          \
                 bx=121.566844007144,ax=2.29573100013046,  \
                 by=218.58505998061,ay=-2.64288999961551)
  f.set_refAPara(lim=0.05,eps_f=1e-5,flg_ftr=True,flg_dbg=False)

  # Get File to read containing the position
  f.getFile();

  # Read File and combine
  f.read_FilesandCombine();

  # fft
  f.analyse_Data()

  # refine fft
  f.refine_analyse_Data()


  
# Compute detuning from tracking data
# --------------------------------------------------------------------------------
for f in Trackingx_mctx1:
  print(f.case_name)
  
  # Set parameter
  f.set_refBPara(mx=62.3099997999436,my=0.32,                          \
                 bx=121.566844007144,ax=2.29573100013046,  \
                 by=218.58505998061,ay=-2.64288999961551)
  f.set_refAPara(lim=0.05,eps_f=1e-5,flg_ftr=True,flg_dbg=False)

  # Get File to read containing the position
  f.getFile();

  # Read File and combine
  f.read_FilesandCombine();

  # fft
  f.analyse_Data()

  # refine fft
  f.refine_analyse_Data()


drtsv="/local/home/tpugnat/Documents/CEA/SixTrack/These_Workplace/SixTrack/Test_Dump2/Result_DetuningwAmp/Python_output/"


axis="x"; max_2Jum=0.052; Qref=0.31;# Q2=0.19   # flat beam without b4
print(axis)




# Detuning with amplitude
# -----------------------------------------------------------------------------
fig, ax = plt.subplots()
for f in Trackingx_mctx0:
  #f.fit_data(x_lab="2J"+axis+"[um]",y_lab="Q"+axis,n_fit=2)
  #f.fit_data(x_lab="2J"+axis+"[um]",y_lab="Q"+axis,n_fit=3)
  f.fit_data(x_lab="2J"+axis+"[um]",y_lab="Q"+axis,n_fit=4,meth=1,Q2=Q2_mctx0[axis])
  f.plot_DQ(fig,ax,x_lab_list="2J"+axis,y_lab_list="Q"+axis,meth=0,Qref=Qref,Q2=Q2_mctx0[axis],sigmaQ2=1e-3,fmt="+",tt="",flg_Jerr=False)


for f in Trackingx_mctx1:
  #f.fit_data(x_lab="2J"+axis+"[um]",y_lab="Q"+axis,n_fit=2)
  #f.fit_data(x_lab="2J"+axis+"[um]",y_lab="Q"+axis,n_fit=3)
  f.fit_data(x_lab="2J"+axis+"[um]",y_lab="Q"+axis,n_fit=4,meth=1,Q2=Q2_mctx1[axis])
  f.plot_DQ(fig,ax,x_lab_list="2J"+axis,y_lab_list="Q"+axis,meth=0,Qref=Qref,Q2=Q2_mctx1[axis],sigmaQ2=1e-3,fmt="x-",tt="",flg_Jerr=False)


fig, plt.ylabel('Q$_'+axis+'$')
fig, plt.subplots_adjust(left=0.17, right=0.97, top=0.92, bottom=0.12)
ax.ticklabel_format(style='sci',axis='both', scilimits=(0,0),useMathText = True)
#      - Save plot
#fig1.savefig(drtsv+'flatbeam/DetuningWithAmplitude_flatbeam_s1-2-3_Rat0_CompMethod.pdf')
plt.show()



# Detuning with amplitude with correction linear part
# -----------------------------------------------------------------------------
fig, ax = plt.subplots()
for t in Analytic_mctx0:
  t.Print_DTune()
  t.Plot_DTune(fig,ax,axis=axis,arr_2Jum=[0,max_2Jum])


for f in Trackingx_mctx0:
  f.set_refAPara(lim=0.05,eps_f=1e-5,flg_ftr=False,flg_dbg=True)
  f.plot_DQ(fig,ax,x_lab_list="2J"+axis,y_lab_list="Q"+axis,meth=1,Qref=Qref,Q2=Q2_mctx0[axis],sigmaQ2=1e-3,fmt="x",tt="",flg_Jerr=True)

fig, plt.ylabel('$\Delta$Q$_'+axis+'$-C$_1$*2J'+axis)
fig, plt.subplots_adjust(left=0.17, right=0.97, top=0.92, bottom=0.12)
ax.ticklabel_format(style='sci',axis='both', scilimits=(0,0),useMathText = True)
ax.set_ylim(-2e-3,2e-3)
ax.legend(prop={'size': 10})
#      - Save plot
#fig.savefig(drtsv+'flatbeam/DetuningWithAmplitude_flatbeam_s1-2-3_Rat0_CompMethod+Analytic_corLinPart.pdf')

#plt.grid()
plt.show()





# Detuning with amplitude withand without correction linear part
# -----------------------------------------------------------------------------
fig, ax = plt.subplots()


for f in Trackingx_mctx0:
  f.set_refAPara(lim=0.05,eps_f=1e-5,flg_ftr=False,flg_dbg=True)
  f.plot_DQ(fig,ax,x_lab_list="2J"+axis,y_lab_list="Q"+axis,meth=1,Qref=Qref,Q2=Q2_mctx0[axis],sigmaQ2=1e-3,fmt="x",tt="MCTX OFF ",flg_Jerr=True)

for f in Trackingx_mctx1:
  f.set_refAPara(lim=0.05,eps_f=1e-5,flg_ftr=False,flg_dbg=True)
  f.plot_DQ(fig,ax,x_lab_list="2J"+axis,y_lab_list="Q"+axis,meth=1,Qref=Qref,Q2=Q2_mctx0[axis],sigmaQ2=1e-3,fmt="-",tt="MCTX ON ",flg_Jerr=True)

fig, plt.ylabel('$\Delta$Q$_'+axis+'$-C$_1$*2J'+axis)
fig, plt.subplots_adjust(left=0.17, right=0.97, top=0.92, bottom=0.12)
ax.ticklabel_format(style='sci',axis='both', scilimits=(0,0),useMathText = True)
ax.set_ylim(-2e-3,2e-3)
ax.legend(prop={'size': 10})
#      - Save plot
#fig.savefig(drtsv+'flatbeam/DetuningWithAmplitude_flatbeam_s1-2-3_Rat0_CompMethod+Analytic_corLinPart.pdf')

#plt.grid()
plt.show()




















