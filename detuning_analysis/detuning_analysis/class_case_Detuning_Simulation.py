import matplotlib.pyplot as plt
import pathlib
import numpy  as np
import pandas as pd
import gc

from class_particule   import particule as prt

# For the fit and the polynomial
#from Outils            import My_Fit, My_Pol

# For the parallelisation
#import multiprocessing
#from multiprocessing   import Pool
#from matplotlib.ticker import FormatStrFormatter
#from joblib import Parallel, delayed

class Case_Detuning_Simulated:
    # Variable
    # ----------------------------------------------------------------------------------------------------
    #        - Case variable
    case_name=""           # Case name. Used for title and legend.
    case_col ="r"          # Case color. Used for plot.
    #        - File
    path     =""           # Path where the datafile are located
    file_type=""           # File type to read. If contain a "*", then combine all files from ls output
    file_list=[]           # Output of ls
    #        - 
    stat_data=0            # Status of data (0:empty, 1:loaded, 2:analysed)
    data_part=[]           # Array of data for all the particles
    #        - Reference value and optic parameter at point of observation
    Qx_ref=0.                # Reference Qx
    Qy_ref=0.                # Reference Qy
    bx=0.                  # beta_x  
    ax=0.                  # alpha_x
    by=0.                  # beta_y
    ay=0.                  # alpha_y
    DQ_Pol=pd.DataFrame()
    #        - Precision asked
    lim=0.                 # windows around the reference value accepted before refine analyse
    eps_f=0.               # precision of the tune measurement
    #        - Flag
    flg_ftr=False          # if True, apply hamming filter
    flg_dbg=False          # if True, show extra windows




    # ----------------------------------------------------------------------------------------------------
    # Initialization
    # ----------------------------------------------------------------------------------------------------
    def __init__(self,case_name,case_col,path="./",file_type="ip3_Data_Part_r01_Rat0_dlt0.txt"):
      # Set input
      self.case_name=case_name
      self.case_col =case_col
      self.path     =path;
      self.file_type=file_type;
      # Reset all other variable
      self.file_list=[];
      self.stat_data=0;
      self.data_part=[];
      self.Qx_ref=0.
      self.Qy_ref=0.
      self.bx=0.
      self.ax=0.
      self.by=0.
      self.ay=0.
      self.DQ_Pol=pd.DataFrame()
      self.lim=0.
      self.eps_f=0.
      self.flg_ftr=False
      
      
    def free(self):
      # Reset all other variable
      self.file_list=[];
      self.stat_data=0;
      self.data_part=[];
      self.Qx_ref=0.
      self.Qy_ref=0.
      self.bx=0.
      self.ax=0.
      self.by=0.
      self.ay=0.
      self.DQ_Pol=pd.DataFrame()
      self.lim=0.
      self.eps_f=0.
      self.flg_ftr=False
      gc.collect();



    # ----------------------------------------------------------------------------------------------------
    # Read File
    # ----------------------------------------------------------------------------------------------------
    def getFile(self,path="",file_type=""):
      # init variable
      if (path != ""):
        self.path     =path;
      if (file_type != ""):
        self.file_type=file_type;
      self.data_part=[];
      self.stat_data=0;
      # define the path
      currentDirectory = pathlib.Path(self.path);
      # get file in directory
      files=[];
      for currentFile in currentDirectory.glob(self.file_type):
        files.append(currentFile)
      self.file_list=files;



    def read_FilesandCombine(self):
      if (((self.bx==0)or(self.ax==0)) or ((self.by==0)or(self.ay==0))):
        raise TypeError("[class Case,read_FilesandCombine]: wrong input for bx,ax,by,ay! Use set_refBPara!")
      if (len(self.file_list)!=0):
        # init variable
        data_list=pd.DataFrame();
        self.data_part=[]
        self.stat_data=0;
        it=0;
        # read and combine File
        for currentFile in self.file_list:
          #    - get the head from the file without the #
          tmp=pd.read_csv(currentFile,header=1,sep=' ',error_bad_lines=False);
          head_list=tmp.columns.tolist();
          head_list.pop(0);
          #    - read the data from the file
          #['ID', 'turn', 's[m]', 'x[mm]', 'xp[mrad]', 'y[mm]', 'yp[mrad]', 'z[mm]', 'dE/E[1]', 'ktrack']
          data=pd.read_csv(currentFile,skiprows=2,sep='\s+', names=head_list);
          data['particleID']+=it;
          #    - combine data
          data_list=pd.concat([data_list,data], ignore_index=True);
          it=data['particleID'].max();
        # select particle
        for i in range(1, data_list['particleID'].max()+1):
          part=prt(i,data_list.loc[data_list['particleID'] == i]);
          part.normalize(bx=self.bx,ax=self.ax,by=self.by,ay=self.ay);
          self.data_part.append(part);
        self.stat_data=1;
        gc.collect();
      else:
        tmp="[class Case,read_FilesandCombine]: files "+self.path+self.file_type+" not found for case "+self.case_name+"!"
        raise TypeError(tmp)



    def read_FilesandCombine_notnormalised(self):
      if (len(self.file_list)!=0):
        # init variable
        data_list=pd.DataFrame();
        self.data_part=[]
        self.stat_data=0;
        it=0;
        # read and combine File
        for currentFile in self.file_list:
          #    - get the head from the file without the #
          tmp=pd.read_csv(currentFile,header=1,sep=' ',error_bad_lines=False);
          print(currentFile)
          head_list=tmp.columns.tolist();
          head_list.pop(0);
          #    - read the data from the file
          #['ID', 'turn', 's[m]', 'x[mm]', 'xp[mrad]', 'y[mm]', 'yp[mrad]', 'z[mm]', 'dE/E[1]', 'ktrack']
          data=pd.read_csv(currentFile,skiprows=2,sep='\s+', names=head_list);
          data['particleID']+=it;
          #    - combine data
          data_list=pd.concat([data_list,data], ignore_index=True);
          it=data['particleID'].max();
          if np.isnan(it):
              raise TypeError("File empty:\n"+currentFile)
        # select particle
        for i in range(1, data_list['particleID'].max()+1):
          part=prt(i,data_list.loc[data_list['particleID'] == i]);
          self.data_part.append(part);
        self.stat_data=1;
        gc.collect();
      else:
        tmp="[class Case,read_FilesandCombine]: files "+self.path+self.file_type+" not found for case "+self.case_name+"!"
        raise TypeError(tmp)




    def set_refBPara(self,mx=0.31,my=0.32,bx=115.1106637,ax=2.108732509,by=249.7771103,ay=-3.019959459):
      self.Qx_ref=mx;   self.Qy_ref=my
      self.bx=bx;     self.ax=ax;     self.by=by;     self.ay=ay



    def set_refAPara(self,lim=0.05,eps_f=1e-5,flg_ftr=False,flg_dbg=False):
      self.lim=lim;                   self.eps_f=eps_f;
      self.flg_ftr=flg_ftr;



    # ----------------------------------------------------------------------------------------------------
    # Analyse data
    # ----------------------------------------------------------------------------------------------------
    def Part_FFT(self,i,flg_dbg=False):
      d=self.data_part[i]
      #d.Detuning_fft(bx=self.bx,ax=self.ax,by=self.by,ay=self.ay,                   \
      #      eps_f=self.eps_f,flg_ftr=self.flg_ftr,flg_dbg=flg_dbg)
      d.Detuning_fft(eps_f=self.eps_f,flg_ftr=self.flg_ftr,flg_dbg=flg_dbg)
      if (abs(d.Qx-(self.Qx_ref-np.floor(self.Qx_ref)))>self.lim):
        d.Qx=0
      if (abs(d.Qy-(self.Qy_ref-np.floor(self.Qy_ref)))>self.lim):
        d.Qy=0
      self.data_part[i]=d
      



    def analyse_Data(self,flg_dbg=False):
      if (self.stat_data==1):
        #num_cores = multiprocessing.cpu_count()
        inputs = range(len(self.data_part) )
        #inputs = range(len(self.data_part)-1,-1,-1)

        #Parallel(n_jobs=num_cores)(delayed(self.Part_FFT)(i) for i in inputs)
        #with Pool(num_cores) as p:
        #  tp = p.map(self.Part_FFT,inputs)
        #print(type(tp))
        #print(type(self.data_part))
        #self.data_part=tp
        for i in inputs:
          self.Part_FFT(i,flg_dbg=flg_dbg)
        self.stat_data=2
      elif (self.stat_data>1):
        print("WARNING [class Case,analyse_Data]: Data already analysed for "+self.case_name+"!")
      else:
        raise TypeError("[class Case,analyse_Data]: No data for "+self.case_name+"!")
      



    def refine_analyse_Data(self,flg_dbg=False):
      if (self.stat_data==2):
        ref_Qx=self.Qx_ref-np.floor(self.Qx_ref);   ref_Qy=self.Qy_ref-np.floor(self.Qy_ref);
        for i in range(len(self.data_part) ):
          d=self.data_part[i]
          if (d.Qx==0) or (abs(d.Qx-ref_Qx)>1e-3):
            d.Coupling_fft(ref_Qx,ref_Qy,axis_cf="x",wdw_sz=0.01, \
                           eps_f=self.eps_f,flg_ftr=self.flg_ftr,flg_dbg=flg_dbg)
          if (d.Qy==0) or (abs(d.Qy-ref_Qy)>1e-3):
            d.Coupling_fft(ref_Qx,ref_Qy,axis_cf="y",wdw_sz=0.01, \
                           eps_f=self.eps_f,flg_ftr=self.flg_ftr,flg_dbg=flg_dbg)
          #elif (abs(d.Qy-d.Qx)<5e-4):
          #  if (d.twoJx_um < d.twoJy_um):
          #    d.Coupling_fft(ref_Qx,ref_Qy,axis_cf="x",wdw_sz=0.01, \
          #                   eps_f=self.eps_f,flg_ftr=self.flg_ftr,flg_dbg=flg_dbg)
          #  else:
          #    d.Coupling_fft(ref_Qx,ref_Qy,axis_cf="y",wdw_sz=0.01, \
          #                   eps_f=self.eps_f,flg_ftr=self.flg_ftr,flg_dbg=flg_dbg)
          ref_Qx=d.Qx;  ref_Qy=d.Qy;
        self.stat_data=3
      elif (self.stat_data>2):
        print("WARNING [class Case,refine_analyse_Data]: Data already analysed for "+self.case_name+"!")
      else:
        raise TypeError("[class Case,refine_analyse_Data]: No data for "+self.case_name+"!")
      



    def fit_data(self,x_lab="2Jx[um]",y_lab="Qx",n_fit=2,meth=0,Q2=0,flg_dbg=False):
      if (self.stat_data>=2) :
        # put data un array
        x_data=[];y_data=[];
        x_dash=[];y_dash=[];
        for d in self.data_part:
          x_data.append(d[x_lab])
          y_data.append(d[y_lab])
          if (d[x_lab]<5e-3):
            x_dash.append(d[x_lab])
            y_dash.append(d[y_lab])
        if (meth==1):
          y_data=np.asarray(y_data)-Q2*np.asarray(x_data)
          y_dash=np.asarray(y_dash)-Q2*np.asarray(x_dash)
        # get polynomial coef
        fp =np.polyfit(x_dash,y_dash,1)
        #t1,re1,t2,t3,t4=np.polyfit(x_data,y_data,n_fit,full=True)
        fp1,co1        =np.polyfit(x_data,y_data,n_fit,cov= True)
        er1=np.sqrt(np.diag(co1))
        print(np.asarray(self.case_name+" sh  d:{0}   C0:{1}  C1:{2}".format(1,fp[1], fp[0])))
        if n_fit>=2:
          print(np.asarray(self.case_name+" dt  d:{0}   C0:{1}({4})  C1:{2}({5})  C2:{3}({6})".format(n_fit, \
                           fp1[n_fit],fp1[n_fit-1],fp1[n_fit-2],er1[n_fit],er1[n_fit-1],er1[n_fit-2])))
        elif n_fit==1:
          print(np.asarray(self.case_name+" dt  d:{0}   C0:{1}({3})  C1:{2}({4})".format(n_fit, \
                           fp1[n_fit],fp1[n_fit-1],er1[n_fit],er1[n_fit-1])))
        else:
          print(np.asarray(self.case_name+" dt  d:{0}   C0:{1}({2})".format(n_fit,fp1[n_fit],er1[n_fit])))
        #fp=My_Fit(x_dash,y_dash,n_fit,"polyfit")
        #fp=My_Fit(pd.DataFrame({'X':x_data}),pd.DataFrame({'y':y_data}),n_fit,"train")
        #self.DQ_Pol[y_lab+'('+x_lab+')']=fp
        if (flg_dbg):
          if (n_fit>1):
            # get function to compute aprox polynomial
            #approx1=My_Pol(fp,x_data,"poly1d")
            #approx2=My_Pol(np.asarray([fp[n_fit-1],fp[n_fit]]),x_data,"poly1d")
            ##approx1=np.poly1d(fp)
            #approx2=np.poly1d(np.asarray([fp[n_fit-1],fp[n_fit]]))

            #print result
            print(fp)
            #print(np.asarray([fp[n_fit-1],fp[n_fit]]))

            # plot result
            #fig, ax = plt.subplots()
            #fig, plt.plot(x_data,y_data-approx1,label="Data-fit1")
            #fig, plt.plot(x_data,y_data-approx2,label="Data-fit lin")
            ##fig, plt.plot(x_data,y_data-approx1(x_data),label="Data-fit1")
            ##fig, plt.plot(x_data,y_data-approx2(x_data),label="Data-fit lin")
            #fig, plt.title(self.case_name)
            #fig, plt.xlabel('Diff '+x_lab)
            #fig, plt.ylabel(y_lab)
	    #fig, plt.subplots_adjust(left=0.15, right=0.97, top=0.92, bottom=0.12)
	    #ax.xaxis.set_major_formatter(FormatStrFormatter('%.0e'))
	    #ax.yaxis.set_major_formatter(FormatStrFormatter('%.0e'))
	    #ax.legend();
          else:
            # get function to compute aprox polynomial
            #approx1=My_Pol(fp,x_data,"poly1d")
            #approx1=np.poly1d(fp)

            # print result
            print(fp)

            # plot result
            #fig, ax = plt.subplots()
            #fig, plt.plot(x_data,y_data-approx1,label="Data-fit lin")
            ##fig, plt.plot(x_data,y_data-approx1(x_data),label="Data-fit lin")
            #fig, plt.title(self.case_name)
            #fig, plt.xlabel('Diff '+x_lab)
            #fig, plt.ylabel(y_lab)
	    #fig, plt.subplots_adjust(left=0.15, right=0.97, top=0.92, bottom=0.12)
	    #ax.xaxis.set_major_formatter(FormatStrFormatter('%.0e'))
	    #ax.yaxis.set_major_formatter(FormatStrFormatter('%.0e'))
	    #ax.legend();
      else:
        raise TypeError("[class Case,fit_data]: Data not ready for "+self.case_name+"!")



    # ----------------------------------------------------------------------------------------------------
    # Plot
    # ----------------------------------------------------------------------------------------------------
    def plot_Data(self,fig,ax,x_lab_list="2Jx[um]",y_lab_list="Qx",part_id=[]):
      if (self.stat_data>=2):
        if len(part_id)>0:
          for idp in part_id:
            # put data un array
            x_data=[];y_data=[];
            tp=False
            for d in self.data_part:
              if d["idp"]==idp:
                x_data.append(d[x_lab_list])
                y_data.append(d[y_lab_list])
                tp=True
            # plot result
            if tp:
              fig, plt.plot(x_data,y_data,label=self.case_name)
              fig, plt.xlabel(x_lab_list)
              fig, plt.ylabel(y_lab_list)
              ax.legend();
        else:
          # put data un array
          x_data=[];y_data=[];
          for d in self.data_part:
            x_data.append(d[x_lab_list])
            y_data.append(d[y_lab_list])
          # plot result
          fig, plt.plot(x_data,y_data,label=self.case_name)
          fig, plt.xlabel(x_lab_list)
          fig, plt.ylabel(y_lab_list)
          ax.legend();
        #print(np.c_[x_data,y_data])
        #fname = "test_file" + x_lab_list + y_lab_list + ".txt"
        #files= open(fname, 'w')
        #files.write("# "+x_lab_list+"  "+y_lab_list+"\n")
        #for i in range(len(x_data)):
        #  tmp="{0}  {1} \n".format(x_data[i],y_data[i])
        #  files.write(tmp)
        #files.close()
      else:
        raise TypeError("[class Case,plot_Data]: Data not ready for "+self.case_name+"!")


    def plot_DQ(self,fig,ax,x_lab_list="2Jx",y_lab_list="Qx",meth=0,Qref=0.31,Q2=0.0295,sigmaQ2=3e-4,fmt=".",tt="",flg_Jerr=True,alpha=1.):
      if (self.stat_data>=2):
        # put data un array
        x_data=[];xmin_data=[];xmax_data=[];y_data=[];
        for d in self.data_part:
          x_data.append(   d[x_lab_list+"[um]"])
          xmin_data.append(d[x_lab_list+"[um]min"])
          xmax_data.append(d[x_lab_list+"[um]max"])
          y_data.append(   d[y_lab_list])
        X_low=np.asarray(x_data)-np.asarray(xmin_data)
        X_hig=                   np.asarray(xmax_data)-np.asarray(x_data)
        # plot result
        if   meth==0:
          #fig, plt.plot(x_data,y_data,fmt,c=self.case_col,label=self.case_name)
          if flg_Jerr:
            fig, plt.errorbar(np.asarray(x_data),np.asarray(y_data),xerr=[X_low,X_hig], fmt=fmt, color=self.case_col, ecolor=self.case_col, label=tt+self.case_name, alpha=alpha)
          else:
            fig, plt.plot(x_data,y_data,fmt,c=self.case_col,label=self.case_name, alpha=alpha)
          fig, plt.ylabel(y_lab_list)
        elif meth==1:
          Y    =    np.asarray(y_data)-(Qref+np.asarray(x_data)* Q2)
          Y_low=Y-( np.asarray(y_data)-(Qref+np.asarray(x_data)*(Q2+sigmaQ2)) )
          Y_hig=  ( np.asarray(y_data)-(Qref+np.asarray(x_data)*(Q2-sigmaQ2)) )-Y
          #fig, plt.plot(np.asarray(x_data), Y, ".", c=self.case_col, label=self.case_name)
          if flg_Jerr:
            fig, plt.errorbar(np.asarray(x_data), Y, xerr=[X_low,X_hig], yerr=[Y_low,Y_hig], \
                              fmt=fmt, color=self.case_col, ecolor=self.case_col, label=tt+self.case_name, alpha=alpha)
          else:
            fig, plt.errorbar(np.asarray(x_data), Y, yerr=[Y_low,Y_hig], \
                              fmt=fmt, color=self.case_col, ecolor=self.case_col, label=tt+self.case_name, alpha=alpha)
          fig, plt.ylabel("D"+y_lab_list+"-lin_coef*"+x_lab_list)
        else:
          raise KeyError("[class Case,plot_DQ]: meth={0}".format(meth))
        fig, plt.xlabel(x_lab_list+" [$\mu$m]")
        ax.legend();
        #plt.grid()
      else:
        raise TypeError("[class Case,plot_DQ]: Data not ready for "+self.case_name+"!")


    def get_maxmin_DQ(self,res,x_lab_list="2Jx",y_lab_list="Qx"):
      if (self.stat_data>=2):
        # put data un array
        x_data=[];y_data=[];#xmin_data=[];xmax_data=[];
        for d in self.data_part:
          x_data.append(   d[x_lab_list+"[um]"])
          #xmin_data.append(d[x_lab_list+"[um]min"])
          #xmax_data.append(d[x_lab_list+"[um]max"])
          y_data.append(   d[y_lab_list])
        if (y_data[-1]>res['max_'+y_lab_list]):
          res['max_'+y_lab_list]=y_data[-1]
          res['max_seed']       =self.case_name
        if (y_data[-1]<res['min_'+y_lab_list]):
          res['min_'+y_lab_list]=y_data[-1]
          res['min_seed']       =self.case_name
        return res
          


    # ----------------------------------------------------------------------------------------------------
    # Save
    # ----------------------------------------------------------------------------------------------------
    def save_Data(self,fname,x_lab_list="2Jx[um]",y_lab_list="Qx"):
      if (self.stat_data>=2):
        # put data un array
        x_data=[];y_data=[];
        for d in self.data_part:
          x_data.append(d[x_lab_list])
          y_data.append(d[y_lab_list])
        #print(np.c_[x_data,y_data])
        #fname = "test_file" + x_lab_list + y_lab_list + ".txt"
        files= open(fname, 'w')
        files.write("# "+x_lab_list+"  "+y_lab_list+"\n")
        for i in range(len(x_data)):
          tmp="{0}  {1} \n".format(x_data[i],y_data[i])
          files.write(tmp)
        files.close()
      else:
        raise TypeError("[class Case,plot_Data]: Data not ready for "+self.case_name+"!")
      


