import matplotlib.pyplot as plt
#import pathlib
import numpy  as np
import pandas as pd
import os

# Multiprocessing
#import multiprocessing
#from multiprocessing   import Pool
#from matplotlib.ticker import FormatStrFormatter

# Interpolator
from scipy.interpolate      import interp1d

# Class FringeField file
from class_file_FringeField import file_FringeField as FFF

# Detuning equations
from detuning_functions import DQxDjx_sext, DQxDjy_sext, DQyDjx_sext, DQyDjy_sext

class case_Detuning_Theoric:
    # Variable
    # --------------------------------------------------------------------------------------------------
    #        - Case variable
    case_name=""                                           # Case name. Used for title and legend.
    case_col ="r"                                          # Case color. Used for plot.
    #        - File
    path_opt =""                                           # Path where the datafile are located
    path_err =""                                           # Path where the datafile are located
    #        - Data Files
    optic_dat=pd.DataFrame()                               # Optic
    error_dat=pd.DataFrame()                               # Error
    #        - Flag
    flg_statu=0                                            # 0: Waiting, 1: File loaded,
    flg_error=0                                            # 0: no error file, 1: their is one
    #        - Coefficient detuning
    DQ_Andr  =pd.DataFrame()
    DQ_Chao  =pd.DataFrame()
    #        - Strength corrector
    KCor     =pd.DataFrame()
    DQRes    =pd.DataFrame()
    #        - Fringe Field
    flg_sttFF=0                                            # 0: Waiting, 1: File loaded,
    file_FF  =[]                                           # Fringe Field Files
    Quad_FF  =pd.DataFrame(columns=['Quad','f_id','Pos','f_betx','f_bety','f_alfx','f_alfy','f_mux','f_muy'])  # Fringe Field Quad
    Skip_FF  =[]                                           # element skip by FF method




    # --------------------------------------------------------------------------------------------------
    # Initialization
    # --------------------------------------------------------------------------------------------------
    def __init__(self,case_name,case_col,path_opt="",path_err=""):
        # Set input
        self.case_name=case_name
        self.case_col =case_col
        self.path_opt =path_opt
        self.path_err =path_err

        self.optic_dat=pd.DataFrame()
        self.error_dat=pd.DataFrame()

        self.flg_statu=0
        self.DQ_Andr  =pd.DataFrame()
        self.DQ_Chao  =pd.DataFrame()

        self.KCor     =pd.DataFrame()
        self.DQRes    =pd.DataFrame()

        self.flg_sttFF=0
        self.file_FF  =[]
        self.Quad_FF  =pd.DataFrame(columns=['Quad','f_id','Pos','f_betx','f_bety','f_alfx','f_alfy','f_mux','f_muy'])
        self.Skip_FF  =[]
    #===================================================================================================





    #===================================================================================================
    # Function:
    #---------------------------------------------------------------------------------------------------
    # Description:
    # Last modification: Thomas PUGNAT (25-06-2020)
    #===================================================================================================
    def set_path(self,path_opt,path_err):
        # Set input
        self.path_opt=path_opt
        self.path_err=path_err
    #===================================================================================================





    #===================================================================================================
    # Function:
    #---------------------------------------------------------------------------------------------------
    # Description:
    # Last modification: Thomas PUGNAT (25-06-2020)
    #===================================================================================================
    def add_File(self,filename,Lengt_in,Position):
        self.file_FF.append(FFF(filename,Lengt_in,Position))
    #===================================================================================================





    #===================================================================================================
    # Function:
    #---------------------------------------------------------------------------------------------------
    # Description:
    # Last modification: Thomas PUGNAT (25-06-2020)
    #===================================================================================================
    def add_Quad(self,name,file_id,Position):
        i=len(self.Quad_FF)
        self.Quad_FF.loc[i,'Quad']=name
        self.Quad_FF.loc[i,'f_id']=file_id
        self.Quad_FF.loc[i,'Pos' ]=Position
    #===================================================================================================





    #===================================================================================================
    # Function:
    #---------------------------------------------------------------------------------------------------
    # Description:
    # Last modification: Thomas PUGNAT (25-06-2020)
    #===================================================================================================
    def add_Skip(self,name):
        self.Skip_FF.append(name)
    #===================================================================================================





    #===================================================================================================
    # Function: Read Optic File
    #---------------------------------------------------------------------------------------------------
    # Description: 
    # Last modification: Thomas PUGNAT (25-06-2020)
    #===================================================================================================
    def load_File(self,name_optic="optic0.tfs",name_error="tripD1D2.errors",flg_MARKER=False):
        if (self.path_opt==""):
            raise TypeError("[class case,load_File]: wrong optic path! Use set_path(path_opt,path_err)!")
        elif (self.path_err==""):
            raise TypeError("[class case,load_File]: wrong error path! Use set_path(path_opt,path_err)!")
        else:
            # read optic
            # ------------------------------------------------------------------------
            currentFile=self.path_opt+name_optic
            #    - get number of row to skip
            tp=pd.read_csv(currentFile,header=None)
            skip=max(tp[tp[0].str.contains("@")].index.tolist())+1
            #    - get the head from the file without the #
            head_opt=pd.read_csv(currentFile,header=skip,sep='\s+',nrows=0).columns[1:];
            #    - read the data from the file
            self.optic_dat=pd.read_csv(currentFile,skiprows=skip+2,sep='\s+',names=head_opt);
            if not flg_MARKER:
                self.optic_dat=self.optic_dat[~self.optic_dat["KEYWORD"].isin(["MARKER"])]
            # read errors
            # ------------------------------------------------------------------------
            currentFile=self.path_err+name_error
            if os.path.isfile(currentFile):
                #    - get number of row to skip
                tp=pd.read_csv(currentFile,header=None)
                skip=max(tp[tp[0].str.contains("@")].index.tolist())+1
                #    - get the head from the file without the #
                head_err=pd.read_csv(currentFile,header=skip,sep='\s+',nrows=0).columns[1:];
                #    - read the data from the file
                self.error_dat=pd.read_csv(currentFile,skiprows=skip+2,sep='\s+',names=head_err);
            #    - Check status
            if (self.optic_dat.empty):
                print("WARNING [class case,load_File] Empty "+name_optic+ \
                      " from :\n"+self.path_opt)
                self.flg_statu=0
            else:
                self.flg_statu=1
                if (self.error_dat.empty):
                    print("WARNING [class case,load_File] Empty "+name_error+ \
                          " from :\n"+self.path_err)
                    self.flg_error=0
                    self.optic_dat["OptErr_flg"]=False
                    self.optic_dat["OptSkp_flg"]=True
                else:
                    self.flg_error=1
                    self.optic_dat["OptErr_flg"]=self.optic_dat["NAME"].isin(self.error_dat["NAME"])
                    self.optic_dat["OptSkp_flg"]=~self.optic_dat["NAME"].isin(self.Skip_FF)

                    drp_ref=["NAME",'S','X','Y','BETX','BETY','ALFX','ALFY','MUX','MUY','DX','DY','DPX','DPY']
                    drp_tp=[x for x in self.optic_dat.columns.tolist() if x in drp_ref]
                    drp=[x for x in self.error_dat.columns.tolist() if x in drp_tp]
                    self.optic_dat = pd.merge(self.optic_dat,self.error_dat, how='outer',on=drp)
                    drp_ref=["DS","DPHI","DTHETA","DPSI","MREX","MREY","MREDX","MREDY","AREX",
                       "AREY","MSCALX","MSCALY","RFM_FREQ","RFM_HARMON","RFM_LAG","P0L","P0SL","P1L","P1SL",
                       "P2L","P2SL","P3L","P3SL","P4L","P4SL","P5L","P5SL","P6L","P6SL","P7L","P7SL",
                       "P8L","P8SL","P9L","P9SL","P10L","P10SL","P11L","P11SL","P12L","P12SL","P13L","P13SL","P14L",
                       "P14SL","P15L","P15SL","P16L","P16SL","P17L","P17SL","P18L","P18SL",
                       "P19L","P19SL","P20L","P20SL"]
                    drp=[x for x in self.optic_dat.columns.tolist() if x in drp_ref]
                    self.optic_dat = self.optic_dat.drop(drp,axis=1)
                    self.optic_dat=self.optic_dat.fillna(0)
                    if (set(["K1L_x"]).issubset(self.optic_dat.columns.tolist())):
                        self.optic_dat.loc[:,"K1L"]=self.optic_dat.loc[:,"K1L_x"]+self.optic_dat.loc[:,"K1L_y"]
                    if (set(["K2L_x"]).issubset(self.optic_dat.columns.tolist())):
                        self.optic_dat.loc[:,"K2L"]=self.optic_dat.loc[:,"K2L_x"]+self.optic_dat.loc[:,"K2L_y"]
                    if (set(["K3L_x"]).issubset(self.optic_dat.columns.tolist())):
                        self.optic_dat.loc[:,"K3L"]=self.optic_dat.loc[:,"K3L_x"]+self.optic_dat.loc[:,"K3L_y"]
    #===================================================================================================





    #===================================================================================================
    # Function: 
    #---------------------------------------------------------------------------------------------------
    # Description: 
    # Last modification: Thomas PUGNAT (25-06-2020)
    #===================================================================================================
    def load_FFFile(self):
        for f in self.file_FF:
            f.load_File()
        self.flg_sttFF=1
    #===================================================================================================





    #===================================================================================================
    # Function:  Print detuning
    #---------------------------------------------------------------------------------------------------
    # Description: 
    # Last modification: Thomas PUGNAT (25-06-2020)
    #===================================================================================================
    def Print_DTune(self):
        if (self.DQ_Chao.empty):
            raise TypeError("[class case,Print_DTune]: no data!")
        else:
            print(self.case_name+":")
            print(self.DQ_Chao)
    #===================================================================================================





    #===================================================================================================
    # Function: Plot detuning
    #---------------------------------------------------------------------------------------------------
    # Description: 
    # Last modification: Thomas PUGNAT (25-06-2020)
    #===================================================================================================
    def Plot_DTune(self,fig1, ax1,axis="x",J_axis="",arr_2Jum=[0,0.052],nb_point=50,alpha=1.):
        if (self.DQ_Chao.empty):
            raise TypeError("[class case,Plot_DTune]: no data!")
        else:
            if (len(arr_2Jum)==2):
                if (J_axis==""):
                    J_axis=axis;
                # Init values
                J =np.linspace(arr_2Jum[0],arr_2Jum[1],num=nb_point)
                DQ_Chao=0
                # Compute detuning
                for i in range(len(self.DQ_Chao)):
                    if (self.DQ_Chao.loc[i,"Coef_DQ"+axis]!=0):
                        #tmp=self.DQ_Chao.loc[i,"Coef_DQ"+axis]*((J*1e-6)**self.DQ_Chao.loc[i,"exp_2J"+axis+"[um]"])
                        tmp=self.DQ_Chao.loc[i,"Coef_DQ"+axis]*(J**self.DQ_Chao.loc[i,"exp_2J"+J_axis+"[um]"])
                        DQ_Chao+=tmp
                # Plot detuning
                fig1, plt.plot(J,DQ_Chao,"-.",c=self.case_col,label=self.case_name,alpha=alpha) #+" Chao")
                fig1, plt.xlabel('2J'+J_axis+' [$\mu$m]')
                fig1, plt.ylabel('DQ$_'+axis+'$')
                ax1.legend();
    #===================================================================================================





    #===================================================================================================
    # Function: Compute detuning coefficient
    #---------------------------------------------------------------------------------------------------
    # Description: 
    # Last modification: Thomas PUGNAT (25-06-2020)
    #===================================================================================================
    def Compute_beta_FF(self):
        if (self.flg_sttFF>0):
            # Compute interpolation of beta function around each point
            for i in range(len(self.Quad_FF)):
                # Get Quad to compute interpolation
                q=self.Quad_FF.loc[i,:]
                quad_ref=self.optic_dat[self.optic_dat["NAME"].values==q['Quad']]

                # Get File FF data
                file_ref=self.file_FF[q['f_id']-1]
                if q['Pos']=='in':
                    s_max=quad_ref['S'].values+file_ref.Length_in
                    s_min=s_max               -file_ref.length
                else:
                    s_min=quad_ref['S'].values-file_ref.Length_in
                    s_max=s_min               +file_ref.length

                # Check File FF data
                if q['Pos']!=file_ref.Position:
                    raise TypeError("[class case,Compute_beta_FF]: Input problem:\n  - "+q["Pos"]+" -> Quad: "+q["NAME"]+"\n  - "+file_ref.Position+" -> FF File: "+file_ref.filename)

                # Select range in S for the interpolation
                tp=self.optic_dat[self.optic_dat["S"].values<s_min]; s_minb=max(tp["S"].values)
                tp=self.optic_dat[self.optic_dat["S"].values>s_max]; s_maxb=min(tp["S"].values)
                mask=np.logical_and(self.optic_dat["S"].values>=s_minb,self.optic_dat["S"].values<=s_maxb)
                opt_sh=self.optic_dat[mask];
                opt_sh.loc[:,"S"]=opt_sh["S"]-s_min

                # Get interpolation function
                opt_sh=opt_sh.drop_duplicates(subset=['S'],keep='last').reset_index()
                X=opt_sh["S"]
                BX=opt_sh["BETX"]
                self.Quad_FF.loc[i,'f_betx']=interp1d(X,BX,kind='slinear') #'cubic')
                BY=opt_sh["BETY"]
                self.Quad_FF.loc[i,'f_bety']=interp1d(X,BY,kind='slinear') #'cubic')
                AX=opt_sh["ALFX"]
                self.Quad_FF.loc[i,'f_alfx']=interp1d(X,AX,kind='slinear') #'cubic')
                AY=opt_sh["ALFY"]
                self.Quad_FF.loc[i,'f_alfy']=interp1d(X,AY,kind='slinear') #'cubic')
                MX=opt_sh["MUX"]
                self.Quad_FF.loc[i,'f_mux'] =interp1d(X,MX,kind='slinear') #'cubic')
                MY=opt_sh["MUY"]
                self.Quad_FF.loc[i,'f_muy'] =interp1d(X,MY,kind='slinear') #'cubic')
        if (len(self.Skip_FF)>0):
            # Skip multipole for the computation
            for M in self.Skip_FF:
                self.optic_dat.loc[self.optic_dat["NAME"].values==M,"OptSkp_flg"]=False
    #===================================================================================================





    #===================================================================================================
    # Function:
    #---------------------------------------------------------------------------------------------------
    # Description:
    # Last modification: Thomas PUGNAT (25-06-2020)
    #===================================================================================================
    def DTune_Chance(self,Qx_ref=62.31, Qy_ref=60.32, flg_Sext=True, print_contrib=True):
        if (self.flg_statu<1):
            raise TypeError("[class case,DTune_Chance]: no data!")
        else:
            # Initialisation
            # -----------------------------------------------------------------
            DQx=[];  DQy=[]
            DQx.append([0, 0, 0.]) #[]
            DQy.append([0, 0, 0.]) #[]
            DQx.append([1, 0, 0.]) #[um-1]
            DQy.append([0, 1, 0.]) #[um-1]
            DQx.append([2, 0, 0.]) #[um-2]
            DQy.append([0, 2, 0.]) #[um-2]
            DF_Qx=pd.DataFrame(DQx,columns=["exp_2Jx[um]","exp_2Jy[um]","Coef_DQx"])
            DF_Qy=pd.DataFrame(DQy,columns=["exp_2Jx[um]","exp_2Jy[um]","Coef_DQy"])

            # Get beta interpolation and element to skip
            # -----------------------------------------------------------------
            self.Compute_beta_FF()

            # Compute detuning coefficient  (Hard Edge term)
            # -----------------------------------------------------------------
            if ((set(["K1L"]).issubset(self.optic_dat.columns.tolist())) or \
                (set(["K3L"]).issubset(self.optic_dat.columns.tolist())) or \
                (set(["K5L"]).issubset(self.optic_dat.columns.tolist()))):
                #   * Extract data for the computation
                if (self.flg_error==1):
                    opt_short=self.optic_dat[np.logical_and(~self.optic_dat["OptErr_flg"],self.optic_dat["OptSkp_flg"])]
                else:
                    opt_short=self.optic_dat[self.optic_dat["OptSkp_flg"]]

                #   * Extract beta
                betx    =opt_short["BETX"].values
                bety    =opt_short["BETY"].values

                #   * Compute detuning coefficient
                DQx_HEd=[];  DQy_HEd=[]
                #if (set(["K1L"]).issubset(self.optic_dat.columns.tolist())):
                #  DQx_HEd.append([0, 0, (betx     *opt_short["K1L" ].values).sum()      /(      2.*2.*np.pi)]) #[] natural chroma
                #  DQy_HEd.append([0, 0,-(bety     *opt_short["K1L" ].values).sum()      /(      2.*2.*np.pi)]) #[] natural chroma
                #else:
                DQx_HEd.append([0, 0, 0.]) #[]
                DQy_HEd.append([0, 0, 0.]) #[]
                if (set(["K3L"]).issubset(self.optic_dat.columns.tolist())):
                    DQx_HEd.append([1, 0,0.]) # (betx**2  *opt_short["K3L" ].values).sum()*3e-6 /(  6.* 8.*2.*np.pi)]) #[um-1]
                    DQy_HEd.append([0, 1,0.]) # (bety**2  *opt_short["K3L" ].values).sum()*3e-6 /(  6.* 8.*2.*np.pi)]) #[um-1]
                else:
                    DQx_HEd.append([1, 0, 0.]) #[um-1]
                    DQy_HEd.append([0, 1, 0.]) #[um-1]
                if (set(["K5L"]).issubset(self.optic_dat.columns.tolist())):
                    DQx_HEd.append([2, 0, (betx**3  *opt_short["K5L" ].values).sum()*5e-12/(120.*16.*2.*np.pi)]) #[um-2]
                    DQy_HEd.append([0, 2,-(bety**3  *opt_short["K5L" ].values).sum()*5e-12/(120.*16.*2.*np.pi)]) #[um-2]
                else:
                    DQx_HEd.append([2, 0, 0.]) #[um-2]
                    DQy_HEd.append([0, 2, 0.]) #[um-2]
                DF_Qx_HEd=pd.DataFrame(DQx_HEd,columns=["exp_2Jx[um]","exp_2Jy[um]","Coef_DQx"])
                DF_Qy_HEd=pd.DataFrame(DQy_HEd,columns=["exp_2Jx[um]","exp_2Jy[um]","Coef_DQy"])

                #   * Sum
                DF_Qx["Coef_DQx"]+=DF_Qx_HEd["Coef_DQx"]
                DF_Qy["Coef_DQy"]+=DF_Qy_HEd["Coef_DQy"]

                #   * Print contribution
                if (print_contrib):
                    print("Hard Edge term:")
                    print(DF_Qx_HEd)
                    print(DF_Qy_HEd)


            # Compute detuning coefficient  (Main Sextupole term)
            # -----------------------------------------------------------------
            if (False):#set(["K2L"]).issubset(self.optic_dat.columns.tolist()) ):# and flg_Sext):
                #   * Extract data for the computation
                #if (self.flg_error==1):
                opt_short=self.optic_dat
                opt_short=opt_short[opt_short["K2L"]!=0.].reset_index()

                #   * Check
                if (not opt_short.empty):
                    #   * Extract beta
                    S       =opt_short["S"   ].values
                    K2L     =opt_short["K2L" ].values
                    betx    =opt_short["BETX"].values
                    bety    =opt_short["BETY"].values
                    mux     =opt_short["MUX" ].values
                    muy     =opt_short["MUY" ].values

                    sQx3pi   =1./(np.sin(3.*np.pi* Qx_ref           ))
                    sQxpi    =1./(np.sin(   np.pi* Qx_ref           ))
                    sQxp2Qypi=1./(np.sin(   np.pi*(Qx_ref+2.*Qy_ref)))
                    sQxm2Qypi=1./(np.sin(   np.pi*(Qx_ref-2.*Qy_ref)))

                    inv16PU=1./(16.*8.*np.pi)

                    #   * Compute detuning coefficient
                    DQx_Sext=[];  DQy_Sext=[]
                    DQx_Sext.append([0, 0, 0.]) #[]
                    DQy_Sext.append([0, 0, 0.]) #[]
                    DQx_Sext10=0.
                    DQy_Sext01=0.

                    for w in range(len(K2L)):
                        for u in range(len(K2L)): #range(w): #
                            # from Bengtsson 1990 SSC-232 (Leon Van Riesen-Haupt)
                            phix=np.pi*(2.*abs(mux[w]-mux[u])-Qx_ref)
                            phiy=np.pi*(2.*abs(muy[w]-muy[u])-Qy_ref)
                            bxubxw=betx[u]*betx[w]
                            byubyw=bety[u]*bety[w]
                            DQx_Sext10+=-( ((K2L[u]*K2L[w])*inv16PU)*(bxubxw*np.sqrt(bxubxw)) )*(3.*(np.cos(phix)*sQxpi) + np.cos(3.*phix)*sQx3pi)
                            DQy_Sext01+=-( ((K2L[u]*K2L[w])*inv16PU)*(byubyw*np.sqrt(bxubxw)) )* \
                                                              ( 4.*(np.cos(phix)*sQxpi) + np.cos(phix+2.*phiy)*sQxp2Qypi - np.cos(phix-2.*phiy)*sQxm2Qypi )#*0.75
                    DQx_Sext.append([1, 0, DQx_Sext10*1e-6]) #[um-1]
                    DQy_Sext.append([0, 1, DQy_Sext01*1e-6]) #[um-1]
                    DQx_Sext.append([2, 0, 0.]) #[um-2]
                    DQy_Sext.append([0, 2, 0.]) #[um-2]
                    DF_Qx_Sext=pd.DataFrame(DQx_Sext,columns=["exp_2Jx[um]","exp_2Jy[um]","Coef_DQx"])
                    DF_Qy_Sext=pd.DataFrame(DQy_Sext,columns=["exp_2Jx[um]","exp_2Jy[um]","Coef_DQy"])

                    #   * Sum
                    DF_Qx["Coef_DQx"]+=DF_Qx_Sext["Coef_DQx"]
                    DF_Qy["Coef_DQy"]+=DF_Qy_Sext["Coef_DQy"]

                    #   * Print contribution
                    if (True):# print_contrib):
                        print("Main Sextupole term:")
                        print(DF_Qx_Sext)
                        print(DF_Qy_Sext)


            # Compute detuning coefficient  (Hard Edge (Error) term)
            # -----------------------------------------------------------------
            if (self.flg_error==1):
                #   * Extract data for the computation
                opt_short=self.optic_dat[np.logical_and(self.optic_dat["OptErr_flg"],self.optic_dat["OptSkp_flg"])]
                #   * Extract beta
                betx    =opt_short["BETX"].values
                bety    =opt_short["BETY"].values

                #   * Compute detuning coefficient
                DQx_Err=[];  DQy_Err=[]
                DQx_Err.append([0, 0,0])# (betx     *opt_short["K1L" ].values).sum()      /(      2.*2.*np.pi)]) #[]
                DQy_Err.append([0, 0,0])#-(bety     *opt_short["K1L" ].values).sum()      /(      2.*2.*np.pi)]) #[]
                DQx_Err.append([1, 0,0])#-(betx**2  *opt_short["K3L" ].values).sum()*3e-6 /(  6.* 8.*2.*np.pi)]) #[um-1]
                DQy_Err.append([0, 1,0])#-(bety**2  *opt_short["K3L" ].values).sum()*3e-6 /(  6.* 8.*2.*np.pi)]) #[um-1]
                DQx_Err.append([2, 0, (betx**3  *opt_short["K5L" ].values).sum()*5e-12/(120.*16.*2.*np.pi)]) #[um-2]
                DQy_Err.append([0, 2,-(bety**3  *opt_short["K5L" ].values).sum()*5e-12/(120.*16.*2.*np.pi)]) #[um-2]
                DF_Qx_Err=pd.DataFrame(DQx_Err,columns=["exp_2Jx[um]","exp_2Jy[um]","Coef_DQx"])
                DF_Qy_Err=pd.DataFrame(DQy_Err,columns=["exp_2Jx[um]","exp_2Jy[um]","Coef_DQy"])

                #   * Sum
                DF_Qx["Coef_DQx"]+=DF_Qx_Err["Coef_DQx"]
                DF_Qy["Coef_DQy"]+=DF_Qy_Err["Coef_DQy"]

                #   * Print contribution
                if (print_contrib):
                    print("Hard Edge (Error) term:")
                    print(DF_Qx_Err)
                    print(DF_Qy_Err)


            # Compute detuning coefficient  (Fringe Field File term)
            # -----------------------------------------------------------------
            if (self.flg_sttFF==1):
                #   * Parameter
                clight =299792458.                      # Vitesse de la Lumiere
                Eproton=9.382720134258239e8             # Energie d'un proton
                Etot   =0.0*Eproton+7.0e12              # Energie totale du systeme
                gamma0 =Etot/Eproton                    # Ratio entre l'energie totale et celle d'un proton
                beta0  =np.sqrt(1.0-1.0/(gamma0*gamma0))  # Parametre beta0
                p0     =beta0*Etot/clight               # Impulsion de reference
                norm   =1.0/p0                          # Coefficient de normalisation du vecteur potentiel

                for i in range(len(self.Quad_FF)):
                    #   * Get Quad to compute interpolation
                    q=self.Quad_FF.loc[i,:]

                    #   * Get File FF data
                    file_ref=self.file_FF[q['f_id']-1]
                    DF_Qx_FF,DF_Qy_FF = file_ref.DTune_Chao(q['f_betx'],q['f_bety'],q['f_alfx'],q['f_alfy'],norm)

                    #   * Sum
                    DF_Qx["Coef_DQx"]+=DF_Qx_FF["Coef_DQx"]
                    DF_Qy["Coef_DQy"]+=DF_Qy_FF["Coef_DQy"]

                    #   * Print contribution
                    #if (print_contrib):
                    if (False):
                        print("File FF ("+file_ref.filename+") term:")
                        print(DF_Qx_FF)
                        print(DF_Qy_FF)


            # Compute detuning coefficient  (Kynematic term effect)
            # -----------------------------------------------------------------
            #if set(["GAMX","GAMY"]).issubset(self.optic_dat.columns.tolist()):
            #    #   * Extract data for the computation
            #    opt_sh=self.optic_dat
            #    #opt_sh=opt_sh.drop_duplicates(subset=['S'],keep='first')
            #
            #    #   * Extract gamma
            #    S     =opt_sh["S"].values
            #    int_gamx=np.trapz(opt_sh["GAMX"].values**2,x=S)v
            #    int_gamy=np.trapz(opt_sh["GAMY"].values**2,x=S)
            #
            #    #   * Compute detuning coefficient
            #    DQx_Kyn=[];  DQy_Kyn=[]
            #    DQx_Kyn.append([0, 0, 0.]) #[]
            #    DQy_Kyn.append([0, 0, 0.]) #[]
            #    DQx_Kyn.append([1, 0, int_gamx *3e-6 /(16.*np.pi)]) #[um-1]
            #    DQy_Kyn.append([0, 1, int_gamy *3e-6 /(16.*np.pi)]) #[um-1]
            #    DQx_Kyn.append([2, 0, 0.]) #[um-2]
            #    DQy_Kyn.append([0, 2, 0.]) #[um-2]
            #    DF_Qx_Kyn=pd.DataFrame(DQx_Kyn,columns=["exp_2Jx[um]","exp_2Jy[um]","Coef_DQx"])
            #    DF_Qy_Kyn=pd.DataFrame(DQy_Kyn,columns=["exp_2Jx[um]","exp_2Jy[um]","Coef_DQy"])
            #
            #    #   * Sum
            #    #DF_Qx["Coef_DQx"]+=DF_Qx_Kyn["Coef_DQx"]
            #    #DF_Qy["Coef_DQy"]+=DF_Qy_Kyn["Coef_DQy"]
            #
            #    #   * Print contribution
            #    if (print_contrib):
            #        print("Kynematic term:")
            #        print(DF_Qx_Kyn)
            #        print(DF_Qy_Kyn)

            #elif set(["ALFX","ALFY"]).issubset(self.optic_dat.columns.tolist()):
            #    #   * Extract data for the computation
            #    opt_sh=self.optic_dat
            #    opt_sh=opt_sh.drop_duplicates(subset=['S'],keep='first')
            #
            #    #   * Extract gamma
            #    S     =opt_sh["S"].values
            #    gamx  =(1.+opt_sh["ALFX"].values**2)/opt_sh["BETX"].values
            #    gamy  =(1.+opt_sh["ALFY"].values**2)/opt_sh["BETY"].values
            #    int_gamx=np.trapz(gamx**2,x=S)
            #    int_gamy=np.trapz(gamy**2,x=S)
            #
            #    #   * Compute detuning coefficient
            #    DQx_Kyn=[];  DQy_Kyn=[]
            #    DQx_Kyn.append([0, 0, 0.]) #[]
            #    DQy_Kyn.append([0, 0, 0.]) #[]
            #    DQx_Kyn.append([1, 0, int_gamx *3e-6 /(16.*np.pi)]) #[um-1]
            #    DQy_Kyn.append([0, 1, int_gamy *3e-6 /(16.*np.pi)]) #[um-1]
            #    DQx_Kyn.append([2, 0, 0.]) #[um-2]
            #    DQy_Kyn.append([0, 2, 0.]) #[um-2]
            #    DF_Qx_Kyn=pd.DataFrame(DQx_Kyn,columns=["exp_2Jx[um]","exp_2Jy[um]","Coef_DQx"])
            #    DF_Qy_Kyn=pd.DataFrame(DQy_Kyn,columns=["exp_2Jx[um]","exp_2Jy[um]","Coef_DQy"])
            #
            #    #   * Sum
            #    #DF_Qx["Coef_DQx"]+=DF_Qx_Kyn["Coef_DQx"]
            #    #DF_Qy["Coef_DQy"]+=DF_Qy_Kyn["Coef_DQy"]
            #
            #    #   * Print contribution
            #    if (print_contrib):
            #        print("Kynematic term:")
            #        print(DF_Qx_Kyn)
            #        print(DF_Qy_Kyn)

            # Save
            # -----------------------------------------------------------------
            self.DQ_Chao = pd.merge(DF_Qx,DF_Qy,how='outer',on=["exp_2Jx[um]","exp_2Jy[um]"]).fillna(0)
    #===================================================================================================





    #===================================================================================================
    # Function:
    #---------------------------------------------------------------------------------------------------
    # Description:
    # Last modification: Thomas PUGNAT (25-06-2020)
    #===================================================================================================
    def DTune_Chao(self):
        if (self.flg_statu<1):
            raise TypeError("[class case,DTune_Chao]: no data!")
        elif (self.flg_sttFF==1):
            # Get beta interpolation and element to skip
            # -----------------------------------------------------------------
            self.Compute_beta_FF()

            # Extract data for the computation
            # -----------------------------------------------------------------
            opt_short=self.optic_dat[np.logical_and(self.optic_dat["OptErr_flg"],self.optic_dat["OptSkp_flg"])]

            # Extract beta
            # -----------------------------------------------------------------
            betx    =opt_short["BETX"].values
            bety    =opt_short["BETY"].values

            # Compute detuning coefficient
            # -----------------------------------------------------------------
            DQx=[];  DQy=[]
            DQx.append([0, 0, 0.])# (betx   *opt_short["K1L" ].values).sum()      /(      2.*2.*np.pi)]) #[]
            DQy.append([0, 0, 0.])#-(bety   *opt_short["K1L" ].values).sum()      /(      2.*2.*np.pi)]) #[]
            DQx.append([1, 0, 0.])# (betx**2*opt_short["K3L" ].values).sum()*3e-6 /(  6.* 8.*2.*np.pi)]) #[um-1]
            DQy.append([0, 1, 0.])#-(bety**2*opt_short["K3L" ].values).sum()*3e-6 /(  6.* 8.*2.*np.pi)]) #[um-1]
            DQx.append([2, 0, (betx**3*opt_short["K5L" ].values).sum()*5e-12/(120.*16.*2.*np.pi)]) #[um-2]
            DQy.append([0, 2,-(bety**3*opt_short["K5L" ].values).sum()*5e-12/(120.*16.*2.*np.pi)]) #[um-2]
            DF_Qx=pd.DataFrame(DQx,columns=["exp_2Jx[um]","exp_2Jy[um]","Coef_DQx"])
            DF_Qy=pd.DataFrame(DQy,columns=["exp_2Jx[um]","exp_2Jy[um]","Coef_DQy"])

            # Add detuning coefficient from FF file
            # -----------------------------------------------------------------
            clight =299792458.                   # Vitesse de la Lumiere
            Eproton=9.382720134258239e8          # Energie d'un proton
            Etot   =0*Eproton+7.e12              # Energie totale du systeme
            gamma0 =Etot/Eproton                 # Ratio entre l'energie totale et celle d'un proton
            beta0  =np.sqrt(1.-1/gamma0/gamma0)  # Parametre beta0
            p0     =beta0*Etot/clight            # Impulsion de reference
            norm   =1./p0                        # Coefficient de normalisation du vecteur potentiel
            for i in range(len(self.Quad_FF)):
                # Get Quad to compute interpolation
                q=self.Quad_FF.loc[i,:]

                # Get File FF data
                file_ref=self.file_FF[q['f_id']-1]
                DF_Qx_FF,DF_Qy_FF = file_ref.DTune_Chao(q['f_betx'],q['f_bety'],norm)
                DF_Qx["Coef_DQx"]+=DF_Qx_FF["Coef_DQx"]
                DF_Qy["Coef_DQy"]+=DF_Qy_FF["Coef_DQy"]
            # Save
            self.DQ_Chao = pd.merge(DF_Qx,DF_Qy,how='outer',on=["exp_2Jx[um]","exp_2Jy[um]"]).fillna(0)
        else:
            # Extract data for the computation
            # -----------------------------------------------------------------
            opt_short=self.optic_dat[np.logical_and(self.optic_dat["OptErr_flg"],self.optic_dat["OptSkp_flg"])]
            #print(opt_short.head(1)["NAME"],opt_short.head(1)["BETX"],opt_short.head(1)["K5L"])

            # Extract beta
            # -----------------------------------------------------------------
            betx    =opt_short["BETX"].values
            bety    =opt_short["BETY"].values

            # Compute detuning coefficient
            # -----------------------------------------------------------------
            DQx=[];  DQy=[]
            DQx.append([0, 0, 0.] ) # (betx   *opt_short["K1L" ].values).sum()     /(    2.*2.*np.pi)]) #[]
            DQy.append([0, 0, 0.] ) #-(bety   *opt_short["K1L" ].values).sum()     /(    2.*2.*np.pi)]) #[]
            DQx.append([1, 0, 0.] ) # (betx**2*opt_short["K3L" ].values).sum()*3e-6/(6.* 8.*2.*np.pi)]) #[um-1]
            DQy.append([0, 1, 0.] ) #-(bety**2*opt_short["K3L" ].values).sum()*3e-6/(6.* 8.*2.*np.pi)]) #[um-1]
            DQx.append([2, 0, (betx**3*opt_short["K5L" ].values).sum()*5e-12/(120.*16.*2.*np.pi)]) #[um-2]
            DQy.append([0, 2,-(bety**3*opt_short["K5L" ].values).sum()*5e-12/(120.*16.*2.*np.pi)]) #[um-2]
            DF_Qx=pd.DataFrame(DQx,columns=["exp_2Jx[um]","exp_2Jy[um]","Coef_DQx"])
            DF_Qy=pd.DataFrame(DQy,columns=["exp_2Jx[um]","exp_2Jy[um]","Coef_DQy"])
            self.DQ_Chao=pd.merge(DF_Qx,DF_Qy,how='outer',on=["exp_2Jx[um]","exp_2Jy[um]"]).fillna(0)
            #print(opt_short[opt_short["NAME"]=="MQXFA.B3L5..FL"])
    #===================================================================================================





    #===================================================================================================
    # Function:
    #---------------------------------------------------------------------------------------------------
    # Description:
    # Last modification: Thomas PUGNAT (25-06-2020)
    #===================================================================================================
    def Compute_Cor(self,nCSX="MCSX.3",nCSSX="MCSSX.3",
                         nCOX="MCOX.3",nCOSX="MCOSX.3",
                         nCDX="MCDX.3",nCDSX="MCDSX.3",
                         nCTX="MCTX.3",nCTSX="MCTSX.3",flg_res=False,flg_shw=True):
        if (self.flg_statu<1):
            raise TypeError("[class case,DTune_Chance]: no data!")
        else:
            # Get beta interpolation and element to skip
            # -----------------------------------------------------------------
            self.Compute_beta_FF()

            for IP in ["1","2","3","4","5","6","7","8"]:#["5"]:#
                # Select IP
                # -----------------------------------------------------------------
                optic_loc=self.optic_dat[np.logical_and(np.logical_or(
                              self.optic_dat["NAME"].str.contains("L"+IP),
                              self.optic_dat["NAME"].str.contains("R"+IP)), self.optic_dat['OptSkp_flg'])]

                # Select Corrector
                # -----------------------------------------------------------------
                #   * B3 and A3 corrector
                corb3_L=optic_loc[optic_loc["NAME"].str.contains(nCSX+"L" )]
                corb3_R=optic_loc[optic_loc["NAME"].str.contains(nCSX+"R" )]
                cora3_L=optic_loc[optic_loc["NAME"].str.contains(nCSSX+"L")]
                cora3_R=optic_loc[optic_loc["NAME"].str.contains(nCSSX+"R")]
                #   * B4 and A4 corrector
                corb4_L=optic_loc[optic_loc["NAME"].str.contains(nCOX+"L" )]
                corb4_R=optic_loc[optic_loc["NAME"].str.contains(nCOX+"R" )]
                cora4_L=optic_loc[optic_loc["NAME"].str.contains(nCOSX+"L")]
                cora4_R=optic_loc[optic_loc["NAME"].str.contains(nCOSX+"R")]
                #   * B5 and A5 corrector
                corb5_L=optic_loc[optic_loc["NAME"].str.contains(nCDX+"L" )]
                corb5_R=optic_loc[optic_loc["NAME"].str.contains(nCDX+"R" )]
                cora5_L=optic_loc[optic_loc["NAME"].str.contains(nCDSX+"L")]
                cora5_R=optic_loc[optic_loc["NAME"].str.contains(nCDSX+"R")]
                #   * B6 and A6 corrector
                corb6_L=optic_loc[optic_loc["NAME"].str.contains(nCTX+"L" )]
                corb6_R=optic_loc[optic_loc["NAME"].str.contains(nCTX+"R" )]
                #cora6_L=optic_loc[optic_loc["NAME"].str.contains("MCTSX.L")]
                #cora6_R=optic_loc[optic_loc["NAME"].str.contains("MCTSX.R")]


                # Select Corrector
                # -----------------------------------------------------------------
                #   * B3 and A3 corrector
                sum30=0.; sum12=0.;
                sum21=0.; sum03=0.;
                #   * B4 and A4 corrector
                sum40=0.; sum22=0.; sum04=0.;
                sum31=0.; sum13=0.;
                #   * B5 and A5 corrector
                sum50=0.; sum32=0.; sum14=0.;
                sum41=0.; sum23=0.; sum05=0.;
                #   * B6 and A6 corrector
                sum60=0.; sum42=0.; sum24=0.; sum06=0.;
                sum51=0.; sum33=0.; sum15=0.;


                # Init
                # -----------------------------------------------------------------
                #   * B3 and A3 corrector
                KL_corb3_L=0.;  KL_corb3_R=0.;  KL_cora3_L=0.;  KL_cora3_R=0.
                #   * B4 and A4 corrector
                KL_corb4_L=0.;  KL_corb4_R=0.;  KL_cora4_L=0.;  KL_cora4_R=0.
                #   * B5 and A5 corrector
                KL_corb5_L=0.;  KL_corb5_R=0.;  KL_cora5_L=0.;  KL_cora5_R=0.
                #   * B6 and A6 corrector
                KL_corb6_L=0.;  KL_corb6_R=0.;  KL_cora6_L=0.;  KL_cora6_R=0.


                # Compute detuning coefficient  (Hard Edge (Error) term)
                # -----------------------------------------------------------------
                if (self.flg_error==1):
                    #   * Extract data for the computation
                    opt_short=optic_loc[optic_loc["OptErr_flg"]]

                    #   * Extract beta
                    betx    =opt_short["BETX"].values
                    bety    =opt_short["BETY"].values
                    sqbetx  =np.sqrt(betx)
                    sqbety  =np.sqrt(bety)
                    betx2   =betx*betx
                    bety2   =bety*bety
                    betx3   =betx2*betx
                    bety3   =bety2*bety

                    #   * Compute detuning coefficient
# <<<<< version madx
                    #      - B3 and A3 corrector
                    Err_sum12=(sqbetx             *bety *opt_short["K2L" ].values).sum();
                    Err_sum21=(       betx *sqbety      *opt_short["K2L" ].values).sum();

                    Err_sum03=(             sqbety*bety *opt_short["K2SL"].values).sum();
                    Err_sum30=(sqbetx*betx              *opt_short["K2SL"].values).sum();
                    #      - B4 and A4 corrector
                    Err_sum40=(       betx2             *opt_short["K3L" ].values).sum();
                    Err_sum04=(                    bety2*opt_short["K3L" ].values).sum();
                    Err_sum22=(       betx        *bety *opt_short["K3L" ].values).sum();

                    Err_sum31=(sqbetx*betx *sqbety      *opt_short["K3SL"].values).sum();
                    Err_sum13=(sqbetx      *sqbety*bety *opt_short["K3SL"].values).sum();
                    #      - B5 and A5 corrector
                    Err_sum50=(sqbetx*betx2             *opt_short["K4L" ].values).sum();
                    Err_sum05=(             sqbety*bety2*opt_short["K4L" ].values).sum();

                    Err_sum32=(sqbetx*betx        *bety *opt_short["K4L" ].values).sum();
                    Err_sum14=(sqbetx             *bety2*opt_short["K4L" ].values).sum();

                    Err_sum41=(       betx2*sqbety      *opt_short["K4SL"].values).sum();
                    Err_sum23=(       betx *sqbety*bety *opt_short["K4SL"].values).sum();
                    #      - B6 and A6 corrector
                    Err_sum60=(       betx3             *opt_short["K5L" ].values).sum();
                    Err_sum42=(       betx2       *bety *opt_short["K5L" ].values).sum();
                    Err_sum24=(       betx        *bety2*opt_short["K5L" ].values).sum();
                    Err_sum06=(                    bety3*opt_short["K5L" ].values).sum();

                    #Err_sum51=(betx     *opt_short["K3SL"].values).sum();
                    #Err_sum33=(betx     *opt_short["K3SL"].values).sum();
                    #Err_sum15=(betx     *opt_short["K3SL"].values).sum();
# <<<<< version madx

# <<<<< version tpugnat
                    ##      - B3 and A3 corrector
                    #Err_sum30=(sqbetx*betx              *opt_short["K2L" ].values).sum();
                    #Err_sum12=(sqbetx             *bety *opt_short["K2L" ].values).sum();
                    #
                    #Err_sum21=(       betx *sqbety      *opt_short["K2SL"].values).sum();
                    #Err_sum03=(             sqbety*bety *opt_short["K2SL"].values).sum();
                    ##      - B4 and A4 corrector
                    #Err_sum40=(       betx2             *opt_short["K3L" ].values).sum();
                    #Err_sum22=(       betx        *bety *opt_short["K3L" ].values).sum();
                    #Err_sum04=(                    bety2*opt_short["K3L" ].values).sum();
                    #
                    #Err_sum31=(sqbetx*betx *sqbety      *opt_short["K3SL"].values).sum();
                    #Err_sum13=(sqbetx      *sqbety*bety *opt_short["K3SL"].values).sum();
                    ##      - B5 and A5 corrector
                    #Err_sum50=(sqbetx*betx2             *opt_short["K4L" ].values).sum();
                    #Err_sum32=(sqbetx*betx        *bety *opt_short["K4L" ].values).sum();
                    #Err_sum14=(sqbetx             *bety2*opt_short["K4L" ].values).sum();
                    #
                    #Err_sum41=(       betx2*sqbety      *opt_short["K4SL"].values).sum();
                    #Err_sum23=(       betx *sqbety*bety *opt_short["K4SL"].values).sum();
                    #Err_sum05=(             sqbety*bety2*opt_short["K4SL"].values).sum();
                    ##      - B6 and A6 corrector
                    #Err_sum60=(       betx3             *opt_short["K5L" ].values).sum();
                    #Err_sum42=(       betx2       *bety *opt_short["K5L" ].values).sum();
                    #Err_sum24=(       betx        *bety2*opt_short["K5L" ].values).sum();
                    #Err_sum06=(                    bety3*opt_short["K5L" ].values).sum();
                    #
                    #Err_sum51=(betx     *opt_short["K3SL"].values).sum();
                    #Err_sum33=(betx     *opt_short["K3SL"].values).sum();
                    #Err_sum15=(betx     *opt_short["K3SL"].values).sum();
# <<<<< version tpugnat

                    #   * Sum
                    #      - B3 and A3 corrector
                    sum30+=Err_sum30; sum12+=Err_sum12;
                    sum21+=Err_sum21; sum03+=Err_sum03;
                    #      - B4 and A4 corrector
                    sum40+=Err_sum40; sum22+=Err_sum22; sum04+=Err_sum04;
                    sum31+=Err_sum31; sum13+=Err_sum13;
                    #      - B5 and A5 corrector
                    sum50+=Err_sum50; sum32+=Err_sum32; sum14+=Err_sum14;
                    sum41+=Err_sum41; sum23+=Err_sum23; sum05+=Err_sum05;
                    #      - B6 and A6 corrector
                    sum60+=Err_sum60; sum42+=Err_sum42; sum24 +=Err_sum24; sum06+=Err_sum06;
                    #sum51+=Err_sum51; sum33+=Err_sum33; sum15+=Err_sum15;


                # Compute detuning coefficient  (Fringe Field File term)
                # -----------------------------------------------------------------
                if (self.flg_sttFF==1):
                    #   * Select IP
                    Quad_FF_loc=self.Quad_FF[np.logical_or(self.Quad_FF["Quad"].str.contains("L"+IP),
                                                           self.Quad_FF["Quad"].str.contains("R"+IP))]

                    #   * Parameter
                    clight =299792458.                        # Vitesse de la Lumiere
                    Eproton=9.382720134258239e8               # Energie d'un proton
                    Etot   =0.0*Eproton+7.0e12                # Energie totale du systeme
                    gamma0 =Etot/Eproton                      # Ratio entre l'energie totale et celle d'un proton
                    beta0  =np.sqrt(1.0-1.0/(gamma0*gamma0))  # Parametre beta0
                    p0     =beta0*Etot/clight                 # Impulsion de reference
                    norm   =1.0/p0                            # Coefficient de normalisation du vecteur potentiel

                    for i in Quad_FF_loc.index:
                        #   * Get Quad to compute interpolation
                        q=Quad_FF_loc.loc[i,:]

                        file_ref=self.file_FF[q['f_id']-1]
                        res = file_ref.Compute_Cor(q['f_betx'],q['f_bety'],q['f_alfx'],q['f_alfy'],norm)
                        FFi_sum40=res["FFi_sum40"];  FFi_sum04=res["FFi_sum04"];  FFi_sum22=res["FFi_sum22"];
                        FFi_sum60=res["FFi_sum60"];  FFi_sum06=res["FFi_sum06"];  FFi_sum42=res["FFi_sum42"];  FFi_sum24=res["FFi_sum24"];

                        #   * Sum
                        #      - B3 and A3 corrector
                        #sum30+=FFi_sum30; sum12+=FFi_sum12;
                        #sum21+=FFi_sum21; sum03+=FFi_sum03;
                        #      - B4 and A4 corrector
                        sum40+=FFi_sum40; sum22+=FFi_sum22; sum04+=FFi_sum04;
                        #sum31+=FFi_sum31; sum13+=FFi_sum13;
                        #      - B5 and A5 corrector
                        #sum50+=FFi_sum50; sum32+=FFi_sum32; sum14+=FFi_sum14;
                        #sum41+=FFi_sum41; sum23+=FFi_sum23; sum05+=FFi_sum05;
                        #      - B6 and A6 corrector
                        sum60+=FFi_sum60; sum06+=FFi_sum06; sum42+=FFi_sum42; sum24 +=FFi_sum24;
                        #sum51+=FFi_sum51; sum33+=FFi_sum33; sum15+=FFi_sum15;


                # Compute corrector strength
                # -----------------------------------------------------------------
# <<<<< version madx
                #      - B3 and A3 corrector
                if ((not corb3_L.empty) and (not corb3_R.empty)):
                    bx_L=corb3_L["BETX"].values; by_L=corb3_L["BETY"].values;
                    bx_R=corb3_R["BETX"].values; by_R=corb3_R["BETY"].values;
                    al12=(np.sqrt(bx_L)*by_L)[0]
                    ar12=(np.sqrt(bx_R)*by_R)[0]
                    al21=(np.sqrt(by_L)*bx_L)[0]
                    ar21=(np.sqrt(by_R)*bx_R)[0]
                    a3=np.array([[al12,-ar12],[al21,-ar21]])
                    b3=np.array([-sum12,-sum21])
                    x3=np.linalg.solve(a3,b3)
                    KL_corb3_L= x3[0]
                    KL_corb3_R= x3[1]
                else:
                    print("WARNING: No b3 correction for IR"+IP+"  -> name corr.: "+nCSX)

                if ((not cora3_L.empty) and (not cora3_R.empty)):
                    bx_L=cora3_L["BETX"].values; by_L=cora3_L["BETY"].values;
                    bx_R=cora3_R["BETX"].values; by_R=cora3_R["BETY"].values;
                    al03=(np.sqrt(by_L)*by_L)[0]
                    ar03=(np.sqrt(by_R)*by_R)[0]
                    al30=(np.sqrt(bx_L)*bx_L)[0]
                    ar30=(np.sqrt(bx_R)*bx_R)[0]
                    as3=np.array([[al03,-ar03],[al30,-ar30]])
                    bs3=np.array([-sum03,-sum30])
                    xs3=np.linalg.solve(as3,bs3)
                    KL_cora3_L= xs3[0]
                    KL_cora3_R= xs3[1]
                else:
                    print("WARNING: No a3 correction for IR"+IP+"  -> name corr.: "+nCSSX)


                #      - B4 and A4 corrector
                if ((not corb4_L.empty) and (not corb4_R.empty)):
                    bx_L=corb4_L["BETX"].values; by_L=corb4_L["BETY"].values;
                    bx_R=corb4_R["BETX"].values; by_R=corb4_R["BETY"].values;
                    al40=(bx_L**2)[0]
                    ar40=(bx_R**2)[0]
                    al04=(by_L**2)[0]
                    ar04=(by_R**2)[0]
                    a4=np.array([[al40,ar40],[al04,ar04]])
                    b4=np.array([-sum40,-sum04])
                    x4=np.linalg.solve(a4,b4)
                    KL_corb4_L= x4[0]
                    KL_corb4_R= x4[1]
                else:
                    print("WARNING: No b4 correction for IR"+IP+"  -> name corr.: "+nCOX)

                if ((not cora4_L.empty) and (not cora4_R.empty)):
                    bx_L=cora4_L["BETX"].values; by_L=cora4_L["BETY"].values;
                    bx_R=cora4_R["BETX"].values; by_R=cora4_R["BETY"].values;
                    al31=(np.sqrt(bx_L*by_L)*bx_L)[0]
                    ar31=(np.sqrt(bx_R*by_R)*bx_R)[0]
                    al13=(np.sqrt(bx_L*by_L)*by_L)[0]
                    ar13=(np.sqrt(bx_R*by_R)*by_R)[0]
                    as4=np.array([[al31,ar31],[al13,ar13]])
                    bs4=np.array([-sum31,-sum13])
                    xs4=np.linalg.solve(as4,bs4)
                    KL_cora4_L= xs4[0]
                    KL_cora4_R= xs4[1]
                else:
                    print("WARNING: No a4 correction for IR"+IP+"  -> name corr.: "+nCOSX)

                #      - B5 and A5 corrector
                if ((not corb5_L.empty) and (not corb5_R.empty)):
                    bx_L=corb5_L["BETX"].values; by_L=corb5_L["BETY"].values;
                    bx_R=corb5_R["BETX"].values; by_R=corb5_R["BETY"].values;
                    al50=(np.sqrt(bx_L)*bx_L**2)[0]
                    ar50=(np.sqrt(bx_R)*bx_R**2)[0]
                    al05=(np.sqrt(by_L)*by_L**2)[0]
                    ar05=(np.sqrt(by_R)*by_R**2)[0]
                    a5=np.array([[al50,-ar50],[al05,-ar05]])
                    b5=np.array([-sum50,-sum05])
                    x5=np.linalg.solve(a5,b5)
                    KL_corb5_L= x5[0]
                    KL_corb5_R= x5[1]
                else:
                    print("WARNING: No b5 correction for IR"+IP+"  -> name corr.: "+nCDX)

                if ((not cora5_L.empty) and (not cora5_R.empty)):
                    bx_L=cora5_L["BETX"].values; by_L=cora5_L["BETY"].values;
                    bx_R=cora5_R["BETX"].values; by_R=cora5_R["BETY"].values;
                    al41=(np.sqrt(by_L)*bx_L**2)[0]
                    ar41=(np.sqrt(by_R)*bx_R**2)[0]
                    al14=(np.sqrt(bx_L)*by_L**2)[0]
                    ar14=(np.sqrt(bx_R)*by_R**2)[0]
                    as5=np.array([[al14,-ar14],[al41,-ar41]])
                    bs5=np.array([-sum14,-sum41])
                    xs5=np.linalg.solve(as5,bs5)
                    KL_cora5_L= xs5[0]
                    KL_cora5_R= xs5[1]
                else:
                    print("WARNING: No a5 correction for IR"+IP+"  -> name corr.: "+nCDSX)

                #      - B6 and A6 corrector
                if ((not corb6_L.empty) and (not corb6_R.empty)):
                    bx_L=corb6_L["BETX"].values; by_L=corb6_L["BETY"].values;
                    bx_R=corb6_R["BETX"].values; by_R=corb6_R["BETY"].values;
                    al60=(bx_L**3)[0]
                    ar60=(bx_R**3)[0]
                    al06=(by_L**3)[0]
                    ar06=(by_R**3)[0]
                    a6=np.array([[al60,ar60],[al06,ar06]])
                    b6=np.array([-sum60,-sum06])
                    x6=np.linalg.solve(a6,b6)
                    KL_corb6_L= x6[0]
                    KL_corb6_R= x6[1]
                else:
                    print("WARNING: No b6 correction for IR"+IP+"  -> name corr.: "+nCTX)

                #if ((not corb3_L.empty) and (not corb3_R.empty)):
                    #bx_L=cora6_L["BETX"].values; by_L=cora6_L["BETY"].values; bx_R=cora6_R["BETX"].values; by_R=cora6_R["BETY"].values;
                    #cora6_L= (bx_R**2*sum15 - by_R**2*sum51)/(np.sqrt(bx_L*by_L          )*((bx_L*by_R)**2-(bx_R*by_L)**2))
                    #cora6_R= (by_L**2*sum51 - bx_L**2*sum15)/(np.sqrt(          bx_R*by_R)*((bx_L*by_R)**2-(bx_R*by_L)**2))
# <<<<< version madx

# <<<<< version tpugnat
      #          #      - B3 and A3 corrector
      #          if ((not corb3_L.empty) and (not corb3_R.empty)):
      #              bx_L=corb3_L["BETX"].values; by_L=corb3_L["BETY"].values;
      #              bx_R=corb3_R["BETX"].values; by_R=corb3_R["BETY"].values;
      #              al12=(np.sqrt(bx_L)*by_L)[0]
      #              ar12=(np.sqrt(bx_R)*by_R)[0]
      #              al30=(np.sqrt(bx_L)*bx_L)[0]
      #              ar30=(np.sqrt(bx_R)*bx_R)[0]
      #              a3=np.array([[al30,-ar30],[al12,-ar12]])
      #              b3=np.array([-sum30,-sum12])
      #              x3=np.linalg.solve(a3,b3)
      #              KL_corb3_L= x3[0]
      #              KL_corb3_R= x3[1]
      #
      #              #KL_corb3_L= (bx_R*sum12 - by_R*sum30)/((bx_L*by_R-bx_R*by_L)*np.sqrt(bx_L))
      #              #KL_corb3_R= (by_L*sum30 - bx_L*sum12)/((bx_L*by_R-bx_R*by_L)*np.sqrt(bx_R))
      #
      #
      #          if ((not cora3_L.empty) and (not cora3_R.empty)):
      #              bx_L=cora3_L["BETX"].values; by_L=cora3_L["BETY"].values;
      #              bx_R=cora3_R["BETX"].values; by_R=cora3_R["BETY"].values;
      #              al21=(np.sqrt(by_L)*bx_L)[0]
      #              ar21=(np.sqrt(by_R)*bx_R)[0]
      #              al03=(np.sqrt(by_L)*by_L)[0]
      #              ar03=(np.sqrt(by_R)*by_R)[0]
      #              as3=np.array([[al03,-ar03],[al21,-ar21]])
      #              bs3=np.array([-sum03,-sum21])
      #              xs3=np.linalg.solve(as3,bs3)
      #              KL_cora3_L= xs3[0]
      #              KL_cora3_R= xs3[1]
      #
      #              #KL_cora3_L= (by_R*sum21 - bx_R*sum03)/((by_L*bx_R-by_R*by_L)*np.sqrt(by_L))
      #              #KL_cora3_R= (bx_L*sum03 - by_L*sum21)/((by_L*bx_R-by_R*bx_L)*np.sqrt(by_R))
      #
      #          #      - B4 and A4 corrector
      #          if ((not corb4_L.empty) and (not corb4_R.empty)):
      #              bx_L=corb4_L["BETX"].values; by_L=corb4_L["BETY"].values;
      #              bx_R=corb4_R["BETX"].values; by_R=corb4_R["BETY"].values;
      #              al40=(bx_L**2)[0]
      #              ar40=(bx_R**2)[0]
      #              al04=(by_L**2)[0]
      #              ar04=(by_R**2)[0]
      #              a4=np.array([[al40,ar40],[al04,ar04]])
      #              b4=np.array([-sum40,-sum04])
      #              x4=np.linalg.solve(a4,b4)
      #              KL_corb4_L= x4[0]
      #              KL_corb4_R= x4[1]
      #
      #              #KL_corb4_L= (bx_R**2*sum04 - by_R**2*sum40)/((bx_L*by_R)**2 - (bx_R*by_L)**2)
      #              #KL_corb4_R= (by_L**2*sum40 - bx_L**2*sum04)/((bx_L*by_R)**2 - (bx_R*by_L)**2)
      #
      #          if ((not cora4_L.empty) and (not cora4_R.empty)):
      #              bx_L=cora4_L["BETX"].values; by_L=cora4_L["BETY"].values;
      #              bx_R=cora4_R["BETX"].values; by_R=cora4_R["BETY"].values;
      #              al31=(np.sqrt(bx_L*by_L)*bx_L)[0]
      #              ar31=(np.sqrt(bx_R*by_R)*bx_R)[0]
      #              al13=(np.sqrt(bx_L*by_L)*by_L)[0]
      #              ar13=(np.sqrt(bx_R*by_R)*by_R)[0]
      #              as4=np.array([[al31,ar31],[al13,ar13]])
      #              bs4=np.array([-sum31,-sum13])
      #              xs4=np.linalg.solve(as4,bs4)
      #              KL_cora4_L= xs4[0]
      #              KL_cora4_R= xs4[1]
      #
      #              #KL_cora4_L= (bx_R*sum13 - by_R*sum31)/(np.sqrt(bx_L*by_L)*(bx_L*by_R-bx_R*by_L))
      #              #KL_cora4_R= (by_L*sum31 - bx_L*sum13)/(np.sqrt(bx_R*by_R)*(bx_L*by_R-bx_R*by_L))
      #
      #          #      - B5 and A5 corrector
      #          if ((not corb5_L.empty) and (not corb5_R.empty)):
      #              bx_L=corb5_L["BETX"].values; by_L=corb5_L["BETY"].values;
      #              bx_R=corb5_R["BETX"].values; by_R=corb5_R["BETY"].values;
      #              al14=(np.sqrt(bx_L)*by_L**2)[0]
      #              ar14=(np.sqrt(bx_R)*by_R**2)[0]
      #              al50=(np.sqrt(bx_L)*bx_L**2)[0]
      #              ar50=(np.sqrt(bx_R)*bx_R**2)[0]
      #              a5=np.array([[al50,-ar50],[al14,-ar14]])
      #              b5=np.array([-sum50,-sum14])
      #              x5=np.linalg.solve(a5,b5)
      #              KL_corb5_L= x5[0]
      #              KL_corb5_R= x5[1]
      #
      #              #KL_corb5_L= (bx_R**2*sum14 - by_R**2*sum50)/((bx_L**2*by_R**2 - bx_R**2*by_L**2)*np.sqrt(bx_L))
      #              #KL_corb5_R= (by_L**2*sum50 - bx_L**2*sum14)/((bx_L**2*by_R**2 - bx_R**2*by_L**2)*np.sqrt(bx_R))
      #
      #          if ((not cora5_L.empty) and (not cora5_R.empty)):
      #              bx_L=cora5_L["BETX"].values; by_L=cora5_L["BETY"].values;
      #              bx_R=cora5_R["BETX"].values; by_R=cora5_R["BETY"].values;
      #              al41=(np.sqrt(by_L)*bx_L**2)[0]
      #              ar41=(np.sqrt(by_R)*bx_R**2)[0]
      #              al05=(np.sqrt(by_L)*by_L**2)[0]
      #              ar05=(np.sqrt(by_R)*by_R**2)[0]
      #              as5=np.array([[al05,-ar05],[al41,-ar41]])
      #              bs5=np.array([-sum05,-sum41])
      #              xs5=np.linalg.solve(as5,bs5)
      #              KL_cora5_L= xs5[0]
      #              KL_cora5_R= xs5[1]
      #
      #              #KL_cora5_L= (by_R**2*sum41 - bx_R**2*sum05)/((by_L**2*bx_R**2 - by_R**2*by_L**2)*np.sqrt(by_L))
      #              #KL_cora5_R= (bx_L**2*sum05 - by_L**2*sum41)/((by_L**2*bx_R**2 - by_R**2*bx_L**2)*np.sqrt(by_R))
      #
      #          #      - B6 and A6 corrector
      #          if ((not corb6_L.empty) and (not corb6_R.empty)):
      #              bx_L=corb6_L["BETX"].values; by_L=corb6_L["BETY"].values;
      #              bx_R=corb6_R["BETX"].values; by_R=corb6_R["BETY"].values;
      #              al60=(bx_L**3)[0]
      #              ar60=(bx_R**3)[0]
      #              al06=(by_L**3)[0]
      #              ar06=(by_R**3)[0]
      #              a6=np.array([[al60,ar60],[al06,ar06]])
      #              b6=np.array([-sum60,-sum06])
      #              x6=np.linalg.solve(a6,b6)
      #              KL_corb6_L= x6[0]
      #              KL_corb6_R= x6[1]
      #
      #              #KL_corb6_L= (bx_R**3*sum06 - by_R**3*sum60)/((bx_L*by_R)**3 - (bx_R*by_L)**3)
      #              #KL_corb6_R= (by_L**3*sum60 - bx_L**3*sum06)/((bx_L*by_R)**3 - (bx_R*by_L)**3)
      #
      #          #if ((not corb3_L.empty) and (not corb3_R.empty)):
      #              #bx_L=cora6_L["BETX"].values; by_L=cora6_L["BETY"].values; bx_R=cora6_R["BETX"].values; by_R=cora6_R["BETY"].values;
      #              #cora6_L= (bx_R**2*sum15 - by_R**2*sum51)/(np.sqrt(bx_L*by_L          )*((bx_L*by_R)**2-(bx_R*by_L)**2))
      #              #cora6_R= (by_L**2*sum51 - bx_L**2*sum15)/(np.sqrt(          bx_R*by_R)*((bx_L*by_R)**2-(bx_R*by_L)**2))
# <<<<< version tpugnat

                #   * Print
                if flg_shw:
                    print("+---------------------- IP"+IP+" -----------------------+")
                    print("    * Correctors strength")
                    #      - B3 and A3 corrector
                    print("KCSX3.L" +IP+" := {0}".format(KL_corb3_L))
                    print("KCSX3.R" +IP+" := {0}".format(KL_corb3_R))
                    print("KCSSX3.L"+IP+" := {0}".format(KL_cora3_L))
                    print("KCSSX3.R"+IP+" := {0}".format(KL_cora3_R))
                    #      - B4 and A4 corrector
                    print("KCOX3.L" +IP+" := {0}".format(KL_corb4_L))
                    print("KCOX3.R" +IP+" := {0}".format(KL_corb4_R))
                    print("KCOSX3.L"+IP+" := {0}".format(KL_cora4_L))
                    print("KCOSX3.R"+IP+" := {0}".format(KL_cora4_R))
                    #      - B5 and A5 corrector
                    print("KCDX3.L" +IP+" := {0}".format(KL_corb5_L))
                    print("KCDX3.R" +IP+" := {0}".format(KL_corb5_R))
                    print("KCDSX3.L"+IP+" := {0}".format(KL_cora5_L))
                    print("KCDSX3.R"+IP+" := {0}".format(KL_cora5_R))
                    #      - B6 and A6 corrector
                    print("KCTX3.L" +IP+" := {0}".format(KL_corb6_L))
                    print("KCTX3.R" +IP+" := {0}".format(KL_corb6_R))
                    print("KCTSX3.L"+IP+" := {0}".format(KL_cora6_L))
                    print("KCTSX3.R"+IP+" := {0}".format(KL_cora6_L))


                    if flg_res:
                        print("\n    * Corrected resonnance")
                        #      - B3 and A3 corrector
                        if ((not corb3_L.empty) and (not corb3_R.empty)):
                            bx_L=corb3_L["BETX"].values; by_L=corb3_L["BETY"].values; bx_R=corb3_R["BETX"].values; by_R=corb3_R["BETY"].values;
                        else:
                            bx_L=0.;              by_L=0.;              bx_R=0.;              by_R=0.;
                        print("b3:  (3,0) -> {0},         (1,2) -> {1}".format(
                                   sum30 +np.sqrt(bx_L)*bx_L*KL_corb3_L +np.sqrt(bx_R)*bx_R*KL_corb3_R,
                                   sum12 +np.sqrt(bx_L)*by_L*KL_corb3_L +np.sqrt(bx_R)*by_R*KL_corb3_R))

                        if ((not cora3_L.empty) and (not cora3_R.empty)):
                            bx_L=cora3_L["BETX"].values; by_L=cora3_L["BETY"].values; bx_R=cora3_R["BETX"].values; by_R=cora3_R["BETY"].values;
                        else:
                            bx_L=0.;              by_L=0.;              bx_R=0.;              by_R=0.;
                        print("a3:  (2,1) -> {0},         (0,3) -> {1}".format(
                                   sum21 +np.sqrt(by_L)*bx_L*KL_cora3_L +np.sqrt(by_R)*bx_R*KL_cora3_R,
                                   sum03 +np.sqrt(by_L)*by_L*KL_cora3_L +np.sqrt(by_R)*by_R*KL_cora3_R))

                        #      - B4 and A4 corrector
                        if ((not corb4_L.empty) and (not corb4_R.empty)):
                            bx_L=corb4_L["BETX"].values; by_L=corb4_L["BETY"].values; bx_R=corb4_R["BETX"].values; by_R=corb4_R["BETY"].values;
                        else:
                            bx_L=0.;              by_L=0.;              bx_R=0.;              by_R=0.;
                        print("b4:  (4,0) -> {0},         (2,2) -> {1},         (0,4) -> {2}".format(
                                   sum40 +bx_L**2  *KL_corb4_L +bx_R**2  *KL_corb4_R,
                                   sum22 +bx_L*by_L*KL_corb4_L +bx_R*by_R*KL_corb4_R,
                                   sum04 +by_L**2  *KL_corb4_L +by_R**2  *KL_corb4_R))

                        if ((not cora4_L.empty) and (not cora4_R.empty)):
                            bx_L=cora4_L["BETX"].values; by_L=cora4_L["BETY"].values; bx_R=cora4_R["BETX"].values; by_R=cora4_R["BETY"].values;
                        else:
                            bx_L=0.;              by_L=0.;              bx_R=0.;              by_R=0.;
                        print("a4:  (3,1) -> {0},         (1,3) -> {1}".format(
                                   sum31 +np.sqrt(bx_L)*np.sqrt(by_L)*bx_L*KL_cora4_L +np.sqrt(bx_R)*np.sqrt(by_R)*bx_R*KL_cora4_R,
                                   sum13 +np.sqrt(bx_L)*np.sqrt(by_L)*by_L*KL_cora4_L +np.sqrt(bx_R)*np.sqrt(by_R)*by_R*KL_cora4_R))

                        #      - B5 and A5 corrector
                        if ((not corb5_L.empty) and (not corb5_R.empty)):
                            bx_L=corb5_L["BETX"].values; by_L=corb5_L["BETY"].values; bx_R=corb5_R["BETX"].values; by_R=corb5_R["BETY"].values;
                        else:
                            bx_L=0.;              by_L=0.;              bx_R=0.;              by_R=0.;
                        print("b5:  (5,0) -> {0},         (3,2) -> {1},         (1,4) -> {2}".format(
                                   sum50 +np.sqrt(bx_L)*bx_L**2  *KL_corb5_L +np.sqrt(bx_R)*bx_R**2  *KL_corb5_R,
                                   sum32 +np.sqrt(bx_L)*bx_L*by_L*KL_corb5_L +np.sqrt(bx_R)*bx_R*by_R*KL_corb5_R,
                                   sum14 +np.sqrt(bx_L)*by_L**2  *KL_corb5_L +np.sqrt(bx_R)*by_R**2  *KL_corb5_R))

                        if ((not cora5_L.empty) and (not cora5_R.empty)):
                            bx_L=cora5_L["BETX"].values; by_L=cora5_L["BETY"].values; bx_R=cora5_R["BETX"].values; by_R=cora5_R["BETY"].values;
                        else:
                            bx_L=0.;              by_L=0.;              bx_R=0.;              by_R=0.;
                        print("a5:  (4,1) -> {0},         (2,3) -> {1},         (0,5) -> {2}".format(
                                   sum41 +np.sqrt(by_L)*bx_L**2  *KL_cora5_L +np.sqrt(by_R)*bx_R**2  *KL_cora5_R,
                                   sum23 +np.sqrt(by_L)*bx_L*by_L*KL_cora5_L +np.sqrt(by_R)*bx_R*by_R*KL_cora5_R,
                                   sum05 +np.sqrt(by_L)*by_L**2  *KL_cora5_L +np.sqrt(by_R)*by_R**2  *KL_cora5_R))

                        #      - B6 and A6 corrector
                        if ((not corb6_L.empty) and (not corb6_R.empty)):
                            bx_L=corb6_L["BETX"].values; by_L=corb6_L["BETY"].values; bx_R=corb6_R["BETX"].values; by_R=corb6_R["BETY"].values;
                        else:
                            bx_L=0.;              by_L=0.;              bx_R=0.;              by_R=0.;
                        print("b6:  (6,0) -> {0},         (4,2) -> {1},         (2,4) -> {2},         (0,6) -> {3}".format(
                                   sum60 +bx_L**3     *KL_corb6_L +bx_R**3     *KL_corb6_R,
                                   sum42 +bx_L**2*by_L*KL_corb6_L +bx_R**2*by_R*KL_corb6_R,
                                   sum24 +bx_L*by_L**2*KL_corb6_L +bx_R*by_R**2*KL_corb6_R,
                                   sum06 +by_L**3     *KL_corb6_L +by_R**3     *KL_corb6_R))

                        print("a6:  (5,1) -> {0},         (3,2) -> {1},         (1,5) -> {2}".format(sum51,sum33,sum15))
                    print("+--------------------------------------------------+")

                #   * Save
                name=self.case_name.replace(" ", "_");  self.KCor.loc[0,"NAME"]=name

                name="KCSX3.L" +IP;  self.KCor.loc[0,name]=KL_corb3_L
                name="KCSX3.R" +IP;  self.KCor.loc[0,name]=KL_corb3_R
                name="KCSSX3.L"+IP;  self.KCor.loc[0,name]=KL_cora3_L
                name="KCSSX3.R"+IP;  self.KCor.loc[0,name]=KL_cora3_R

                name="KCOX3.L" +IP;  self.KCor.loc[0,name]=KL_corb4_L
                name="KCOX3.R" +IP;  self.KCor.loc[0,name]=KL_corb4_R
                name="KCOSX3.L"+IP;  self.KCor.loc[0,name]=KL_cora4_L
                name="KCOSX3.R"+IP;  self.KCor.loc[0,name]=KL_cora4_R

                name="KCDX3.L" +IP;  self.KCor.loc[0,name]=KL_corb5_L
                name="KCDX3.R" +IP;  self.KCor.loc[0,name]=KL_corb5_R
                name="KCDSX3.L"+IP;  self.KCor.loc[0,name]=KL_cora5_L
                name="KCDSX3.R"+IP;  self.KCor.loc[0,name]=KL_cora5_R

                name="KCTX3.L" +IP;  self.KCor.loc[0,name]=KL_corb6_L
                name="KCTX3.R" +IP;  self.KCor.loc[0,name]=KL_corb6_R
                name="KCTSX3.L"+IP;  self.KCor.loc[0,name]=KL_cora6_L
                name="KCTSX3.R"+IP;  self.KCor.loc[0,name]=KL_cora6_R
    #===================================================================================================





    #===================================================================================================
    # Function:
    #---------------------------------------------------------------------------------------------------
    # Description:
    # Last modification: Thomas PUGNAT (25-06-2020)
    #===================================================================================================
    def Compute_AmpBetaBeating(self,Amp_2J=pd.DataFrame(),Amp_2Jx=np.array([]),Amp_2Jy=np.array([]),
                                    DQ_mes=pd.DataFrame(),selfdependent=False):

        # Check input
        # -----------------------------------------------------------------
        if len(Amp_2J)>0:
            if (not ("2Jx" in Amp_2J.columns.tolist())) and (not ("2Jy" in Amp_2J.columns.tolist())):
                raise KeyError("[class case,Compute_AmpBetaBeating]: Amp_2J must be a dataframe with either '2Jx' or '2Jy' or both as column!")
            if ("2Jx" in Amp_2J.columns.tolist()):
                Amp_2Jx=Amp_2J['2Jx']
            if ("2Jy" in Amp_2J.columns.tolist()):
                Amp_2Jy=Amp_2J['2Jy']

        if (len(Amp_2Jx)==0 and len(Amp_2Jy)>0):
            Amp_2Jx=np.zeros(np.array(Amp_2Jy).shape)
        elif (len(Amp_2Jx)>0 and len(Amp_2Jy)==0):
            Amp_2Jy=np.zeros(np.array(Amp_2Jx).shape)
        elif (len(Amp_2Jx)!=len(Amp_2Jy)) or (len(Amp_2Jx)==0 and len(Amp_2Jy)==0):
            raise KeyError("[class case,Compute_AmpBetaBeating]: Amp_2Jx and Amp_2Jy must have the same length /= 0! Or use Amp_2J (dataframe({'2Jx':...,'2Jy':...})")


        if (self.flg_statu<1):
            raise TypeError("[class case,Compute_AmpBetaBeating]: no data!")

        elif not selfdependent:
            # Initialisation variable
            # -----------------------------------------------------------------
            inv_384=1.0e0/(384e0)
            inv_96 =1.0e0/( 96e0)
            inv_64 =1.0e0/( 64e0)
            inv_32 =1.0e0/( 32e0)

            Qx=max(self.optic_dat["MUX"])
            Qy=max(self.optic_dat["MUY"])

            #nomexp_p4Qx=np.ones(np.array(Amp_2Jx).shape)
            nomexp_p2Qx=np.ones(np.array(Amp_2Jx).shape)
            #nomexp_p4Qy=np.ones(np.array(Amp_2Jx).shape)
            nomexp_p2Qy=np.ones(np.array(Amp_2Jx).shape)
            if len(DQ_mes)==len(Amp_2Jx):
                if (not ("Qx" in DQ_mes.columns.tolist())) and (not ("Qy" in DQ_mes.columns.tolist())):
                    raise KeyError("[class case,Compute_AmpBetaBeating]: Amp_2J must be a dataframe with either '2Jx' or '2Jy' or both as column!")
                if ("Qx" in DQ_mes.columns.tolist()):
                    nomexp_p2Qx=1 #( 1-np.exp( (4j*np.pi)* Qx ) )/( 1-np.exp( (4j*np.pi)* DQ_mes['Qx'] ) )
                if ("Qy" in DQ_mes.columns.tolist()):
                    nomexp_p2Qy=1 #( 1-np.exp( (4j*np.pi)* Qy ) )/( 1-np.exp( (4j*np.pi)* DQ_mes['Qy'] ) )
                

            # Get beta interpolation and element to skip
            # -----------------------------------------------------------------
            #self.Compute_beta_FF()


            # Select Element
            # -----------------------------------------------------------------
            #    - BPM
            BPMs=self.optic_dat[self.optic_dat["NAME"].str.contains("BPM")]

            #    - Octupoles
            Oct =self.optic_dat[self.optic_dat["K3L"]!=0]
            betx=Oct.BETX;  bety=Oct.BETY
            betxbetx=(betx*betx);    betxbety=(betx*bety);    betybety=(bety*bety)
            #Oct.loc[:,"ccf4000"] =-( Oct.K3L * betxbetx * inv_384 )/( 1-np.exp( (8j*np.pi)* Qx    ) )
            Oct.loc[:,"ccf3100"] =-( Oct.K3L * betxbetx * inv_96  )/( 1-np.exp( (4j*np.pi)* Qx    ) )
            #Oct.loc[:,"ccf2020"] =-( Oct.K3L * betxbety * inv_64  )/( 1-np.exp( (4j*np.pi)*(Qx+Qy)) )
            Oct.loc[:,"ccf2011"] =-(-Oct.K3L * betxbety * inv_32  )/( 1-np.exp( (4j*np.pi)* Qx    ) )
            Oct.loc[:,"ccf1120"] =-(-Oct.K3L * betxbety * inv_32  )/( 1-np.exp( (4j*np.pi)*    Qy ) )
            #Oct.loc[:,"ccf2002"] =-( Oct.K3L * betxbety * inv_64  )/( 1-np.exp( (4j*np.pi)*(Qx-Qy)) )
            #Oct.loc[:,"ccf0040"] =-( Oct.K3L * betybety * inv_384 )/( 1-np.exp( (8j*np.pi)*    Qy ) )
            Oct.loc[:,"ccf0031"] =-( Oct.K3L * betybety * inv_96  )/( 1-np.exp( (4j*np.pi)*    Qy ) )

            #    - Dodecapoles
            #DoDe=self.optic_dat[self.optic_dat["K5L"]!=0]

            # Initialise and Check
            # -----------------------------------------------------------------
            amp=np.sqrt(Amp_2Jx**2 + Amp_2Jy**2)
            AmpBBting_X=pd.DataFrame(index=BPMs.NAME,columns=amp)
            AmpBBting_Y=pd.DataFrame(index=BPMs.NAME,columns=amp)
            if len(Oct)==0:
                AmpBBting_X.loc[:,:]=0;   AmpBBting_Y.loc[:,:]=0;
                return AmpBBting_X,AmpBBting_Y

            # Compute RDT
            # -----------------------------------------------------------------
            for ib in BPMs.index.tolist():
                b=BPMs.loc[ib,:]

                f3100b=0;  f3100b2=0;
                f0031b=0;  f0031b2=0;
                mux1 =b.MUX -Oct.MUX;  mux=np.array([x+Qx if x<0 else x for x in mux1]);
                muy1 =b.MUY -Oct.MUY;  muy=np.array([y+Qy if y<0 else y for y in muy1]);

#                #      * Amplitude and Cross detuning coef
#                h2200b  =(Oct.ch2200).sum()    # Horizontal plane
#                h1111b  =(Oct.ch1100).sum()    # Cross plane
#                h0022b  =(Oct.ch0022).sum()    # Vertical plane
#                #      * Chromaticity coef
#                c20b    =(Oct.cc20).sum()    # Horizontal plane
#                c02b    =(Oct.cc02).sum()    # Vertical plane
                #      * Other RDTs coef
                #              - Horizontal plane
                #f4000b  =(Oct.ccf4000*np.exp( (8j*np.pi)* mux       )).sum()
                f3100b  =(Oct.ccf3100*np.exp( (4j*np.pi)* mux       )).sum()
                #              - Cross plane
                #f2020b  =(Oct.ccf2020*np.exp( (4j*np.pi)*(mux+muy) )).sum()
                f2011b  =(Oct.ccf2011*np.exp( (4j*np.pi)* mux       )).sum()
                f1120b  =(Oct.ccf1120*np.exp( (4j*np.pi)*      muy  )).sum()
                #f2002b  =(Oct.ccf2002*np.exp( (4j*np.pi)*(mux-muy) )).sum()
                #              - Vertical plane
                #f0040b  =(Oct.ccf0040*np.exp( (8j*np.pi)*      muy  )).sum()
                f0031b  =(Oct.ccf0031*np.exp( (4j*np.pi)*      muy  )).sum()

                #BPMs.loc[ib,"f4000"]=f4000b
                BPMs.loc[ib,"f3100"]=f3100b

                #BPMs.loc[ib,"f2020"]=f2020b
                BPMs.loc[ib,"f2011"]=f2011b
                BPMs.loc[ib,"f1120"]=f1120b
                #BPMs.loc[ib,"f2002"]=f2002b

                #BPMs.loc[ib,"f0040"]=f0040b
                BPMs.loc[ib,"f0031"]=f0031b


            Im_f3100         =np.imag(BPMs.loc[:,"f3100"]); avg_Im_f3100     =np.mean(Im_f3100)
            Im_f2011         =np.imag(BPMs.loc[:,"f2011"]); avg_Im_f2011     =np.mean(Im_f2011)
            Im_f1120         =np.imag(BPMs.loc[:,"f1120"]); avg_Im_f1120     =np.mean(Im_f1120)
            Im_f0031         =np.imag(BPMs.loc[:,"f0031"]); avg_Im_f0031     =np.mean(Im_f0031)


            Re_f3100f3100=np.real(BPMs.loc[:,"f3100"]*np.conj(BPMs.loc[:,"f3100"])); avg_Re_f3100f3100=np.mean(Re_f3100f3100)
            Re_f2011f2011=np.real(BPMs.loc[:,"f2011"]*np.conj(BPMs.loc[:,"f2011"])); avg_Re_f2011f2011=np.mean(Re_f2011f2011)
            Re_f1120f1120=np.real(BPMs.loc[:,"f1120"]*np.conj(BPMs.loc[:,"f1120"])); avg_Re_f1120f1120=np.mean(Re_f1120f1120)
            Re_f0031f0031=np.real(BPMs.loc[:,"f0031"]*np.conj(BPMs.loc[:,"f0031"])); avg_Re_f0031f0031=np.mean(Re_f0031f0031)


            Re_f3100f2011=np.real(BPMs.loc[:,"f3100"]*np.conj(BPMs.loc[:,"f2011"])); avg_Re_f3100f2011=np.mean(Re_f3100f2011)
            Re_f1120f0031=np.real(BPMs.loc[:,"f1120"]*np.conj(BPMs.loc[:,"f0031"])); avg_Re_f1120f0031=np.mean(Re_f1120f0031)

            for i in range(len(amp)):
                AmpBBting_X.loc[:,amp[i]]=(
                                             (1
                                              +( 8*Im_f2011     )* (Amp_2Jy[i]*1e-6)             *nomexp_p2Qx[i]
                                              +(12*Im_f3100     )* (Amp_2Jx[i]*1e-6)             *nomexp_p2Qx[i]
                                              +(16*Re_f2011f2011)*((Amp_2Jy[i]*1e-6)**2)         *nomexp_p2Qx[i]**2
                                              +(36*Re_f3100f3100)*((Amp_2Jx[i]*1e-6)**2)         *nomexp_p2Qx[i]**2
                                              +(48*Re_f3100f2011)*( Amp_2Jx[i]*Amp_2Jy[i]*1e-12 )*nomexp_p2Qx[i]**2
                                             )#/(1
                                               #+( 8*avg_Im_f2011     )* (Amp_2Jy[i]*1e-6)            *nomexp_p2Qx[i]
                                               #+(12*avg_Im_f3100     )* (Amp_2Jx[i]*1e-6)            *nomexp_p2Qx[i]
                                               #+(16*avg_Re_f2011f2011)*((Amp_2Jy[i]*1e-6)**2)        *nomexp_p2Qx[i]
                                               #+(36*avg_Re_f3100f3100)*((Amp_2Jx[i]*1e-6)**2)        *nomexp_p2Qx[i]
                                               #+(48*avg_Re_f3100f2011)*( Amp_2Jx[i]*Amp_2Jy[i]*1e-12)*nomexp_p2Qx[i]
                                             #)
                                          -1)*100

                AmpBBting_Y.loc[:,amp[i]]=(
                                             (1
                                              +( 8*Im_f1120     )* (Amp_2Jx[i]*1e-6)            *nomexp_p2Qy[i]
                                              +(12*Im_f0031     )* (Amp_2Jy[i]*1e-6)            *nomexp_p2Qy[i]
                                              +(16*Re_f1120f1120)*((Amp_2Jx[i]*1e-6)**2)        *nomexp_p2Qy[i]**2
                                              +(36*Re_f0031f0031)*((Amp_2Jy[i]*1e-6)**2)        *nomexp_p2Qy[i]**2
                                              +(48*Re_f1120f0031)*(Amp_2Jx[i]*Amp_2Jy[i]*1e-12 )*nomexp_p2Qy[i]**2
                                             )#/(1
                                              #+( 8*avg_Im_f1120     )* (Amp_2Jx[i]*1e-6)            *nomexp_p2Qy[i]
                                              #+(12*avg_Im_f0031     )* (Amp_2Jy[i]*1e-6)            *nomexp_p2Qy[i]
                                              #+(16*avg_Re_f1120f1120)*((Amp_2Jx[i]*1e-6)**2)        *nomexp_p2Qy[i]**2
                                              #+(36*avg_Re_f0031f0031)*((Amp_2Jy[i]*1e-6)**2)        *nomexp_p2Qy[i]**2
                                              #+(48*avg_Re_f1120f0031)*( Amp_2Jx[i]*Amp_2Jy[i]*1e-12)*nomexp_p2Qy[i]**2
                                             #)
                                          -1)*100

            return AmpBBting_X,AmpBBting_Y

        else:
            raise TypeError("[class case,Compute_AmpBetaBeating]: Not done yet!")
    #===================================================================================================





    #===================================================================================================
    # Function: Plot detuning
    #---------------------------------------------------------------------------------------------------
    # Description: 
    # Last modification: Thomas PUGNAT (25-06-2020)
    #===================================================================================================
#    def aaa(self,bb,cc):
#        return dd;
    #===================================================================================================

