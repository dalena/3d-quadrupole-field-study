import matplotlib.pyplot as plt
#import pathlib
import numpy  as np
import pandas as pd
#import multiprocessing
#from multiprocessing import Pool
#from matplotlib.ticker import FormatStrFormatter



class file_FringeField:
    # Variable
    # --------------------------------------------------------------------------------------------------
    filename =""                 #
    Length_in=0                  #
    Position ='in'               # True=enter, False=exit
    Vec_Pot_r=pd.DataFrame()     #
    Vec_Pot_d=[]                 #
    dz       =0.                 # step in z
    length   =0.                 # length
    



    # --------------------------------------------------------------------------------------------------
    # Initialization
    # --------------------------------------------------------------------------------------------------
    def __init__(self,filename,Length_in,Position='in'):
      # Set input
      self.filename =filename
      self.Length_in=Length_in
      self.Position =Position
      self.Vec_Pot_r=pd.DataFrame()
      self.Vec_Pot_d=[]
      self.dz       =0.
      self.length   =0.




    def set(self,filename,Length_in,Position='in'):
      # Set input
      self.filename =filename
      self.Length_in=Length_in
      self.Position =Position
      self.Vec_Pot_r=pd.DataFrame()
      self.Vec_Pot_d=pd.DataFrame()
      self.dz       =0.
      self.length   =0.




    # --------------------------------------------------------------------------------------------------
    # Read File
    # --------------------------------------------------------------------------------------------------
    def load_File(self):
      if (self.filename==""):
        raise TypeError("[class file_FringeField,load_File]: wrong filename! Use set(filename,Length_in,Position)!")
      else:
        #    - read the data from the file
        self.Vec_Pot_r=pd.read_csv(self.filename,sep='\s+', names=['z','expx','expy','expz','C_Ax','C_Ay','C_Az']);
        if (not self.Vec_Pot_r.empty):
          z=self.Vec_Pot_r.z.unique()
          #      > get file info
          min_z      =min(z)
          self.length=max(z)-min_z
          self.dz    =self.length/(len(z)-1.)
          #      > Cut file by step
          vp_tp=[]
          for s in z:
            vp_sh=self.Vec_Pot_r[self.Vec_Pot_r["z"]==s]
            vp_tp.append(vp_sh)
          self.Vec_Pot_d=pd.DataFrame({"z":z-min_z, "data":vp_tp})
          #      > debug
          #print(z)
          #print(max(z))
          #print(min(z))
          #print(self.dz)




    # --------------------------------------------------------------------------------------------------
    # Compute detuning using Chao
    # --------------------------------------------------------------------------------------------------
    def DTune_Chao(self,f_BETX, f_BETY, f_ALFX, f_ALFY, norm):
      if (not self.Vec_Pot_r.empty):
        # Get KnL
        # ------------------------------------------------------------------------------
        #              * b3 + b1''
        mask=np.logical_and(self.Vec_Pot_r.expx.values==4,self.Vec_Pot_r.expy.values==0)
        Az3_KL_x4 = -self.Vec_Pot_r.loc[mask,'C_Az'].values*self.dz*norm
        Az3_S_x4  = self.Vec_Pot_r.loc[mask,'z'].values
        Az3_betx_x4=f_BETX(Az3_S_x4)
        #Az3_bety_x4=f_BETY(Az3_S_x4)

        mask=np.logical_and(self.Vec_Pot_r.expx.values==2,self.Vec_Pot_r.expy.values==2)
        Az3_KL_x2y2 = -self.Vec_Pot_r.loc[mask,'C_Az'].values*self.dz*norm
        Az3_S_x2y2  = self.Vec_Pot_r.loc[mask,'z'].values
        Az3_betx_x2y2=f_BETX(Az3_S_x2y2)
        Az3_bety_x2y2=f_BETY(Az3_S_x2y2)

        mask=np.logical_and(self.Vec_Pot_r.expx.values==0,self.Vec_Pot_r.expy.values==4)
        Az3_KL_y4 = -self.Vec_Pot_r.loc[mask,'C_Az'].values*self.dz*norm
        Az3_S_y4  = self.Vec_Pot_r.loc[mask,'z'].values
        #Az3_betx_y4=f_BETX(Az3_S_y4)
        Az3_bety_y4=f_BETY(Az3_S_y4)
	
        #              * b5 + b3'' + b1''''
        mask=np.logical_and(self.Vec_Pot_r.expx.values==6,self.Vec_Pot_r.expy.values==0)
        Az5_KL_x6  = -self.Vec_Pot_r.loc[mask,'C_Az'].values*self.dz*norm
        Az5_S_x6   = self.Vec_Pot_r.loc[mask,'z'].values
        Az5_betx_x6=f_BETX(Az5_S_x6)
        #Az5_bety_x6=f_BETY(Az5_S_x6)

        mask=np.logical_and(self.Vec_Pot_r.expx.values==4,self.Vec_Pot_r.expy.values==2)
        Az5_KL_x4y2  = -self.Vec_Pot_r.loc[mask,'C_Az'].values*self.dz*norm
        Az5_S_x4y2   = self.Vec_Pot_r.loc[mask,'z'].values
        Az5_betx_x4y2=f_BETX(Az5_S_x4y2)
        Az5_bety_x4y2=f_BETY(Az5_S_x4y2)

        mask=np.logical_and(self.Vec_Pot_r.expx.values==2,self.Vec_Pot_r.expy.values==4)
        Az5_KL_x2y4  = -self.Vec_Pot_r.loc[mask,'C_Az'].values*self.dz*norm
        Az5_S_x2y4   = self.Vec_Pot_r.loc[mask,'z'].values
        Az5_betx_x2y4=f_BETX(Az5_S_x2y4)
        Az5_bety_x2y4=f_BETY(Az5_S_x2y4)

        mask=np.logical_and(self.Vec_Pot_r.expx.values==0,self.Vec_Pot_r.expy.values==6)
        Az5_KL_y6  = -self.Vec_Pot_r.loc[mask,'C_Az'].values*self.dz*norm
        Az5_S_y6   = self.Vec_Pot_r.loc[mask,'z'].values
        #Az5_betx_y6=f_BETX(Az5_S_y6)
        Az5_bety_y6=f_BETY(Az5_S_y6)


        # Get AnL (odd derivatives)
        # ------------------------------------------------------------------------------
        #              * b1'
        mask=np.logical_and(self.Vec_Pot_r.expx.values==3,self.Vec_Pot_r.expy.values==0)
        Ax3_KL_x3  = self.Vec_Pot_r.loc[mask,'C_Ax'].values*self.dz*norm
        Ax3_S_x3   = self.Vec_Pot_r.loc[mask,'z'].values
        Ax3_betx_x3=f_BETX(Ax3_S_x3)
        Ax3_alfx_x3=f_ALFX(Ax3_S_x3)

        mask=np.logical_and(self.Vec_Pot_r.expx.values==1,self.Vec_Pot_r.expy.values==2)
        Ax3_KL_xy2  = self.Vec_Pot_r.loc[mask,'C_Ax'].values*self.dz*norm
        Ax3_S_xy2   = self.Vec_Pot_r.loc[mask,'z'].values
        Ax3_betx_xy2=f_BETX(Ax3_S_xy2)
        Ax3_alfx_xy2=f_ALFX(Ax3_S_xy2)
        Ax3_bety_xy2=f_BETY(Ax3_S_xy2)
        Ax3_alfy_xy2=f_ALFY(Ax3_S_xy2)

        mask=np.logical_and(self.Vec_Pot_r.expx.values==2,self.Vec_Pot_r.expy.values==1)
        Ay3_KL_x2y  = self.Vec_Pot_r.loc[mask,'C_Ay'].values*self.dz*norm
        Ay3_S_x2y   = self.Vec_Pot_r.loc[mask,'z'].values
        Ay3_betx_x2y=f_BETX(Ay3_S_x2y)
        Ay3_alfx_x2y=f_ALFX(Ay3_S_x2y)
        Ay3_bety_x2y=f_BETY(Ay3_S_x2y)
        Ay3_alfy_x2y=f_ALFY(Ay3_S_x2y)

        mask=np.logical_and(self.Vec_Pot_r.expx.values==0,self.Vec_Pot_r.expy.values==3)
        Ay3_KL_y3  = self.Vec_Pot_r.loc[mask,'C_Ay'].values*self.dz*norm
        Ay3_S_y3   = self.Vec_Pot_r.loc[mask,'z'].values
        Ay3_bety_y3=f_BETY(Ay3_S_y3)
        Ay3_alfy_y3=f_ALFY(Ay3_S_y3)
	
        #              * b3' + b1'''
        mask=np.logical_and(self.Vec_Pot_r.expx.values==5,self.Vec_Pot_r.expy.values==0)
        Ax5_KL_x5  = self.Vec_Pot_r.loc[mask,'C_Ax'].values*self.dz*norm
        Ax5_S_x5   = self.Vec_Pot_r.loc[mask,'z'].values
        Ax5_betx_x5=f_BETX(Ax5_S_x5)
        Ax5_alfx_x5=f_ALFX(Ax5_S_x5)

        mask=np.logical_and(self.Vec_Pot_r.expx.values==3,self.Vec_Pot_r.expy.values==2)
        Ax5_KL_x3y2  = self.Vec_Pot_r.loc[mask,'C_Ax'].values*self.dz*norm
        Ax5_S_x3y2   = self.Vec_Pot_r.loc[mask,'z'].values
        Ax5_betx_x3y2=f_BETX(Ax5_S_x3y2)
        Ax5_alfx_x3y2=f_ALFX(Ax5_S_x3y2)
        Ax5_bety_x3y2=f_BETY(Ax5_S_x3y2)
        Ax5_alfy_x3y2=f_ALFY(Ax5_S_x3y2)

        mask=np.logical_and(self.Vec_Pot_r.expx.values==1,self.Vec_Pot_r.expy.values==4)
        Ax5_KL_xy4  = self.Vec_Pot_r.loc[mask,'C_Ax'].values*self.dz*norm
        Ax5_S_xy4   = self.Vec_Pot_r.loc[mask,'z'].values
        Ax5_betx_xy4=f_BETX(Ax5_S_xy4)
        Ax5_alfx_xy4=f_ALFX(Ax5_S_xy4)
        Ax5_bety_xy4=f_BETY(Ax5_S_xy4)
        Ax5_alfy_xy4=f_ALFY(Ax5_S_xy4)

        mask=np.logical_and(self.Vec_Pot_r.expx.values==4,self.Vec_Pot_r.expy.values==1)
        Ay5_KL_x4y  = self.Vec_Pot_r.loc[mask,'C_Ay'].values*self.dz*norm
        Ay5_S_x4y   = self.Vec_Pot_r.loc[mask,'z'].values
        Ay5_betx_x4y=f_BETX(Ay5_S_x4y)
        Ay5_alfx_x4y=f_ALFX(Ay5_S_x4y)
        Ay5_bety_x4y=f_BETY(Ay5_S_x4y)
        Ay5_alfy_x4y=f_ALFY(Ay5_S_x4y)

        mask=np.logical_and(self.Vec_Pot_r.expx.values==2,self.Vec_Pot_r.expy.values==3)
        Ay5_KL_x2y3  = self.Vec_Pot_r.loc[mask,'C_Ay'].values*self.dz*norm
        Ay5_S_x2y3   = self.Vec_Pot_r.loc[mask,'z'].values
        Ay5_betx_x2y3=f_BETX(Ay5_S_x2y3)
        Ay5_alfx_x2y3=f_ALFX(Ay5_S_x2y3)
        Ay5_bety_x2y3=f_BETY(Ay5_S_x2y3)
        Ay5_alfy_x2y3=f_ALFY(Ay5_S_x2y3)

        mask=np.logical_and(self.Vec_Pot_r.expx.values==0,self.Vec_Pot_r.expy.values==5)
        Ay5_KL_y5  = self.Vec_Pot_r.loc[mask,'C_Ay'].values*self.dz*norm
        Ay5_S_y5   = self.Vec_Pot_r.loc[mask,'z'].values
        Ay5_bety_y5=f_BETY(Ay5_S_y5)
        Ay5_alfy_y5=f_ALFY(Ay5_S_y5)


        # Compute detuning coefficient
        # ------------------------------------------------------------------------------
        DQx=[];  DQy=[]
        DQx.append( [ 0, 0, 0.]) # (bety1   *K1Lx).sum()     /( 2.*2.*np.pi)])  #[]
        DQy.append( [ 0, 0, 0.]) # (betx2   *K1Ly).sum()     /( 2.*2.*np.pi)])  #[]
        DQx.append( [ 1, 0, ((Az3_betx_x4**2*(4*Az3_KL_x4)).sum()+( (Ax3_betx_x3    *Ax3_alfx_x3)*(4*Ax3_KL_x3)).sum())*3e-6 /( 8.*(2.*np.pi))])  #[um-1]
        DQx.append( [ 0, 1, 0.])  #[um-1]
        DQy.append( [ 0, 1, ((Az3_bety_y4**2*(4*Az3_KL_y4)).sum()+( (Ay3_bety_y3    *Ay3_alfy_y3)*(4*Ay3_KL_y3)).sum())*3e-6 /( 8.*(2.*np.pi))])  #[um-1]
        DQy.append( [ 1, 0, 0.])  #[um-1]
        DQx.append( [ 2, 0, ((Az5_betx_x6**3*(6*Az5_KL_x6)).sum()+(((Ax5_betx_x5**2)*Ax5_alfx_x5)*(6*Ax5_KL_x5)).sum())*5e-12/(16.*(2.*np.pi))])  #[um-2]
        DQx.append( [ 1, 1, 0.])  #[um-2]
        DQx.append( [ 0, 2, 0.])  #[um-2]
        DQy.append( [ 0, 2, ((Az5_bety_y6**3*(6*Az5_KL_y6)).sum()+(((Ay5_bety_y5**2)*Ay5_alfy_y5)*(6*Ay5_KL_y5)).sum())*5e-12/(16.*(2.*np.pi))])  #[um-2]
        DQy.append( [ 1, 1, 0.])  #[um-2]
        DQy.append( [ 2, 0, 0.])  #[um-2]
        DF_Qx=pd.DataFrame(DQx,columns=["exp_2Jx[um]","exp_2Jy[um]","Coef_DQx"])
        DF_Qy=pd.DataFrame(DQy,columns=["exp_2Jx[um]","exp_2Jy[um]","Coef_DQy"])
        return DF_Qx,DF_Qy




    # --------------------------------------------------------------------------------------------------
    # ?????????????????????????????????
    # --------------------------------------------------------------------------------------------------
    def Compute_Cor(self,f_BETX, f_BETY,f_ALFX, f_ALFY, norm):
      res={"FFi_sum40":0,"FFi_sum04":0,"FFi_sum22":0,"FFi_sum60":0,"FFi_sum06":0,"FFi_sum42":0,"FFi_sum24":0}
      if (not self.Vec_Pot_r.empty):
        # Compute detuning coefficient
        #              * b3 + b1''
        mask=np.logical_and(self.Vec_Pot_r.expx.values==4,self.Vec_Pot_r.expy.values==0)
        Az3_KL_x4  = -self.Vec_Pot_r.loc[mask,'C_Az'].values*self.dz*norm #*6
        if len(Az3_KL_x4)>0:
          Az3_KL_x4[len(Az3_KL_x4)-1]=0.
        Az3_S_x4   = self.Vec_Pot_r.loc[mask,'z'].values
        Az3_betx_x4=f_BETX(Az3_S_x4)
        Az3_bety_x4=f_BETY(Az3_S_x4)

        mask=np.logical_and(self.Vec_Pot_r.expx.values==0,self.Vec_Pot_r.expy.values==4)
        Az3_KL_y4  = -self.Vec_Pot_r.loc[mask,'C_Az'].values*self.dz*norm #*6
        if len(Az3_KL_y4)>0:
          Az3_KL_y4[len(Az3_KL_y4)-1]=0.
        Az3_S_y4   = self.Vec_Pot_r.loc[mask,'z'].values
        Az3_betx_y4=f_BETX(Az3_S_y4)
        Az3_bety_y4=f_BETY(Az3_S_y4)

        #              * b3 + b1''
        mask=np.logical_and(self.Vec_Pot_r.expx.values==6,self.Vec_Pot_r.expy.values==0)
        Az5_KL_x6  = -self.Vec_Pot_r.loc[mask,'C_Az'].values*self.dz*norm #*120
        if len(Az5_KL_x6)>0:
          Az5_KL_x6[len(Az5_KL_x6)-1]=0.
        Az5_S_x6   = self.Vec_Pot_r.loc[mask,'z'].values
        Az5_betx_x6=f_BETX(Az5_S_x6)
        Az5_bety_x6=f_BETY(Az5_S_x6)

        mask=np.logical_and(self.Vec_Pot_r.expx.values==0,self.Vec_Pot_r.expy.values==6)
        Az5_KL_y6  = -self.Vec_Pot_r.loc[mask,'C_Az'].values*self.dz*norm #*120
        if len(Az5_KL_y6)>0:
          Az5_KL_y6[len(Az5_KL_y6)-1]=0.
        Az5_S_y6   = self.Vec_Pot_r.loc[mask,'z'].values
        Az5_betx_y6=f_BETX(Az5_S_y6)
        Az5_bety_y6=f_BETY(Az5_S_y6)

        #              * b1'
        # For HFC gauge, Ax=0 
        mask=np.logical_and(self.Vec_Pot_r.expx.values==3,self.Vec_Pot_r.expy.values==0)
        Ax3_KL_x3  = self.Vec_Pot_r.loc[mask,'C_Ax'].values*self.dz*norm
        if len(Ax3_KL_x3)>0:
          Ax3_KL_x3[len(Ax3_KL_x3)-1]=0.
        Ax3_S_x3   = self.Vec_Pot_r.loc[mask,'z'].values
        Ax3_betx_x3=f_BETX(Ax3_S_x3)
        Ax3_alfx_x3=f_ALFX(Ax3_S_x3)

        mask=np.logical_and(self.Vec_Pot_r.expx.values==0,self.Vec_Pot_r.expy.values==3)
        Ay3_KL_y3  = self.Vec_Pot_r.loc[mask,'C_Ay'].values*self.dz*norm
        if len(Ay3_KL_y3)>0:
          Ay3_KL_y3[len(Ay3_KL_y3)-1]=0.
        Ay3_S_y3   = self.Vec_Pot_r.loc[mask,'z'].values
        Ay3_bety_y3=f_BETY(Ay3_S_y3)
        Ay3_alfy_y3=f_ALFY(Ay3_S_y3)
	
        #              * b3' + b1'''
        mask=np.logical_and(self.Vec_Pot_r.expx.values==5,self.Vec_Pot_r.expy.values==0)
        Ax5_KL_x5  = self.Vec_Pot_r.loc[mask,'C_Ax'].values*self.dz*norm
        if len(Ax5_KL_x5)>0:
          Ax5_KL_x5[len(Ax5_KL_x5)-1]=0.
        Ax5_S_x5   = self.Vec_Pot_r.loc[mask,'z'].values
        Ax5_betx_x5=f_BETX(Ax5_S_x5)
        Ax5_alfx_x5=f_ALFX(Ax5_S_x5)

        mask=np.logical_and(self.Vec_Pot_r.expx.values==0,self.Vec_Pot_r.expy.values==5)
        Ay5_KL_y5  = self.Vec_Pot_r.loc[mask,'C_Ay'].values*self.dz*norm
        if len(Ay5_KL_y5)>0:
          Ay5_KL_y5[len(Ay5_KL_y5)-1]=0.
        Ay5_S_y5   = self.Vec_Pot_r.loc[mask,'z'].values
        Ay5_bety_y5=f_BETY(Ay5_S_y5)
        Ay5_alfy_y5=f_ALFY(Ay5_S_y5)

        #      - B3 and A3 corrector
        #FFi_sum12=0; 
        #FFi_sum21=0.; 

        #FFi_sum03=0.;
        #FFi_sum30=0.;
        #      - B4 and A4 corrector
        FFi_sum40= (((Az3_betx_x4**2)*Az3_KL_x4).sum() + ((Ax3_betx_x3   *Ax3_alfx_x3)*Ax3_KL_x3).sum() )*6*4; 
        FFi_sum04= (((Az3_bety_y4**2)*Az3_KL_y4).sum() + ((Ay3_bety_y3   *Ay3_alfy_y3)*Ay3_KL_y3).sum() )*6*4;
        FFi_sum22=0#((Az3_betx_x   *Az3_bety_x   )*Az3_KL_x4).sum()#+((Ax3_bety_x*Ax3_alfx_x-Ax3_betx_x*Ax3_alfy_x)*Ax3_KL_x).sum(); 

        #FFi_sum31=0.; 
        #FFi_sum13=0.;
        #      - B5 and A5 corrector
        #FFi_sum50=0.;
        #FFi_sum05=0.;

        #FFi_sum32=0.;
        #FFi_sum14=0.;

        #FFi_sum41=0.;
        #FFi_sum23=0.;
        #      - B6 and A6 corrector
        FFi_sum60= (((Az5_betx_x6**3)*Az5_KL_x6).sum() + ((Ax5_betx_x5**2)*(Ax5_alfx_x5*Ax5_KL_x5)).sum())*120*6;
        FFi_sum06=-(((Az5_bety_y6**3)*Az5_KL_y6).sum() + ((Ay5_bety_y5**2)*(Ay5_alfy_y5*Ay5_KL_y5)).sum())*120*6;
        FFi_sum42=0#((Az5_betx_x**2 *Az5_bety_x   )*Az5_KL_x).sum()#+((Ax5_betx_x*(Ax5_bety_x*Ax5_alfx_x-Ax5_betx_x*Ax5_alfy_x))*Ax5_KL_x).sum();
        FFi_sum24=0#((Az5_betx_x    *Az5_bety_x**2)*Az5_KL_x).sum()#+((Ax5_bety_x*(Ax5_bety_x*Ax5_alfx_x-Ax5_betx_x*Ax5_alfy_x))*Ax5_KL_x).sum();

        # Compute detuning coefficient
        res={"FFi_sum40":FFi_sum40,"FFi_sum04":FFi_sum04,"FFi_sum22":FFi_sum22,
             "FFi_sum60":FFi_sum60,"FFi_sum06":FFi_sum06,"FFi_sum42":FFi_sum42,"FFi_sum24":FFi_sum24}
      return res





    # --------------------------------------------------------------------------------------------------
    # Plot data
    # --------------------------------------------------------------------------------------------------
    def plot_vecpot(self,fig,ax,axis,rx=0.1,ry=0.):
      if (self.Vec_Pot_r.empty):
        raise TypeError("[class file_FringeField,plot_vecpot]: No data!")
      else:
        #    - read the data from the file
        tab=[]
        for i in range(len(self.Vec_Pot_d)):
          tp=self.Vec_Pot_d.loc[i,'data']
          t =sum((rx**tp['expx'])*(ry**tp['expy'])*tp['C_'+axis])
          tab.append(t)
        fig, plt.plot(self.Vec_Pot_d["z"],tab,"-.",label=axis)
        ax.legend();
        plt.grid()
