# From Leon Van Riesen-Haupt (11/2019)
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

PI2I = 2 * np.pi * complex(0, 1)

#def read_machine_params(filename):
#    if filename[-3:] == 'sad':
#        TW = pd.read_csv(filename, names=["S", "BETX", "BETY", "MUX", "MUY", "K1L", "K2L", "K3L", "K1SL", "K2SL", "K3SL" ],sep='\t',engine='python')
#    elif filename[-3:] == 'out':
#        TW = pd.read_csv(filename, header=[46,47],sep='\s{2,}',engine='python')
#
#        TW.columns = TW.columns.droplevel(1)
#        TW.columns = list(TW.columns[1:]) + ['END']
#    else:
#        print 'Unknown File Format'
#        exit()
#
#    QX = TW.MUX[-1]
#    QY = TW.MUY[-1]
#
#    BETAX = TW.BETX[0]
#    BETAY = TW.BETY[0]
#
#    return [QX, QY, BETAX, BETAY]


#def read_twiss(filename):
#    #TW = pd.read_csv(filename, header=[46,47],sep='\s{2,}',engine='python')
#
#    #TW.columns = TW.columns.droplevel(1)
#    #TW.columns = list(TW.columns[1:]) + ['END']
#
#    if filename[-3:] == 'sad':
#        TW = pd.read_csv(filename, names=["S", "BETX", "BETY", "MUX", "MUY", "K1L", "K2L", "K3L", "K1SL", "K2SL", "K3SL" ],sep='\t',engine='python')
#    elif filename[-3:] == 'out':
#        TW = pd.read_csv(filename, header=[46,47],sep='\s{2,}',engine='python')
#
#        TW.columns = TW.columns.droplevel(1)
#        TW.columns = list(TW.columns[1:]) + ['END']
#    else:
#        print 'Unknown File Format'
#        exit()
#
#    QX = TW.MUX[-1]
#    QY = TW.MUY[-1]
#
#    quad = TW[TW.K1L != 0]
#    sext = TW[TW.K2L != 0]
#    octo = TW[TW.K3L != 0]
#
#    skew_quad = TW[TW.K1SL != 0]
#    skew_sext = TW[TW.K2SL != 0]
#    skew_octo = TW[TW.K3SL != 0]
#
#    return [QX, QY, quad, sext, octo, skew_quad, skew_sext, skew_octo]



#=======================================================================================
#   Resonnance terme
#---------------------------------------------------------------------------------------
def T(nx,ny,dmux,dmuy,qx,qy):
    return np.cos(2*np.pi*(nx*(np.abs(dmux)-qx*0.5)+ny*(np.abs(dmuy)-qy*0.5)))/np.sin(np.pi*(nx*qx+ny*qy))
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def DQxDjx_skew_sext(Tw,qx,qy):
    dvx = 0
    bx = Tw.BETX
    by = Tw.BETY
    mx = Tw.MUX
    my = Tw.MUY
    a2 = Tw.K2SL

    for j in range(len(bx)):
        for k in range(len(bx)):
            dmux = mx[j] - mx[k]
            dmuy = my[j] - my[k]

            dvx = dvx + (1.0/8) * a2[j]*a2[k] * ( -4*bx[j]*bx[k]* np.sqrt(by[j]*by[k]) * T(0, 1,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/8) * a2[j]*a2[k] * ( -  bx[j]*bx[k]* np.sqrt(by[j]*by[k]) * T(2, 1,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/8) * a2[j]*a2[k] * (    bx[j]*bx[k]* np.sqrt(by[j]*by[k]) * T(2,-1,dmux,dmuy,qx,qy) )

    return dvx / (2*np.pi)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def DQxDjx_sext(Tw,qx,qy):
    dvx = 0
    bx = Tw.BETX
    by = Tw.BETY
    mx = Tw.MUX
    my = Tw.MUY
    b2 = Tw.K2L

    
    for j in range(len(bx)):
        for k in range(len(bx)):
            dmux = mx[j] - mx[k]
            dmuy = my[j] - my[k]

            dvx = dvx + (1.0/8) * b2[j]*b2[k] * ( -3*bx[j]*bx[k]* np.sqrt(bx[j]*bx[k]) * T(1,0,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/8) * b2[j]*b2[k] * ( -  bx[j]*bx[k]* np.sqrt(bx[j]*bx[k]) * T(3,0,dmux,dmuy,qx,qy) )

    return dvx / (2*np.pi)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
#def DQxDjx(filename):
#    [QX, QY, quad, sext, octo, skew_quad, skew_sext, skew_octo] = read_twiss(filename)
#    return DQxDjx_skew_sext(skew_sext,QX,QY) + DQxDjx_sext(sext,QX,QY)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def D2QxDjx2_skew_octo(Tw,qx,qy):
    dvx = 0
    bx = Tw.BETX
    by = Tw.BETY
    mx = Tw.MUX
    my = Tw.MUY
    a3 = Tw.K3SL

    for j in range(len(bx)):
        for k in range(len(bx)):
            dmux = mx[j] - mx[k]
            dmuy = my[j] - my[k]
            
            dvx = dvx + (1.0/32) * a3[j]*a3[k] * ( - 3 * np.sqrt(bx[j]*bx[k]*by[j]*by[k]) * bx[j]*bx[k] * 2 * T(3, 1,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/32) * a3[j]*a3[k] * (   3 * np.sqrt(bx[j]*bx[k]*by[j]*by[k]) * bx[j]*bx[k] * 2 * T(3,-1,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/32) * a3[j]*a3[k] * ( -27 * np.sqrt(bx[j]*bx[k]*by[j]*by[k]) * bx[j]*bx[k] * 2 * T(1, 1,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/32) * a3[j]*a3[k] * (  27 * np.sqrt(bx[j]*bx[k]*by[j]*by[k]) * bx[j]*bx[k] * 2 * T(1,-1,dmux,dmuy,qx,qy) )

    return dvx / (2*np.pi)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def D2QxDjx2_octo(Tw,qx,qy):
    dvx = 0
    bx = Tw.BETX
    by = Tw.BETY
    mx = Tw.MUX
    my = Tw.MUY
    b3 = Tw.K3L

    for j in range(len(bx)):
        for k in range(len(bx)):
            dmux = mx[j] - mx[k]
            dmuy = my[j] - my[k]
            
            dvx = dvx + (1.0/32) * b3[j]*b3[k] * ( - 3 * bx[j]*bx[j]*bx[k]*bx[k] * 2 * T(4,0,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/32) * b3[j]*b3[k] * ( -24 * bx[j]*bx[j]*bx[k]*bx[k] * 2 * T(2,0,dmux,dmuy,qx,qy) )

    return dvx / (2*np.pi)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
#def D2QxDjx2(filename):
#    [QX, QY, quad, sext, octo, skew_quad, skew_sext, skew_octo] = read_twiss(filename)
#    return D2QxDjx2_skew_octo(skew_octo,QX,QY) + D2QxDjx2_octo(octo,QX,QY)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def DQxDjy_skew_sext(Tw,qx,qy):
    dvx = 0
    bx = Tw.BETX
    by = Tw.BETY
    mx = Tw.MUX
    my = Tw.MUY
    a2 = Tw.K2SL

    for j in range(len(bx)):
        for k in range(len(bx)):
            dmux = mx[j] - mx[k]
            dmuy = my[j] - my[k]

            dvx = dvx + (1.0/8) * a2[j]*a2[k] * (  4 * bx[j]*by[k] * np.sqrt(by[j]*by[k])     * T(0, 1,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/8) * a2[j]*a2[k] * ( -    bx[j]*bx[k] * np.sqrt(by[j]*by[k]) * 2 * T(2, 1,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/8) * a2[j]*a2[k] * ( -    bx[j]*bx[k] * np.sqrt(by[j]*by[k]) * 2 * T(2,-1,dmux,dmuy,qx,qy) )

    return dvx / (2*np.pi)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def DQxDjy_sext(Tw,qx,qy):
    dvx = 0
    bx = Tw.BETX
    by = Tw.BETY
    mx = Tw.MUX
    my = Tw.MUY
    b2 = Tw.K2L

    for j in range(len(bx)):
        for k in range(len(bx)):
            dmux = mx[j] - mx[k]
            dmuy = my[j] - my[k]

            dvx = dvx + (1.0/8) * b2[j]*b2[k] * (  4 * np.sqrt(bx[j]*bx[k]) * bx[k]*by[j] * T(1,0,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/8) * b2[j]*b2[k] * (  2 * np.sqrt(bx[j]*bx[k]) * by[j]*by[k] * (T(1,-2,dmux,dmuy,qx,qy) - T(1,2,dmux,dmuy,qx,qy)) )

    return dvx / (2*np.pi)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
#def DQxDjy(filename):
#    [QX, QY, quad, sext, octo, skew_quad, skew_sext, skew_octo] = read_twiss(filename)
#    return DQxDjy_skew_sext(skew_sext,QX,QY) + DQxDjy_sext(sext,QX,QY)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def D2QxDjy2_skew_octo(Tw,qx,qy):
    dvx = 0
    bx = Tw.BETX
    by = Tw.BETY
    mx = Tw.MUX
    my = Tw.MUY
    a3 = Tw.K3SL

    for j in range(len(bx)):
        for k in range(len(bx)):
            dmux = mx[j] - mx[k]
            dmuy = my[j] - my[k]

            dvx = dvx + (1.0/32) * a3[j]*a3[k] * (   9 * np.sqrt(bx[j]*bx[k]*by[j]*by[k]) * by[j]*by[k] * 2 * (T(1,-3,dmux,dmuy,qx,qy) - T(1,3,dmux,dmuy,qx,qy))  )
            dvx = dvx + (1.0/32) * a3[j]*a3[k] * ( -27 * np.sqrt(bx[j]*bx[k]*by[j]*by[k]) * by[j]*by[k] * 2 *  T(1, 1,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/32) * a3[j]*a3[k] * (  36 * np.sqrt(bx[j]*bx[k]*by[j]*by[k]) * by[j]*bx[k] * 2 *  T(1, 1,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/32) * a3[j]*a3[k] * (  27 * np.sqrt(bx[j]*bx[k]*by[j]*by[k]) * by[j]*by[k] * 2 *  T(1,-1,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/32) * a3[j]*a3[k] * (  36 * np.sqrt(bx[j]*bx[k]*by[j]*by[k]) * by[j]*bx[k] * 2 *  T(1,-1,dmux,dmuy,qx,qy) )

    return dvx / (2*np.pi)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def D2QxDjy2_octo(Tw,qx,qy):
    dvx = 0
    bx = Tw.BETX
    by = Tw.BETY
    mx = Tw.MUX
    my = Tw.MUY
    b3 = Tw.K3L

    for j in range(len(bx)):
        for k in range(len(bx)):
            dmux = mx[j] - mx[k]
            dmuy = my[j] - my[k]

            dvx = dvx + (1.0/32) * b3[j]*b3[k] * (  -9 * bx[j]*bx[k]*by[j]*by[k] * 2 * T(2, 2,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/32) * b3[j]*b3[k] * (  -9 * bx[j]*bx[k]*by[j]*by[k] * 2 * T(2,-2,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/32) * b3[j]*b3[k] * (  36 * bx[j]*by[j]*by[k]*by[k] * 2 * T(0, 2,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/32) * b3[j]*b3[k] * ( -36 * bx[j]*bx[k]*by[j]*by[k] * 2 * T(2, 0,dmux,dmuy,qx,qy) )

    return dvx / (2*np.pi)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
#def D2QxDjy2(filename):
#    [QX, QY, quad, sext, octo, skew_quad, skew_sext, skew_octo] = read_twiss(filename)
#    return D2QxDjy2_skew_octo(skew_octo,QX,QY) + D2QxDjy2_octo(octo,QX,QY)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def D2QxDjxDjy_skew_octo(Tw,qx,qy):
    dvx = 0
    bx = Tw.BETX
    by = Tw.BETY
    mx = Tw.MUX
    my = Tw.MUY
    a3 = Tw.K3SL

    for j in range(len(bx)):
        for k in range(len(bx)):
            dmux = mx[j] - mx[k]
            dmuy = my[j] - my[k]

            dvx = dvx + (1.0/32) * a3[j]*a3[k] * (  -3 * np.sqrt(bx[j]*bx[k]*by[j]*by[k]) * bx[j]*bx[k] * 6 * T(3, 1,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/32) * a3[j]*a3[k] * (  -3 * np.sqrt(bx[j]*bx[k]*by[j]*by[k]) * bx[j]*bx[k] * 6 * T(3,-1,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/32) * a3[j]*a3[k] * (  72 * np.sqrt(bx[j]*bx[k]*by[j]*by[k]) * bx[k]*by[j] *     T(1, 1,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/32) * a3[j]*a3[k] * ( -54 * np.sqrt(bx[j]*bx[k]*by[j]*by[k]) * bx[j]*bx[k] *     T(1, 1,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/32) * a3[j]*a3[k] * ( -72 * np.sqrt(bx[j]*bx[k]*by[j]*by[k]) * bx[k]*by[j] *     T(1,-1,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/32) * a3[j]*a3[k] * ( -54 * np.sqrt(bx[j]*bx[k]*by[j]*by[k]) * bx[j]*bx[k] *     T(1,-1,dmux,dmuy,qx,qy) )

    return dvx / (2*np.pi)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def D2QxDjxDjy_octo(Tw,qx,qy):
    dvx = 0
    bx = Tw.BETX
    by = Tw.BETY
    mx = Tw.MUX
    my = Tw.MUY
    b3 = Tw.K3L

    for j in range(len(bx)):
        for k in range(len(bx)):
            dmux = mx[j] - mx[k]
            dmuy = my[j] - my[k]

            dvx = dvx + (1.0/32) * b3[j]*b3[k] * (  -9 * bx[j]*bx[k]*by[j]*by[k] * 2 * T(2, 2,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/32) * b3[j]*b3[k] * (   9 * bx[j]*bx[k]*by[j]*by[k] * 2 * T(2,-2,dmux,dmuy,qx,qy))
            dvx = dvx + (1.0/32) * b3[j]*b3[k] * (  72 * bx[j]*bx[k]*bx[k]*by[j] *     T(2, 0,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/32) * b3[j]*b3[k] * ( -72 * bx[j]*bx[k]*by[j]*by[k] *     T(0, 2,dmux,dmuy,qx,qy) )

    return dvx / (2*np.pi)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
#def D2QxDjxDjy(filename):
#    [QX, QY, quad, sext, octo, skew_quad, skew_sext, skew_octo] = read_twiss(filename)
#    return D2QxDjxDjy_skew_octo(skew_octo,QX,QY) + D2QxDjxDjy_octo(octo,QX,QY)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def DQyDjx_skew_sext(Tw,qx,qy):
    dvy = 0
    bx = Tw.BETX
    by = Tw.BETY
    mx = Tw.MUX
    my = Tw.MUY
    a2 = Tw.K2SL

    for j in range(len(bx)):
        for k in range(len(bx)):
            dmux = mx[j] - mx[k]
            dmuy = my[j] - my[k]

            dvy = dvy + (1.0/8) * a2[j]*a2[k] * ( 4 * bx[j]*by[k] * np.sqrt(by[j]*by[k]) *  T(0, 1,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/8) * a2[j]*a2[k] * ( 2 * bx[j]*bx[k] * np.sqrt(by[j]*by[k]) * (T(2,-1,dmux,dmuy,qx,qy) - T(2,1,dmux,dmuy,qx,qy)) )

    return dvy / (2*np.pi)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def DQyDjx_sext(Tw,qx,qy):
    dvy = 0
    bx = Tw.BETX
    by = Tw.BETY
    mx = Tw.MUX
    my = Tw.MUY
    b2 = Tw.K2L

    for j in range(len(bx)):
        for k in range(len(bx)):
            dmux = mx[j] - mx[k]
            dmuy = my[j] - my[k]

            dvy = dvy + (1.0/8) * b2[j]*b2[k] * (  4 * bx[k]*by[j] * np.sqrt(bx[j]*bx[k]) *     T(1, 0,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/8) * b2[j]*b2[k] * ( -    by[j]*by[k] * np.sqrt(bx[j]*bx[k]) * 2 * T(1, 2,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/8) * b2[j]*b2[k] * ( -    by[j]*by[k] * np.sqrt(bx[j]*bx[k]) * 2 * T(1,-2,dmux,dmuy,qx,qy) )

    return dvy / (2*np.pi)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
#def DQyDjx(filename):
#    [QX, QY, quad, sext, octo, skew_quad, skew_sext, skew_octo] = read_twiss(filename)
#    return DQyDjx_skew_sext(skew_sext,QX,QY) + DQyDjx_sext(sext,QX,QY)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def D2QyDjx2_skew_octo(Tw,qx,qy):
    dvy = 0
    bx = Tw.BETX
    by = Tw.BETY
    mx = Tw.MUX
    my = Tw.MUY
    a3 = Tw.K3SL

    for j in range(len(bx)):
        for k in range(len(bx)):
            dmux = mx[j] - mx[k]
            dmuy = my[j] - my[k]

            dvy = dvy + (1.0/32) * a3[j]*a3[k] * (  -9 * np.sqrt(bx[j]*bx[k]*by[j]*by[k]) * bx[j]*bx[k] * 2 * (T(3,-1,dmux,dmuy,qx,qy) + T(3,1,dmux,dmuy,qx,qy)) )
            dvy = dvy + (1.0/32) * a3[j]*a3[k] * ( -27 * np.sqrt(bx[j]*bx[k]*by[j]*by[k]) * bx[j]*bx[k] * 2 *  T(1, 1,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/32) * a3[j]*a3[k] * (  36 * np.sqrt(bx[j]*bx[k]*by[j]*by[k]) * by[j]*bx[k] * 2 *  T(1, 1,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/32) * a3[j]*a3[k] * ( -27 * np.sqrt(bx[j]*bx[k]*by[j]*by[k]) * bx[j]*bx[k] * 2 *  T(1,-1,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/32) * a3[j]*a3[k] * ( -36 * np.sqrt(bx[j]*bx[k]*by[j]*by[k]) * by[j]*bx[k] * 2 *  T(1,-1,dmux,dmuy,qx,qy) )

    return dvy / (2*np.pi)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def D2QyDjx2_octo(Tw,qx,qy):
    dvy = 0
    bx = Tw.BETX
    by = Tw.BETY
    mx = Tw.MUX
    my = Tw.MUY
    b3 = Tw.K3L

    for j in range(len(bx)):
        for k in range(len(bx)):
            dmux = mx[j] - mx[k]
            dmuy = my[j] - my[k]

            dvy = dvy + (1.0/32) * b3[j]*b3[k] * (  -9 * bx[j]*bx[k]*by[j]*by[k] * 2 * T(2, 2,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/32) * b3[j]*b3[k] * (   9 * bx[j]*bx[k]*by[j]*by[k] * 2 * T(2,-2,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/32) * b3[j]*b3[k] * (  36 * bx[j]*bx[k]*by[j]*bx[k] * 2 * T(2, 0,dmux,dmuy,qx,qy) )  # ???????
            dvy = dvy + (1.0/32) * b3[j]*b3[k] * ( -36 * bx[j]*bx[k]*by[j]*by[k] * 2 * T(0, 2,dmux,dmuy,qx,qy) )

    return dvy / (2*np.pi)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
#def D2QyDjx2(filename):
#    [QX, QY, quad, sext, octo, skew_quad, skew_sext, skew_octo] = read_twiss(filename)
#    return D2QyDjx2_skew_octo(skew_octo,QX,QY) + D2QyDjx2_octo(octo,QX,QY)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def DQyDjy_skew_sext(Tw,qx,qy):
    dvy = 0
    bx = Tw.BETX
    by = Tw.BETY
    mx = Tw.MUX
    my = Tw.MUY
    a2 = Tw.K2SL

    for j in range(len(bx)):
        for k in range(len(bx)):
            dmux = mx[j] - mx[k]
            dmuy = my[j] - my[k]

            dvy = dvy + (1.0/8) * a2[j]*a2[k] * ( -3 * by[j]*by[k] * np.sqrt(by[j]*by[k]) * T(0,1,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/8) * a2[j]*a2[k] * (      by[j]*by[k] * np.sqrt(by[j]*by[k]) * T(0,3,dmux,dmuy,qx,qy) )

    return dvy / (2*np.pi)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def DQyDjy_sext(Tw,qx,qy):
    dvy = 0
    bx = Tw.BETX
    by = Tw.BETY
    mx = Tw.MUX
    my = Tw.MUY
    b2 = Tw.K2L

    for j in range(len(bx)):
        for k in range(len(bx)):
            dmux = mx[j] - mx[k]
            dmuy = my[j] - my[k]

            dvy = dvy + (1.0/8) * b2[j]*b2[k] * ( -4 * np.sqrt(bx[j]*bx[k]) * by[j]*by[k] * T(1, 0,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/8) * b2[j]*b2[k] * (    - np.sqrt(bx[j]*bx[k]) * by[j]*by[k] * T(1, 2,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/8) * b2[j]*b2[k] * (      np.sqrt(bx[j]*bx[k]) * by[j]*by[k] * T(1,-2,dmux,dmuy,qx,qy) )

    return dvy / (2*np.pi)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
#def DQyDjy(filename):
#    [QX, QY, quad, sext, octo, skew_quad, skew_sext, skew_octo] = read_twiss(filename)
#    return DQyDjy_skew_sext(skew_sext,QX,QY) + DQyDjy_sext(sext,QX,QY)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def D2QyDjy2_skew_octo(Tw,qx,qy):
    dvy = 0
    bx = Tw.BETX
    by = Tw.BETY
    mx = Tw.MUX
    my = Tw.MUY
    a3 = Tw.K3SL

    for j in range(len(bx)):
        for k in range(len(bx)):
            dmux = mx[j] - mx[k]
            dmuy = my[j] - my[k]

            dvy = dvy + (1.0/32) * a3[j]*a3[k] * (   3 * np.sqrt(bx[j]*bx[k]*by[j]*by[k]) * by[j]*by[k] * 2 * T(1,-3,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/32) * a3[j]*a3[k] * (   3 * np.sqrt(bx[j]*bx[k]*by[j]*by[k]) * by[j]*by[k] * 2 * T(1, 3,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/32) * a3[j]*a3[k] * ( -27 * np.sqrt(bx[j]*bx[k]*by[j]*by[k]) * by[j]*by[k] * 2 * T(1, 1,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/32) * a3[j]*a3[k] * ( -27 * np.sqrt(bx[j]*bx[k]*by[j]*by[k]) * by[j]*by[k] * 2 * T(1,-1,dmux,dmuy,qx,qy) )

    return dvy / (2*np.pi)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def D2QyDjy2_octo(Tw,qx,qy):
    dvy = 0
    bx = Tw.BETX
    by = Tw.BETY
    mx = Tw.MUX
    my = Tw.MUY
    b3 = Tw.K3L

    for j in range(len(bx)):
        for k in range(len(bx)):
            dmux = mx[j] - mx[k]
            dmuy = my[j] - my[k]

            dvy = dvy + (1.0/32) * b3[j]*b3[k] * (  -3 * bx[j]*bx[j]*bx[k]*bx[k] * 2 * T(0,4,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/32) * b3[j]*b3[k] * ( -24 * by[j]*by[j]*by[k]*by[k] * 2 * T(0,2,dmux,dmuy,qx,qy) )

    return dvy / (2*np.pi)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
#def D2QyDjy2(filename):
#    [QX, QY, quad, sext, octo, skew_quad, skew_sext, skew_octo] = read_twiss(filename)
#    return D2QyDjy2_skew_octo(skew_octo,QX,QY) + D2QyDjy2_octo(octo,QX,QY)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def D2QyDjxDjy_skew_octo(Tw,qx,qy):
    dvy = 0
    bx = Tw.BETX
    by = Tw.BETY
    mx = Tw.MUX
    my = Tw.MUY
    a3 = Tw.K3SL

    for j in range(len(bx)):
        for k in range(len(bx)):
            dmux = mx[j] - mx[k]
            dmuy = my[j] - my[k]

            dvy = dvy + (1.0/32) * a3[j]*a3[k] * (   3 * np.sqrt(bx[j]*bx[k]*by[j]*by[k]) * by[j]*by[k] * 6 * T(1,-3,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/32) * a3[j]*a3[k] * (  -3 * np.sqrt(bx[j]*bx[k]*by[j]*by[k]) * by[j]*by[k] * 6 * T(1, 3,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/32) * a3[j]*a3[k] * ( -54 * np.sqrt(bx[j]*bx[k]*by[j]*by[k]) * by[j]*by[k]     * T(1, 1,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/32) * a3[j]*a3[k] * (  72 * np.sqrt(bx[j]*bx[k]*by[j]*by[k]) * by[j]*bx[k]     * T(1, 1,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/32) * a3[j]*a3[k] * (  54 * np.sqrt(bx[j]*bx[k]*by[j]*by[k]) * by[j]*by[k]     * T(1,-1,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/32) * a3[j]*a3[k] * (  72 * np.sqrt(bx[j]*bx[k]*by[j]*by[k]) * by[j]*bx[k]     * T(1,-1,dmux,dmuy,qx,qy) )

    return dvy / (2*np.pi)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def D2QyDjxDjy_octo(Tw,qx,qy):
    dvy = 0
    bx = Tw.BETX
    by = Tw.BETY
    mx = Tw.MUX
    my = Tw.MUY
    b3 = Tw.K3L

    for j in range(len(bx)):
        for k in range(len(bx)):
            dmux = mx[j] - mx[k]
            dmuy = my[j] - my[k]

            dvy = dvy + (1.0/32) * b3[j]*b3[k] * (  -9 * bx[j]*bx[k]*by[j]*by[k] * 2 * T(2, 2,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/32) * b3[j]*b3[k] * (  -9 * bx[j]*bx[k]*by[j]*by[k] * 2 * T(2,-2,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/32) * b3[j]*b3[k] * (  72 * bx[j]*by[k]*by[j]*by[k]     * T(0, 2,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/32) * b3[j]*b3[k] * ( -72 * bx[j]*bx[k]*bx[k]*by[j]     * T(2, 0,dmux,dmuy,qx,qy) )

    return dvy / (2*np.pi)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
#def D2QyDjxDjy(filename):
#    [QX, QY, quad, sext, octo, skew_quad, skew_sext, skew_octo] = read_twiss(filename)
#    return D2QyDjxDjy_skew_octo(skew_octo,QX,QY) + D2QyDjxDjy_octo(octo,QX,QY)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def DQx_skew_quad(Tw,qx,qy,jx,jy):
    dvx = 0
    bx = Tw.BETX
    by = Tw.BETY
    mx = Tw.MUX
    my = Tw.MUY
    a1 = Tw.K1SL

    for j in range(len(bx)):
        for k in range(len(bx)):
            dmux = mx[j] - mx[k]
            dmuy = my[j] - my[k]

            dvx = dvx + (1.0/8) * a1[j]*a1[k] * np.sqrt(bx[j]*bx[k]*by[j]*by[k]) * (T(1,-1,dmux,dmuy,qx,qy) - T(1,1,dmux,dmuy,qx,qy))

    return dvx / (2*np.pi)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def DQx_skew_sext(Tw,qx,qy,jx,jy):
    dvx = 0
    bx = Tw.BETX
    by = Tw.BETY
    mx = Tw.MUX
    my = Tw.MUY
    a2 = Tw.K2SL

    for j in range(len(bx)):
        for k in range(len(bx)):
            dmux = mx[j] - mx[k]
            dmuy = my[j] - my[k]

            dvx = dvx + (1.0/8) * a2[j]*a2[k] * ( -4 * bx[j]*bx[k] * np.sqrt(by[j]*by[k]) *  jx         * T(0,1,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/8) * a2[j]*a2[k] * (  4 * bx[j]*by[k] * np.sqrt(by[j]*by[k]) *         jy  * T(0,1,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/8) * a2[j]*a2[k] * (    - bx[j]*bx[k] * np.sqrt(by[j]*by[k]) * (jx + 2*jy) * T(2,1,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/8) * a2[j]*a2[k] * (      bx[j]*bx[k] * np.sqrt(by[j]*by[k]) * (jx - 2*jy) * T(2,-1,dmux,dmuy,qx,qy) )
 
    return dvx / (2*np.pi)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def DQx_sext(Tw,qx,qy,jx,jy):

    dvx = 0

    bx = Tw.BETX
    by = Tw.BETY

    mx = Tw.MUX
    my = Tw.MUY


    b2 = Tw.K2L

    for j in range(len(bx)):
        for k in range(len(bx)):
            dmux = mx[j] - mx[k]
            dmuy = my[j] - my[k]

            dvx = dvx + (1.0/8) * b2[j] * b2[k] * (  -3 * bx[j] * bx[k] * np.sqrt(bx[j] * bx[k])                      * jx                                    * T(1,0,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/8) * b2[j] * b2[k] * (   4 * np.sqrt(bx[j] * bx[k]) * bx[k] * by[j]                      * jy                                    * T(1,0,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/8) * b2[j] * b2[k] * (     - bx[j] * bx[k] * np.sqrt(bx[j] * bx[k])                      * jx                                    * T(3,0,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/8) * b2[j] * b2[k] * (   2 * by[j] * by[k] * np.sqrt(bx[j] * bx[k])                      * jy                                    * (T(1,-2,dmux,dmuy,qx,qy) - T(1,2,dmux,dmuy,qx,qy)) )

    return dvx / (2*np.pi)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def DQx_skew_octo(Tw,qx,qy,jx,jy):

    dvx = 0

    bx = Tw.BETX
    by = Tw.BETY

    mx = Tw.MUX
    my = Tw.MUY

    a3 = Tw.K3SL

    for j in range(len(bx)):
        for k in range(len(bx)):
            dmux = mx[j] - mx[k]
            dmuy = my[j] - my[k]

            dvx = dvx + (1.0/32) * a3[j] * a3[k] * (  9 * np.sqrt(bx[j] * bx[k] * by[j] * by[k]) * by[j] * by[k]      * jy * jy                               * (T(1,-3,dmux,dmuy,qx,qy) - T(1,3,dmux,dmuy,qx,qy))  )
            dvx = dvx + (1.0/32) * a3[j] * a3[k] * ( -3 * np.sqrt(bx[j] * bx[k] * by[j] * by[k]) * bx[j] * bx[k]      * (jx * jx + 6 * jx * jy)               *  T(3, 1,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/32) * a3[j] * a3[k] * ( +3 * np.sqrt(bx[j] * bx[k] * by[j] * by[k]) * bx[j] * bx[k]      * (jx * jx - 6 * jx * jy)               *  T(3,-1,dmux,dmuy,qx,qy) )

            dvx = dvx + (1.0/32) * a3[j] * a3[k] * (-27 * np.sqrt(bx[j] * bx[k] * by[j] * by[k]) * by[j] * by[k]      * jy * jy                               *  T(1,1,dmux,dmuy,qx,qy))
            dvx = dvx + (1.0/32) * a3[j] * a3[k] * ( 36 * np.sqrt(bx[j] * bx[k] * by[j] * by[k]) * bx[k] * by[j]      * jy * jy                               *  T(1,1,dmux,dmuy,qx,qy))
            dvx = dvx + (1.0/32) * a3[j] * a3[k] * ( 72 * np.sqrt(bx[j] * bx[k] * by[j] * by[k]) * bx[k] * by[j]      * jx * jy                               *  T(1,1,dmux,dmuy,qx,qy))
            dvx = dvx + (1.0/32) * a3[j] * a3[k] * (-54 * np.sqrt(bx[j] * bx[k] * by[j] * by[k]) * bx[j] * bx[k]      * jx * jy                               *  T(1,1,dmux,dmuy,qx,qy))
            dvx = dvx + (1.0/32) * a3[j] * a3[k] * (-27 * np.sqrt(bx[j] * bx[k] * by[j] * by[k]) * bx[j] * bx[k]      * jx * jx                               *  T(1,1,dmux,dmuy,qx,qy))

            dvx = dvx + (1.0/32) * a3[j] * a3[k] * ( 27 * np.sqrt(bx[j] * bx[k] * by[j] * by[k]) * by[j] * by[k]      * jy * jy                               *  T(1,-1,dmux,dmuy,qx,qy))
            dvx = dvx + (1.0/32) * a3[j] * a3[k] * ( 36 * np.sqrt(bx[j] * bx[k] * by[j] * by[k]) * bx[k] * by[j]      * jy * jy                               *  T(1,-1,dmux,dmuy,qx,qy))
            dvx = dvx + (1.0/32) * a3[j] * a3[k] * (-72 * np.sqrt(bx[j] * bx[k] * by[j] * by[k]) * bx[k] * by[j]      * jx * jy                               *  T(1,-1,dmux,dmuy,qx,qy))
            dvx = dvx + (1.0/32) * a3[j] * a3[k] * (-54 * np.sqrt(bx[j] * bx[k] * by[j] * by[k]) * bx[j] * bx[k]      * jx * jy                               *  T(1,-1,dmux,dmuy,qx,qy))
            dvx = dvx + (1.0/32) * a3[j] * a3[k] * ( 27 * np.sqrt(bx[j] * bx[k] * by[j] * by[k]) * bx[j] * bx[k]      * jx * jx                               *  T(1,-1,dmux,dmuy,qx,qy))

    return dvx / (2*np.pi)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def DQx_octo(Tw,qx,qy,jx,jy):

    dvx = 0

    bx = Tw.BETX
    by = Tw.BETY

    mx = Tw.MUX
    my = Tw.MUY

    b3 = Tw.K3L

    for j in range(len(bx)):
        for k in range(len(bx)):
            dmux = mx[j] - mx[k]
            dmuy = my[j] - my[k]

            dvx = dvx + (1.0/32) * b3[j] * b3[k] * ( -9 * bx[j] * bx[k] * by[j] * by[k]                               * (2 * jx * jy + jy * jy)               *  T(2,2,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/32) * b3[j] * b3[k] * (  9 * bx[j] * bx[k] * by[j] * by[k]                               * (2 * jx * jy - jy * jy)               *  T(2,-2,dmux,dmuy,qx,qy))
            dvx = dvx + (1.0/32) * b3[j] * b3[k] * ( -3 * bx[j] * bx[j] * bx[k] * bx[k]                               * jx * jx                               *  T(4,0,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/32) * b3[j] * b3[k] * ( 36 * bx[j] * by[j] * by[k] * by[k]                               * jy * jy                               *  T(0,2,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/32) * b3[j] * b3[k] * (-36 * bx[j] * bx[k] * by[j] * by[k]                               * jy * jy                               *  T(2,0,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/32) * b3[j] * b3[k] * ( 72 * bx[j] * bx[k] * bx[k] * by[j]                               * jx * jy                               *  T(2,0,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/32) * b3[j] * b3[k] * (-72 * bx[j] * bx[k] * by[j] * by[k]                               * jx * jy                               *  T(0,2,dmux,dmuy,qx,qy) )
            dvx = dvx + (1.0/32) * b3[j] * b3[k] * (-24 * bx[j] * bx[j] * bx[k] * bx[k]                               * jx * jx                               *  T(2,0,dmux,dmuy,qx,qy) )

    return dvx / (2*np.pi)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def DQx(filename,jx,jy):
    [QX, QY, quad, sext, octo, skew_quad, skew_sext, skew_octo] = read_twiss(filename)

    return DQx_skew_quad(skew_quad,QX,QY,jx,jy) + DQx_skew_sext(skew_sext,QX,QY,jx,jy) + DQx_sext(sext,QX,QY,jx,jy) + DQx_skew_octo(skew_octo,QX,QY,jx,jy) + DQx_octo(octo,QX,QY,jx,jy)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def DQy_skew_quad(Tw,qx,qy,jx,jy):

    dvy = 0

    bx = Tw.BETX
    by = Tw.BETY

    mx = Tw.MUX
    my = Tw.MUY

    a1 = Tw.K1SL

    for j in range(len(bx)):
        for k in range(len(bx)):
            dmux = mx[j] - mx[k]
            dmuy = my[j] - my[k]

            dvy = dvy + (1.0/8) * a1[j] * a1[k] *          np.sqrt(bx[j] * bx[k] * by[j] * by[k])                                                             * (T(1,-1,dmux,dmuy,qx,qy) + T(1,1,dmux,dmuy,qx,qy))

    return dvy / (2*np.pi)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def DQy_skew_sext(Tw,qx,qy,jx,jy):

    dvy = 0

    bx = Tw.BETX
    by = Tw.BETY

    mx = Tw.MUX
    my = Tw.MUY

    a2 = Tw.K2SL

    for j in range(len(bx)):
        for k in range(len(bx)):
            dmux = mx[j] - mx[k]
            dmuy = my[j] - my[k]


            dvy = dvy + (1.0/8) * a2[j] * a2[k] * (   4 * bx[j] * by[k] * np.sqrt(by[j] * by[k])                      * jx                                    *  T(0,1,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/8) * a2[j] * a2[k] * (  -3 * by[j] * by[k] * np.sqrt(by[j] * by[k])                      * jy                                    *  T(0,1,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/8) * a2[j] * a2[k] * (   2 * bx[j] * bx[k] * np.sqrt(by[j] * by[k])                      * jx                                    * (T(2,-1,dmux,dmuy,qx,qy) - T(2,1,dmux,dmuy,qx,qy)) )
            dvy = dvy + (1.0/8) * a2[j] * a2[k] * (       by[j] * by[k] * np.sqrt(by[j] * by[k])                      * jy                                    *  T(0,3,dmux,dmuy,qx,qy) )
    
    return dvy / (2*np.pi)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def DQy_sext(Tw,qx,qy,jx,jy):

    dvy = 0

    bx = Tw.BETX
    by = Tw.BETY

    mx = Tw.MUX
    my = Tw.MUY

    b2 = Tw.K2L

    for j in range(len(bx)):
        for k in range(len(bx)):
            dmux = mx[j] - mx[k]
            dmuy = my[j] - my[k]

            dvy = dvy + (1.0/8) * b2[j] * b2[k] * (   4 * bx[k] * by[j] * np.sqrt(bx[j] * bx[k])                      * jx                                    *  T(1,0,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/8) * b2[j] * b2[k] * (  -4 * np.sqrt(bx[j] * bx[k]) * by[k] * by[j]                      * jy                                    *  T(1,0,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/8) * b2[j] * b2[k] * (     - by[j] * by[k] * np.sqrt(bx[j] * bx[k])                      * (2*jx + jy)                           *  T(1,2,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/8) * b2[j] * b2[k] * (     - by[j] * by[k] * np.sqrt(bx[j] * bx[k])                      * (2*jx - jy)                           *  T(1,-2,dmux,dmuy,qx,qy))

    return dvy / (2*np.pi)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def DQy_skew_octo(Tw,qx,qy,jx,jy):

    dvy = 0

    bx = Tw.BETX
    by = Tw.BETY

    mx = Tw.MUX
    my = Tw.MUY

    a3 = Tw.K3SL

    for j in range(len(bx)):
        for k in range(len(bx)):
            dmux = mx[j] - mx[k]
            dmuy = my[j] - my[k]

            dvy = dvy + (1.0/32) * a3[j] * a3[k] * ( -9 * np.sqrt(bx[j] * bx[k] * by[j] * by[k]) * bx[j] * bx[k]      * jx * jx                               * (T(3,-1,dmux,dmuy,qx,qy) + T(3,1,dmux,dmuy,qx,qy)) )
            dvy = dvy + (1.0/32) * a3[j] * a3[k] * (  3 * np.sqrt(bx[j] * bx[k] * by[j] * by[k]) * by[j] * by[k]      * (6 * jx * jy + jy * jy)               *  T(1,-3,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/32) * a3[j] * a3[k] * ( -3 * np.sqrt(bx[j] * bx[k] * by[j] * by[k]) * by[j] * by[k]      * (6 * jx * jy - jy * jy)               *  T(1,3,dmux,dmuy,qx,qy) )

            dvy = dvy + (1.0/32) * a3[j] * a3[k] * (-27 * np.sqrt(bx[j] * bx[k] * by[j] * by[k]) * by[j] * by[k]      * jy * jy                               *  T(1,1,dmux,dmuy,qx,qy))
            dvy = dvy + (1.0/32) * a3[j] * a3[k] * (-54 * np.sqrt(bx[j] * bx[k] * by[j] * by[k]) * by[k] * by[j]      * jx * jy                               *  T(1,1,dmux,dmuy,qx,qy))
            dvy = dvy + (1.0/32) * a3[j] * a3[k] * ( 72 * np.sqrt(bx[j] * bx[k] * by[j] * by[k]) * bx[k] * by[j]      * jx * jy                               *  T(1,1,dmux,dmuy,qx,qy))
            dvy = dvy + (1.0/32) * a3[j] * a3[k] * (-27 * np.sqrt(bx[j] * bx[k] * by[j] * by[k]) * bx[j] * bx[k]      * jx * jx                               *  T(1,1,dmux,dmuy,qx,qy))
            dvy = dvy + (1.0/32) * a3[j] * a3[k] * ( 36 * np.sqrt(bx[j] * bx[k] * by[j] * by[k]) * by[j] * bx[k]      * jx * jx                               *  T(1,1,dmux,dmuy,qx,qy))

            dvy = dvy + (1.0/32) * a3[j] * a3[k] * (-27 * np.sqrt(bx[j] * bx[k] * by[j] * by[k]) * by[j] * by[k]      * jy * jy                               *  T(1,-1,dmux,dmuy,qx,qy))
            dvy = dvy + (1.0/32) * a3[j] * a3[k] * ( 54 * np.sqrt(bx[j] * bx[k] * by[j] * by[k]) * by[k] * by[j]      * jx * jy                               *  T(1,-1,dmux,dmuy,qx,qy))
            dvy = dvy + (1.0/32) * a3[j] * a3[k] * ( 72 * np.sqrt(bx[j] * bx[k] * by[j] * by[k]) * bx[k] * by[j]      * jx * jy                               *  T(1,-1,dmux,dmuy,qx,qy))
            dvy = dvy + (1.0/32) * a3[j] * a3[k] * (-27 * np.sqrt(bx[j] * bx[k] * by[j] * by[k]) * bx[j] * bx[k]      * jx * jx                               *  T(1,-1,dmux,dmuy,qx,qy))
            dvy = dvy + (1.0/32) * a3[j] * a3[k] * (-36 * np.sqrt(bx[j] * bx[k] * by[j] * by[k]) * bx[k] * by[j]      * jx * jx                               *  T(1,-1,dmux,dmuy,qx,qy))

    return dvy / (2*np.pi)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def DQy_octo(Tw,qx,qy,jx,jy):

    dvy = 0

    bx = Tw.BETX
    by = Tw.BETY

    b3 = Tw.K3L

    for j in range(len(bx)):
        for k in range(len(bx)):
            dmux = mx[j] - mx[k]
            dmuy = my[j] - my[k]

            dvy = dvy + (1.0/32) * b3[j] * b3[k] * ( -9 * bx[j] * bx[k] * by[j] * by[k]                               * (2 * jx * jy + jx * jx)               *  T(2,2,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/32) * b3[j] * b3[k] * ( -9 * bx[j] * bx[k] * by[j] * by[k]                               * (2 * jx * jy - jx * jx)               *  T(2,-2,dmux,dmuy,qx,qy))
            dvy = dvy + (1.0/32) * b3[j] * b3[k] * ( -3 * bx[j] * bx[j] * bx[k] * bx[k]                               * jy * jy                               *  T(0,4,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/32) * b3[j] * b3[k] * (-24 * by[j] * by[j] * by[k] * by[k]                               * jy * jy                               *  T(0,2,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/32) * b3[j] * b3[k] * ( 72 * bx[j] * by[k] * by[j] * by[k]                               * jx * jy                               *  T(0,2,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/32) * b3[j] * b3[k] * (-72 * bx[j] * bx[k] * bx[k] * by[j]                               * jx * jy                               *  T(2,0,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/32) * b3[j] * b3[k] * ( 36 * bx[j] * bx[k] * by[j] * bx[k]                               * jx * jx                               *  T(2,0,dmux,dmuy,qx,qy) )
            dvy = dvy + (1.0/32) * b3[j] * b3[k] * (-36 * bx[j] * by[j] * bx[k] * by[k]                               * jx * jx                               *  T(0,2,dmux,dmuy,qx,qy) )

    return dvy / (2*np.pi)
#_______________________________________________________________________________________





#=======================================================================================
#   
#---------------------------------------------------------------------------------------
def DQy(filename,jx,jy):
    [QX, QY, quad, sext, octo, skew_quad, skew_sext, skew_octo] = read_twiss(filename)

    return DQy_skew_quad(skew_quad,QX,QY,jx,jy) + DQy_skew_sext(skew_sext,QX,QY,jx,jy) + DQy_sext(sext,QX,QY,jx,jy) + DQy_skew_octo(skew_octo,QX,QY,jx,jy) + DQy_octo(octo,QX,QY,jx,jy)
#_______________________________________________________________________________________


