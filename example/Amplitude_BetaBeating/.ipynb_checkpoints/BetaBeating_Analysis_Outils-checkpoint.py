import numpy  as np
import pandas as pd
import pathlib
import matplotlib.pyplot as plt
#import PyNAFF as pnf
#import math   as mt
import re

# Import the class
import sys

sys.path.insert(0, '/local/home/tpugnat/Documents/CEA/SixTrack/These_Workplace/CERN/Python/bpm_analysis/Model_Analysis/')
from class_Model    import cModel




# =================================================================== #
#   Function to extract Datas from BetaBeat output:
# =================================================================== #
def get_data(path,data_name="BETX"):
    getkick_dat      =["DPP","QX","QXRMS","QY","QYRMS","NATQX","NATQXRMS","NATQY","NATQYRMS","sqrt2JX","sqrt2JXSTD","sqrt2JY","sqrt2JYSTD","2JX","2JXSTD","2JY","2JYSTD"]
    getbetx_dat      =["NAME","S","COUNT","BETX","SYSBETX","STATBETX","ERRBETX","CORR_ALFABETA","ALFX","SYSALFX","STATALFX","ERRALFX","BETXMDL","ALFXMDL","MUXMDL","NCOMBINATIONS"]
    getbety_dat      =["NAME","S","COUNT","BETY","SYSBETY","STATBETY","ERRBETY","CORR_ALFABETA","ALFY","SYSALFY","STATALFY","ERRALFY","BETYMDL","ALFYMDL","MUYMDL","NCOMBINATIONS"]
    getampbetx_dat   =["AMPBETX","AMPBETXSTD","AMPBETXRES","AMPBETXSTDRES"]
    getampbety_dat   =["AMPBETY","AMPBETYSTD","AMPBETYRES","AMPBETYSTDRES"]
    getphasex_dat    =["PHASEX","STDPHX","PHXMDL","MUXMDL"]
    getphasey_dat    =["PHASEY","STDPHY","PHYMDL","MUYMDL"]
    getphasetotx_dat =["TOTPHASEX","TOTSTDPHX","TOTPHXMDL","TOTMUXMDL"]
    getphasetoty_dat =["TOTPHASEY","TOTSTDPHY","TOTPHYMDL","TOTMUYMDL"]
    
    # get file type
    path_type="*_NORMALANALYSIS_SUSSIX_*/"
    if np.isin(data_name,getkick_dat):
        file="/getkick.out"
    elif np.isin(data_name,getbetx_dat):
        file="/getbetax.out"
    elif np.isin(data_name,getbety_dat):
        file="/getbetay.out"
    elif np.isin(data_name,getphasex_dat):
        file="/getphasex.out"
    elif np.isin(data_name,getphasey_dat):
        file="/getphasey.out"
    elif np.isin(data_name,getampbetx_dat):
        file="/getampbetax.out"
        data_name=re.sub("AMP","",data_name)
    elif np.isin(data_name,getampbety_dat):
        file="/getampbetay.out"
        data_name=re.sub("AMP","",data_name)
    elif np.isin(data_name,getphasetotx_dat):
        file="/getphasetotx.out"
        data_name=re.sub("TOT","",data_name)
    elif np.isin(data_name,getphasetoty_dat):
        file="/getphasetoty.out"
        data_name=re.sub("TOT","",data_name)
    else:
        mess="ERROR [GET_DATA]: Wrong value for data_name ("+data_name+")!"
        raise(KeyError(mess))
        
    # define the path
    currentDirectory = pathlib.Path(path);
    # get file in directory
    paths=[];
    for currentFile in sorted(currentDirectory.glob(path_type)):
        paths.append(currentFile)
    
    if len(paths)==0:
        raise(KeyError("ERROR [GET_DATA]: Wrong path ("+path+"), couldn't find "+path_type+file+"!"))
    
    datas=[]
    it=0
    for currentFile in paths:
        #    - get the head from the file without the #
        skip=0
        with open(str(currentFile)+file) as f:
            for l in f:
                skip=skip+1
                split_l=l.split()
                if split_l[0]=="*":
                    split_l.pop(0);
                    head_list=split_l
                    break
        f.close()
        d=pd.read_csv(str(currentFile)+file,skiprows=skip+1,sep='\s+', names=head_list);
        if np.isin("NAME",d.columns.tolist()):
            d=d.set_index('NAME');
        #data=pd.DataFrame([])
        #data[str(it)]=d[data_name];
        data=pd.DataFrame(d[data_name].values,columns=[str(it)],index=d.index.tolist());
        it=it+1
        #print(data.head(5))
        datas.append(data)
    
    return pd.concat(datas, axis=1)
# =================================================================== #




# =================================================================== #
#   Function to get the beta-beating (in percent)
# =================================================================== #
def get_betabeating(J2="2JX", axis="X",path="/local/home/tpugnat/Documents/CEA/SixTrack/These_Workplace/SixTrack/bpms_optics/noerrors/Results/"):
    # Set bad BPMs
    bad_BPMs=["BPM.8L5.B1","BPM.6L5.B1","BPM.33R1.B1","BPM.34R1.B1","BPM.33L2.B1","BPM.32L2.B1","BPM.7L2.B1","BPMR.6L2.B1","BPMYB.5L2.B1","BPMYB.4L2.B1","BPMR.7L5.B1"]
    
    # Get Data
    dat_mes=get_data(path,data_name="BET"+axis      );
    dat_mdl=get_data(path,data_name="BET"+axis+"MDL");
    dat_2J =get_data(path,data_name=J2              );
    dat_mes=dat_mes.rename(columns=dat_2J.loc[0,:])
    dat_mdl=dat_mdl.rename(columns=dat_2J.loc[0,:])

    # Filter bad-BPMs
    ls=dat_mes.index.tolist()
    mask= [l for l in ls if ~np.isin(l,bad_BPMs)]
    dat_mes=dat_mes.loc[mask,:]
    dat_mdl=dat_mdl.loc[mask,:]

    # Get beta-beating
    dat_rel=(dat_mes/dat_mdl-1.)*100
    return dat_rel
# =================================================================== #




# =================================================================== #
#   Function to get the variation of the phase
# =================================================================== #
def get_ampbetabeating(J2="2JX", axis="X",path="/local/home/tpugnat/Documents/CEA/SixTrack/These_Workplace/SixTrack/bpms_optics/noerrors/Results/"):
    # Set bad BPMs
    bad_BPMs=["BPM.8L5.B1","BPM.6L5.B1","BPM.33R1.B1","BPM.34R1.B1","BPM.33L2.B1","BPM.32L2.B1","BPM.7L2.B1","BPMR.6L2.B1","BPMYB.5L2.B1","BPMYB.4L2.B1","BPMR.7L5.B1"]
    
    # Get Data
    dat_mes=get_data(path,data_name="AMPBET"+axis   );
    dat_mdl=get_data(path,data_name="BET"+axis+"MDL");
    dat_2J =get_data(path,data_name=J2              );
    dat_mes=dat_mes.rename(columns=dat_2J.loc[0,:])
    dat_mdl=dat_mdl.rename(columns=dat_2J.loc[0,:])

    # Filter bad-BPMs
    ls=dat_mes.index.tolist()
    mask= [l for l in ls if ~np.isin(l,bad_BPMs)]
    dat_mes=dat_mes.loc[mask,:]
    dat_mdl=dat_mdl.loc[mask,:]

    # Get beta-beating
    dat_rel=(dat_mes/dat_mdl-1.)*100
    return dat_rel
# =================================================================== #




# =================================================================== #
#   Function to get the variation of the tune
# =================================================================== #
def get_diffphase(J2="2JX", axis="X",path="/local/home/tpugnat/Documents/CEA/SixTrack/These_Workplace/SixTrack/bpms_optics/noerrors/Results/"):
    # Set bad BPMs
    bad_BPMs=["BPM.8L5.B1","BPM.6L5.B1","BPM.33R1.B1","BPM.34R1.B1","BPM.33L2.B1","BPM.32L2.B1","BPM.7L2.B1","BPMR.6L2.B1","BPMYB.5L2.B1","BPMYB.4L2.B1","BPMR.7L5.B1"]
    
    # Get Data
    dat_mes=get_data(path,data_name="PHASE"+axis   );
    dat_mdl=get_data(path,data_name="PH"+axis+"MDL");
    dat_2J =get_data(path,data_name=J2             );
    dat_mes=dat_mes.rename(columns=dat_2J.loc[0,:])
    dat_mdl=dat_mdl.rename(columns=dat_2J.loc[0,:])

    # Filter bad-BPMs
    ls=dat_mes.index.tolist()
    mask= [l for l in ls if ~np.isin(l,bad_BPMs)]
    dat_mes=dat_mes.loc[mask,:]
    dat_mdl=dat_mdl.loc[mask,:]

    # Get beta-beating
    dat_dif=dat_mes-dat_mdl
    return dat_dif
# =================================================================== #




# =================================================================== #
#   
# =================================================================== #
def get_values(J2="2JX", values="QX",path="/local/home/tpugnat/Documents/CEA/SixTrack/These_Workplace/SixTrack/bpms_optics/noerrors/Results/"):
    # Get Data
    dat_mes   =get_data(path,data_name=values);
    dat_2J    =get_data(path,data_name=J2      );
    dat_mes   =dat_mes.rename(columns=dat_2J.loc[0,:])
    return dat_mes
# =================================================================== #




# =================================================================== #
#   
# =================================================================== #
def Get_MD_Data(name,axe,Path_Data,Path_Modl):
    
    S              =get_values(        J2="2J"+axe, values="S" ,path=Path_Data)
    S=S.loc[:,min(S.columns.tolist())]
    
    detuningX_2J   =get_values(        J2="2J"+axe, values="NATQX",path=Path_Data)
    detuningY_2J   =get_values(        J2="2J"+axe, values="NATQY",path=Path_Data)
    BetaBeatX_2J   =get_betabeating(   J2="2J"+axe, axis="X",   path=Path_Data)
    BetaBeatY_2J   =get_betabeating(   J2="2J"+axe, axis="Y",   path=Path_Data)
    AmpBetaBeatX_2J=get_ampbetabeating(J2="2J"+axe, axis="X",   path=Path_Data)
    AmpBetaBeatY_2J=get_ampbetabeating(J2="2J"+axe, axis="Y",   path=Path_Data)

    struct=cModel(case_name=name,case_col="b",path_opt=Path_Modl,path_err=Path_Modl)
    struct.load_File(name_optic="twiss_elements.dat",name_error="nothing.dat",flg_MARKER=True)
    struct.load_FFFile()
    struct.optic_dat=struct.optic_dat.set_index(struct.optic_dat.loc[:,"NAME"]);

    MD={"NAME":name,
        "AXE":axe,
        "QX(2J"+axe+")":detuningX_2J,
        "QY(2J"+axe+")":detuningY_2J,
        "BETX(2J"+axe+")":BetaBeatX_2J,
        "BETY(2J"+axe+")":BetaBeatY_2J,
        "ABETX(2J"+axe+")":AmpBetaBeatX_2J,
        "ABETY(2J"+axe+")":AmpBetaBeatY_2J,
        "S":S,
        "struct":struct
       }
    return MD
# =================================================================== #




# =================================================================== #
#   Function to plot Structure
# =================================================================== #
def plot_struct_vs_S(ax,struct,ylim=[-1,1],ls='-.',lw=1,col='k',alpha=.5):
    IP=struct.optic_dat[struct.optic_dat['NAME'].str.contains("IP")].drop_duplicates(subset=['S'],keep='last')
    for i in IP.index.tolist():
        ax.axvline(x=IP.loc[i,"S"],ymin=ylim[0],ymax=ylim[1],ls=ls,lw=lw,c=col,alpha=alpha)
        ax.text(IP.loc[i,"S"], ylim[1],IP.loc[i,"NAME"], rotation='vertical', va="top",
        bbox={'facecolor':'w', 'alpha':0.5, 'pad':1})
# =================================================================== #




# =================================================================== #
#   Function to plot Data
# =================================================================== #
def plot_data_vs_2J(ax,data,label="",ls='-',alpha=1):
    c=data.columns.tolist()
    c.sort()
    d=[data[i] for i in c]
    ax.plot(c,d,ls,label=label,alpha=alpha)
# =================================================================== #




# =================================================================== #
#   
# =================================================================== #
def plot_data_vs_Sand2J(ax,data,S,struct,ylim=[-1,1],ls='-',alpha=1,cm=[],vc=[0,0.025],n2J=5,mmin2J=1):
    if ~struct.optic_dat.empty:
        plot_struct_vs_S(ax,struct,ylim=ylim)
        
    list_bpm=[x for x in S.index.tolist() if x in data.index.tolist() ]
    if cm is list:
        for i in data.columns.tolist():
            d=data.loc[list_bpm,i]
            sc=ax.plot(S[list_bpm],d,ls,label=i,alpha=alpha)
    else:
        v2J=data.columns.tolist()
        v2J.sort()

        if (n2J==0) or (n2J>len(v2J)):
            n2J=len(v2J)
        tp=np.linspace(0,len(v2J)-1,n2J)
        i2J=[ int(c) for c in tp]
        if mmin2J==2:
            d0=np.array(data.loc[list_bpm,v2J[-1]].values)
            for i in i2J:
                ii=v2J[i]
                d=np.array(data.loc[list_bpm,ii].values)
                #sc=ax.scatter(x=S[data.index.tolist()], y=d-d0, c=ii*np.ones(d.shape), cmap=cm, vmin=vc[0], vmax=vc[1], alpha=alpha)
                sc=ax.scatter(x=S[list_bpm], y=d-d0, c=ii*np.ones(d.shape), cmap=cm, vmin=0, vmax=v2J[-1], alpha=alpha)
        elif mmin2J==1:
            d0=np.array(data.loc[list_bpm,v2J[0]].values)
            for i in i2J:
                ii=v2J[i]
                d=np.array(data.loc[list_bpm,ii].values)
                #sc=ax.scatter(x=S[data.index.tolist()], y=d-d0, c=ii*np.ones(d.shape), cmap=cm, vmin=vc[0], vmax=vc[1], alpha=alpha)
                sc=ax.scatter(x=S[list_bpm], y=d-d0, c=ii*np.ones(d.shape), cmap=cm, vmin=0, vmax=v2J[-1], alpha=alpha)
        else:
            for i in i2J:
                ii=v2J[i]
                d=np.array(data.loc[list_bpm,ii].values)
                #sc=ax.scatter(x=S[data.index.tolist()], y=d   , c=ii*np.ones(d.shape), cmap=cm, vmin=vc[0], vmax=vc[1], alpha=alpha)
                sc=ax.scatter(x=S[list_bpm], y=d   , c=ii*np.ones(d.shape), cmap=cm, vmin=0, vmax=v2J[-1], alpha=alpha)
    return sc
# =================================================================== #




# =================================================================== #
#   Function to plot Stat Data
# =================================================================== #
def plot_hist_oneax(datax,nb_pgroup=5,ttl_x=r"$\beta_x$-beating [%]",xlim_x=[-10,10]):
    col=datax.columns.tolist()
    nb_group =int(len(col)/nb_pgroup)


    fig, axes=plt.subplots(nrows=nb_group, ncols=1,figsize=(28, 14))
    for i in range(nb_group):
        short_2J=col[i*nb_pgroup:min(len(col),(i+1)*nb_pgroup)]

        datax.loc[:,short_2J].plot(kind='hist',xlim=[-6,6],alpha=0.5, bins=40,legend=True, ax=axes[i,0])
        axes[i,0].set_ylabel(r"Count")
        if i==nb_group-1:
            axes[i,0].set_xlabel(ttl_x)
    plt.show()
# =================================================================== #




# =================================================================== #
#   
# =================================================================== #
def plot_hist_twoax(datax,datay,nb_pgroup=5,ttl_x=r"$\beta_x$-beating [%]",ttl_y=r"$\beta_y$-beating [%]",xlim_x=[-10,10],xlim_y=[-10,10]):
    col=datax.columns.tolist()
    nb_group =int(len(col)/nb_pgroup)
    fig, axes=plt.subplots(nrows=nb_group, ncols=2,figsize=(28, 14))
    for i in range(nb_group):
        short_2J=col[i*nb_pgroup:min(len(col),(i+1)*nb_pgroup)]

        datax.loc[:,short_2J].plot(kind='hist',xlim=[-6,6],alpha=0.5, bins=40,legend=True, ax=axes[i,0])
        axes[i,0].set_ylabel(r"Count")
        if i==nb_group-1:
            axes[i,0].set_xlabel(ttl_x)

        datay.loc[:,short_2J].plot(kind='hist',xlim=[-10,10],alpha=0.5, bins=40,legend=True, ax=axes[i,1])
        axes[i,1].set_ylabel(r"")
        if i==nb_group-1:
            axes[i,1].set_xlabel(ttl_y)
    plt.show()
# =================================================================== #




# =================================================================== #
#   
# =================================================================== #
def plot_stat_vs_2J(ax,Data,alpha_D=.25,alpha_S=1,met=""):
    col=Data.columns.tolist()
    col.sort()
    #minbet=Data[]
    if met=="abs":
        tp=pd.DataFrame(columns=col)
        for c in col:
            tp[c]=Data[c]

        for t in tp.iterrows():
            tmp=pd.DataFrame(t[1])
            ax.plot(tmp.index.tolist(),abs(tmp[tmp.columns.tolist()[0]].values),':b',alpha=alpha_D)

        tp_mean=pd.DataFrame(tp.mean()).T
        tp_std =pd.DataFrame(tp.std()).T
        ax.plot(tp_std.columns.tolist(),abs(tp_mean.loc[0,:].values),                       '-m',label="mean",alpha=alpha_S)
        ax.plot(tp_std.columns.tolist(),abs(tp_mean.loc[0,:].values+tp_std.loc[0,:].values),'-r',label="RMS",alpha=alpha_S)
        
    elif met=="-0":
        tp=pd.DataFrame(columns=col)
        for c in col:
            tp[c]=Data[c]
        ref=min(col)
        for t in tp.iterrows():
            tmp=pd.DataFrame(t[1])
            if ref in tmp.index.tolist():
                ref_val=tmp.loc[ref,tmp.columns.tolist()[0]]
                ax.plot(tmp.index.tolist(),tmp[tmp.columns.tolist()[0]].values-ref_val,':b',alpha=alpha_D)

        tp_mean=pd.DataFrame((tp.sub(tp.loc[:,ref],axis='index')).mean()).T
        tp_std =pd.DataFrame((tp.sub(tp.loc[:,ref],axis='index')).std()).T
        ax.plot(tp_std.columns.tolist(),tp_mean.loc[0,:].values,                       '-m',label="mean",alpha=alpha_S)
        ax.plot(tp_std.columns.tolist(),tp_mean.loc[0,:].values+tp_std.loc[0,:].values,'-r',label="RMS",alpha=alpha_S)
        ax.plot(tp_std.columns.tolist(),tp_mean.loc[0,:].values-tp_std.loc[0,:].values,'-r',alpha=alpha_S)
        
    elif met=="-1":
        tp=pd.DataFrame(columns=col)
        for c in col:
            tp[c]=Data[c]
        ref=max(col)
        for t in tp.iterrows():
            tmp=pd.DataFrame(t[1])
            if ref in tmp.index.tolist():
                ref_val=tmp.loc[ref,tmp.columns.tolist()[0]]
                ax.plot(tmp.index.tolist(),tmp[tmp.columns.tolist()[0]].values-ref_val,':b',alpha=alpha_D)

        tp_mean=pd.DataFrame((tp.sub(tp.loc[:,ref],axis='index')).mean()).T
        tp_std =pd.DataFrame((tp.sub(tp.loc[:,ref],axis='index')).std()).T
        ax.plot(tp_std.columns.tolist(),tp_mean.loc[0,:].values,                       '-m',label="mean",alpha=alpha_S)
        ax.plot(tp_std.columns.tolist(),tp_mean.loc[0,:].values+tp_std.loc[0,:].values,'-r',label="RMS",alpha=alpha_S)
        ax.plot(tp_std.columns.tolist(),tp_mean.loc[0,:].values-tp_std.loc[0,:].values,'-r',alpha=alpha_S)
    else:
        tp=pd.DataFrame(columns=col)
        for c in col:
            tp[c]=Data[c]
        #tp.fillna(0)

        for t in tp.iterrows():
            tmp=pd.DataFrame(t[1])
            ax.plot(tmp.index.tolist(),tmp[tmp.columns.tolist()[0]].values,':b',alpha=alpha_D)

        tp_mean=pd.DataFrame(tp.mean()).T
        tp_std =pd.DataFrame(tp.std()).T
        ax.plot(tp_std.columns.tolist(),tp_mean.loc[0,:].values,                       '-', color='limegreen', label="mean",alpha=alpha_S, linewidth=3)
        ax.plot(tp_std.columns.tolist(),tp_mean.loc[0,:].values+tp_std.loc[0,:].values,'-', color='red',       label="RMS", alpha=alpha_S, linewidth=3)
        ax.plot(tp_std.columns.tolist(),tp_mean.loc[0,:].values-tp_std.loc[0,:].values,'-', color='red',                    alpha=alpha_S, linewidth=3)
# =================================================================== #




# =================================================================== #
#   Function to manipulate Dataframe
# =================================================================== #
IP1=["BPM.10L1.B1","BPM.9L1.B1","BPM.8L1.B1","BPMR.7L1.B1","BPM.6L1.B1","BPMYC.5L1.B1",
     "BPMYY.4L1.B1","BPMWD.4L1.B1","BPMWC.4L1.B1","BPMSQ.B2L1.B1","BPMSQ.A2L1.B1",
     "BPMSQ.1L1.B1","BPMSQW.1L1.B1","BPMSQW.1R1.B1","BPMSQ.1R1.B1","BPMSQ.A2R1.B1",
     "BPMSQ.B2R1.B1" ,"BPMWC.4R1.B1","BPMWD.4R1.B1","BPMYY.4R1.B1","BPMYC.5R1.B1",
     "BPMR.6R1.B1","BPM_A.7R1.B1","BPM.8R1.B1" ,"BPM.9R1.B1","BPM.10R1.B1"]
IP2=["BPM.10L2.B1","BPM.9L2.B1","BPM.8L2.B1","BPM.7L2.B1","BPMR.6L2.B1","BPMYB.5L2.B1",        
     "BPMYB.4L2.B1","BPMWI.4L2.B1","BPMSX.4L2.B1","BPMS.2L2.B1","BPMSW.1L2.B1","BPMSW.1R2.B1", 
     "BPMS.2R2.B1","BPMSX.4R2.B1","BPMWB.4R2.B1","BPMYB.4R2.B1","BPMR.5R2.B1","BPM.6R2.B1",    
     "BPM_A.7R2.B1","BPM.8R2.B1","BPM.9R2.B1","BPM.10R2.B1"]
IP3=["BPM.10L3.B1","BPM.9L3.B1","BPM.8L3.B1","BPM.7L3.B1","BPM.6L3.B1","BPMWG.A5L3.B1",        
     "BPMW.5L3.B1","BPMWE.4L3.B1","BPMW.4L3.B1","BPMW.4R3.B1","BPMWE.4R3.B1","BPMW.5R3.B1",    
     "BPMWJ.A5R3.B1","BPMWC.6R3.B1","BPMR.6R3.B1","BPM_A.7R3.B1","BPM.8R3.B1","BPM.9R3.B1",    
     "BPM.10R3.B1"]
IP4=["BPMCS.10L4.B1","BPM.10L4.B1","BPMCS.9L4.B1","BPM.9L4.B1","BPMCS.8L4.B1","BPM.8L4.B1",
     "BPMCS.7L4.B1","BPM.7L4.B1","BPMYB.6L4.B1","BPMYA.5L4.B1","BPMWI.A5L4.B1","BPMWA.B5L4.B1",
     "BPMWA.A5L4.B1","BPMWA.A5R4.B1","BPMWA.B5R4.B1","BPMYB.5R4.B1","BPMYA.6R4.B1",
     "BPMCS.7R4.B1","BPM.7R4.B1","BPMCS.8R4.B1","BPM.8R4.B1","BPMCS.9R4.B1","BPM.9R4.B1",
     "BPMCS.10R4.B1","BPM.10R4.B1"]
IP5=["BPM.10L5.B1","BPM.9L5.B1","BPM.8L5.B1","BPMR.7L5.B1","BPM.6L5.B1","BPMYC.5L5.B1",
     "BPMYY.4L5.B1","BPMWD.4L5.B1","BPMWC.4L5.B1","BPMSQ.B2L5.B1","BPMSQ.A2L5.B1",
     "BPMSQ.1L5.B1","BPMSQW.1L5.B1","BPMSQW.1R5.B1","BPMSQ.1R5.B1","BPMSQ.A2R5.B1",
     "BPMSQ.B2R5.B1","BPMWC.4R5.B1","BPMWD.4R5.B1","BPMYY.4R5.B1","BPMYC.5R5.B1",
     "BPMR.6R5.B1","BPM_A.7R5.B1","BPM.8R5.B1","BPM.9R5.B1","BPM.10R5.B1"]
IP6=["BPM.10L6.B1","BPM.9L6.B1","BPM.8L6.B1","BPMYA.5L6.B1","BPMYB.4L6.B1","BPMSX.B4L6.B1",
     "BPMSX.A4L6.B1","BPMSE.4L6.B1","BPMSA.4R6.B1","BPMSI.A4R6.B1","BPMSI.B4R6.B1",
     "BPMYA.4R6.B1","BPMYB.5R6.B1","BPM.8R6.B1","BPM.9R6.B1","BPM.10R6.B1"]
IP7=["BPM.10L7.B1","BPM.9L7.B1","BPM.8L7.B1","BPM.7L7.B1","BPM.6L7.B1","BPMWC.6L7.B1",
     "BPMWE.5L7.B1","BPMW.5L7.B1","BPMWE.4L7.B1","BPMW.4L7.B1","BPMW.4R7.B1","BPMWE.4R7.B1",
     "BPMW.5R7.B1","BPMWE.5R7.B1","BPMR.6R7.B1","BPM_A.7R7.B1","BPM.8R7.B1","BPM.9R7.B1",
     "BPM.10R7.B1"]
IP8=["BPM.10L8.B1","BPM.9L8.B1","BPM.8L8.B1","BPM.7L8.B1","BPMR.6L8.B1","BPM.5L8.B1",
     "BPMYB.4L8.B1","BPMWB.4L8.B1","BPMSX.4L8.B1","BPMS.2L8.B1","BPMSW.1L8.B1",
     "BPMSW.1R8.B1","BPMS.2R8.B1","BPMSX.4R8.B1","BPMWB.4R8.B1","BPMYB.4R8.B1","BPMYB.5R8.B1",
     "BPM.6R8.B1","BPM_A.7R8.B1","BPM.8R8.B1","BPM.9R8.B1","BPM.10R8.B1"]

AL1=[]
AL2=[]
AL3=[]
AL4=[]
AL5=[]
AL6=[]
AL7=[]
AL8=[]

AR1=[]
AR2=[]
AR3=[]
AR4=[]
AR5=[]
AR6=[]
AR7=[]
AR8=[]

def shorten_data(BPMS_list, data):
    BPMS_data=data.index.tolist()
    BPMs_both=[b for b in BPMS_list if np.isin(b,BPMS_data)]
    return data.loc[BPMs_both,:]
# =================================================================== #




# =================================================================== #
#   
# =================================================================== #
# =================================================================== #




# =================================================================== #
#   
# =================================================================== #
# =================================================================== #




# =================================================================== #
#   
# =================================================================== #
# =================================================================== #




# =================================================================== #
#   
# =================================================================== #
# =================================================================== #




# =================================================================== #
#   
# =================================================================== #
# =================================================================== #




# =================================================================== #
#   
# =================================================================== #
# =================================================================== #




# =================================================================== #
#   
# =================================================================== #
# =================================================================== #




# =================================================================== #
#   
# =================================================================== #
# =================================================================== #




# =================================================================== #
#   
# =================================================================== #
# =================================================================== #




# =================================================================== #
#   
# =================================================================== #
# =================================================================== #




# =================================================================== #
#   
# =================================================================== #
        