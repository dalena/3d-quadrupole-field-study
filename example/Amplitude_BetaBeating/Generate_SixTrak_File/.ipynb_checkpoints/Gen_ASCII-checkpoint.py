#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 12 13:16:14 2020

@author: tpugnat
"""

# Import library and usefull function
import os
import sys
import pathlib

import re
import time

import pandas as pd

# Special class
#sys.path.insert(0, '/local/home/tpugnat/Documents/CEA/SixTrack/These_Workplace/Combin/NAFF/python/')
sys.path.insert(0, '../../../detuning_analysis/detuning_analysis/')
from class_case_Detuning_Simulation import Case_Detuning_Simulated as cas_DT_Sm


# Define workplace and file type
workplace="./2018_inj/Tracking/"
file_tfs ="../../Data/LHC/2018_inj/model/twiss.dat"
file_type="bpm*_Data_Part.txt"


# Check workplace exist
if not os.path.exists(workplace):
    os.makedirs(workplace)


# Define case studied
rat_l =["0","999999"]
case_l=["noerror","AmpBetaBeating"]
nbpart=30
nbturn=1000


flg_gen=True
flg_srt=True

# Get order BPMs from tfs
if flg_gen:
  # Move all files in same folder
  for ca in case_l:
    for rat in rat_l:
      path=workplace+ca+"/Output_"+ca+"_Rat"+rat+"/"
      os.system("mkdir "+path)
      os.system("mv "+workplace+ca+"/Output_"+ca+"_Rat"+rat+"_BPM*/bpm*.txt "+path)
  
  # Generate ASCII file
  for ca in case_l:
    for rat in rat_l:
      path=workplace+ca+"/Output_"+ca+"_Rat"+rat+"/"
      
      
      # get file in directory
      files=[];
      currentDirectory = pathlib.Path(path);
      for currentFile in currentDirectory.glob(file_type):
        files.append(currentFile)
      
      bpms= [re.sub("_Data_Part.txt","",str(x)) for x in files]
      bpms= [re.sub(path,"",str(x)) for x in bpms]
      
      # Generate all the bpms
      c_bpms=[]
      for i in range(len(bpms)): #3): #
        file=re.sub(path,"",str(files[i]))
        bpm =re.sub(path,"",str(bpms[i] ))
        c=cas_DT_Sm(bpm,case_col="",path=path, file_type=file)
        c_bpms.append(c)
    

      # Generate thet File with the data
      nbHBPM=len(c_bpms)
      nbVBPM=len(c_bpms)
      for p in range(nbpart): #1): #
        with open(path+'BPMs_Part{0}.sdds'.format(p+1), 'w') as the_file:
            the_file.write('#SDDSDASCIIFORMAT v1 \n')
            the_file.write('#Created: '+time.strftime("%Y-%m-%d at %H:%M:%S")+' By: Thomas PUGANT - Python SDDS converter\n')
            the_file.write('#Numbers of turns: '+str(nbturn)+'\n')
            the_file.write('#Numbers of horizontal monitors: '+str(nbHBPM)+'\n')
            the_file.write('#Numbers of vertical monitors: '+str(nbVBPM)+'\n')
            the_file.write('#Acquisition date: '+time.strftime("%Y-%m-%d at %H:%M:%S")+'\n')
        the_file.close()
    
    
      for c in c_bpms:
        c.getFile()
        c.read_FilesandCombine_notnormalised()
        for p in range(nbpart): #1): #
            with open(path+'BPMs_Part{0}.sdds'.format(p+1), 'a') as the_file:
                str_b='0 {0} {1}  '.format(c.case_name.upper(),0.0)
                str_p=re.sub("\n","",str(c.data_part[2*p+1].dtrack['x[mm]'].values)[1:-1])
                the_file.write(str_b+str_p+'\n')
                
                str_b='1 {0} {1}  '.format(c.case_name.upper(),0.0)
                str_p=re.sub("\n","",str(c.data_part[2*p+1].dtrack['y[mm]'].values)[1:-1])
                the_file.write(str_b+str_p+'\n')
            the_file.close()
        c.free()


if flg_srt:
  tp=pd.read_csv(file_tfs,skiprows=48,sep='\s+',header=None)
  bpm_tfs=tp.loc[:,0]
  # Sort ASCII file
  for ca in case_l:
    for rat in rat_l:
      path=workplace+ca+"/Output_"+ca+"_Rat"+rat+"/"
      
      
      # get file in directory
      files=[];
      currentDirectory = pathlib.Path(path);
      for currentFile in currentDirectory.glob(file_type):
        files.append(currentFile)
      
      bpms= list({re.sub("_Data_Part.txt","",str(x)) for x in files})
      bpms= list({re.sub(path,"",str(x)) for x in bpms})
      
      # Generate all the bpms
      c_bpms=[]
      for i in range(len(bpms)): #3): #
        file=re.sub(path,"",str(files[i]))
        bpm =re.sub(path,"",str(bpms[i] ))
        c=cas_DT_Sm(bpm,case_col="",path=path, file_type=file)
        c_bpms.append(c)
    

      nbHBPM=len(c_bpms)
      nbVBPM=len(c_bpms)
      for p in range(nbpart):
        d=pd.read_csv(path+'BPMs_Part{0}.sdds'.format(p+1),header=None,skiprows=6,sep='\s+',error_bad_lines=False)
        bpm_ndata=d.loc[:,1].values
        bpm_adata=d.loc[:,0].values
        d=d.set_index([pd.Index(bpm_adata),pd.Index(bpm_ndata)])
        #print(d.head(5))

        list_idx=[x for x in bpm_tfs if x in bpm_ndata];
        with open(path+'BPMs_Part{0}_sorted.sdds'.format(p+1), 'w') as the_file:
            the_file.write('#SDDSDASCIIFORMAT v1 \n')
            the_file.write('#Created: '+time.strftime("%Y-%m-%d at %H:%M:%S")+' By: Thomas PUGNAT - Python SDDS converter\n')
            the_file.write('#Numbers of turns: '+str(nbturn)+'\n')
            the_file.write('#Numbers of horizontal monitors: '+str(nbHBPM)+'\n')
            the_file.write('#Numbers of vertical monitors: '+str(nbVBPM)+'\n')
            the_file.write('#Acquisition date: '+time.strftime("%Y-%m-%d at %H:%M:%S")+'\n')
            for idx in list_idx:
                linex=d.loc[(0,idx),:]
                liney=d.loc[(1,idx),:]
                #print(linex)

                str_b='0 {0} {1}  '.format(linex[1],0.0)
                str_p=re.sub("\n","",str(linex[3:].values)[1:-1])
                the_file.write(str_b+str_p+'\n')

                str_b='1 {0} {1}  '.format(liney[1],0.0)
                str_p=re.sub("\n","",str(liney[3:].values)[1:-1])
                the_file.write(str_b+str_p+'\n')
        the_file.close()
#FORMAT_STRING="{:.6f}
