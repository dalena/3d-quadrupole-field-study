seed_min=1
seed_max=1
bpm_lists=( 'list1' 'list2' 'list3' 'list4' 'list5' 'list6' )
case_list=( 'nom_allerrors' 'nom_b4only' 'nom_b6only' 'ricc_allerrors' 'ricc_b6only' )

# Case with no seed
ca_l="noerrors"
for bp_l in ${bpm_lists[@]}; do
	clubatch "./scan.sh '"$ca_l"_"$bp_l"' '"$ca_l"/' 'fort.3_source' 'fort.3_DUMP_BPM_"$bp_l"' 'fort.13_r01_Rat0_dlt0'"
	sleep 2.0s
done




for ca_l in ${case_list[@]}; do
	for seed in `seq $seed_min $seed_max`; do
		for bp_l in ${bpm_lists[@]}; do
			script="./scan.sh '"$ca_l"_$bp_l' '$ca_l/' 'fort.3_source' 'fort.3_DUMP_BPM_$bp_l' 'fort.13_r01_Rat0_dlt0' '_$seed'"
			clubatch $script
			sleep 2.0s
		done
	done
done
