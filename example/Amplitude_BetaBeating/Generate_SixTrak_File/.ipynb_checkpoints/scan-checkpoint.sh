#!/bin/bash
#args=("$@")


#if [ $# -ge 4 ]; then
#	echo "HELP: should be (at least) followed by:"
#	echo "  ./***.sh name_case Path_fort Path_fort3_src Path_fort3_bpm Path_fort13 ( name_seed ( Path_fort3_ffi nm_fringe ))"
#	echo "with:"
#	echo ""
#else
if [ $# -ge 4 ]; then
	# Important path
	#      * iclust
	#path_iclust=/home/nfs/manip/mnt/perso/payet/fringefields/New_C2-6-10-14/bpms_optics
	#path_icnode=/mnt/data_tmp
	#six_path=/home/nfs/manip/mnt/perso/payet/bin/SixTrack_50003-332fd32_libarchive_crlibm_rn_Linux_gfortran_static_x86_64_64bit_double
	#      * tpugnat
	path_iclust=./
	path_icnode=./
	six_path=/home/tpugnat/Documents/CEA/SixTrack/These_Workplace/SixTrack/SixTrack_01_2019/SixTrack/build/SixTrack_cmakesix_FFIELD_gfortran_defaultbuildtype/SixTrack_50200-7e4197d_FFIELD_crlibm_rn_Linux_gfortran_static_x86_64_64bit_double 

	# Default parameter
	name_seed=''
	Path_fort3_ffi=''

	# Get argument
	name_case=$1
	Path_fort=$2
	Path_fort3_src=$3
	Path_fort3_bpm=$4
	Path_fort13=$5
	if [ $# -ge 6 ]; then
		name_seed=$6
	fi
	if [ $# -ge 8 ]; then
		Path_fort3_ffi=$7
		nm_fringe=$8
	fi

	# Create working folder
	w_folder=$path_icnode/sixtrack_$name_case$name_seed
	if [ -d $w_folder ]; then 
		rm $w_folder/*
		echo "           - $w_folder found and cleaned!"
	else
		mkdir $w_folder
		echo "           - $w_folder created!"
	fi

	# Create output folder
	o_folder=$w_folder/Output_$name_case$name_seed$nm_fringe
	if [ -d $o_folder ]; then 
		rm $o_folder/*
		echo "           - $o_folder found and cleaned!"
	else
		mkdir $o_folder
		echo "           - $o_folder created!"
	fi

	# Import file into workplace
	cp $path_iclust/$Path_fort/fort.*$name_seed $w_folder/.
	cp $path_iclust/$Path_fort/fort.3           $w_folder/fort.3_mul
	cp $path_iclust/$Path_fort3_src             $w_folder/fort.3_src
	cp $path_iclust/$Path_fort3_bpm             $w_folder/fort.3_bpm
	if [ "$Path_fort13" != "" ];then
		cp $path_iclust/$Path_fort13        $w_folder/fort.13
	fi
	if [ "$Path_fort3_ffi" != "" ];then
		cp $path_iclust/$Path_fort3_ffi     $w_folder/fort.3_ffi
	fi

	# Build fort.3
	awk '/MULT/{system("cat '$w_folder'/fort.3_mul")}1' $w_folder/fort.3_src  > $w_folder/tmp.1
	awk '/DUMP/{system("cat '$w_folder'/fort.3_bpm")}1' $w_folder/tmp.1 > $w_folder/tmp.2
	if [ "$Path_fort3_ffi" != "" ];then
		mv $w_folder/tmp.2 $w_folder/tmp.1
		awk '/FFIELD/{system("cat '$w_folder'/fort.3_ffi")}1' $w_folder/tmp.1 > $w_folder/tmp.2
	fi
	cp $w_folder/tmp.2 $w_folder/fort.3

	# Change fort to fort
	cp $w_folder/fort.2$name_seed $w_folder/fort.2
	#cp $w_folder/fort.3 $w_folder/fort.3
	if [ -e $w_folder/fort.8$name_seed ]; then
		cp $w_folder/fort.8$name_seed  $w_folder/fort.8
	fi
	if [ -e $w_folder/fort.16$name_seed ]; then
		cp $w_folder/fort.16$name_seed $w_folder/fort.16
	fi

	# Run SixTrack
	cd $w_folder/
	$six_path > log.txt

	# Move all output
	mv bpm*.txt $o_folder/
	mv log.txt  $o_folder/
	sleep 10.0s

	# Copy iclust
	cp -r $o_folder/ $path_iclust/$Path_fort/
	sleep 10.0s

	# Clean node
	cd $path_icnode
	#rm -r $w_folder
fi

