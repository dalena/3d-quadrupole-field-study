#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt
import numpy  as np
import pandas as pd
import scipy.optimize  as spop

#from outils import *



# Input
# -------------------------------------------------------------------------------
#                - Ref. Config: 
BPM_Ref_Name="BPM.34R3.B1"
#BPM_Ref_Name="BPM.34R8.B1"  # add b
config      ="4"
#                - Objectif:
Max_2Jxmum =0.01;   Max_2Jymum =0.01;
M_ABBeat_x =0.05;   M_ABBeat_y =-0.00;
#                - Flag
flg_print_file=False



# Get optic
# -------------------------------------------------------------------------------
currentFile="../../2018_inj/output/optic_matched.tfs"
head_opt=pd.read_csv(currentFile,header=46,sep='\s+',nrows=0).columns[1:];
optics0=pd.read_csv(currentFile,skiprows=48,sep='\s+',names=head_opt).set_index('NAME',drop=False);

currentFile="../../2018_inj/output/bpms_matched.tfs"
head_opt=pd.read_csv(currentFile,header=46,sep='\s+',nrows=0).columns[1:];
BPMs    =pd.read_csv(currentFile,skiprows=48,sep='\s+',names=head_opt).set_index('NAME',drop=False)
BPM_Ref =BPMs.loc[BPM_Ref_Name,:]
mux_BPM_Ref=BPM_Ref.MUX; muy_BPM_Ref=BPM_Ref.MUY;

l_MOF=[
  "MO.25R1.B1","MO.29R1.B1","MO.31R1.B1","MO.33R1.B1",
  "MO.33L2.B1","MO.31L2.B1","MO.29L2.B1","MO.25L2.B1",
  "MO.22R2.B1","MO.24R2.B1","MO.26R2.B1","MO.28R2.B1","MO.30R2.B1","MO.32R2.B1","MO.34R2.B1",
  "MO.32L3.B1","MO.30L3.B1","MO.28L3.B1","MO.26L3.B1","MO.24L3.B1","MO.22L3.B1",
  "MO.25R3.B1","MO.29R3.B1","MO.31R3.B1","MO.33R3.B1",
  "MO.33L4.B1","MO.31L4.B1","MO.29L4.B1","MO.25L4.B1",
  "MO.22R4.B1","MO.24R4.B1","MO.26R4.B1","MO.28R4.B1","MO.30R4.B1","MO.32R4.B1","MO.34R4.B1",
  "MO.32L5.B1","MO.30L5.B1","MO.28L5.B1","MO.26L5.B1","MO.24L5.B1","MO.22L5.B1",
  "MO.25R5.B1","MO.29R5.B1","MO.31R5.B1","MO.33R5.B1",
  "MO.33L6.B1","MO.31L6.B1","MO.29L6.B1","MO.25L6.B1",
  "MO.22R6.B1","MO.24R6.B1","MO.26R6.B1","MO.28R6.B1","MO.30R6.B1","MO.32R6.B1","MO.34R6.B1",
  "MO.32L7.B1","MO.30L7.B1","MO.28L7.B1","MO.26L7.B1","MO.24L7.B1","MO.22L7.B1",
  "MO.25R7.B1","MO.29R7.B1","MO.31R7.B1","MO.33R7.B1",
  "MO.33L8.B1","MO.31L8.B1","MO.29L8.B1","MO.25L8.B1",
  "MO.22R8.B1","MO.24R8.B1","MO.26R8.B1","MO.28R8.B1","MO.30R8.B1","MO.32R8.B1","MO.34R8.B1",
  "MO.32L1.B1","MO.30L1.B1","MO.28L1.B1","MO.26L1.B1","MO.24L1.B1","MO.22L1.B1"
]

l_MOD=[
  "MO.22R1.B1","MO.24R1.B1","MO.26R1.B1","MO.28R1.B1","MO.30R1.B1","MO.32R1.B1","MO.34R1.B1",
  "MO.32L2.B1","MO.30L2.B1","MO.28L2.B1","MO.26L2.B1","MO.24L2.B1","MO.22L2.B1",
  "MO.25R2.B1","MO.29R2.B1","MO.31R2.B1","MO.33R2.B1",
  "MO.33L3.B1","MO.31L3.B1","MO.29L3.B1","MO.25L3.B1",
  "MO.22R3.B1","MO.24R3.B1","MO.26R3.B1","MO.30R3.B1","MO.34R3.B1",
  "MO.32L4.B1","MO.30L4.B1","MO.28L4.B1","MO.26L4.B1","MO.24L4.B1","MO.22L4.B1",
  "MO.25R4.B1","MO.29R4.B1","MO.31R4.B1","MO.33R4.B1",
  "MO.33L5.B1","MO.31L5.B1","MO.29L5.B1","MO.25L5.B1",
  "MO.22R5.B1","MO.24R5.B1","MO.26R5.B1","MO.28R5.B1","MO.30R5.B1","MO.32R5.B1","MO.34R5.B1",
  "MO.32L6.B1","MO.30L6.B1","MO.28L6.B1","MO.26L6.B1","MO.24L6.B1","MO.22L6.B1",
  "MO.25R6.B1","MO.29R6.B1","MO.31R6.B1","MO.33R6.B1",
  "MO.33L7.B1","MO.31L7.B1","MO.29L7.B1","MO.25L7.B1",
  "MO.22R7.B1","MO.24R7.B1","MO.26R7.B1","MO.28R7.B1","MO.30R7.B1","MO.32R7.B1","MO.34R7.B1",
  "MO.32L8.B1","MO.30L8.B1","MO.28L8.B1","MO.26L8.B1","MO.24L8.B1","MO.22L8.B1",
  "MO.25R8.B1","MO.29R8.B1","MO.31R8.B1","MO.33R8.B1",
  "MO.33L1.B1","MO.31L1.B1","MO.29L1.B1","MO.25L1.B1"
]

# Extract Octupole corrector
# -------------------------------------------------------------------------------
Oct = optics0[np.logical_or(optics0.loc[:,"NAME"].str.contains("MCO"),optics0.loc[:,"NAME"].str.contains("MO"))].set_index('NAME',drop=False)
Oct = Oct[Oct.loc[:,"KEYWORD"].str.contains("MULTIPOLE")]
Qx=optics0.MUX.max();  Qy=optics0.MUY.max()

print("\n\nInfo optic and BPM Ref:")
print("-------------------------------------------------------------------------")
print("\t- Optics:\n\t\tQx: {0},\n\t\tQy: {1}\n\n\t- BPM Ref:".format(Qx,Qy))
print(BPM_Ref)


if False:
    # Compute Coef RDTs
    # -------------------------------------------------------------------------------
    mux =Oct.MUX -mux_BPM_Ref;   muy =Oct.MUY -muy_BPM_Ref
    mux=np.array([x+Qx if x<0 else x for x in mux]);
    muy=np.array([x+Qy if x<0 else x for x in muy]);
    betx=Oct.BETX;  bety=Oct.BETY
    dx  =Oct.DX;    dy  =Oct.DY;
    betxbetx=(betx*betx)
    betxbety=(betx*bety)
    betybety=(bety*bety)

    # -------------------------------------------------------------
    # Equ. RDTs for octupole only:
    # -------------------------------------------------------------
    #
    #   f_{4000} =( 1/16) \sum K3L bet_x^2 e^{8\pi \mu_x}/(1-e^{8\pi Q_x})
    #   f_{3100} =( 1/ 4) \sum K3L bet_x^2 e^{4\pi \mu_x}/(1-e^{4\pi Q_x})
    #   h_{2200} =( 3/ 8) \sum K3L bet_x^2
    #
    #   f_{2020} =(-6/16) \sum K3L bet_x bet_y e^{4\pi(\mu_x+\mu_y)}/(1-e^{4\pi(Q_x+Q_y)})
    #   f_{2011} =(-6/ 8) \sum K3L bet_x bet_y e^{4\pi \mu_x       }/(1-e^{4\pi Q_x     })
    #   f_{1120} =(-6/ 8) \sum K3L bet_x bet_y e^{4\pi       \mu_y }/(1-e^{4\pi     Q_y })
    #   f_{2002} =(-6/16) \sum K3L bet_x bet_y e^{4\pi(\mu_x-\mu_y)}/(1-e^{4\pi(Q_x-Q_y)})
    #   h_{1111} =(-6/ 4) \sum K3L bet_x bet_y
    #
    #   f_{0040} =( 1/16) \sum K3L bet_y^2 e^{8\pi \mu_y}/(1-e^{8\pi Q_y})
    #   f_{0031} =( 1/ 4) \sum K3L bet_y^2 e^{4\pi \mu_y}/(1-e^{4\pi Q_y})
    #   h_{0022} =( 3/ 8) \sum K3L bet_y^2
    #
    # -------------------------------------------------------------


    #      * Amplitude and cross-detuning coef
    Oct.loc[:,"ch2200"] =( 3/ 8)*betxbetx/(12*np.pi)    # Horizontal plane
    Oct.loc[:,"ch1111"] =(-3/ 2)*betxbety/(12*np.pi)    # Cross plane
    Oct.loc[:,"ch0022"] =( 3/ 8)*betybety/(12*np.pi)    # Vertical plane

    #      * Chromaticity coef
    Oct.loc[:,"cc20"] =( 3/ 2)*(betx*(dx*dx))/(12*np.pi)    # Horizontal plane
    Oct.loc[:,"cc02"] =( 3/ 2)*(bety*(dx*dx))/(12*np.pi)    # Vertical plane

    #      * Other RDTs coef
    #              - Horizontal plane
    Oct.loc[:,"cf4000"] =( 1/16)*( betxbetx*np.exp( (8j*np.pi)* mux    )  )/( 1-np.exp( (8j*np.pi)* Qx    ) )/(12*np.pi)
    Oct.loc[:,"cf3100"] =( 1/ 4)*( betxbetx*np.exp( (4j*np.pi)* mux    )  )/( 1-np.exp( (4j*np.pi)* Qx    ) )/(12*np.pi)
    Oct.loc[:,"cf4000r"]=np.real(Oct.loc[:,"cf4000"].values)
    Oct.loc[:,"cf3100r"]=np.real(Oct.loc[:,"cf3100"].values)
    Oct.loc[:,"cf4000i"]=np.imag(Oct.loc[:,"cf4000"].values)
    Oct.loc[:,"cf3100i"]=np.imag(Oct.loc[:,"cf3100"].values)
    #              - Cross plane
    Oct.loc[:,"cf2020"] =(-3/ 8)*( betxbety*np.exp( (4j*np.pi)*(mux+muy)) )/( 1-np.exp( (4j*np.pi)*(Qx+Qy)) )/(12*np.pi)
    Oct.loc[:,"cf2011"] =(-3/ 4)*( betxbety*np.exp( (4j*np.pi)* mux     ) )/( 1-np.exp( (4j*np.pi)* Qx    ) )/(12*np.pi)
    Oct.loc[:,"cf1120"] =(-3/ 4)*( betxbety*np.exp( (4j*np.pi)*     muy ) )/( 1-np.exp( (4j*np.pi)*    Qy ) )/(12*np.pi)
    Oct.loc[:,"cf2002"] =(-3/ 8)*( betxbety*np.exp( (4j*np.pi)*(mux-muy)) )/( 1-np.exp( (4j*np.pi)*(Qx-Qy)) )/(12*np.pi)
    Oct.loc[:,"cf2020r"]=np.real(Oct.loc[:,"cf2020"].values)
    Oct.loc[:,"cf2011r"]=np.real(Oct.loc[:,"cf2011"].values)
    Oct.loc[:,"cf1120r"]=np.real(Oct.loc[:,"cf1120"].values)
    Oct.loc[:,"cf2002r"]=np.real(Oct.loc[:,"cf2002"].values)
    Oct.loc[:,"cf2020i"]=np.imag(Oct.loc[:,"cf2020"].values)
    Oct.loc[:,"cf2011i"]=np.imag(Oct.loc[:,"cf2011"].values)
    Oct.loc[:,"cf1120i"]=np.imag(Oct.loc[:,"cf1120"].values)
    Oct.loc[:,"cf2002i"]=np.imag(Oct.loc[:,"cf2002"].values)
    #              - Vertical plane
    Oct.loc[:,"cf0040"] =( 1/16)*( betybety*np.exp( (8j*np.pi)*     muy ) )/( 1-np.exp( (8j*np.pi)*    Qy ) )/(12*np.pi)
    Oct.loc[:,"cf0031"] =( 1/ 4)*( betybety*np.exp( (4j*np.pi)*     muy ) )/( 1-np.exp( (4j*np.pi)*    Qy ) )/(12*np.pi)
    Oct.loc[:,"cf0040r"]=np.real(Oct.loc[:,"cf0040"].values)
    Oct.loc[:,"cf0031r"]=np.real(Oct.loc[:,"cf0031"].values)
    Oct.loc[:,"cf0040i"]=np.imag(Oct.loc[:,"cf0040"].values)
    Oct.loc[:,"cf0031i"]=np.imag(Oct.loc[:,"cf0031"].values)

else:
    # Compute Coef RDTs
    # -------------------------------------------------------------------------------
    mux1 =mux_BPM_Ref -Oct.MUX;  mux=np.array([x+Qx if x<0 else x for x in mux1]);
    muy1 =muy_BPM_Ref -Oct.MUY;  muy=np.array([y+Qy if y<0 else y for y in muy1]);

    betx=Oct.BETX;  bety=Oct.BETY
    dx  =Oct.DX;    dy  =Oct.DY;
    betxbetx=(betx*betx)
    betxbety=(betx*bety)
    betybety=(bety*bety)

    # -------------------------------------------------------------
    # Equ. RDTs for octupole only:
    # -------------------------------------------------------------
    #
    #   f_{4000} =(-1/384) \sum K3L bet_x^2 e^{8\pi \mu_x}/(1-e^{8\pi Q_x})
    #   f_{3100} =(-1/ 96) \sum K3L bet_x^2 e^{4\pi \mu_x}/(1-e^{4\pi Q_x})
    #   h_{2200} =( 3/  8) \sum K3L bet_x^2
    #
    #   f_{2020} =(-1/ 64) \sum K3L bet_x bet_y e^{4\pi(\mu_x+\mu_y)}/(1-e^{4\pi(Q_x+Q_y)})
    #   f_{2011} =( 1/ 32) \sum K3L bet_x bet_y e^{4\pi \mu_x       }/(1-e^{4\pi Q_x     })
    #   f_{1120} =( 1/ 32) \sum K3L bet_x bet_y e^{4\pi       \mu_y }/(1-e^{4\pi     Q_y })
    #   f_{2002} =(-1/ 64) \sum K3L bet_x bet_y e^{4\pi(\mu_x-\mu_y)}/(1-e^{4\pi(Q_x-Q_y)})
    #   h_{1111} =(-6/  4) \sum K3L bet_x bet_y
    #
    #   f_{0040} =( 1/384) \sum K3L bet_y^2 e^{8\pi \mu_y}/(1-e^{8\pi Q_y})
    #   f_{0031} =( 1/ 96) \sum K3L bet_y^2 e^{4\pi \mu_y}/(1-e^{4\pi Q_y})
    #   h_{0022} =( 3/  8) \sum K3L bet_y^2
    #
    # -------------------------------------------------------------


    #      * Amplitude and cross-detuning coef
    Oct.loc[:,"ch2200"] =( 3/ 8)*betxbetx/(12*np.pi)    # Horizontal plane
    Oct.loc[:,"ch1111"] =(-3/ 2)*betxbety/(12*np.pi)    # Cross plane
    Oct.loc[:,"ch0022"] =( 3/ 8)*betybety/(12*np.pi)    # Vertical plane

    #      * Chromaticity coef
    Oct.loc[:,"cc20"] =( 3/ 2)*(betx*(dx*dx))/(12*np.pi)    # Horizontal plane
    Oct.loc[:,"cc02"] =( 3/ 2)*(bety*(dx*dx))/(12*np.pi)    # Vertical plane

    inv_384=1.0e0/(384e0)
    inv_96 =1.0e0/( 96e0)
    inv_64 =1.0e0/( 64e0)
    inv_32 =1.0e0/( 32e0)
    #      * Other RDTs coef
    #              - Horizontal plane
    Oct.loc[:,"cf4000"] =-( betxbetx * np.exp( (8j*np.pi)* mux       ) * inv_384 )/( 1-np.exp( (8j*np.pi)* Qx    ) )
    Oct.loc[:,"cf3100"] =-( betxbetx * np.exp( (4j*np.pi)* mux       ) * inv_96  )/( 1-np.exp( (4j*np.pi)* Qx    ) )
    Oct.loc[:,"cf4000r"]=np.real(Oct.loc[:,"cf4000"].values)
    Oct.loc[:,"cf3100r"]=np.real(Oct.loc[:,"cf3100"].values)
    Oct.loc[:,"cf4000i"]=np.imag(Oct.loc[:,"cf4000"].values)
    Oct.loc[:,"cf3100i"]=np.imag(Oct.loc[:,"cf3100"].values)
    #              - Cross plane
    Oct.loc[:,"cf2020"] =-( betxbety * np.exp( (4j*np.pi)*(mux+muy) ) * inv_64  )/( 1-np.exp( (4j*np.pi)*(Qx+Qy)) )
    Oct.loc[:,"cf2011"] =-(-betxbety * np.exp( (4j*np.pi)* mux      ) * inv_32  )/( 1-np.exp( (4j*np.pi)* Qx    ) )
    Oct.loc[:,"cf1120"] =-(-betxbety * np.exp( (4j*np.pi)*     muy  ) * inv_32  )/( 1-np.exp( (4j*np.pi)*    Qy ) )
    Oct.loc[:,"cf2002"] =-( betxbety * np.exp( (4j*np.pi)*(mux-muy) ) * inv_64  )/( 1-np.exp( (4j*np.pi)*(Qx-Qy)) )
    Oct.loc[:,"cf2020r"]=np.real(Oct.loc[:,"cf2020"].values)
    Oct.loc[:,"cf2011r"]=np.real(Oct.loc[:,"cf2011"].values)
    Oct.loc[:,"cf1120r"]=np.real(Oct.loc[:,"cf1120"].values)
    Oct.loc[:,"cf2002r"]=np.real(Oct.loc[:,"cf2002"].values)
    Oct.loc[:,"cf2020i"]=np.imag(Oct.loc[:,"cf2020"].values)
    Oct.loc[:,"cf2011i"]=np.imag(Oct.loc[:,"cf2011"].values)
    Oct.loc[:,"cf1120i"]=np.imag(Oct.loc[:,"cf1120"].values)
    Oct.loc[:,"cf2002i"]=np.imag(Oct.loc[:,"cf2002"].values)
    #              - Vertical plane
    Oct.loc[:,"cf0040"] =-( betybety * np.exp( (8j*np.pi)*      muy  ) * inv_384 )/( 1-np.exp( (8j*np.pi)*    Qy ) )
    Oct.loc[:,"cf0031"] =-( betybety * np.exp( (4j*np.pi)*      muy  ) * inv_96  )/( 1-np.exp( (4j*np.pi)*    Qy ) )
    Oct.loc[:,"cf0040r"]=np.real(Oct.loc[:,"cf0040"].values)
    Oct.loc[:,"cf0031r"]=np.real(Oct.loc[:,"cf0031"].values)
    Oct.loc[:,"cf0040i"]=np.imag(Oct.loc[:,"cf0040"].values)
    Oct.loc[:,"cf0031i"]=np.imag(Oct.loc[:,"cf0031"].values)


# Define Family
# -------------------------------------------------------------------------------
ONAME=Oct["NAME"]
MO_T_a12b1 = (Oct[np.logical_and(ONAME.str.contains("MO."),np.logical_or(ONAME.str.contains("R1"),ONAME.str.contains("L2")))])
MO_S_a12b1 = MO_T_a12b1.sum(axis=0).to_frame(name="MO.a12b1").T; MO_S_a12b1["NAME"] = MO_S_a12b1.index.tolist()
MOF_T_a12b1= MO_T_a12b1.loc[[x for x in MO_T_a12b1.NAME if x in l_MOF],:]
MOF_S_a12b1= MOF_T_a12b1.sum(axis=0).to_frame(name="MOF.a12b1").T; MOF_S_a12b1["NAME"] = MOF_S_a12b1.index.tolist()
MOD_T_a12b1= MO_T_a12b1.loc[[x for x in MO_T_a12b1.NAME if x in l_MOD],:]
MOD_S_a12b1= MOD_T_a12b1.sum(axis=0).to_frame(name="MOD.a12b1").T; MOD_S_a12b1["NAME"] = MOD_S_a12b1.index.tolist()

MO_T_a23b1 = (Oct[np.logical_and(ONAME.str.contains("MO."),np.logical_or(ONAME.str.contains("R2"),ONAME.str.contains("L3")))])
MO_S_a23b1 = MO_T_a23b1.sum(axis=0).to_frame(name="MO.a23b1").T; MO_S_a23b1["NAME"] = MO_S_a23b1.index.tolist()
MOF_T_a23b1= MO_T_a23b1.loc[[x for x in MO_T_a23b1.NAME if x in l_MOF],:]
MOF_S_a23b1= MOF_T_a23b1.sum(axis=0).to_frame(name="MOF.a23b1").T; MOF_S_a23b1["NAME"] = MOF_S_a23b1.index.tolist()
MOD_T_a23b1= MO_T_a23b1.loc[[x for x in MO_T_a23b1.NAME if x in l_MOD],:]
MOD_S_a23b1= MOD_T_a23b1.sum(axis=0).to_frame(name="MOD.a23b1").T; MOD_S_a23b1["NAME"] = MOD_S_a23b1.index.tolist()

MO_T_a34b1 = (Oct[np.logical_and(ONAME.str.contains("MO."),np.logical_or(ONAME.str.contains("R3"),ONAME.str.contains("L4")))])
MO_S_a34b1 = MO_T_a34b1.sum(axis=0).to_frame(name="MO.a34b1").T; MO_S_a34b1["NAME"] = MO_S_a34b1.index.tolist()
MOF_T_a34b1= MO_T_a34b1.loc[[x for x in MO_T_a34b1.NAME if x in l_MOF],:]
MOF_S_a34b1= MOF_T_a34b1.sum(axis=0).to_frame(name="MOF.a34b1").T; MOF_S_a34b1["NAME"] = MOF_S_a34b1.index.tolist()
MOD_T_a34b1= MO_T_a34b1.loc[[x for x in MO_T_a34b1.NAME if x in l_MOD],:]
MOD_S_a34b1= MOD_T_a34b1.sum(axis=0).to_frame(name="MOD.a34b1").T; MOD_S_a34b1["NAME"] = MOD_S_a34b1.index.tolist()

MO_T_a45b1 = (Oct[np.logical_and(ONAME.str.contains("MO."),np.logical_or(ONAME.str.contains("R4"),ONAME.str.contains("L5")))])
MO_S_a45b1 = MO_T_a45b1.sum(axis=0).to_frame(name="MO.a45b1").T; MO_S_a45b1["NAME"] = MO_S_a45b1.index.tolist()
MOF_T_a45b1= MO_T_a45b1.loc[[x for x in MO_T_a45b1.NAME if x in l_MOF],:]
MOF_S_a45b1= MOF_T_a45b1.sum(axis=0).to_frame(name="MOF.a45b1").T; MOF_S_a45b1["NAME"] = MOF_S_a45b1.index.tolist()
MOD_T_a45b1= MO_T_a45b1.loc[[x for x in MO_T_a45b1.NAME if x in l_MOD],:]
MOD_S_a45b1= MOD_T_a45b1.sum(axis=0).to_frame(name="MOD.a45b1").T; MOD_S_a45b1["NAME"] = MOD_S_a45b1.index.tolist()

MO_T_a56b1 = (Oct[np.logical_and(ONAME.str.contains("MO."),np.logical_or(ONAME.str.contains("R5"),ONAME.str.contains("L6")))]) # PB
MO_S_a56b1 = MO_T_a56b1.sum(axis=0).to_frame(name="MO.a56b1").T; MO_S_a56b1["NAME"] = MO_S_a56b1.index.tolist() # PB
MOF_T_a56b1= MO_T_a56b1.loc[[x for x in MO_T_a56b1.NAME if x in l_MOF],:]
MOF_S_a56b1= MOF_T_a56b1.sum(axis=0).to_frame(name="MOF.a56b1").T; MOF_S_a56b1["NAME"] = MOF_S_a56b1.index.tolist()
MOD_T_a56b1= MO_T_a56b1.loc[[x for x in MO_T_a56b1.NAME if x in l_MOD],:]
MOD_S_a56b1= MOD_T_a56b1.sum(axis=0).to_frame(name="MOD.a56b1").T; MOD_S_a56b1["NAME"] = MOD_S_a56b1.index.tolist()

MO_T_a67b1 = (Oct[np.logical_and(ONAME.str.contains("MO."),np.logical_or(ONAME.str.contains("R6"),ONAME.str.contains("L7")))])
MO_S_a67b1 = MO_T_a67b1.sum(axis=0).to_frame(name="MO.a67b1").T; MO_S_a67b1["NAME"] = MO_S_a67b1.index.tolist()
MOF_T_a67b1= MO_T_a67b1.loc[[x for x in MO_T_a67b1.NAME if x in l_MOF],:]
MOF_S_a67b1= MOF_T_a67b1.sum(axis=0).to_frame(name="MOF.a67b1").T; MOF_S_a67b1["NAME"] = MOF_S_a67b1.index.tolist()
MOD_T_a67b1= MO_T_a67b1.loc[[x for x in MO_T_a67b1.NAME if x in l_MOD],:]
MOD_S_a67b1= MOD_T_a67b1.sum(axis=0).to_frame(name="MOD.a67b1").T; MOD_S_a67b1["NAME"] = MOD_S_a67b1.index.tolist()

MO_T_a78b1 = (Oct[np.logical_and(ONAME.str.contains("MO."),np.logical_or(ONAME.str.contains("R7"),ONAME.str.contains("L8")))])
MO_S_a78b1 = MO_T_a78b1.sum(axis=0).to_frame(name="MO.a78b1").T; MO_S_a78b1["NAME"] = MO_S_a78b1.index.tolist()
MOF_T_a78b1= MO_T_a78b1.loc[[x for x in MO_T_a78b1.NAME if x in l_MOF],:]
MOF_S_a78b1= MOF_T_a78b1.sum(axis=0).to_frame(name="MOF.a78b1").T; MOF_S_a78b1["NAME"] = MOF_S_a78b1.index.tolist()
MOD_T_a78b1= MO_T_a78b1.loc[[x for x in MO_T_a78b1.NAME if x in l_MOD],:]
MOD_S_a78b1= MOD_T_a78b1.sum(axis=0).to_frame(name="MOD.a78b1").T; MOD_S_a78b1["NAME"] = MOD_S_a78b1.index.tolist()

MO_T_a81b1 = (Oct[np.logical_and(ONAME.str.contains("MO."),np.logical_or(ONAME.str.contains("R8"),ONAME.str.contains("L1")))])
MO_S_a81b1 = MO_T_a81b1.sum(axis=0).to_frame(name="MO.a81b1").T; MO_S_a81b1["NAME"] = MO_S_a81b1.index.tolist()
MOF_T_a81b1= MO_T_a81b1.loc[[x for x in MO_T_a81b1.NAME if x in l_MOF],:]
MOF_S_a81b1= MOF_T_a81b1.sum(axis=0).to_frame(name="MOF.a81b1").T; MOF_S_a81b1["NAME"] = MOF_S_a81b1.index.tolist()
MOD_T_a81b1= MO_T_a81b1.loc[[x for x in MO_T_a81b1.NAME if x in l_MOD],:]
MOD_S_a81b1= MOD_T_a81b1.sum(axis=0).to_frame(name="MOD.a81b1").T; MOD_S_a81b1["NAME"] = MOD_S_a81b1.index.tolist()


MCO_T_a12b1 = (Oct[np.logical_and(ONAME.str.contains("MCO\."),np.logical_or(ONAME.str.contains("R1"),ONAME.str.contains("L2")))])
MCO_S_a12b1 = MCO_T_a12b1.sum(axis=0).to_frame(name="MCO.a12b1").T; MCO_S_a12b1["NAME"] = MCO_S_a12b1.index.tolist()
MCO_T_a23b1 = (Oct[np.logical_and(ONAME.str.contains("MCO\."),np.logical_or(ONAME.str.contains("R2"),ONAME.str.contains("L3")))])
MCO_S_a23b1 = MCO_T_a23b1.sum(axis=0).to_frame(name="MCO.a23b1").T; MCO_S_a23b1["NAME"] = MCO_S_a23b1.index.tolist()
MCO_T_a34b1 = (Oct[np.logical_and(ONAME.str.contains("MCO\."),np.logical_or(ONAME.str.contains("R3"),ONAME.str.contains("L4")))])
MCO_S_a34b1 = MCO_T_a34b1.sum(axis=0).to_frame(name="MCO.a34b1").T; MCO_S_a34b1["NAME"] = MCO_S_a34b1.index.tolist()
MCO_T_a45b1 = (Oct[np.logical_and(ONAME.str.contains("MCO\."),np.logical_or(ONAME.str.contains("R4"),ONAME.str.contains("L5")))])
MCO_S_a45b1 = MCO_T_a45b1.sum(axis=0).to_frame(name="MCO.a45b1").T; MCO_S_a45b1["NAME"] = MCO_S_a45b1.index.tolist()
MCO_T_a56b1 = (Oct[np.logical_and(ONAME.str.contains("MCO\."),np.logical_or(ONAME.str.contains("R5"),ONAME.str.contains("L6")))])
MCO_S_a56b1 = MCO_T_a56b1.sum(axis=0).to_frame(name="MCO.a56b1").T; MCO_S_a56b1["NAME"] = MCO_S_a56b1.index.tolist()
MCO_T_a67b1 = (Oct[np.logical_and(ONAME.str.contains("MCO\."),np.logical_or(ONAME.str.contains("R6"),ONAME.str.contains("L7")))])
MCO_S_a67b1 = MCO_T_a67b1.sum(axis=0).to_frame(name="MCO.a67b1").T; MCO_S_a67b1["NAME"] = MCO_S_a67b1.index.tolist()
MCO_T_a78b1 = (Oct[np.logical_and(ONAME.str.contains("MCO\."),np.logical_or(ONAME.str.contains("R7"),ONAME.str.contains("L8")))])
MCO_S_a78b1 = MCO_T_a78b1.sum(axis=0).to_frame(name="MCO.a78b1").T; MCO_S_a78b1["NAME"] = MCO_S_a78b1.index.tolist()
MCO_T_a81b1 = (Oct[np.logical_and(ONAME.str.contains("MCO\."),np.logical_or(ONAME.str.contains("R8"),ONAME.str.contains("L1")))])
MCO_S_a81b1 = MCO_T_a81b1.sum(axis=0).to_frame(name="MCO.a81b1").T; MCO_S_a81b1["NAME"] = MCO_S_a81b1.index.tolist()

MCOX_IPLeft = Oct[np.logical_and(ONAME.str.contains("MCOX."),ONAME.str.contains("L"))]
MCOX_IPRigh = Oct[np.logical_and(ONAME.str.contains("MCOX."),ONAME.str.contains("R"))]

cdrop=['NAME','S','BETX','BETY','X','Y','ALFX','ALFY','DX','DPX','DY','DPY','MUX','MUY','PX','PY','K1L','K2L','KEYWORD']




print("\n\nStat Octupole:")
print("-------------------------------------------------------------------------")
print("Nb MO_T_a12b1: {}".format(len(MO_T_a12b1)));   #print(MO_S_a12b1)
print("Nb MO_T_a23b1: {}".format(len(MO_T_a23b1)));   #print(MO_S_a23b1)
print("Nb MO_T_a34b1: {}".format(len(MO_T_a34b1)));   #print(MO_S_a34b1)
print("Nb MO_T_a45b1: {}".format(len(MO_T_a45b1)));   #print(MO_S_a45b1)
print("Nb MO_T_a56b1: {}  #PB *0.775".format(len(MO_T_a56b1)));   #print(MO_S_a56b1)
print("Nb MO_T_a67b1: {}".format(len(MO_T_a67b1)));   #print(MO_S_a67b1)
print("Nb MO_T_a78b1: {}".format(len(MO_T_a78b1)));   #print(MO_S_a78b1)
print("Nb MO_T_a81b1: {}".format(len(MO_T_a81b1)));   #print(MO_S_a81b1)

print("Nb MOF_T_a12b1: {0},   MOD_T_a12b1: {1}".format(len(MOF_T_a12b1),len(MOD_T_a12b1)));
print("Nb MOF_T_a23b1: {0},   MOD_T_a23b1: {1}".format(len(MOF_T_a23b1),len(MOD_T_a23b1)));
print("Nb MOF_T_a34b1: {0},   MOD_T_a34b1: {1}".format(len(MOF_T_a34b1),len(MOD_T_a34b1)));
print("Nb MOF_T_a45b1: {0},   MOD_T_a45b1: {1}".format(len(MOF_T_a45b1),len(MOD_T_a45b1)));
print("Nb MOF_T_a56b1: {0},   MOD_T_a56b1: {1}  #PB *0.775".format(len(MOF_T_a56b1),len(MOD_T_a56b1)));
print("Nb MOF_T_a67b1: {0},   MOD_T_a67b1: {1}".format(len(MOF_T_a67b1),len(MOD_T_a67b1)));
print("Nb MOF_T_a78b1: {0},   MOD_T_a78b1: {1}".format(len(MOF_T_a78b1),len(MOD_T_a78b1)));
print("Nb MOF_T_a81b1: {0},   MOD_T_a81b1: {1}".format(len(MOF_T_a81b1),len(MOD_T_a81b1)));

print("Nb MCO_T_a12b1:  {}".format(len(MCO_T_a12b1)));   #print(MCO_S_a12b1)
print("Nb MCO_T_a23b1:  {}".format(len(MCO_T_a23b1)));   #print(MCO_S_a23b1)
print("Nb MCO_T_a34b1:  {}".format(len(MCO_T_a34b1)));   #print(MCO_S_a34b1)
print("Nb MCO_T_a45b1:  {}".format(len(MCO_T_a45b1)));   #print(MCO_S_a45b1)
print("Nb MCO_T_a56b1:  {}".format(len(MCO_T_a56b1)));   #print(MCO_S_a56b1)
print("Nb MCO_T_a67b1:  {}".format(len(MCO_T_a67b1)));   #print(MCO_S_a67b1)
print("Nb MCO_T_a78b1:  {}".format(len(MCO_T_a78b1)));   #print(MCO_S_a78b1)
print("Nb MCO_T_a81b1:  {}".format(len(MCO_T_a81b1)));   #print(MCO_S_a81b1)

print("Nb MCOX_IPLeft: {}".format(len(MCOX_IPLeft)))
print("Nb MCOX_IPRigh: {}\n".format(len(MCOX_IPRigh)))

# Define target
# -------------------------------------------------------------------------------

# -------------------------------------------------------------
# Equ. Amplitude Beta-Beating for octupole only:
# -------------------------------------------------------------
#
#   |H_x|^2/bet_{x0} = (2I_x) (Dbet_x/bet_{x0} + 1) [ 1 +
#                                                     12 (2I_x) |f_{3100}| sin(q_{3100}) +
#                                                     36 (2I_x)^2 |f_{3100}|^2
#
# -------------------------------------------------------------


#                - Target parameter:

DAmpBB = lambda x : M_ABBeat - (12*x*Max_2Jmum*1e-6)/(1+36*((x*Max_2Jmum*1e-6)**2))

M_ABBeat=M_ABBeat_x;  Max_2Jmum=Max_2Jxmum;
f3100=1j*M_ABBeat_x/(12*Max_2Jxmum*1e-6);         print("f3100 (Simple):{}".format(f3100))
#f3100=spop.broyden1(DAmpBB, 0,f_tol=1e-4);     print("f3100 (Full)  :{}".format(f3100))
#print((12*f3100*Max_2Jmum*1e-6)/(1+36*((f3100*Max_2Jmum*1e-6)**2)))
#stop;
M_ABBeat=M_ABBeat_y;  Max_2Jmum=Max_2Jymum;
f0031=1j*M_ABBeat_y/(12*Max_2Jymum*1e-6);         print("f0031 (Simple):{}".format(f0031))
#f0031=spop.broyden1(DAmpBB, 0,f_tol=1e-4);     print("f0031 (Full)  :{}".format(f0031))
#stop;

h2200=0.;
h1111=0.;
h0022=0.;
c20  =0.;
c02  =0.;
f4000=0.;
f2020=0.;
f1120=0.;
f2011=0.;
f2002=0.;
f0040=0.;

if (config=="1") or (config=="2") or (config=="3") :
    d={
     "ch2200":[h2200],
     "ch1111":[h1111],
     "ch0022":[h0022],
     "cf3100r":[np.real(f3100)],
     "cf0031r":[np.real(f0031)],

     "cf4000r":[np.real(f4000)],
     "cf0040r":[np.real(f0040)],
     "cf2020r":[np.real(f2020)],
     "cf1120r":[np.real(f1120)],
     "cf2011r":[np.real(f2011)],
     "cf2002r":[np.real(f2002)],
     "cf3100i":[np.imag(f3100)],
     "cf0031i":[np.imag(f0031)],
     "cf4000i":[np.imag(f4000)],
     "cf0040i":[np.imag(f0040)],
     "cf2020i":[np.imag(f2020)],
     "cf1120i":[np.imag(f1120)],
     "cf2011i":[np.imag(f2011)],
     "cf2002i":[np.imag(f2002)]
    }
elif (config=="4"):
    d={
     "ch2200":[h2200],
     "ch1111":[h1111],
     "ch0022":[h0022],

     "cf3100r":[np.real(f3100)],
     "cf0031r":[np.real(f0031)],

     "cf3100i":[np.imag(f3100)],
     "cf0031i":[np.imag(f0031)],
    }
else:
    config="new";
    d={
     "ch2200":[h2200],
     "ch1111":[h1111],
     "ch0022":[h0022],
#     "cc20":  [c20  ],
#     "cc02":  [c02  ],
#     "cf3100":[f3100]#,
#     "cf0031":[f0031]#,
#     "cf4000":[f4000]#,
#     "cf0040":[f0040]#,
#     "cf2020":[f2020]#,
#     "cf1120":[f1120]#,
#     "cf2011":[f2011]#,
#     "cf2002":[f2002]#,
     "cf3100r":[np.real(f3100)],
     "cf0031r":[np.real(f0031)],

#     "cf4000r":[np.real(f4000)],
#     "cf0040r":[np.real(f0040)],
#     "cf2020r":[np.real(f2020)],
#     "cf1120r":[np.real(f1120)],
#     "cf2011r":[np.real(f2011)],
#     "cf2002r":[np.real(f2002)],
     "cf3100i":[np.imag(f3100)],
     "cf0031i":[np.imag(f0031)],
#     "cf4000i":[np.imag(f4000)],
#     "cf0040i":[np.imag(f0040)],
#     "cf2020i":[np.imag(f2020)],
#     "cf1120i":[np.imag(f1120)],
#     "cf2011i":[np.imag(f2011)],
#     "cf2002i":[np.imag(f2002)]#,
    }
Target=pd.DataFrame(data=d)

#                - Octupole cible:
#Oct_Cible=pd.concat([MO_S_a12b1,MO_S_a23b1,MO_S_a34b1,MO_S_a45b1,MO_S_a67b1,MO_S_a78b1,MO_S_a81b1,MCOX_IPLeft])
#Oct_Cible=pd.concat([MO_S_a12b1,MO_S_a23b1,MO_S_a67b1,MCOX_IPLeft.loc[["MCOX.3L1","MCOX.3L8"],:],MCOX_IPRigh.loc[["MCOX.3R1","MCOX.3R8"],:]])
if config=="1":
    Oct_Cible=pd.concat([MO_S_a12b1, MO_S_a23b1, MO_S_a34b1,
                         MO_S_a45b1, MO_S_a67b1, MO_S_a78b1,

                         MCO_S_a12b1,MCO_S_a23b1,MCO_S_a34b1,
                         MCO_S_a56b1,MCO_S_a67b1,MCO_S_a81b1,

                         MCOX_IPLeft.loc[['MCOX.3L5', 'MCOX.3L8', 'MCOX.3L1'],:],
                         MCOX_IPRigh.loc[['MCOX.3R5', 'MCOX.3R8', 'MCOX.3R1', 'MCOX.3R2'],:]
                     ])

elif config=="2":
    Oct_Cible=pd.concat([MO_S_a12b1, MO_S_a23b1, MO_S_a34b1, MO_S_a45b1,
                         MO_S_a67b1, MO_S_a78b1, MO_S_a81b1,

                         MCO_S_a12b1,MCO_S_a23b1,MCO_S_a34b1,MCO_S_a45b1,
                         MCO_S_a56b1,MCO_S_a67b1,MCO_S_a78b1,MCO_S_a81b1,

                         MCOX_IPLeft.loc[['MCOX.3L5', 'MCOX.3L1'],:],
                         MCOX_IPRigh.loc[['MCOX.3R5', 'MCOX.3R1'],:]
                     ])

elif config=="3":
    Oct_Cible=pd.concat([MO_S_a23b1, MO_S_a45b1, MO_S_a67b1, MO_S_a81b1,

                         MCO_S_a12b1,MCO_S_a23b1,MCO_S_a34b1,MCO_S_a45b1,
                         MCO_S_a56b1,MCO_S_a67b1,MCO_S_a78b1,MCO_S_a81b1,

                         MCOX_IPLeft.loc[['MCOX.3L5', 'MCOX.3L8', 'MCOX.3L1'],:],
                         MCOX_IPRigh.loc[['MCOX.3R5', 'MCOX.3R8', 'MCOX.3R1', 'MCOX.3R2'],:]
                     ])

elif config=="4":
    Oct_Cible=pd.concat([
                         MCOX_IPLeft.loc[['MCOX.3L5', 'MCOX.3L8', 'MCOX.3L1'],:],
                         MCOX_IPRigh.loc[['MCOX.3R5', 'MCOX.3R8', 'MCOX.3R1', 'MCOX.3R2'],:]
                     ])

else:
    Oct_Cible=pd.concat([#MO_S_a12b1,  # c=1,
                         #MO_S_a23b1,  # c=1,2
                         #MO_S_a34b1,  # c=1,
                         #MO_S_a45b1,  # c=1,2
                         #MO_S_a56b1,  # c= # PB
                         #MO_S_a67b1,  # c=1,2
                         #MO_S_a78b1,  # c=1
                         #MO_S_a81b1,  # c=  2

                         #MOF_S_a12b1,  # c=
                         #MOF_S_a23b1,  # c=
                         #MOF_S_a34b1,  # c=
                         #MOF_S_a45b1,  # c=
                         #MOF_S_a56b1,  # c= # PB
                         #MOF_S_a67b1,  # c=
                         #MOF_S_a78b1,  # c=
                         #MOF_S_a81b1,  # c=

                         #MOD_S_a12b1,  # c=
                         #MOD_S_a23b1,  # c=
                         #MOD_S_a34b1,  # c=
                         #MOD_S_a45b1,  # c=
                         #MOD_S_a56b1,  # c= # PB
                         #MOD_S_a67b1,  # c=
                         #MOD_S_a78b1,  # c=
                         #MOD_S_a81b1,  # c=

                         #MCO_S_a12b1,  # c=1,2
                         #MCO_S_a23b1,  # c=1,2
                         #MCO_S_a34b1,  # c=1,2
                         #MCO_S_a45b1,  # c=  2
                         #MCO_S_a56b1,  # c=1,2
                         #MCO_S_a67b1,  # c=1,2
                         #MCO_S_a78b1,  # c=  2
                         #MCO_S_a81b1,  # c=1,2

                         MCOX_IPLeft.loc[['MCOX.3L5', 'MCOX.3L8', 'MCOX.3L1'],:],  # c=1,
                         MCOX_IPRigh.loc[['MCOX.3R5', 'MCOX.3R8', 'MCOX.3R1', 'MCOX.3R2'],:]  #],:]  # c=1,
                       ])
Oct_Cible=Oct_Cible.drop(cdrop,axis=1)

print("\nTarget RDT:")
print("-------------------------------------------------------------------------")
print("\t- Objectif:")
print("dDb_x/b0/d2Jx = {}".format(M_ABBeat_x/Max_2Jxmum))
print("dDb_y/b0/d2Jy = {}".format(M_ABBeat_y/Max_2Jymum))

print("\n\t- Target parameter:")
print(Target)

print("\n\t- Octupole cible:")
print(Oct_Cible.index.tolist())
#print(Oct_Cible.T.values)


# Solve system MX=b -> X=M^{-1}b
# -------------------------------------------------------------------------------
#M=np.array(Oct_Cible.loc[:,Target.columns.tolist()],dtype=np.complex128).T
#b=np.array(Target.values,dtype=np.complex128).T
df_M=Oct_Cible.loc[:,Target.columns.tolist()].T
df_b=Target.loc[:,Target.columns.tolist()].T

# <<<<<<<<<<<<<<<<<< Debug
print('# <<<<<<<<<<<<<<<<<< Debug')
print(df_M)
print("size M: {}".format(df_M.shape))
print(df_b)
print("size b: {}".format(df_b.shape))
print('# <<<<<<<<<<<<<<<<<< Debug')
# <<<<<<<<<<<<<<<<<< Debug

M=np.array(df_M,dtype=np.float64)
b=np.array(df_b,dtype=np.float64)

F = lambda x : b - np.dot(M,np.real(x))
#def F(x):
#  Mx   = np.dot(M,np.real(x))
#  return (b - Mx)

FF = lambda x : b*np.conj(b) - (np.dot(M,np.real(x)))*np.dot(np.conj(M),np.real(x))
#def FF(x):
#  bcb  = b*np.conj(b)
#  Mx   = np.dot(M,x)
#  cMx  = np.dot(np.conj(M),x)
#  xMcMx=Mx*cMx
#  return np.real(bcb - xMcMx)



print("\n\nSolution Linear:")
print("-------------------------------------------------------------------------")
print("Check:")
print("\t* Det: {}".format(np.linalg.det(M)))
print("\t* Eigenvalues: ")
print(np.linalg.eigvals(M).T)

print("\nResult:")
x_line=np.linalg.solve(M,b)
Oct_Cible.loc[:,"K3L"]=x_line
print(Oct_Cible.loc[:,"K3L"])

print("\nError:")
print(F(x_line))
print(FF(x_line))




#print("\n\nSolution Non-Linear:")
#print("-------------------------------------------------------------------------")
#x_noli = spop.broyden1(F, np.real(x_line),f_tol=1e-2)
#
#print("Result:")
#Oct_Cible.loc[:,"K3L"]=x_noli
#print(Oct_Cible.loc[:,"K3L"])
#
#print("Error:")
#print(F(x_noli))
#print(FF(x_noli))
#


tp="RDT exited at "+BPM_Ref.NAME+":\n"
tp+="-------------------------------------------------------------------------\n"
tp+="\t> ( H( 1, 0) , V( -, -) )  h2200 : {0}\n".format(    (Oct_Cible.K3L*Oct_Cible.ch2200).sum() )
tp+="\t> ( H( 1, 0) , V( 0, 1) )  h1111 : {0}\n".format(    (Oct_Cible.K3L*Oct_Cible.ch1111).sum() )
tp+="\t> ( H( -, -) , V( 0, 1) )  h0022 : {0}\n".format(    (Oct_Cible.K3L*Oct_Cible.ch0022).sum() )
tp+="\t> ( H( 1, 0) , V( -, -) )  c20   : {0}\n".format(    (Oct_Cible.K3L*Oct_Cible.cc20  ).sum() )
tp+="\t> ( H( -, -) , V( 0, 1) )  c02   : {0}\n".format(    (Oct_Cible.K3L*Oct_Cible.cc02  ).sum() )
tp+="\t> ( H(-3, 0) , V( -, -) ) |f4000|: {0}\n".format(abs((Oct_Cible.K3L*Oct_Cible.cf4000).sum()))
tp+="\t> ( H(-1, 0) , V( -, -) ) |f3100|: {0}\n".format(abs((Oct_Cible.K3L*Oct_Cible.cf3100).sum()))
tp+="\t> ( H(-1,-2) , V(-2,-1) ) |f2020|: {0}\n".format(abs((Oct_Cible.K3L*Oct_Cible.cf2020).sum()))
tp+="\t> ( H( 1,-2) , V( 0,-1) ) |f1120|: {0}\n".format(abs((Oct_Cible.K3L*Oct_Cible.cf1120).sum()))
tp+="\t> ( H(-1, 0) , V(-2, 1) ) |f2011|: {0}\n".format(abs((Oct_Cible.K3L*Oct_Cible.cf2011).sum()))
tp+="\t> ( H(-1, 2) , V( 2,-1) ) |f2002|: {0}\n".format(abs((Oct_Cible.K3L*Oct_Cible.cf2002).sum()))
tp+="\t> ( H( -, -) , V( 0,-3) ) |f0040|: {0}\n".format(abs((Oct_Cible.K3L*Oct_Cible.cf0040).sum()))
tp+="\t> ( H( -, -) , V( 0,-1) ) |f0031|: {0}\n".format(abs((Oct_Cible.K3L*Oct_Cible.cf0031).sum()))
print("\n\n"+tp)

if flg_print_file:
    print("\n\nPrint:")
    print("-------------------------------------------------------------------------")
    with open("Oct_Strength_AmpBetBeating.mad","w") as f:
        f.write("/*\n"+tp+"\n*/\n\n\n")
        for O in Oct_Cible.index.tolist():
            O_line=Oct_Cible.loc[O,:]
        
            kstr1=""
            kstr2=""
            if "MO."   in O:
                kstr1="kof."+O.strip("MO.")
                kstr2="kod."+O.strip("MO.")
                kmax=63100 * 6
                L=0.32
            elif "MOF."   in O:
                kstr1="kof."+O.strip("MOF.")
                kmax=63100 * 6
                L=0.32
            elif "MOD."   in O:
                kstr2="kod."+O.strip("MOD.")
                kmax=63100 * 6
                L=0.32
            elif "MCO."  in O:
                kstr1="kco."+O.strip("MCO.")
                kmax= 8150 * 6
                L=0.066
            elif "MCOX." in O:
                kstr1="kcox3."+O.strip("MCOX.3").lower()
                kmax= 9230 * 6
                L=0.137
    
            #if "MO.a56b1" in O:
            #    O_line.K3L = O_line.K3L*0.775

            #print(O)
            if kstr1:
                print(kstr1+" = {0}; // Kmax={1};".format(np.real(O_line.K3L)/L,kmax))
                f.write(kstr1+" = {0}; // Kmax={1};\n".format(O_line.K3L/L,kmax))
            if kstr2:
                print(kstr2+" = {0}; // Kmax={1};".format(np.real(O_line.K3L)/L,kmax))
                f.write(kstr2+" = {0}; // Kmax={1};\n".format(O_line.K3L/L,kmax))






#print("\n\nplot BPMs f3100:")
#print("-------------------------------------------------------------------------")
#S_list=[]; f3100b_list=[]
#for ib in BPMs.index.tolist():
#  b=BPMs.loc[ib,:]
#
#  mux_b=mux_BPM_Ref-b.MUX; muy_b=muy_BPM_Ref-b.MUY;
#  S_list.append(b.S);
#  f3100b=0;
#  f0031b=0;
#  for O in Oct_Cible.index.tolist():
#    O_line=Oct_Cible.loc[O,:]
#    f3100b+=O_line.K3L*O_line.cf3100*np.exp( (4j*np.pi)* mux_b       )
#    f0031b+=O_line.K3L*O_line.cf0031*np.exp( (4j*np.pi)*       muy_b )
#  f3100b_list.append(f3100b)
#
#fig, ax=plt.subplots()
#ax.plot([BPM_Ref.S,BPM_Ref.S],np.abs(f3100)*np.array([-1,1]),"-r",label="BPM Ref")
#ax.plot(S_list,np.imag(f3100b_list),'+',label="Im(f3100)")
#ax.plot(BPMs.S,np.abs(f3100)*np.sin((4*np.pi)* (mux_BPM_Ref-BPMs.MUX)),'x',label="Th.")
#ax.set_xlabel("S")
#ax.set_ylabel("Im(f3100)")
#ax.legend(loc="center right")
#ax.ticklabel_format(style='sci',axis='y', scilimits=(0,0),useMathText = True)
#plt.show()
#fig.savefig('Tracking/Analysis_AmpBetaBeat/Photo/Theory/f3100b_vs_S.pdf')





print("\n\nPut KnL into Oct:")
print("-------------------------------------------------------------------------")
for O in Oct_Cible.index.tolist():
    O_line=Oct_Cible.loc[O,:]
#    print(O)
#    print(O_line.index.tolist())
   
    if   O == "MO.a12b1":
        Oct.loc[MO_T_a12b1.NAME ,"K3L"]=O_line.K3L
    elif O == "MOF.a12b1":
        Oct.loc[MOF_T_a12b1.NAME,"K3L"]=O_line.K3L
    elif O == "MOD.a12b1":
        Oct.loc[MOD_T_a12b1.NAME,"K3L"]=O_line.K3L
  
    elif O == "MO.a23b1":
        Oct.loc[MO_T_a23b1.NAME ,"K3L"]=O_line.K3L
    elif O == "MOF.a23b1":
        Oct.loc[MOF_T_a23b1.NAME,"K3L"]=O_line.K3L
    elif O == "MOD.a23b1":
        Oct.loc[MOD_T_a23b1.NAME,"K3L"]=O_line.K3L
   
    elif O == "MO.a34b1":
        Oct.loc[MO_T_a34b1.NAME ,"K3L"]=O_line.K3L
    elif O == "MOF.a34b1":
        Oct.loc[MOF_T_a34b1.NAME,"K3L"]=O_line.K3L
    elif O == "MOD.a34b1":
        Oct.loc[MOD_T_a34b1.NAME,"K3L"]=O_line.K3L
  
    elif O == "MO.a45b1":
        Oct.loc[MO_T_a45b1.NAME ,"K3L"]=O_line.K3L
    elif O == "MOF.a45b1":
        Oct.loc[MOF_T_a45b1.NAME,"K3L"]=O_line.K3L
    elif O == "MOD.a45b1":
        Oct.loc[MOD_T_a45b1.NAME,"K3L"]=O_line.K3L
  
    elif O == "MO.a56b1":
        Oct.loc[MO_T_a56b1.NAME ,"K3L"]=O_line.K3L # PB
    elif O == "MOF.a56b1":
        Oct.loc[MOF_T_a56b1.NAME,"K3L"]=O_line.K3L # PB
    elif O == "MOD.a56b1":
        Oct.loc[MOD_T_a56b1.NAME,"K3L"]=O_line.K3L # PB

    elif O == "MO.a67b1":
        Oct.loc[MO_T_a67b1.NAME ,"K3L"]=O_line.K3L
    elif O == "MOF.a67b1":
        Oct.loc[MOF_T_a67b1.NAME,"K3L"]=O_line.K3L
    elif O == "MOD.a67b1":
        Oct.loc[MOD_T_a67b1.NAME,"K3L"]=O_line.K3L

    elif O == "MO.a78b1":
        Oct.loc[MO_T_a78b1.NAME ,"K3L"]=O_line.K3L
    elif O == "MOF.a78b1":
        Oct.loc[MOF_T_a78b1.NAME,"K3L"]=O_line.K3L
    elif O == "MOD.a78b1":
        Oct.loc[MOD_T_a78b1.NAME,"K3L"]=O_line.K3L

    elif O == "MO.a81b1":
        Oct.loc[MO_T_a81b1.NAME ,"K3L"]=O_line.K3L
    elif O == "MOF.a81b1":
        Oct.loc[MOF_T_a81b1.NAME,"K3L"]=O_line.K3L
    elif O == "MOD.a81b1":
        Oct.loc[MOD_T_a81b1.NAME,"K3L"]=O_line.K3L

    elif O == "MCO.a12b1":
        Oct.loc[MCO_T_a12b1.NAME,"K3L"]=O_line.K3L
    elif O == "MCO.a23b1":
        Oct.loc[MCO_T_a23b1.NAME,"K3L"]=O_line.K3L
    elif O == "MCO.a34b1":
        Oct.loc[MCO_T_a34b1.NAME,"K3L"]=O_line.K3L
    elif O == "MCO.a45b1":
        Oct.loc[MCO_T_a45b1.NAME,"K3L"]=O_line.K3L
    elif O == "MCO.a56b1":
        Oct.loc[MCO_T_a56b1.NAME,"K3L"]=O_line.K3L
    elif O == "MCO.a67b1":
        Oct.loc[MCO_T_a67b1.NAME,"K3L"]=O_line.K3L
    elif O == "MCO.a78b1":
        Oct.loc[MCO_T_a78b1.NAME,"K3L"]=O_line.K3L
    elif O == "MCO.a81b1":
        Oct.loc[MCO_T_a81b1.NAME,"K3L"]=O_line.K3L

    else:
        Oct.loc[O,"K3L"]=O_line.K3L
    #else:
    #    sys.exit("ERROR: Couldn't find "+O+" in Oct!!!")



# Get correct RDTs

# Compute Coef RDTs
# -------------------------------------------------------------------------------
mux1 =mux_BPM_Ref -Oct.MUX;  mux=np.array([x+Qx if x<0 else x for x in mux1]);
muy1 =muy_BPM_Ref -Oct.MUY;  muy=np.array([y+Qy if y<0 else y for y in muy1]);

betx=Oct.BETX;  bety=Oct.BETY
dx  =Oct.DX;    dy  =Oct.DY;
betxbetx=(betx*betx)
betxbety=(betx*bety)
betybety=(bety*bety)

# -------------------------------------------------------------
# Equ. RDTs for octupole only:
# -------------------------------------------------------------
#
#   f_{4000} =(-1/384) \sum K3L bet_x^2 e^{8\pi \mu_x}/(1-e^{8\pi Q_x})
#   f_{3100} =(-1/ 96) \sum K3L bet_x^2 e^{4\pi \mu_x}/(1-e^{4\pi Q_x})
#   h_{2200} =( 3/  8) \sum K3L bet_x^2
#
#   f_{2020} =(-1/ 64) \sum K3L bet_x bet_y e^{4\pi(\mu_x+\mu_y)}/(1-e^{4\pi(Q_x+Q_y)})
#   f_{2011} =( 1/ 32) \sum K3L bet_x bet_y e^{4\pi \mu_x       }/(1-e^{4\pi Q_x     })
#   f_{1120} =( 1/ 32) \sum K3L bet_x bet_y e^{4\pi       \mu_y }/(1-e^{4\pi     Q_y })
#   f_{2002} =(-1/ 64) \sum K3L bet_x bet_y e^{4\pi(\mu_x-\mu_y)}/(1-e^{4\pi(Q_x-Q_y)})
#   h_{1111} =(-6/  4) \sum K3L bet_x bet_y
#
#   f_{0040} =( 1/384) \sum K3L bet_y^2 e^{8\pi \mu_y}/(1-e^{8\pi Q_y})
#   f_{0031} =( 1/ 96) \sum K3L bet_y^2 e^{4\pi \mu_y}/(1-e^{4\pi Q_y})
#   h_{0022} =( 3/  8) \sum K3L bet_y^2
#
# -------------------------------------------------------------


#      * Amplitude and cross-detuning coef
#Oct.loc[:,"ch2200"] =( 3/ 8)*betxbetx/(12*np.pi)    # Horizontal plane
#Oct.loc[:,"ch1111"] =(-3/ 2)*betxbety/(12*np.pi)    # Cross plane
#Oct.loc[:,"ch0022"] =( 3/ 8)*betybety/(12*np.pi)    # Vertical plane

#      * Chromaticity coef
#Oct.loc[:,"cc20"] =( 3/ 2)*(betx*(dx*dx))/(12*np.pi)# Horizontal plane
#Oct.loc[:,"cc02"] =( 3/ 2)*(bety*(dx*dx))/(12*np.pi)    # Vertical plane

inv_384=1.0e0/(384e0)
inv_96 =1.0e0/( 96e0)
inv_64 =1.0e0/( 64e0)
inv_32 =1.0e0/( 32e0)
#      * Other RDTs coef
#              - Horizontal plane
Oct.loc[:,"ccf4000"] =-( betxbetx * inv_384 )/( 1-np.exp( (8j*np.pi)* Qx    ) )
Oct.loc[:,"ccf3100"] =-( betxbetx * inv_96  )/( 1-np.exp( (4j*np.pi)* Qx    ) )
#              - Cross plane
Oct.loc[:,"ccf2020"] =-( betxbety * inv_64  )/( 1-np.exp( (4j*np.pi)*(Qx+Qy)) )
Oct.loc[:,"ccf2011"] =-(-betxbety * inv_32  )/( 1-np.exp( (4j*np.pi)* Qx    ) )
Oct.loc[:,"ccf1120"] =-(-betxbety * inv_32  )/( 1-np.exp( (4j*np.pi)*    Qy ) )
Oct.loc[:,"ccf2002"] =-( betxbety * inv_64  )/( 1-np.exp( (4j*np.pi)*(Qx-Qy)) )
#              - Vertical plane
Oct.loc[:,"ccf0040"] =-( betybety * inv_384 )/( 1-np.exp( (8j*np.pi)*    Qy ) )
Oct.loc[:,"ccf0031"] =-( betybety * inv_96  )/( 1-np.exp( (4j*np.pi)*    Qy ) )




S_list=[];
f4000b_list=[];  f4000b2_list=[];
f3100b_list=[];  f3100b2_list=[];
f2020b_list=[];  f2020b2_list=[];
f2011b_list=[];  f2011b2_list=[];
f1120b_list=[];  f1120b2_list=[];
f2002b_list=[];  f2002b2_list=[];
f0040b_list=[];  f0040b2_list=[];
f0031b_list=[];  f0031b2_list=[];
for ib in BPMs.index.tolist():
    b=BPMs.loc[ib,:]

    f3100b=0;  f3100b2=0;
    f0031b=0;  f0031b2=0;
    mux1 =b.MUX -Oct.MUX;  mux2=np.array([x+Qx if x<0 else x for x in mux1]); 
    muy1 =b.MUX -Oct.MUY;  muy2=np.array([x+Qy if x<0 else x for x in muy1]);

#    #      * Amplitude and Cross detuning coef
#    h2200b  =(Oct.K3L*Oct.ch2200).sum()    # Horizontal plane
#    h1111b  =(Oct.K3L*Oct.ch1100).sum()    # Cross plane
#    h0022b  =(Oct.K3L*Oct.ch0022).sum()    # Vertical plane
#    #      * Chromaticity coef
#    c20b    =(Oct.K3L*Oct.cc20).sum()    # Horizontal plane
#    c02b    =(Oct.K3L*Oct.cc02).sum()    # Vertical plane
    #      * Other RDTs coef
    #              - Horizontal plane
    f4000b  =(Oct.K3L*Oct.ccf4000*np.exp( (8j*np.pi)* mux2       )).sum()
    f3100b  =(Oct.K3L*Oct.ccf3100*np.exp( (4j*np.pi)* mux2       )).sum()
    #              - Cross plane
    f2020b  =(Oct.K3L*Oct.ccf2020*np.exp( (4j*np.pi)*(mux2+muy2) )).sum()
    f2011b  =(Oct.K3L*Oct.ccf2011*np.exp( (4j*np.pi)* mux2       )).sum()
    f1120b  =(Oct.K3L*Oct.ccf1120*np.exp( (4j*np.pi)*      muy2  )).sum()
    f2002b  =(Oct.K3L*Oct.ccf2002*np.exp( (4j*np.pi)*(mux2-muy2) )).sum()
    #              - Vertical plane
    f0040b  =(Oct.K3L*Oct.ccf0040*np.exp( (8j*np.pi)*      muy2  )).sum()
    f0031b  =(Oct.K3L*Oct.ccf0031*np.exp( (4j*np.pi)*      muy2  )).sum()


    #for O in Oct.index.tolist():
    #  O_line=Oct.loc[O,:]
    #  mux_b=O_line.MUX-b.MUX; muy_b=O_line.MUY-b.MUY;
    #  f3100b+=O_line.K3L*O_line.cf3100*np.exp( (4j*np.pi)* mux_b       )
    #  f0031b+=O_line.K3L*O_line.cf0031*np.exp( (4j*np.pi)*       muy_b )

    #  #f3100b2+=O_line.K3L*O_line.cf3100*np.exp( (4j*np.pi)* mux_b       )
    #  #f0031b2+=O_line.K3L*O_line.cf0031*np.exp( (4j*np.pi)*       muy_b )
    #  if O_line.S<b.S:
    #    f3100b2+=O_line.K3L*O_line.cf3100*np.exp( (4j*np.pi)* mux_b       )
    #    f0031b2+=O_line.K3L*O_line.cf0031*np.exp( (4j*np.pi)*       muy_b )
    S_list.append(b.S);
    f4000b_list.append(f4000b);  f3100b_list.append(f3100b)

    f2020b_list.append(f2020b);  f2011b_list.append(f2011b)
    f1120b_list.append(f1120b);  f2002b_list.append(f2002b)

    f0040b_list.append(f0040b);  f0031b_list.append(f0031b)
    #f3100b2_list.append(f3100b2)

    if (BPM_Ref.NAME==b.NAME) and True:
        if f4000!=0:
            f4000=f4000b
        if f3100!=0:
            f3100=f3100b

        if f2020!=0:
            f2020=f2020b
        if f2011!=0:
            f2011=f2011b
        if f2002!=0:
            f2002=f2002b
        if f1120!=0:
            f1120=f1120b

        if f0040!=0:
            f0040=f0040b
        if f0031!=0:
            f0031=f0031b




fig, ax=plt.subplots()
ax.plot(S_list,               np.imag(f3100b_list)          ,'+' ,label="Im(f3100)")
ax.plot(S_list,               np.real(f3100b_list)          ,'x' ,label="Re(f3100)")
ax.plot(S_list,               np.abs(f3100b_list)           ,'-' ,label="|f3100|")
ax.plot([BPM_Ref.S,BPM_Ref.S],np.abs(f3100)*np.array([-1,1]),"-r",label="BPM Ref",alpha=.7)
ax.plot([BPM_Ref.S,BPM_Ref.S],np.array([0,0]),               ".r",alpha=.7)
ax.set_xlabel(r"S [m]")
ax.set_ylabel(r"f3100 [m$^{-1}$]")
ax.legend()#loc="center right")
ax.ticklabel_format(style='sci',axis='y', scilimits=(0,0),useMathText = True)
fig.savefig('Photo/Theory/f3100b_vs_S_c'+config+'.pdf')

#fig, ax=plt.subplots()
#ax.plot(S_list,               np.imag(f4000b_list)          ,'+' ,label="Im(f4000)")
#ax.plot(S_list,               np.real(f4000b_list)          ,'x' ,label="Re(f4000)")
#ax.plot(S_list,               np.abs(f4000b_list)           ,'-' ,label="|f4000|")
#ax.plot([BPM_Ref.S,BPM_Ref.S],np.abs(f4000)*np.array([-1,1]),"-r",label="BPM Ref",alpha=.7)
#ax.plot([BPM_Ref.S,BPM_Ref.S],np.array([0,0]),               ".r",alpha=.7)
#ax.set_xlabel(r"S [m]")
#ax.set_ylabel(r"f4000 [m$^{-1}$]")
#ax.legend()#loc="center right")
#ax.ticklabel_format(style='sci',axis='y', scilimits=(0,0),useMathText = True)
#fig.savefig('Photo/Theory/f4000b_vs_S_c'+config+'.pdf')


fig, ax=plt.subplots()
ax.plot(S_list,               np.imag(f0031b_list)          ,'+' ,label="Im(f0031)")
ax.plot(S_list,               np.real(f0031b_list)          ,'x' ,label="Re(f0031)")
ax.plot(S_list,               np.abs(f0031b_list)           ,'-' ,label="|f0031|")
ax.plot([BPM_Ref.S,BPM_Ref.S],np.abs(f0031)*np.array([-1,1]),"-r",label="BPM Ref",alpha=.7)
ax.plot([BPM_Ref.S,BPM_Ref.S],np.array([0,0])               ,".r",alpha=.7)
ax.set_xlabel(r"S [m]")
ax.set_ylabel(r"f0031 [m$^{-1}$]")
ax.legend()#loc="center right")
ax.ticklabel_format(style='sci',axis='y', scilimits=(0,0),useMathText = True)
fig.savefig('Photo/Theory/f0031b_vs_S_c'+config+'.pdf')


fig, ax=plt.subplots()
ax.plot(S_list,               np.imag(f1120b_list)          ,'+' ,label="Im(f1120)")
ax.plot(S_list,               np.real(f1120b_list)          ,'+' ,label="Re(f1120)")
ax.plot(S_list,               np.abs(f1120b_list)           ,'-' ,label="|f1120|")
ax.plot([BPM_Ref.S,BPM_Ref.S],np.abs(f1120)*np.array([-1,1]),"-r",label="BPM Ref",alpha=.7)
ax.plot([BPM_Ref.S,BPM_Ref.S],np.array([-1,1]),              ".r",alpha=.7)
ax.set_xlabel(r"S [m]")
ax.set_ylabel(r"f1120 [m$^{-1}$]")
ax.legend()#loc="center right")
ax.ticklabel_format(style='sci',axis='y', scilimits=(0,0),useMathText = True)
fig.savefig('Photo/Theory/f1120b_vs_S_c'+config+'.pdf')


fig, ax=plt.subplots()
ax.plot(S_list,               np.imag(f2011b_list)          ,'+' ,label="Im(f2011)")
ax.plot(S_list,               np.real(f2011b_list)          ,'+' ,label="Re(f2011)")
ax.plot(S_list,               np.abs(f2011b_list)           ,'-' ,label="|f2011|")
ax.plot([BPM_Ref.S,BPM_Ref.S],np.abs(f1120)*np.array([-1,1]),"-r",label="BPM Ref",alpha=.7)
ax.plot([BPM_Ref.S,BPM_Ref.S],np.array([-1,1]),              ".r",alpha=.7)
ax.set_xlabel(r"S [m]")
ax.set_ylabel(r"f2011 [m$^{-1}$]")
ax.legend()#loc="center right")
ax.ticklabel_format(style='sci',axis='y', scilimits=(0,0),useMathText = True)
fig.savefig('Photo/Theory/f2011b_vs_S_c'+config+'.pdf')
plt.show()






