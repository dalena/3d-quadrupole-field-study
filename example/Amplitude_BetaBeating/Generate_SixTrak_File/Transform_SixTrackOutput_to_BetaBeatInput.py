#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 12 13:16:14 2019

@author: tpugnat
"""

# Import libraryand usefull function
import sys
import pathlib

import re
import time

import pandas as pd

#import matplotlib.pyplot as plt
#from matplotlib.pyplot import rcParams, rc


# Speciial class
sys.path.insert(0, '/local/home/tpugnat/Documents/CEA/SixTrack/These_Workplace/Combin/NAFF/python/')
from class_case_Detuning_Simulation import Case_Detuning_Simulated as cas_DT_Sm

# Set plot environement
#rcParams.update({'font.size': 14})
#rc('xtick', labelsize=14)
#rc('ytick', labelsize=14)



case_l=["nom_allerrors_MCOX0_MCTX0","nom_b4only_MCOX0","nom_b6only_MCTX0","ricc_allerrors_MCOX0_MCTX0","ricc_b6only_MCTX0"] # ["noerrors","nom_allerrors","nom_b4only","nom_b6only","ricc_allerrors","ricc_b6only"] #["tst"] #
# 
workplace="/local/home/tpugnat/Documents/CEA/SixTrack/These_Workplace/SixTrack/bpms_optics/"
file_type="bpm*_Data_Part.txt"
for ca in case_l:
    path=workplace+ca+"/Output_"+ca+"/"
    
    
    currentDirectory = pathlib.Path(path);
    # get file in directory
    files=[];
    for currentFile in currentDirectory.glob(file_type):
        files.append(currentFile)
    
    
    bpms= [re.sub("_Data_Part.txt","",str(x)) for x in files]
    bpms= [re.sub(path,"",str(x)) for x in bpms]
    
    
    # Generate all the bpms
    c_bpms=[]
    for i in range(len(bpms)): #3): #
        file=re.sub(path,"",str(files[i]))
        bpm =re.sub(path,"",str(bpms[i] ))
        c=cas_DT_Sm(bpm,case_col="",path=path, file_type=file)
        c_bpms.append(c)
    
    # Generate thet File with the data
    nbpart=30 #
    nbturn=1000
    nbHBPM=len(c_bpms)
    nbVBPM=len(c_bpms)

    # Sort the file with the data
#    currentDirectory = pathlib.Path(path);
#    #     - clean directory
#    for p in range(nbpart):
#        os.remove(path+'BPMs_Part{0}_sorted.sdds'.format(p+1))

    #     - sort
    tp=pd.read_csv(workplace+"twiss.dat",skiprows=48,sep='\s+',header=None)
    #print(tp.head(5))
    #print(tp[1].head(5))
    bpm_tfs=tp.loc[:,0]
    #print(bpm_tfs.head(5))
    for p in range(nbpart):
        d=pd.read_csv(path+'BPMs_Part{0}.sdds'.format(p+1),header=None,skiprows=6,sep='\s+',error_bad_lines=False)
        bpm_ndata=d.loc[:,1].values
        bpm_adata=d.loc[:,0].values
        d=d.set_index([pd.Index(bpm_adata),pd.Index(bpm_ndata)])
        #print(d.head(5))

        list_idx=[x for x in bpm_tfs if x in bpm_ndata];
        with open(path+'BPMs_Part{0}_sorted.sdds'.format(p+1), 'w') as the_file:
            the_file.write('#SDDSDASCIIFORMAT v1 \n')
            the_file.write('#Created: '+time.strftime("%Y-%m-%d at %H:%M:%S")+' By: Thomas PUGNAT - Python SDDS converter\n')
            the_file.write('#Numbers of turns: '+str(nbturn)+'\n')
            the_file.write('#Numbers of horizontal monitors: '+str(nbHBPM)+'\n')
            the_file.write('#Numbers of vertical monitors: '+str(nbVBPM)+'\n')
            the_file.write('#Acquisition date: '+time.strftime("%Y-%m-%d at %H:%M:%S")+'\n')
            for idx in list_idx:
                linex=d.loc[(0,idx),:]
                liney=d.loc[(1,idx),:]
                #print(linex)

                str_b='0 {0} {1}  '.format(linex[1],0.0)
                str_p=re.sub("\n","",str(linex[3:].values)[1:-1])
                the_file.write(str_b+str_p+'\n')

                str_b='1 {0} {1}  '.format(liney[1],0.0)
                str_p=re.sub("\n","",str(liney[3:].values)[1:-1])
                the_file.write(str_b+str_p+'\n')
        the_file.close()
        
#FORMAT_STRING="{:.6f}