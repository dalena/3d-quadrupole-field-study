#!/usr/bin/env python3
import sys
import matplotlib.pyplot as plt


sys.path.insert(0, '../../detuning_analysis/')
from class_case_Detuning_Simulation import Case_Detuning_Simulated as cas_DT_Sm


sys.path.insert(0, '../../detuning_prediction/')
from class_case_Detuning_Theoric    import case_Detuning_Theoric   as cas_DT_Th


plt.rcParams.update({'font.size': 14})
plt.rc('xtick', labelsize=14) 
plt.rc('ytick', labelsize=14) 








# --------------------------------------------------------------------------------
# Parameter
# --------------------------------------------------------------------------------
flg_save=False
drtsv="/home/tpugnat/Documents/CEA/SixTrack/These_Workplace/SixTrack/Test_Dump2/Result_DetuningwAmp/Python_output/"


axis="y";                           # Axis of the action increase
QrefX=0.32; QrefY=0.32;             # Reference Tunes
Q2_mctx1={'x':0.0155, 'y':0.0145}   # Default 1st order amplitude detuning (cross not implemented yet)
max_2Jum=0.038                      # Action max shown








# --------------------------------------------------------------------------------
# Data to analyse
# --------------------------------------------------------------------------------
#      - Theory
# --------------------------------------------------------------------------------
Analytic_mctx1=[]
drt="/home/tpugnat/Documents/CEA/SixTrack/These_Workplace/SixTrack/Test_Dump2/"
#               * Nominal
#                   > Define case
c=cas_DT_Th(case_name="HE Analytic",case_col="g", \
            path_opt=drt+"six_inp_flatbeam_nominal_corupto6_hllhcv1.0_triponly_withb4/", \
#            path_err=drt+"six_inp_flatbeam_nominal_corupto6_hllhcv1.0_triponly_withb4/temp_1/")
            path_err=drt+"six_inp_flatbeam_nominal_corupto6_hllhcv1.0_onlyb6/temp_1/")
#                   > Add Quad
#                   > Add Skip
#                   > Add File
#                   > Save case
Analytic_mctx1.append(c)

#               * Lie2 ND0
#                   > Define case
c=cas_DT_Th(case_name="Lie2 ND0 Analytic",case_col="b", \
            path_opt=drt+"six_inp_flatbeam_Lie2ND0_corupto6_hllhcv1.0_triponly_withb4/", \
#            path_err=drt+"six_inp_flatbeam_Lie2ND0_corupto6_hllhcv1.0_triponly_withb4/temp_1/")
            path_err=drt+"six_inp_flatbeam_Lie2ND0_corupto6_hllhcv1.0_onlyb6/temp_1/")
#                   > Add Quad
c.add_Quad("MQXFA.A1L5..1" ,5 , 1); c.add_Quad("MQXFA.A1L5..16",10, 0)
c.add_Quad("MQXFA.B1L5..1" ,4 , 1); c.add_Quad("MQXFA.B1L5..16",12, 0)
c.add_Quad("MQXFB.A2L5..1" ,3 , 1); c.add_Quad("MQXFB.A2L5..16",8 , 0)
c.add_Quad("MQXFB.B2L5..1" ,1 , 1); c.add_Quad("MQXFB.B2L5..16",7 , 0)
c.add_Quad("MQXFA.A3L5..1" ,5 , 1); c.add_Quad("MQXFA.A3L5..16",10, 0)
c.add_Quad("MQXFA.B3L5..1" ,4 , 1); c.add_Quad("MQXFA.B3L5..16",12, 0)
c.add_Quad("MQXFA.A3R5..1" ,3 , 1); c.add_Quad("MQXFA.A3R5..16",9 , 0)
c.add_Quad("MQXFA.B3R5..1" ,2 , 1); c.add_Quad("MQXFA.B3R5..16",7 , 0)
c.add_Quad("MQXFB.A2R5..1" ,6 , 1); c.add_Quad("MQXFB.A2R5..16",10, 0)
c.add_Quad("MQXFB.B2R5..1" ,4 , 1); c.add_Quad("MQXFB.B2R5..16",11, 0)
c.add_Quad("MQXFA.A1R5..1" ,3 , 1); c.add_Quad("MQXFA.A1R5..16",9 , 0)
c.add_Quad("MQXFA.B1R5..1" ,2 , 1); c.add_Quad("MQXFA.B1R5..16",7 , 0)
c.add_Quad("MQXFA.A1L1..1" ,5 , 1); c.add_Quad("MQXFA.A1L1..16",10, 0)
c.add_Quad("MQXFA.B1L1..1" ,4 , 1); c.add_Quad("MQXFA.B1L1..16",12, 0)
c.add_Quad("MQXFB.A2L1..1" ,3 , 1); c.add_Quad("MQXFB.A2L1..16",8 , 0)
c.add_Quad("MQXFB.B2L1..1" ,1 , 1); c.add_Quad("MQXFB.B2L1..16",7 , 0)
c.add_Quad("MQXFA.A3L1..1" ,5 , 1); c.add_Quad("MQXFA.A3L1..16",10, 0)
c.add_Quad("MQXFA.B3L1..1" ,4 , 1); c.add_Quad("MQXFA.B3L1..16",12, 0)
c.add_Quad("MQXFA.A3R1..1" ,3 , 1); c.add_Quad("MQXFA.A3R1..16",9 , 0)
c.add_Quad("MQXFA.B3R1..1" ,2 , 1); c.add_Quad("MQXFA.B3R1..16",7 , 0)
c.add_Quad("MQXFB.A2R1..1" ,6 , 1); c.add_Quad("MQXFB.A2R1..16",10, 0)
c.add_Quad("MQXFB.B2R1..1" ,4 , 1); c.add_Quad("MQXFB.B2R1..16",11, 0)
c.add_Quad("MQXFA.A1R1..1" ,3 , 1); c.add_Quad("MQXFA.A1R1..16",9 , 0)
c.add_Quad("MQXFA.B1R1..1" ,2 , 1); c.add_Quad("MQXFA.B1R1..16",7 , 0)
#                   > Add Skip
c.add_Skip("MQXFA.B3L5..FL"); c.add_Skip("MQXFA.A3L5..FL"); c.add_Skip("MQXFB.B2L5..FL")
c.add_Skip("MQXFB.A2L5..FL"); c.add_Skip("MQXFA.B1L5..FL"); c.add_Skip("MQXFA.A1L5..FL")
c.add_Skip("MQXFA.A1R5..FL"); c.add_Skip("MQXFA.B1R5..FL"); c.add_Skip("MQXFB.A2R5..FL")
c.add_Skip("MQXFB.B2R5..FL"); c.add_Skip("MQXFA.A3R5..FL"); c.add_Skip("MQXFA.B3R5..FL")
c.add_Skip("MQXFA.B3L1..FL"); c.add_Skip("MQXFA.A3L1..FL"); c.add_Skip("MQXFB.B2L1..FL")
c.add_Skip("MQXFB.A2L1..FL"); c.add_Skip("MQXFA.B1L1..FL"); c.add_Skip("MQXFA.A1L1..FL")
c.add_Skip("MQXFA.A1R1..FL"); c.add_Skip("MQXFA.B1R1..FL"); c.add_Skip("MQXFB.A2R1..FL")
c.add_Skip("MQXFB.B2R1..FL"); c.add_Skip("MQXFA.A3R1..FL"); c.add_Skip("MQXFA.B3R1..FL")
c.add_Skip("MQXFA.B3L5..FR"); c.add_Skip("MQXFA.A3L5..FR"); c.add_Skip("MQXFB.B2L5..FR")
c.add_Skip("MQXFB.A2L5..FR"); c.add_Skip("MQXFA.B1L5..FR"); c.add_Skip("MQXFA.A1L5..FR")
c.add_Skip("MQXFA.A1R5..FR"); c.add_Skip("MQXFA.B1R5..FR"); c.add_Skip("MQXFB.A2R5..FR")
c.add_Skip("MQXFB.B2R5..FR"); c.add_Skip("MQXFA.A3R5..FR"); c.add_Skip("MQXFA.B3R5..FR")
c.add_Skip("MQXFA.B3L1..FR"); c.add_Skip("MQXFA.A3L1..FR"); c.add_Skip("MQXFB.B2L1..FR")
c.add_Skip("MQXFB.A2L1..FR"); c.add_Skip("MQXFA.B1L1..FR"); c.add_Skip("MQXFA.A1L1..FR")
c.add_Skip("MQXFA.A1R1..FR"); c.add_Skip("MQXFA.B1R1..FR"); c.add_Skip("MQXFB.A2R1..FR")
c.add_Skip("MQXFB.B2R1..FR"); c.add_Skip("MQXFA.A3R1..FR"); c.add_Skip("MQXFA.B3R1..FR")
#                   > Add File
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_in_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m.out"             ,5.91783358704972118e-01,True ) #,1.420
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_in_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_mid.out"         ,5.91783358704972118e-01,True ) #,1.420
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_in_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_inv.out"         ,6.29915713814027667e-01,True ) #,1.620
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_in_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_inv_opp.out"     ,6.29915713814027667e-01,True ) #,1.620
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_in_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_opp_mid.out"     ,5.91783358704972118e-01,True ) #,1.420
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_in_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_opp.out"         ,5.91783358704972118e-01,True ) #,1.420
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_out_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m.out"            ,6.09915713246783286e-01,False) #,1.620
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_out_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_inv.out"        ,5.71783359230295996e-01,False) #,1.420
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_out_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_inv_mid.out"    ,5.71783359230295996e-01,False) #,1.420
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_out_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_opp.out"        ,6.09915713246783286e-01,False) #,1.620
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_out_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_inv_opp.out"    ,5.71783359230295996e-01,False) #,1.420
c.add_File(drt+"C2-6-10-14_ND0_dz02_AF/coeff_out_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_inv_opp_mid.out",5.71783359230295996e-01,False) #,1.420
#                   > Save case
Analytic_mctx1.append(c)




#      - Simulation (SixTrack DUMP Output)
# --------------------------------------------------------------------------------
drt="/home/tpugnat/Documents/CEA/SixTrack/These_Workplace/SixTrack/Test_Dump2/"
#           - Case x
# --------------------------------------------------------------------------------
Trackingx_mctx1=[]
#               * nosextnoerr
#                   > Define case
c=cas_DT_Sm("MCTX ON HE Tracking",case_col="g",path=drt+"track_nominal/",   \
            file_type="onlyb6/ip3_Data_Part_Rat999999_mctx1.txt")
#                   > Save case
Trackingx_mctx1.append(c)

#               * b3Ran_1
#                   > Define case
c=cas_DT_Sm("MCTX ON Lie2 ND0 Tracking",case_col="b",path=drt+"track_Lie2_ND0/",   \
#            file_type="onlyb6/ip3_Data_Part_Rat999999_C2-6_mctx1.txt")
            file_type="onlyb6/ip3_Data_Part_Rat999999_C6_mctx1.txt")
#                   > Save case
Trackingx_mctx1.append(c)





# --------------------------------------------------------------------------------
# Data analysis and Prediction
# --------------------------------------------------------------------------------
#     - Compute detuning from analytic data
# --------------------------------------------------------------------------------
for t in Analytic_mctx0:
    print(t.case_name)
  
    t.load_File()
  
    t.load_FFFile()
    t.DTune_Chance()




#     - Compute detuning from tracking data
# --------------------------------------------------------------------------------
for f in Trackingx_mctx1:
    print(f.case_name)
  
    # Set parameter
    f.set_refBPara(mx=0.31,my=0.32,                          \
                   bx=121.566844007144,ax=2.29573100013046,  \
                   by=218.58505998061,ay=-2.64288999961551)
    f.set_refAPara(lim=0.05,eps_f=1e-5,flg_ftr=True,flg_dbg=False)

    # Get File to read containing the position
    f.getFile();

    # Read File and combine
    f.read_FilesandCombine();

    # fft
    f.analyse_Data()

    # refine fft
    f.refine_analyse_Data()





# --------------------------------------------------------------------------------
# Presentation of the results
# --------------------------------------------------------------------------------
print("# --------------------------------------------------------------------- #")
print("#                      Amplitude Detuning Analysis                      #")
print("# --------------------------------------------------------------------- #")
print("Result of the amplitude detuning for the "+axis+" axis:")
# Detuning with amplitude
# -----------------------------------------------------------------------------
print("       * Fitting of the Amplitude Detuning:")
print("              - Horizontal detuning:")
fig, ax = plt.subplots()
for f in Trackingx_mctx1:
    #f.fit_data(x_lab="2J"+axis+"[um]",y_lab="QX",n_fit=2)
    #f.fit_data(x_lab="2J"+axis+"[um]",y_lab="QX",n_fit=3)
    f.fit_data(x_lab="2J"+axis+"[um]",y_lab="Qx",n_fit=4,meth=1,Q2=Q2_mctx1["x"])
    f.plot_DQ(fig,ax,x_lab_list="2J"+axis,y_lab_list="Qx",meth=0,Qref=QrefX,Q2=Q2_mctx1["x"],sigmaQ2=1e-3,fmt="x-",tt="",flg_Jerr=False)


fig, plt.ylabel('Q$_x$')
fig, plt.subplots_adjust(left=0.17, right=0.97, top=0.92, bottom=0.12)
ax.ticklabel_format(style='sci',axis='both', scilimits=(0,0),useMathText = True)
#      - Save plot
if flg_save:
    fig1.savefig(drtsv+'flatbeam/DetuningWithAmplitudeX_vs_2J'+axis+'.pdf')
plt.show()



print("\n\n              - Vertical detuning:")
fig, ax = plt.subplots()
for f in Trackingx_mctx1:
    #f.fit_data(x_lab="2J"+axis+"[um]",y_lab="QY",n_fit=2)
    #f.fit_data(x_lab="2J"+axis+"[um]",y_lab="QY",n_fit=3)
    f.fit_data(x_lab="2J"+axis+"[um]",y_lab="Qy",n_fit=4,meth=1,Q2=Q2_mctx1["y"])
    f.plot_DQ(fig,ax,x_lab_list="2J"+axis,y_lab_list="Qy",meth=0,Qref=QrefY,Q2=Q2_mctx1["y"],sigmaQ2=1e-3,fmt="x-",tt="",flg_Jerr=False)


fig, plt.ylabel('Q$_y$')
fig, plt.subplots_adjust(left=0.17, right=0.97, top=0.92, bottom=0.12)
ax.ticklabel_format(style='sci',axis='both', scilimits=(0,0),useMathText = True)
#      - Save plot
if flg_save:
    fig1.savefig(drtsv+'DetuningWithAmplitudeY_vs_2J'+axis+'_CompMethod.pdf')
plt.show()



# Detuning with amplitude with correction linear part
# -----------------------------------------------------------------------------
print("\n\n       * Comparison Amplitude Detuning from theory and simulation:")
print("              - Horizontal detuning:")
fig, ax = plt.subplots()
for t in Analytic_mctx1:
    t.Print_DTune()
    t.Plot_DTune(fig,ax,axis="x",arr_2Jum=[0,max_2Jum])

for f in Trackingx_mctx1:
    f.set_refAPara(lim=0.05,eps_f=1e-5,flg_ftr=False,flg_dbg=True)
    f.plot_DQ(fig,ax,x_lab_list="2J"+axis,y_lab_list="Qx",meth=1,Qref=QrefX,Q2=Q2_mctx0["x"],sigmaQ2=1e-3,fmt="x",tt="",flg_Jerr=True)

fig, plt.ylabel('$\Delta$Q$_x$-C$_1$*2J'+axis)
fig, plt.subplots_adjust(left=0.17, right=0.97, top=0.92, bottom=0.12)
ax.ticklabel_format(style='sci',axis='both', scilimits=(0,0),useMathText = True)
ax.set_ylim(-2.5e-3,5e-4)
ax.legend(prop={'size': 10})
#      - Save plot
if flg_save:
    fig.savefig(drtsv+'DetuningWithAmplitudeX_vs_2J'+axis+'_CompMethod+Analytic.pdf')

#plt.grid()
plt.show()



print("\n\n              - Vertical detuning:")
fig, ax = plt.subplots()
for t in Analytic_mctx1:
    t.Print_DTune()
    t.Plot_DTune(fig,ax,axis="y",arr_2Jum=[0,max_2Jum])

for f in Trackingx_mctx1:
    f.set_refAPara(lim=0.05,eps_f=1e-5,flg_ftr=False,flg_dbg=True)
    f.plot_DQ(fig,ax,x_lab_list="2J"+axis,y_lab_list="Qy",meth=1,Qref=QrefY,Q2=Q2_mctx0["y"],sigmaQ2=1e-3,fmt="x",tt="",flg_Jerr=True)

fig, plt.ylabel('$\Delta$Q$_y$-C$_1$*2J'+axis)
fig, plt.subplots_adjust(left=0.17, right=0.97, top=0.92, bottom=0.12)
ax.ticklabel_format(style='sci',axis='both', scilimits=(0,0),useMathText = True)
ax.set_ylim(-2.5e-3,5e-4)
ax.legend(prop={'size': 10})
#      - Save plot
if flg_save:
    fig.savefig(drtsv+'DetuningWithAmplitudeY_vs_2J'+axis+'_CompMethod+Analytic.pdf')

#plt.grid()
plt.show()
























