#!/usr/bin/env python3
import sys
import pandas as pd


sys.path.insert(0, '../../detuning_analysis/detuning_prediction/')
from class_case_Detuning_Theoric    import case_Detuning_Theoric   as cas_DT_Th




# Data analytic
# -----------------------------------------------------------------------------------
drt=       "../optics/HL-LHC/"
drt_VecPot="../Vector_Potential/"

#         * nominal
Analytic_Nom=[]
for i in range(1,61):
	#             > Define case
	c=cas_DT_Th(case_name="HE Analytic s"+str(i),case_col="g", \
		    path_opt=drt+"Ref/", \
                    path_err=drt+"six_inp_flatbeam_nominal_corupto6_hllhcv1.0_triponly_withb4/temp_"+str(i)+"/")
	#             > Save case
	Analytic_Nom.append(c)


#         * Riccardo
Analytic_Ric=[]
for i in range(1,61):
	#             > Define case
	c=cas_DT_Th(case_name="HE+Heads Analytic s"+str(i),case_col="r", \
		    path_opt=drt+"Ref/", \
		    path_err=drt+"six_inp_flatbeam_riccardo_corupto6_hllhcv1.0_triponly_withb4/temp_"+str(i)+"/")
	#             > Save case
	Analytic_Ric.append(c)


#         * ND0
Analytic_ND0=[]
for i in range(1,61):
	#             > Define case
	c=cas_DT_Th(case_name="Lie2 ND0 Analytic s"+str(i),case_col="b", \
		    path_opt=drt+"Ref/", \
		    path_err=drt+"six_inp_flatbeam_riccardo_corupto6_hllhcv1.0_triponly_withb4/temp_"+str(i)+"/")
	#             > Add Quad
	c.add_Quad("MQXFA.A1L5..1" ,      5 ,      "in" ); c.add_Quad("MQXFA.A1L5..16",      10,      "out")
	c.add_Quad("MQXFA.B1L5..1" ,      4 ,      "in" ); c.add_Quad("MQXFA.B1L5..16",      12,      "out")
	c.add_Quad("MQXFB.A2L5..1" ,      3 ,      "in" ); c.add_Quad("MQXFB.A2L5..16",      8 ,      "out")
	c.add_Quad("MQXFB.B2L5..1" ,      1 ,      "in" ); c.add_Quad("MQXFB.B2L5..16",      7 ,      "out")
	c.add_Quad("MQXFA.A3L5..1" ,      5 ,      "in" ); c.add_Quad("MQXFA.A3L5..16",      10,      "out")
	c.add_Quad("MQXFA.B3L5..1" ,      4 ,      "in" ); c.add_Quad("MQXFA.B3L5..16",      12,      "out")
	c.add_Quad("MQXFA.A3R5..1" ,      3 ,      "in" ); c.add_Quad("MQXFA.A3R5..16",      9 ,      "out")
	c.add_Quad("MQXFA.B3R5..1" ,      2 ,      "in" ); c.add_Quad("MQXFA.B3R5..16",      7 ,      "out")
	c.add_Quad("MQXFB.A2R5..1" ,      6 ,      "in" ); c.add_Quad("MQXFB.A2R5..16",      10,      "out")
	c.add_Quad("MQXFB.B2R5..1" ,      4 ,      "in" ); c.add_Quad("MQXFB.B2R5..16",      11,      "out")
	c.add_Quad("MQXFA.A1R5..1" ,      3 ,      "in" ); c.add_Quad("MQXFA.A1R5..16",      9 ,      "out")
	c.add_Quad("MQXFA.B1R5..1" ,      2 ,      "in" ); c.add_Quad("MQXFA.B1R5..16",      7 ,      "out")
	c.add_Quad("MQXFA.A1L1..1" ,      5 ,      "in" ); c.add_Quad("MQXFA.A1L1..16",      10,      "out")
	c.add_Quad("MQXFA.B1L1..1" ,      4 ,      "in" ); c.add_Quad("MQXFA.B1L1..16",      12,      "out")
	c.add_Quad("MQXFB.A2L1..1" ,      3 ,      "in" ); c.add_Quad("MQXFB.A2L1..16",      8 ,      "out")
	c.add_Quad("MQXFB.B2L1..1" ,      1 ,      "in" ); c.add_Quad("MQXFB.B2L1..16",      7 ,      "out")
	c.add_Quad("MQXFA.A3L1..1" ,      5 ,      "in" ); c.add_Quad("MQXFA.A3L1..16",      10,      "out")
	c.add_Quad("MQXFA.B3L1..1" ,      4 ,      "in" ); c.add_Quad("MQXFA.B3L1..16",      12,      "out")
	c.add_Quad("MQXFA.A3R1..1" ,      3 ,      "in" ); c.add_Quad("MQXFA.A3R1..16",      9 ,      "out")
	c.add_Quad("MQXFA.B3R1..1" ,      2 ,      "in" ); c.add_Quad("MQXFA.B3R1..16",      7 ,      "out")
	c.add_Quad("MQXFB.A2R1..1" ,      6 ,      "in" ); c.add_Quad("MQXFB.A2R1..16",      10,      "out")
	c.add_Quad("MQXFB.B2R1..1" ,      4 ,      "in" ); c.add_Quad("MQXFB.B2R1..16",      11,      "out")
	c.add_Quad("MQXFA.A1R1..1" ,      3 ,      "in" ); c.add_Quad("MQXFA.A1R1..16",      9 ,      "out")
	c.add_Quad("MQXFA.B1R1..1" ,      2 ,      "in" ); c.add_Quad("MQXFA.B1R1..16",      7 ,      "out")
	#             > Add Skip
	c.add_Skip("MQXFA.B3L5..FL"); c.add_Skip("MQXFA.B3L5..FR")
	c.add_Skip("MQXFA.A3L5..FL"); c.add_Skip("MQXFA.A3L5..FR")
	c.add_Skip("MQXFB.B2L5..FL"); c.add_Skip("MQXFB.B2L5..FR")
	c.add_Skip("MQXFB.A2L5..FL"); c.add_Skip("MQXFB.A2L5..FR")
	c.add_Skip("MQXFA.B1L5..FL"); c.add_Skip("MQXFA.B1L5..FR")
	c.add_Skip("MQXFA.A1L5..FL"); c.add_Skip("MQXFA.A1L5..FR")
	c.add_Skip("MQXFA.A1R5..FL"); c.add_Skip("MQXFA.A1R5..FR")
	c.add_Skip("MQXFA.B1R5..FL"); c.add_Skip("MQXFA.B1R5..FR")
	c.add_Skip("MQXFB.A2R5..FL"); c.add_Skip("MQXFB.A2R5..FR")
	c.add_Skip("MQXFB.B2R5..FL"); c.add_Skip("MQXFB.B2R5..FR")
	c.add_Skip("MQXFA.A3R5..FL"); c.add_Skip("MQXFA.A3R5..FR")
	c.add_Skip("MQXFA.B3R5..FL"); c.add_Skip("MQXFA.B3R5..FR")
	c.add_Skip("MQXFA.B3L1..FL"); c.add_Skip("MQXFA.B3L1..FR")
	c.add_Skip("MQXFA.A3L1..FL"); c.add_Skip("MQXFA.A3L1..FR")
	c.add_Skip("MQXFB.B2L1..FL"); c.add_Skip("MQXFB.B2L1..FR")
	c.add_Skip("MQXFB.A2L1..FL"); c.add_Skip("MQXFB.A2L1..FR")
	c.add_Skip("MQXFA.B1L1..FL"); c.add_Skip("MQXFA.B1L1..FR")
	c.add_Skip("MQXFA.A1L1..FL"); c.add_Skip("MQXFA.A1L1..FR")
	c.add_Skip("MQXFA.A1R1..FL"); c.add_Skip("MQXFA.A1R1..FR")
	c.add_Skip("MQXFA.B1R1..FL"); c.add_Skip("MQXFA.B1R1..FR")
	c.add_Skip("MQXFB.A2R1..FL"); c.add_Skip("MQXFB.A2R1..FR")
	c.add_Skip("MQXFB.B2R1..FL"); c.add_Skip("MQXFB.B2R1..FR")
	c.add_Skip("MQXFA.A3R1..FL"); c.add_Skip("MQXFA.A3R1..FR")
	c.add_Skip("MQXFA.B3R1..FL"); c.add_Skip("MQXFA.B3R1..FR")
	#             > Add File
	c.add_File(drt_VecPot+"C2-6-10-14_ND0_dz02_AF/coeff_in_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m.out"             ,5.91783358704972118e-01,'in' ) #,1.420
	c.add_File(drt_VecPot+"C2-6-10-14_ND0_dz02_AF/coeff_in_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_mid.out"         ,5.91783358704972118e-01,'in' ) #,1.420
	c.add_File(drt_VecPot+"C2-6-10-14_ND0_dz02_AF/coeff_in_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_inv.out"         ,6.29915713814027667e-01,'in' ) #,1.620
	c.add_File(drt_VecPot+"C2-6-10-14_ND0_dz02_AF/coeff_in_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_inv_opp.out"     ,6.29915713814027667e-01,'in' ) #,1.620
	c.add_File(drt_VecPot+"C2-6-10-14_ND0_dz02_AF/coeff_in_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_opp_mid.out"     ,5.91783358704972118e-01,'in' ) #,1.420
	c.add_File(drt_VecPot+"C2-6-10-14_ND0_dz02_AF/coeff_in_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_opp.out"         ,5.91783358704972118e-01,'in' ) #,1.420
	c.add_File(drt_VecPot+"C2-6-10-14_ND0_dz02_AF/coeff_out_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m.out"            ,6.09915713246783286e-01,'out') #,1.620
	c.add_File(drt_VecPot+"C2-6-10-14_ND0_dz02_AF/coeff_out_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_inv.out"        ,5.71783359230295996e-01,'out') #,1.420
	c.add_File(drt_VecPot+"C2-6-10-14_ND0_dz02_AF/coeff_out_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_inv_mid.out"    ,5.71783359230295996e-01,'out') #,1.420
	c.add_File(drt_VecPot+"C2-6-10-14_ND0_dz02_AF/coeff_out_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_opp.out"        ,6.09915713246783286e-01,'out') #,1.620
	c.add_File(drt_VecPot+"C2-6-10-14_ND0_dz02_AF/coeff_out_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_inv_opp.out"    ,5.71783359230295996e-01,'out') #,1.420
	c.add_File(drt_VecPot+"C2-6-10-14_ND0_dz02_AF/coeff_out_AF_C2-6-10-14_ND0_Ran50mm_BmR_dz0.020m_inv_opp_mid.out",5.71783359230295996e-01,'out') #,1.420
	#             > Save case
	Analytic_ND0.append(c)



#         * ND6
Analytic_ND6=[]
for i in range(1,61):
	#             > Define case
	c=cas_DT_Th(case_name="Lie2 ND6 Analytic s"+str(i),case_col="b", \
		    path_opt=drt+"Ref/", \
		    path_err=drt+"six_inp_flatbeam_riccardo_corupto6_hllhcv1.0_triponly_withb4/temp_"+str(i)+"/")
	#             > Add Quad
	c.add_Quad("MQXFA.A1L5..1" ,      5 ,      "in" ); c.add_Quad("MQXFA.A1L5..16",      10,      "out")
	c.add_Quad("MQXFA.B1L5..1" ,      4 ,      "in" ); c.add_Quad("MQXFA.B1L5..16",      12,      "out")
	c.add_Quad("MQXFB.A2L5..1" ,      3 ,      "in" ); c.add_Quad("MQXFB.A2L5..16",      8 ,      "out")
	c.add_Quad("MQXFB.B2L5..1" ,      1 ,      "in" ); c.add_Quad("MQXFB.B2L5..16",      7 ,      "out")
	c.add_Quad("MQXFA.A3L5..1" ,      5 ,      "in" ); c.add_Quad("MQXFA.A3L5..16",      10,      "out")
	c.add_Quad("MQXFA.B3L5..1" ,      4 ,      "in" ); c.add_Quad("MQXFA.B3L5..16",      12,      "out")
	c.add_Quad("MQXFA.A3R5..1" ,      3 ,      "in" ); c.add_Quad("MQXFA.A3R5..16",      9 ,      "out")
	c.add_Quad("MQXFA.B3R5..1" ,      2 ,      "in" ); c.add_Quad("MQXFA.B3R5..16",      7 ,      "out")
	c.add_Quad("MQXFB.A2R5..1" ,      6 ,      "in" ); c.add_Quad("MQXFB.A2R5..16",      10,      "out")
	c.add_Quad("MQXFB.B2R5..1" ,      4 ,      "in" ); c.add_Quad("MQXFB.B2R5..16",      11,      "out")
	c.add_Quad("MQXFA.A1R5..1" ,      3 ,      "in" ); c.add_Quad("MQXFA.A1R5..16",      9 ,      "out")
	c.add_Quad("MQXFA.B1R5..1" ,      2 ,      "in" ); c.add_Quad("MQXFA.B1R5..16",      7 ,      "out")
	c.add_Quad("MQXFA.A1L1..1" ,      5 ,      "in" ); c.add_Quad("MQXFA.A1L1..16",      10,      "out")
	c.add_Quad("MQXFA.B1L1..1" ,      4 ,      "in" ); c.add_Quad("MQXFA.B1L1..16",      12,      "out")
	c.add_Quad("MQXFB.A2L1..1" ,      3 ,      "in" ); c.add_Quad("MQXFB.A2L1..16",      8 ,      "out")
	c.add_Quad("MQXFB.B2L1..1" ,      1 ,      "in" ); c.add_Quad("MQXFB.B2L1..16",      7 ,      "out")
	c.add_Quad("MQXFA.A3L1..1" ,      5 ,      "in" ); c.add_Quad("MQXFA.A3L1..16",      10,      "out")
	c.add_Quad("MQXFA.B3L1..1" ,      4 ,      "in" ); c.add_Quad("MQXFA.B3L1..16",      12,      "out")
	c.add_Quad("MQXFA.A3R1..1" ,      3 ,      "in" ); c.add_Quad("MQXFA.A3R1..16",      9 ,      "out")
	c.add_Quad("MQXFA.B3R1..1" ,      2 ,      "in" ); c.add_Quad("MQXFA.B3R1..16",      7 ,      "out")
	c.add_Quad("MQXFB.A2R1..1" ,      6 ,      "in" ); c.add_Quad("MQXFB.A2R1..16",      10,      "out")
	c.add_Quad("MQXFB.B2R1..1" ,      4 ,      "in" ); c.add_Quad("MQXFB.B2R1..16",      11,      "out")
	c.add_Quad("MQXFA.A1R1..1" ,      3 ,      "in" ); c.add_Quad("MQXFA.A1R1..16",      9 ,      "out")
	c.add_Quad("MQXFA.B1R1..1" ,      2 ,      "in" ); c.add_Quad("MQXFA.B1R1..16",      7 ,      "out")
	#             > Add Skip
	c.add_Skip("MQXFA.B3L5..FL"); c.add_Skip("MQXFA.B3L5..FR")
	c.add_Skip("MQXFA.A3L5..FL"); c.add_Skip("MQXFA.A3L5..FR")
	c.add_Skip("MQXFB.B2L5..FL"); c.add_Skip("MQXFB.B2L5..FR")
	c.add_Skip("MQXFB.A2L5..FL"); c.add_Skip("MQXFB.A2L5..FR")
	c.add_Skip("MQXFA.B1L5..FL"); c.add_Skip("MQXFA.B1L5..FR")
	c.add_Skip("MQXFA.A1L5..FL"); c.add_Skip("MQXFA.A1L5..FR")
	c.add_Skip("MQXFA.A1R5..FL"); c.add_Skip("MQXFA.A1R5..FR")
	c.add_Skip("MQXFA.B1R5..FL"); c.add_Skip("MQXFA.B1R5..FR")
	c.add_Skip("MQXFB.A2R5..FL"); c.add_Skip("MQXFB.A2R5..FR")
	c.add_Skip("MQXFB.B2R5..FL"); c.add_Skip("MQXFB.B2R5..FR")
	c.add_Skip("MQXFA.A3R5..FL"); c.add_Skip("MQXFA.A3R5..FR")
	c.add_Skip("MQXFA.B3R5..FL"); c.add_Skip("MQXFA.B3R5..FR")
	c.add_Skip("MQXFA.B3L1..FL"); c.add_Skip("MQXFA.B3L1..FR")
	c.add_Skip("MQXFA.A3L1..FL"); c.add_Skip("MQXFA.A3L1..FR")
	c.add_Skip("MQXFB.B2L1..FL"); c.add_Skip("MQXFB.B2L1..FR")
	c.add_Skip("MQXFB.A2L1..FL"); c.add_Skip("MQXFB.A2L1..FR")
	c.add_Skip("MQXFA.B1L1..FL"); c.add_Skip("MQXFA.B1L1..FR")
	c.add_Skip("MQXFA.A1L1..FL"); c.add_Skip("MQXFA.A1L1..FR")
	c.add_Skip("MQXFA.A1R1..FL"); c.add_Skip("MQXFA.A1R1..FR")
	c.add_Skip("MQXFA.B1R1..FL"); c.add_Skip("MQXFA.B1R1..FR")
	c.add_Skip("MQXFB.A2R1..FL"); c.add_Skip("MQXFB.A2R1..FR")
	c.add_Skip("MQXFB.B2R1..FL"); c.add_Skip("MQXFB.B2R1..FR")
	c.add_Skip("MQXFA.A3R1..FL"); c.add_Skip("MQXFA.A3R1..FR")
	c.add_Skip("MQXFA.B3R1..FL"); c.add_Skip("MQXFA.B3R1..FR")
	#             > Add File
	c.add_File(drt_VecPot+"C2-6-10-14_ND6_dz02_AF/coeff_in_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m.out"             ,5.91783358704972118e-01,"in" ) #,1.420
	c.add_File(drt_VecPot+"C2-6-10-14_ND6_dz02_AF/coeff_in_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m_mid.out"         ,5.91783358704972118e-01,"in" ) #,1.420
	c.add_File(drt_VecPot+"C2-6-10-14_ND6_dz02_AF/coeff_in_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m_inv.out"         ,6.29915713814027667e-01,"in" ) #,1.620
	c.add_File(drt_VecPot+"C2-6-10-14_ND6_dz02_AF/coeff_in_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m_inv_opp.out"     ,6.29915713814027667e-01,"in" ) #,1.620
	c.add_File(drt_VecPot+"C2-6-10-14_ND6_dz02_AF/coeff_in_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m_opp_mid.out"     ,5.91783358704972118e-01,"in" ) #,1.420
	c.add_File(drt_VecPot+"C2-6-10-14_ND6_dz02_AF/coeff_in_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m_opp.out"         ,5.91783358704972118e-01,"in" ) #,1.420
	c.add_File(drt_VecPot+"C2-6-10-14_ND6_dz02_AF/coeff_out_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m.out"            ,6.09915713246783286e-01,"out") #,1.620
	c.add_File(drt_VecPot+"C2-6-10-14_ND6_dz02_AF/coeff_out_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m_inv.out"        ,5.71783359230295996e-01,"out") #,1.420
	c.add_File(drt_VecPot+"C2-6-10-14_ND6_dz02_AF/coeff_out_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m_inv_mid.out"    ,5.71783359230295996e-01,"out") #,1.420
	c.add_File(drt_VecPot+"C2-6-10-14_ND6_dz02_AF/coeff_out_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m_opp.out"        ,6.09915713246783286e-01,"out") #,1.620
	c.add_File(drt_VecPot+"C2-6-10-14_ND6_dz02_AF/coeff_out_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m_inv_opp.out"    ,5.71783359230295996e-01,"out") #,1.420
	c.add_File(drt_VecPot+"C2-6-10-14_ND6_dz02_AF/coeff_out_AF_C2-6-10-14_ND6_Ran50mm_BmR_dz0.020m_inv_opp_mid.out",5.71783359230295996e-01,"out") #,1.420
	#             > Save case
	Analytic_ND6.append(c)








# Compute detuning from analytic data
# --------------------------------------------------------------------------------
KCor_Nom=pd.DataFrame()
for it in range(len(Analytic_Nom)):
  t=Analytic_Nom[it]
  t.load_File()
  t.load_FFFile()
  t.Compute_Cor(flg_shw=False)
  KCor_Nom=KCor_Nom.append(t.KCor, ignore_index=True)
print(KCor_Nom)
KCor_Nom.to_csv(drt+r"six_inp_flatbeam_nominal_corupto6_hllhcv1.0_triponly_withb4/MCX_setting_madx_nom.mad",index=None,sep=" ")



KCor_Ric=pd.DataFrame()
for it in range(len(Analytic_Ric)):
  t=Analytic_Ric[it]
  t.load_File()
  t.load_FFFile()
  t.Compute_Cor(flg_shw=False)
  KCor_Ric=KCor_Ric.append(t.KCor, ignore_index=True)
print(KCor_Ric)
KCor_Ric.to_csv(drt+r"six_inp_flatbeam_riccardo_corupto6_hllhcv1.0_triponly_withb4/MCX_setting_madx_Ric.mad",index=None,sep=" ")


KCor_ND0=pd.DataFrame()
for it in range(len(Analytic_ND0)):
  t=Analytic_ND0[it]
  t.load_File()
  t.load_FFFile()
  t.Compute_Cor(flg_shw=False)
  KCor_ND0=KCor_ND0.append(t.KCor, ignore_index=True)
print(KCor_ND0)
KCor_ND0.to_csv(drt+r"six_inp_flatbeam_Lie2ND0_corupto6_hllhcv1.0_triponly_withb4/MCX_setting_madx_ND0.mad",index=None,sep=" ")


KCor_ND6=pd.DataFrame()
for it in range(len(Analytic_ND6)):
  t=Analytic_ND6[it]
  t.load_File()
  t.load_FFFile()
  t.Compute_Cor(flg_shw=False)
  KCor_ND6=KCor_ND6.append(t.KCor, ignore_index=True)
print(KCor_ND6)
KCor_ND6.to_csv(drt+r"six_inp_flatbeam_Lie2ND6_corupto6_hllhcv1.0_triponly_withb4/MCX_setting_madx_ND6.mad",index=None,sep=" ")



















# 
