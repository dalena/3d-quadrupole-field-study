#!/usr/bin/env python3
#import sys
import matplotlib.pyplot as plt
#from matplotlib.pyplot import *
import pandas as pd
import numpy  as np


plt.rcParams.update({'font.size': 14})
plt.rc('xtick', labelsize=14) 
plt.rc('ytick', labelsize=14) 

path="../optics/HL-LHC/"

#KCor_Nom=pd.read_csv(path+"six_inp_flatbeam_nominal_corupto6_hllhcv1.0_triponly/onlytriper_b4eq0/MCX_setting_madx_nom.mad",sep='\s+')
KCor_Nom=pd.read_csv(path+"six_inp_flatbeam_nominal_corupto6_hllhcv1.0_triponly_withb4/MCX_setting_madx_nom.mad",sep='\s+')
#KCor_Nom=pd.read_csv(path+"six_inp_flatbeam_nominal_corupto6_hllhcv1.0_onlyb6/MCX_setting_madx_nom.mad",sep='\s+')

#KCor_Ric=pd.read_csv(path+"six_inp_flatbeam_riccardo_corupto6_hllhcv1.0_triponly/onlytrip_nob4/MCX_setting_madx_Ric.mad",sep='\s+')
KCor_Ric=pd.read_csv(path+"six_inp_flatbeam_riccardo_corupto6_hllhcv1.0_triponly_withb4/MCX_setting_madx_Ric.mad",sep='\s+')
#KCor_Ric=pd.read_csv(path+"six_inp_flatbeam_riccardo_corupto6_hllhcv1.0_onlyb6/MCX_setting_madx_Ric.mad",sep='\s+')

#KCor_ND0=pd.read_csv(path+"six_inp_flatbeam_riccardo_corupto6_hllhcv1.0_triponly/onlytrip_nob4/MCX_setting_madx_ND0.mad",sep='\s+')
KCor_ND0=pd.read_csv(path+"six_inp_flatbeam_Lie2ND0_corupto6_hllhcv1.0_triponly_withb4/MCX_setting_madx_ND0.mad",sep='\s+')
#KCor_ND0=pd.read_csv(path+"six_inp_flatbeam_riccardo_corupto6_hllhcv1.0_onlyb6/MCX_setting_madx_ND0.mad",sep='\s+')

#KCor_ND6=pd.read_csv(path+"six_inp_flatbeam_riccardo_corupto6_hllhcv1.0_triponly/onlytrip_nob4/MCX_setting_madx_ND6.mad",sep='\s+')
KCor_ND6=pd.read_csv(path+"six_inp_flatbeam_Lie2ND6_corupto6_hllhcv1.0_triponly_withb4/MCX_setting_madx_ND6.mad",sep='\s+')
#KCor_ND6=pd.read_csv(path+"six_inp_flatbeam_riccardo_corupto6_hllhcv1.0_onlyb6/MCX_setting_madx_ND6.mad",sep='\s+')

#n=4; print(-KCor_ND0.loc[0,"KCOX3.L5"]/math.factorial(n-1))
#n=6; print(-KCor_ND0.loc[0,"KCTX3.L5"]*17**(n-1)*10**(3-3*n)/math.factorial(n-1))


Flg_log=False

KCOX_Nom_L=pd.concat([KCor_Nom["KCOX3.L1"],KCor_Nom["KCOX3.L5"]])
KCTX_Nom_L=pd.concat([KCor_Nom["KCTX3.L1"],KCor_Nom["KCTX3.L5"]])
KCOX_Nom_R=pd.concat([KCor_Nom["KCOX3.R1"],KCor_Nom["KCOX3.R5"]])
KCTX_Nom_R=pd.concat([KCor_Nom["KCTX3.R1"],KCor_Nom["KCTX3.R5"]])

KCOX_Ric_L=pd.concat([KCor_Ric["KCOX3.L1"],KCor_Ric["KCOX3.L5"]])
KCTX_Ric_L=pd.concat([KCor_Ric["KCTX3.L1"],KCor_Ric["KCTX3.L5"]])
KCOX_Ric_R=pd.concat([KCor_Ric["KCOX3.R1"],KCor_Ric["KCOX3.R5"]])
KCTX_Ric_R=pd.concat([KCor_Ric["KCTX3.R1"],KCor_Ric["KCTX3.R5"]])

KCOX_ND0_L=pd.concat([KCor_ND0["KCOX3.L1"],KCor_ND0["KCOX3.L5"]])
KCTX_ND0_L=pd.concat([KCor_ND0["KCTX3.L1"],KCor_ND0["KCTX3.L5"]])
KCOX_ND0_R=pd.concat([KCor_ND0["KCOX3.R1"],KCor_ND0["KCOX3.R5"]])
KCTX_ND0_R=pd.concat([KCor_ND0["KCTX3.R1"],KCor_ND0["KCTX3.R5"]])

KCOX_ND6_L=pd.concat([KCor_ND6["KCOX3.L1"],KCor_ND6["KCOX3.L5"]])
KCTX_ND6_L=pd.concat([KCor_ND6["KCTX3.L1"],KCor_ND6["KCTX3.L5"]])
KCOX_ND6_R=pd.concat([KCor_ND6["KCOX3.R1"],KCor_ND6["KCOX3.R5"]])
KCTX_ND6_R=pd.concat([KCor_ND6["KCTX3.R1"],KCor_ND6["KCTX3.R5"]])


KCOX_comp=0.0470759818276933
KCOX_spec=0.0945631076014800
KCTX_comp=942.341924446053
KCTX_spec=1414.33517456127
c_x=np.array([-1.0,1.0,1.0,-1.0,-1.0])
c_y=np.array([-1.0,-1.0,1.0,1.0,-1.0])


Fig1,ax1=plt.subplots()
Fig1, plt.scatter(KCOX_Nom_L,KCOX_Nom_R,c="g",label="HE",alpha=0.5)
Fig1, plt.scatter(KCOX_ND6_L,KCOX_ND6_R,c="k",label="Lie2 ND6",alpha=0.5)
Fig1, plt.xlabel(r'K3L Left [m-3]')
Fig1, plt.ylabel(r'K3L Right [m-3]')
ax1.ticklabel_format(style='sci',axis='both', scilimits=(0,0),useMathText = True)
ax1.set_xlim(-4.2e-2,4.2e-2)
ax1.set_ylim(-4.2e-2,4.2e-2)
Fig1, plt.subplots_adjust(left=0.12, right=0.92, top=0.93, bottom=0.13)
ax1.legend(loc='lower left',)


Fig2,ax2=plt.subplots()
Fig2, plt.scatter(KCOX_Nom_L,KCOX_Nom_R,c="g",label="HE",alpha=0.5)
Fig2, plt.scatter(KCOX_ND6_L,KCOX_ND6_R,c="k",label="Lie2 ND6",alpha=0.5)
Fig2, plt.plot(KCOX_comp*c_x,KCOX_comp*c_y,":r",label="Comp.")
Fig2, plt.plot(KCOX_spec*c_x,KCOX_spec*c_y,"-r",label="Spec.")
Fig2, plt.xlabel(r'K3L Left [m-3]')
Fig2, plt.ylabel(r'K3L Right [m-3]')
ax2.ticklabel_format(style='sci',axis='both', scilimits=(0,0),useMathText = True)
Fig2, plt.subplots_adjust(left=0.12, right=0.92, top=0.93, bottom=0.13)
ax2.legend(loc='lower left',)


Fig3,ax3=plt.subplots()
Fig3, plt.scatter(KCTX_Nom_L,KCTX_Nom_R,c="g",label="HE",alpha=0.5)
Fig3, plt.scatter(KCTX_Ric_L,KCTX_Ric_R,c="r",label="HE+Heads",alpha=0.5)
Fig3, plt.scatter(KCTX_ND0_L,KCTX_ND0_R,c="b",label="Lie2 ND0",alpha=0.5)
Fig3, plt.scatter(KCTX_ND6_L,KCTX_ND6_R,c="k",label="Lie2 ND6",alpha=0.5)
Fig3, plt.xlabel(r'K5L Left [m-5]')
Fig3, plt.ylabel(r'K5L Right [m-5]')
ax3.set_xlim(-6.2e2,6.2e2)
ax3.set_ylim(-6.2e2,6.2e2)
ax3.ticklabel_format(style='sci',axis='both', scilimits=(0,0),useMathText = True)
Fig3, plt.subplots_adjust(left=0.12, right=0.92, top=0.93, bottom=0.13)
ax3.legend(loc='lower left',)


Fig4,ax4=plt.subplots()
Fig4, plt.scatter(KCTX_Nom_L,KCTX_Nom_R,c="g",label="HE",alpha=0.5)
Fig4, plt.scatter(KCTX_Ric_L,KCTX_Ric_R,c="r",label="HE+Heads",alpha=0.5)
Fig4, plt.scatter(KCTX_ND0_L,KCTX_ND0_R,c="b",label="Lie2 ND0",alpha=0.5)
Fig4, plt.scatter(KCTX_ND6_L,KCTX_ND6_R,c="k",label="Lie2 ND6",alpha=0.5)
Fig4, plt.plot(KCTX_comp*c_x,KCTX_comp*c_y,":r",label="Comp.")
Fig4, plt.plot(KCTX_spec*c_x,KCTX_spec*c_y,"-r",label="Spec.")
Fig4, plt.xlabel(r'K5L Left [m-5]')
Fig4, plt.ylabel(r'K5L Right [m-5]')
ax4.ticklabel_format(style='sci',axis='both', scilimits=(0,0),useMathText = True)
Fig4, plt.subplots_adjust(left=0.12, right=0.92, top=0.93, bottom=0.13)
ax4.legend(loc='lower left',)


plt.show()
