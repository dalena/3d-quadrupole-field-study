#import pylab as pl
#import matplotlib as plt
import glob
import re
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd 
import sys
from  matplotlib.pyplot import *
from multiprocessing import Process, Queue
import csv

plt.rcParams.update({'font.size': 18})
plt.rc('xtick', labelsize=18) 
plt.rc('ytick', labelsize=18) 
plt.rc('text', usetex=True) 
#df = {df ={} , df={}}
alph=0.5
dares = {}
n_seeds = 1
fig = plt.figure()
df = {}
df_nd6 = {}
df_nd6_he = {}
df_ricc = {}
df_nom = {}

def csv2df(file):
    """ Read a csv file created by SixDeskDB and return a pandas DataFrame:
        using 'nturn' up to 'turn_max' as index and dassimp as value for DA.
        
        :param file: csv file
        :type  file: str
        :return: a pandas dataframe nturn in index and dassimp in the 1st column
        :rtype: pd.DataFrame
        @styrel
    """
    df = pd.read_csv(file, sep='\t', usecols=['dassimp', 'turn_max', 'nturn'])
    if df.empty:
        print(f"WARNING: {file} is empty")
        return 
    #print(range(df.turn_max.max()))
    df_x = pd.DataFrame(np.array(range(df.turn_max.max())), columns=['x'])
    # For df, remove the column 'turn_max',
    # change 'nturn' in int and move it in index 
    df.drop(columns='turn_max', inplace=True)
    df['nturn'] = df['nturn'].transform(int)
    df.set_index('nturn', inplace=True)
    # name df_x index as nturn and merge df and df_x:
    df_x.index.name = 'nturn'
    df_concat = pd.concat([df, df_x], sort=False, axis='columns')
    df_concat.drop(columns='x', inplace=True)
    df_concat.fillna(method='ffill', inplace=True)
    return df_concat



if __name__ == '__main__':
    
    # plots data files as they are
    ### Hard Hedge
    for j in range(0,n_seeds,1):
        fname = "nom/seed"+str(j+1)+"_da_vs_turn_nom"
        print("Analyzing file: {0}".format(fname))
        df_nom[j]=pd.read_csv(fname, sep='\s+')
        dfp_nom= df_nom[j].drop(df_nom[j].tail(1).index) # inplace to change DF directly
        y = dfp_nom['dassimp']
        x = dfp_nom['nturn']
        if j == 0:
            fig, plt.plot(x,y,'o',color="green",label='HE',alpha=alph)
        else:
            fig, plt.plot(x,y,'o',color="green",alpha=alph)

    ### Hard Hedge + Heads
    for j in range(0,n_seeds,1):
        fname = "ricc/seed"+str(j+1)+"_da_vs_turn_ricc"
        print("Analyzing file: {0}".format(fname))
        df_ricc[j]=pd.read_csv(fname, sep='\s+')
        dfp_ricc= df_ricc[j].drop(df_ricc[j].tail(1).index)
        y = dfp_ricc['dassimp']
        x = dfp_ricc['nturn']
        if j == 0:
            fig, plt.plot(x,y,'o',color="red",label='HE+heads',alpha=alph)
        else:
            fig, plt.plot(x,y,'o',color="red",alpha=alph)

    ### ND0 Lie2
    for j in range(0,n_seeds,1):
        fname = "nd0_Lie2/seed"+str(j+1)+"_da_vs_turn_nd0_lie2_mctxON"
        print("Analyzing file: {0}".format(fname))
        df[j]=pd.read_csv(fname, sep='\s+')
        dfp =df[j].drop(df[j].tail(1).index)
        y = dfp['dassimp']
        x = dfp['nturn']
        
        if j == 0:
            fig, plt.plot(x,y,'o',color="blue",label='Lie2 ND0',alpha=alph)
        else:
            fig, plt.plot(x,y,'o',color="blue",alpha=alph)

    ### ND6 Lie2        
    for j in range(0,n_seeds,1):
        fname = "nd6_Lie2/seed"+str(j+1)+"_da_vs_turn_nd6_lie2_mctxON"
        print("Analyzing file: {0}".format(fname))
        df_nd6[j]=pd.read_csv(fname, sep='\s+')
        dfp_nd6=df_nd6[j].drop(df_nd6[j].tail(1).index)
        y = dfp_nd6['dassimp']
        x = dfp_nd6['nturn']
        
        if j == 0:
            fig, plt.plot(x,y,'o',color="black",label='Lie2 ND6',alpha=alph)
        else:
            fig, plt.plot(x,y,'o',color="black",alpha=alph)

    ### ND6 Lie2 HE correctors (b4 b6)      
    for j in range(0,n_seeds,1):
        fname = "nd6_Lie2_he/seed"+str(j+1)+"_da_vs_turn_nd6_lie2_mctxON_he"
        print("Analyzing file: {0}".format(fname))
        df_nd6_he[j]=pd.read_csv(fname, sep='\s+')
        dfp_nd6_he=df_nd6_he[j].drop(df_nd6_he[j].tail(1).index)
        y = dfp_nd6_he['dassimp']
        x = dfp_nd6_he['nturn']
        
        if j == 0:
            fig, plt.plot(x,y,'o',color="darkgray",label='Lie2 ND6 HE cor',alpha=alph)
        else:
            fig, plt.plot(x,y,'o',color="darkgray",alpha=alph)

    fig, plt.xlabel(r'turns')
    fig, plt.ylabel(r'DA(N) [$\sigma$]')
    fig, plt.ylim([10,20])
    plt.subplots_adjust(left=0.12, right=0.92, top=0.93, bottom=0.13)
    plt.legend(loc='upper right', prop={'size': 16})


    # now fill all the turns with DA values
    ### Ricc
    directory = "ricc"
    files_ricc = glob.glob(directory + "/*")
    # sort the files in function of the number after seed
    files_ricc.sort(key = lambda string: int(re.search(r"\d+",string).group()))
    ricc_df = [csv2df(file) for file in files_ricc]

    ### Nom
    directory = "nom"
    files_nom = glob.glob(directory + "/*")
    files_nom.sort(key = lambda string: int(re.search(r"\d+",string).group()))
    nom_df = [csv2df(file) for file in files_nom]
    
    ### nd0 Lie2
    directory = "nd0_Lie2"
    files_nd0 = glob.glob(directory + "/*")
    files_nd0.sort(key = lambda string: int(re.search(r"\d+",string).group()))
    nd0_Lie2_df = [csv2df(file) for file in files_nd0]

    ### nd6 Lie2
    directory = "nd6_Lie2"
    files_nd6 = glob.glob(directory + "/*")
    files_nd6.sort(key = lambda string: int(re.search(r"\d+",string).group()))
    nd6_Lie2_df = [csv2df(file) for file in files_nd6]

    ### nd6 Lie2 HE cor
    directory = "nd6_Lie2_he"
    files_nd6_he = glob.glob(directory + "/*")
    files_nd6_he.sort(key = lambda string: int(re.search(r"\d+",string).group()))
    nd6_Lie2_df_he = [csv2df(file) for file in files_nd6_he]

    # plots all seeds for all turns and fill DF _all for statistics
    fig2 = plt.figure()
    j = 0
    nom_all  = pd.DataFrame()
    ricc_all = pd.DataFrame()
    nd0_all  = pd.DataFrame()
    nd6_all  = pd.DataFrame()
    nd6_all_he  = pd.DataFrame()
    
    for file in files_nom:
        # we build a new DataFrame with dassimp from all seeds
        # Will use to get min, average and max values 
        nom_all[str(j)] = nom_df[j]['dassimp'].values
        # we build arrays for plotting
        y_nom = nom_df[j]['dassimp']
        x_nom = nom_df[j].index.tolist()
        if j == 0:
            fig2, plt.plot(x_nom,y_nom,color="green",label='HE')
        #else:
            #fig2, plt.plot(x_nom,y_nom,color="green",alpha=alph)
        j+=1

    j = 0
    for file in files_ricc:
        if ricc_df[j] is not None:
            # we build a new DataFrame with dassimp from all seeds
            # Will use to get min, average and max values 
            ricc_all[str(j)] = ricc_df[j]['dassimp'].values
            # we build arrays for plotting
            y_ricc = ricc_df[j]['dassimp']
            x_ricc = ricc_df[j].index.tolist()
            if j == 0:
                fig2, plt.plot(x_ricc,y_ricc,color="red",label='HE+Heads')
            #else:
                #fig2, plt.plot(x_ricc,y_ricc,color="red",alpha=alph)
        j+=1

    j = 0
    for file in files_nd0:
        if nd0_Lie2_df[j] is not None:
            nd0_all[str(j)] = nd0_Lie2_df[j]['dassimp'].values
            y_nd0 = nd0_Lie2_df[j]['dassimp']
            x_nd0 = nd0_Lie2_df[j].index.tolist()
            if j == 0:
                fig2, plt.plot(x_nd0,y_nd0,color="blue",label='ND0 Lie2')
            #else:
                #fig2, plt.plot(x_nd0,y_nd0,color="blue",alpha=alph)
        j+=1

    j = 0
    for file in files_nd6:
        if nd6_Lie2_df[j] is not None:
            nd6_all[str(j)] = nd6_Lie2_df[j]['dassimp'].values
            y_nd6 = nd6_Lie2_df[j]['dassimp']
            x_nd6 = nd6_Lie2_df[j].index.tolist()
            if j == 0:
                fig2, plt.plot(x_nd6,y_nd6,color="black",label='ND6 Lie2')
            #else:
                #fig2, plt.plot(x_nd6,y_nd6,color="black",alpha=alph)
        j+=1

    j = 0
    for file in files_nd6_he:
        if nd6_Lie2_df_he[j] is not None:
            nd6_all_he[str(j)] = nd6_Lie2_df_he[j]['dassimp'].values
            y_nd6_he = nd6_Lie2_df_he[j]['dassimp']
            x_nd6_he = nd6_Lie2_df_he[j].index.tolist()
            if j == 0:
                fig2, plt.plot(x_nd6_he,y_nd6_he,color="darkgray",label='ND6 Lie2 HE cor')
            #else:
                #fig2, plt.plot(x_nd6,y_nd6,color="black",alpha=alph)
        j+=1
    
    fig2, plt.xlabel(r'turns')
    fig2, plt.ylabel(r'DA(N) [$\sigma$]')
    fig2, plt.ylim([10,16])
    plt.subplots_adjust(left=0.12, right=0.92, top=0.93, bottom=0.13)
    plt.legend(loc='lower left', prop={'size': 16})
        
    # Now get min, max, average and median at each turns for all seeds
    y_nom_min  =  nom_all.min(axis=1); y_nom_med  =  nom_all.median(axis=1); y_nom_max  =   nom_all.max(axis=1)
    y_ricc_min = ricc_all.min(axis=1); y_ricc_med = ricc_all.median(axis=1); y_ricc_max =  ricc_all.max(axis=1)
    y_nd0_min  =  nd0_all.min(axis=1); y_nd0_med  =  nd0_all.median(axis=1); y_nd0_max  =   nd0_all.max(axis=1)
    y_nd6_min  =  nd6_all.min(axis=1); y_nd6_med  =  nd6_all.median(axis=1); y_nd6_max  =   nd6_all.max(axis=1)
    y_nd6_min_he  =  nd6_all_he.min(axis=1); y_nd6_med_he  =  nd6_all_he.median(axis=1); y_nd6_max_he  =   nd6_all_he.max(axis=1)
     
    y_nom_ave  =  nom_all.mean(axis=1)
    y_ricc_ave = ricc_all.mean(axis=1)
    y_nd0_ave  =  nd0_all.mean(axis=1)
    y_nd6_ave  =  nd6_all.mean(axis=1)
    y_nd6_ave_he  =  nd6_all_he.mean(axis=1)

    fig3 = plt.figure()

    fig3, plt.plot(x_nom,y_nom_min,color="green",alpha=alph)
    fig3, plt.plot(x_nom,y_nom_ave,color="green",label='HE',alpha=alph)
    #fig3, plt.plot(x_nom,y_nom_med,color="green",alpha=alph, linestyle='--')
    fig3, plt.plot(x_nom,y_nom_max,color="green",alpha=alph)
    fig3, plt.fill_between(x_nom, y_nom_max, y_nom_min, color='lightgreen')
    fig3, plt.plot(x_ricc,y_ricc_min,color="red",alpha=alph)
    fig3, plt.plot(x_ricc,y_ricc_ave,color="red",label='HE+Heads',alpha=alph)
    #fig3, plt.plot(x_ricc,y_ricc_med,color="red",alpha=alph, linestyle='--')
    fig3, plt.plot(x_ricc,y_ricc_max,color="red",alpha=alph)
    fig3, plt.fill_between(x_ricc, y_ricc_max, y_ricc_min, color='lightcoral')
    fig3, plt.plot(x_nd6,y_nd6_min,color="black",alpha=alph)
    fig3, plt.plot(x_nd6,y_nd6_ave,color="black",label='ND6 Lie2',alpha=alph)
    #fig3, plt.plot(x_nd6,y_nd6_med,color="black",alpha=alph, linestyle='--')
    fig3, plt.plot(x_nd6,y_nd6_max,color="black",alpha=alph)
    fig3, plt.fill_between(x_nd6, y_nd6_max, y_nd6_min, color='lightgrey')
    fig3, plt.plot(x_nd0,y_nd0_min,color="blue",alpha=alph)
    fig3, plt.plot(x_nd0,y_nd0_ave,color="blue",label='ND0 Lie2',alpha=alph)
    #fig3, plt.plot(x_nd0,y_nd0_med,color="blue",alpha=alph, linestyle='--')
    fig3, plt.plot(x_nd0,y_nd0_max,color="blue",alpha=alph)
    fig3, plt.fill_between(x_nd0, y_nd0_max, y_nd0_min, color='lightblue')
#    fig3, plt.plot(x_nd6_he,y_nd6_min_he,color="darkgray",alpha=alph)
#    fig3, plt.plot(x_nd6_he,y_nd6_ave_he,color="black",linestyle=':',label='ND6 Lie2 HE cor',alpha=alph)
#    #fig3, plt.plot(x_nd6_he,y_nd6_med_he,color="darkgray",alpha=alph, linestyle='--')
#    fig3, plt.plot(x_nd6_he,y_nd6_max_he,color="darkgray",alpha=alph)
#    fig3, plt.fill_between(x_nd6_he, y_nd6_max_he, y_nd6_min_he, color='darkgrey')

    fig3, plt.xlabel(r'turns')
    fig3, plt.ylabel(r'DA(N) [$\sigma$]')
    fig3, plt.ylim([10,19])
    plt.subplots_adjust(left=0.12, right=0.92, top=0.93, bottom=0.13)
    plt.legend(loc='upper right', prop={'size': 16})

    plt.show()

