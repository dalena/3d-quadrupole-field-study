#import pylab as pl
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd 
import sys
from  matplotlib.pyplot import *

matplotlib.rcParams.update({'font.size': 18})
matplotlib.rc('xtick', labelsize=18) 
matplotlib.rc('ytick', labelsize=18) 
matplotlib.rc('text', usetex=True) 
#df = {df ={} , df={}}
angles=11
angstep=7.5
nseeds=60
nd6lie2="dares_hl10_rnd150_allerr_10to4_ND6_mctxON/DAres.hl10_rnd150.62.31_60.32.5."
nd6lie2nom="dares_hl10_rnd150_allerr_10to4_ND6_mctxON_hecor/DAres.hl10_rnd150.62.31_60.32.5."
ricc="dares_job_tracking_nocross_ricc_10to4_allerr/DAres.job_tracking_nocross.62.31_60.32.5."
nom="dares_job_tracking_nocross_nom_10to4_allerr/DAres.job_tracking_nocross.62.31_60.32.5."
#nd0lie2="dares_hl10_rnd150/DAres.hl10_rnd150.62.31_60.32.5."
nd0lie2="dares_hl10_rnd150_allerr_10to4_ND0_mctxON/DAres.hl10_rnd150.62.31_60.32.5."
# ND0
#minda = [[0 for i in range(angles)] for j in range(nseeds)]
#aveda = [[0 for i in range(angles)] for j in range(nseeds)]
#maxda = [[0 for i in range(angles)] for j in range(nseeds)]
minseed = [] 
minda = [] 
aveda = [] 
maxda = [] 
# ND6
minseed_nd6 = [] 
minda_nd6 = [] 
aveda_nd6 = [] 
maxda_nd6 = [] 
# ND6 b6 nom b2 deriv not corrected
minseed_nd6nom = [] 
minda_nd6nom = [] 
aveda_nd6nom = [] 
maxda_nd6nom = [] 
# Ricc
minseed_ricc = []
minda_ricc = [] 
aveda_ricc = [] 
maxda_ricc = [] 
# Nom
minseed_nom = [] 
minda_nom = [] 
aveda_nom = [] 
maxda_nom = [] 
ang = []

#print (minda)

yerr_lower = [[0 for i in range(angles)] for j in range(nseeds)]
yerr_max = [[0 for i in range(angles)] for j in range(nseeds)]
dares = {}
dares_ricc = {}
dares_nom = {}
dares_nd6 = {}
dares_nd6nom = {}

for j in range(0,nseeds,1):
    df1 = {}
    df_ricc = {}
    df_nom = {}
    df_nd6 = {}
    df_nd6nom = {}
    for i in range(1,angles+1,1):
        fname1=nd0lie2+str(i)
        fname_ricc =ricc+str(i)
        fname_nom = nom+str(i)
        fname_nd6 = nd6lie2+str(i)
        fname_nd6nom = nd6lie2nom+str(i)
        df1[i]    =pd.read_table(fname1, sep='\s+', header=None)
        df_ricc[i]=pd.read_table(fname_ricc, sep='\s+', header=None)
        df_nom[i] =pd.read_table(fname_nom, sep='\s+', header=None)
        df_nd6nom[i] =pd.read_table(fname_nd6nom, sep='\s+', header=None)
        df_nd6[i] =pd.read_table(fname_nd6, sep='\s+', header=None)
        if j == 0:
            minseed.append(df1[i][4].argmin())
            minda.append(df1[i][4].min())
            aveda.append(df1[i][4].mean())
            maxda.append(df1[i][4].max())
            minseed_ricc.append(df_ricc[i][4].argmin())
            minda_ricc.append(df_ricc[i][4].min())
            aveda_ricc.append(df_ricc[i][4].mean())
            maxda_ricc.append(df_ricc[i][4].max())
            minseed_nom.append(df_nom[i][4].argmin())
            minda_nom.append(df_nom[i][4].min())
            aveda_nom.append(df_nom[i][4].mean())
            maxda_nom.append(df_nom[i][4].max())
            minseed_nd6.append(df_nd6[i][4].argmin())
            minda_nd6.append(df_nd6[i][4].min())
            aveda_nd6.append(df_nd6[i][4].mean())
            maxda_nd6.append(df_nd6[i][4].max())
            minseed_nd6nom.append(df_nd6nom[i][4].argmin())
            minda_nd6nom.append(df_nd6nom[i][4].min())
            aveda_nd6nom.append(df_nd6nom[i][4].mean())
            maxda_nd6nom.append(df_nd6nom[i][4].max())
            ang.append(i*angstep)
            print ("*********************************************")
            print ("Lie2 ND0 Lost2 Min DA ={0} angle ={1} seed={2}".format(df1[i][4].min(),i*angstep,df1[i][4].argmin()))
            print ("Lie2 ND6 Lost2 Min DA ={0} angle ={1} seed={2}".format(df_nd6[i][4].min(),i*angstep,df_nd6[i][4].argmin()))
            print ("Nom Lost2 Min DA ={0} angle ={1} seed={2}".format(df_nom[i][4].min(),i*angstep,df_nom[i][4].argmin()))
            print ("Ricc Lost2 Min DA ={0} angle ={1} seed={2}".format(df_ricc[i][4].min(),i*angstep,df_ricc[i][4].argmin()))
            print ("*********************************************")
            print ("Lie2 ND0 Lost2 Mean DA ={0} angle ={1} ".format(df1[i][4].mean(),i*angstep))
            print ("Lie2 ND6 Lost2 Mean DA ={0} angle ={1} ".format(df_nd6[i][4].mean(),i*angstep))
            print ("Nom Lost2 Mean DA ={0} angle ={1} ".format(df_nom[i][4].mean(),i*angstep))
            print ("Ricc Lost2 Mean DA ={0} angle ={1} ".format(df_ricc[i][4].mean(),i*angstep))

        df1[i] = df1[i].iloc[j]
        df_ricc[i] = df_ricc[i].iloc[j]
        df_nom[i] = df_nom[i].iloc[j]
        df_nd6[i] = df_nd6[i].iloc[j]
        df_nd6nom[i] = df_nd6nom[i].iloc[j]
        drop_da0=df1[i].index[df1[i][4]==0].tolist() # 0 DA means all stable particles
        df1[i]=df1[i].drop(df1[i].index[drop_da0])
        drop_da0=df_ricc[i].index[df_ricc[i][4]==0].tolist() # 0 DA means all stable particles
        df_ricc[i]=df_ricc[i].drop(df_ricc[i].index[drop_da0])
        drop_da0=df_nom[i].index[df_nom[i][4]==0].tolist() # 0 DA means all stable particles
        df_nom[i]=df_nom[i].drop(df_nom[i].index[drop_da0])
        drop_da0=df_nd6[i].index[df_nd6[i][4]==0].tolist() # 0 DA means all stable particles
        df_nd6[i]=df_nd6[i].drop(df_nd6[i].index[drop_da0])
        drop_da0=df_nd6nom[i].index[df_nd6nom[i][4]==0].tolist() # 0 DA means all stable particles
        df_nd6nom[i]=df_nd6nom[i].drop(df_nd6nom[i].index[drop_da0])
        df1[i][7]=i
        #df[i].info()
        #yerr_lower[j][i-1] = aveda[j][i-1]-minda[j][i-1]
        #yerr_max[j][i-1]   = maxda[j][i-1]-aveda[j][i-1]
        # Ricc
        df_ricc[i][7]=i
        #df[i].info()
        # Nom
        df_nom[i][7]=i
        #df[i].info()
        df_nd6[i][7]=i
        df_nd6nom[i][7]=i
        #df[i].info()
        #print (j) 
       
#    print (minda[j])
        #    df[i].info()
        #    print(df[i].head())
        
#    dares[j] = pd.concat([df[1],df[2],df[3],df[4],df[5],df[6],df[7],df[8],df[9],df[10],df[11],df[12],df[13],df[14],df[15],df[16],df[17],df[18],df[19],df[20], df[21],df[22],df[23],df[24],df[25],df[26],df[27],df[28],df[29],df[30],df[31],df[32],df[33],df[34],df[35],df[36],df[37],df[38],df[39],df[40],df[41],df[42],df[43],df[44],df[45],df[46],df[47],df[48],df[49],df[50],df[51],df[52],df[53],df[54],df[55],df[56],df[57],df[58],df[59]], axis=0)
    dares[j] = pd.concat([df1[1],df1[2],df1[3],df1[4],df1[5],df1[6],df1[7],df1[8],df1[9],df1[10],df1[11]], axis=0)
    dares_ricc[j] = pd.concat([df_ricc[1],df_ricc[2],df_ricc[3],df_ricc[4],df_ricc[5],df_ricc[6],df_ricc[7],df_ricc[8],df_ricc[9],df_ricc[10],df_ricc[11]], axis=0)
    dares_nom[j] = pd.concat([df_nom[1],df_nom[2],df_nom[3],df_nom[4],df_nom[5],df_nom[6],df_nom[7],df_nom[8],df_nom[9],df_nom[10],df_nom[11]], axis=0)
    dares_nd6[j] = pd.concat([df_nd6[1],df_nd6[2],df_nd6[3],df_nd6[4],df_nd6[5],df_nd6[6],df_nd6[7],df_nd6[8],df_nd6[9],df_nd6[10],df_nd6[11]], axis=0)
    dares_nd6nom[j] = pd.concat([df_nd6nom[1],df_nd6nom[2],df_nd6nom[3],df_nd6nom[4],df_nd6nom[5],df_nd6nom[6],df_nd6nom[7],df_nd6nom[8],df_nd6nom[9],df_nd6nom[10],df_nd6nom[11]], axis=0)
    #print(dares[0].head())
    #dares.info()

 #   print("******************************************************")
 #   print(j)
 #   print "Lost1 nd0 Lie2 Min DA ={0}".format(dares[j][3].min())
 #   print "Lost2 nd0 Lie2 Min DA ={0}".format(dares[j][4].min())
 #   print "Lost1 nd0 Lie2 Max DA ={0}".format(dares[j][3].max())
 #   print "Lost2 nd0 Lie2 Max DA ={0}".format(dares[j][4].max())
#   Ricc
#    print "Lost1 Ricc Min DA ={0}".format(dares_ricc[j][3].min())
#    print "Lost2 Ricc Min DA ={0}".format(dares_ricc[j][4].min())
#    print "Lost1 Ricc Max DA ={0}".format(dares_ricc[j][3].max())
#    print "Lost2 Ricc Max DA ={0}".format(dares_ricc[j][4].max())
#   Nom
#    print "Lost1 Nom Min DA ={0}".format(dares_nom[j][3].min())
#    print "Lost2 Nom Min DA ={0}".format(dares_nom[j][4].min())
#    print "Lost1 Nom Max DA ={0}".format(dares_nom[j][3].max())
#    print "Lost2 Nom Max DA ={0}".format(dares_nom[j][4].max())
# ND6
#    print "Lost1 nd6 Lie2 Min DA ={0}".format(dares_nd6[j][3].min())
#    print "Lost2 nd6 Lie2 Min DA ={0}".format(dares_nd6[j][4].min())
#    print "Lost1 nd6 Lie2 Max DA ={0}".format(dares_nd6[j][3].max())
#    print "Lost2 nd6 Lie2 Max DA ={0}".format(dares_nd6[j][4].max())



#print (ang)
#print (minda[0])
#print (minda[1])
#print (aveda[0])
#print (maxda[0])

fig = plt.figure()
#fig, plt.plot(ang,minda)
#fig, plt.plot(ang,aveda)
fig, plt.plot(ang,dares[0][4],'bo-',label='Lie2 ND0')
fig, plt.plot(ang,dares_ricc[0][4],'ro-',label='HE+ heads',mec='red')
fig, plt.plot(ang,dares_nom[0][4],'go-',label='HE ',mec='green')
fig, plt.plot(ang,dares_nd6[0][4],'ko-',label='Lie2 ND6')
fig, plt.plot(ang,dares_nd6nom[0][4],'o-',color='darkgray',markeredgecolor='darkgray',label='Lie2 ND6 HE cor')
fig, plt.subplots_adjust(left=0.12, right=0.92, top=0.94, bottom=0.14)
#fig, plt.errobar(ang, aveda[0], yerr=[yerr_lower[0],yerr_max[0]],label='w b$_{3}$ and b$_5$ cor',fmt='o', capthick=2)
#fig, plt.plot([0,90], [12,12], 'r--',lw=2)
fig, plt.xlabel(r'angles [$^\circ$]')
fig, plt.ylabel(r'DA [$\sigma$]')
#fig, plt.title('arc dipole, triplet, LO current=-15/720A')
fig, plt.xlim([0,90])
fig, plt.ylim([0,20])
plt.legend(loc='lower left')
fig2 = plt.figure()
#fig, plt.plot(ang,minda)
#fig, plt.plot(ang,aveda)
#fig, plt.plot(ang,maxda)
fig2, dares[j][3].hist(bins=range(-110, 20, 1), facecolor='r', label='alost1', alpha=0.5)
fig2, dares[j][4].hist(bins=range(-110, 20, 1), facecolor='b', label='alost2', alpha=0.5)
#fig2, plt.hist(dares[4])
#fig2, plt.plot([0,90], [12,12], 'r--',lw=2)
fig2, plt.ylabel('count ')
fig2, plt.xlabel(r'DA [$\sigma$]')
#fig2, plt.title('ARC DIPOLE')
#fig2, plt.axis([0,15,0,10])
#fig2, plt.ylim([0,20])
fig3 = plt.figure()
fig3, plt.plot(ang,minda_ricc,'ro-',label='HE+ heads min DA',mec='red')
fig3, plt.plot(ang,minda_nom,'go-',label='HE min DA',mec='green')
fig3, plt.plot(ang,minda,'bo-',label='Lie2 ND0 min DA')
fig3, plt.plot(ang,minda_nd6,'ko-',label='Lie2 ND6 min DA')
fig3, plt.subplots_adjust(left=0.12, right=0.92, top=0.94, bottom=0.14)
print("**************min DA************")
print("Lost2 nd0 Lie2 ={0}".format(minda))
print("Lost2 nd6 Lie2 ={0}".format(minda_nd6))
print("Lost2 Nom ={0}".format(minda_nom))
print("Lost2 Ricc ={0}".format(minda_ricc))
print("**************mean DA************")
print("Lost2 nd0 Lie2 ={0}".format(aveda))
print("Lost2 nd6 Lie2 ={0}".format(aveda_nd6))
print("Lost2 Nom ={0}".format(aveda_nom))
print("Lost2 Ricc ={0}".format(aveda_ricc))

#print(dares[0])
#fig3, plt.xlim([0,90])
#fig3, plt.ylim([0,20])
#plt.legend(loc='lower right')
#plt.show()
#sys.exit(0)
for jj in range(0,nseeds,1):
    y = dares[jj][4]
    x = dares[jj][7]*angstep
    y_ricc = dares_ricc[jj][4]
    x_ricc = dares_ricc[jj][7]*angstep
    y_nom = dares_nom[jj][4]
    x_nom = dares_nom[jj][7]*angstep
    y_nd6 = dares_nd6[jj][4]
    #    print(x, y)
    fig3, plt.plot(x_ricc,y_ricc,'.', color="red", markerfacecolor='red',alpha=0.5)
    fig3, plt.plot(x_nom,y_nom,'.', color="green",markerfacecolor='green',alpha=0.5)
    fig3, plt.plot(x,y,'.', color="blue",alpha=0.5)
    fig3, plt.plot(x,y_nd6,'.', color="black",alpha=0.5)
    #fig, plt.title('arc dipole, triplet, LO current=-15/720A')
    #fig3, plt.plot([0,90], [12,12], 'r--',lw=2,label=r'12 $\sigma$ target')

fig3, plt.xlabel(r'angles [$^\circ$]')
fig3, plt.ylabel(r'DA [$\sigma$]')
fig3, plt.xlim([0,90.5])
fig3, plt.ylim([0,20])

plt.legend(loc='lower left')
plt.show()
sys.exit(0)
