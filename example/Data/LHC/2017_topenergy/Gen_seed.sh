if [ $# -ge 2 ]; then
	MASKMADX=$1
	SEED=$2

	#mkdir -p Seed_$SEED
	#rm Seed_$SEED/*

	awk '/%SEEDRAN/{
	sv=$0
	n=gsub(/%SEEDRAN/, "", $0)
	$0=sv
	for (i=0; i<n; i++)
	{
	  c++;
	  sub("%SEEDRAN",'"$SEED"');
	}
	}1' $MASKMADX.madx > "$MASKMADX"_S$SEED.madx

	madx "$MASKMADX"_S$SEED.madx

	rm "$MASKMADX"_S$SEED.madx
fi

