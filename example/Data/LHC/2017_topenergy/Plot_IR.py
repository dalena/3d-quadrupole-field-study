import matplotlib.pyplot as plt
#import pathlib
import numpy  as np
import pandas as pd
import sys
plt.rcParams.update({'font.size':14})
plt.rc('xtick',labelsize=12)
plt.rc('ytick',labelsize=12)


# Definition
# --------------------------------------------------------------------------------
#     - Plot Quad
def plot_quad(name_s,name_e,opt,ax,ymin,ymax,cside="",coffset=1,color="xkcd:azure"):
    mqxf_s =opt[opt['NAME'].str.contains(name_s)]
    mqxf  = opt[opt['NAME'].str.contains(name_s[:-3])]
    mqxf_e =opt[opt['NAME'].str.contains(name_e)]
    print(mqxf_s)
    print(mqxf_e)
    h1=(ymax-ymin)
    x_pts = np.array([0,1,1,0])
    y_pts_f = np.array([0,0,h1,h1]) 
    y_pts_d = np.array([0,0,-h1,-h1]) 
    elem_len =mqxf_e["S"].values[0]-mqxf_s["S"].values[0]
    print(elem_len)
    if(mqxf['K1L'].values[1]>=0):
        ax.fill(mqxf_s["S"].values-Smin+elem_len*x_pts,y_pts_f,color=color)
        if   cside=="L":
            ax.plot((mqxf_s["S"].values-Smin)-coffset, ymax*0.5,marker='o',markerfacecolor=color,markeredgecolor="w")
        elif cside=="R":
            ax.plot((mqxf_e["S"].values-Smin)+coffset, ymax*0.5,marker='o',markerfacecolor=color,markeredgecolor="w")
        #if (mqxf_s['NAME'].str.contains("MQXFA.A").bool()):
        #    ax.plot((mqxf_s["S"].values-Smin), ymax*0.5,marker='o',markerfacecolor='r',markeredgecolor="w")
        #elif (mqxf_e['NAME'].str.contains("MQXFA.B").bool()):
        #    ax.plot((mqxf_e["S"].values-Smin), ymax*0.5,marker='o',markerfacecolor='r',markeredgecolor="w")
        #elif (mqxf_e['NAME'].str.contains("MQXFB.B").bool()):
        #    ax.plot((mqxf_e["S"].values-Smin), ymax*0.5,marker='o',markerfacecolor='r',markeredgecolor="w")
        #elif (mqxf_s['NAME'].str.contains("MQXFB.A").bool()):
        #    ax.plot((mqxf_s["S"].values-Smin), ymax*0.5,marker='o',markerfacecolor='r',markeredgecolor="w")
        print(" pos")
    elif(mqxf['K1L'].values[1]<=0):
        ax.fill(mqxf_s["S"].values[0]-Smin+elem_len*x_pts,y_pts_d,color=color)
        if   cside=="L":
            ax.plot((mqxf_s["S"].values-Smin)-coffset,-ymax*0.5,marker='o',markerfacecolor=color,markeredgecolor="w")
        elif cside=="R":
            ax.plot((mqxf_e["S"].values-Smin)+coffset,-ymax*0.5,marker='o',markerfacecolor=color,markeredgecolor="w")
        #if (mqxf_s['NAME'].str.contains("MQXFA.B").bool()):
        #    ax.plot((mqxf_s["S"].values-Smin), -ymax*0.5,marker='o',markerfacecolor='r',markeredgecolor="w")
        #elif (mqxf_e['NAME'].str.contains("MQXFA.A").bool()):
        #    ax.plot((mqxf_e["S"].values-Smin), -ymax*0.5,marker='o',markerfacecolor='r',markeredgecolor="w")
        #elif (mqxf_e['NAME'].str.contains("MQXFB.A").bool()):
        #    ax.plot((mqxf_e["S"].values-Smin), -ymax*0.5,marker='o',markerfacecolor='r',markeredgecolor="w")
        #elif (mqxf_s['NAME'].str.contains("MQXFB.B").bool()):
        #    ax.plot((mqxf_s["S"].values-Smin), -ymax*0.5,marker='o',markerfacecolor='r',markeredgecolor="w")
        print("neg")


#     - Plot Dipole
def plot_dip(name_s,name_e,opt,ax,ymin,ymax,cside="",coffset=1,color="gold"):
    mb_s =opt[opt['NAME'].str.contains(name_s)]
    mb_e =opt[opt['NAME'].str.contains(name_e)]
    print(mb_s)
    h1=(ymax-ymin)
    x_pts = np.array([0,1,1,0])#-1
    y_pts = np.array([0,0,h1,h1]) #+0.5*(ymax+ymin)
    elem_len =mb_e["S"].values-mb_s["S"].values
    print(elem_len)
    ax.fill(mb_s["S"].values-Smin+elem_len*x_pts,y_pts,color=color)
    if   cside=="L":
        ax.plot((mb_s["S"].values-Smin)-coffset, ymax*0.5,marker='o',markerfacecolor=color,markeredgecolor="w")
    elif cside=="R":
        ax.plot((mb_e["S"].values-Smin)+coffset, ymax*0.5,marker='o',markerfacecolor=color,markeredgecolor="w")


#     - Plot Corrector
def plot_Cor(name_c,elem_len,opt,ax,ymin,ymax,cside="",coffset=1,color="xkcd:orchid"):
    mc =opt[opt['NAME'].str.contains(name_c)]
    print(mc)
    h1=(ymax-ymin)
    x_pts = np.array([0,1,1,0])#-1
    y_pts = np.array([-h1,-h1,h1,h1]) #+0.5*(ymax+ymin)
    print(elem_len)
    ax.fill(mc["S"].values-Smin-elem_len*.5+elem_len*x_pts,y_pts,color=color)
    if   cside=="L":
        ax.plot((mc["S"].values-Smin)-coffset, ymax*0.5,marker='o',markerfacecolor=color,markeredgecolor="w")
    elif cside=="R":
        ax.plot((mc["S"].values-Smin)+coffset, ymax*0.5,marker='o',markerfacecolor=color,markeredgecolor="w")




name_optic="LHC"
if name_optic=="HLLHC":
	Smin=6584.2784327563
	Smax=6744.8584327563
elif name_optic=="LHC":
	Smin=6581.9164327563
	Smax=6749.5084327563


currentFile="Optic/optic0.tfs"
head_opt=pd.read_csv(currentFile,header=46,sep='\s+',nrows=0).columns[1:];
optic_dat=pd.read_csv(currentFile,skiprows=48,sep='\s+',names=head_opt);
#optic_dat=optic_dat[~optic_dat["KEYWORD"].isin(["MARKER"])]

optic_dat=optic_dat[np.logical_and(optic_dat["S"].values-Smin>=0,optic_dat["S"].values-Smin<=Smax-Smin)]


fig, ax=plt.subplots(nrows=2, ncols=1, sharex=True, sharey=False)#,figsize=(14, 7))
ax_opt=ax[0]

# - Plot beta
ax[1].plot(optic_dat["S"]-Smin,optic_dat["BETX"]/1000.,"--r",label=r'$\beta_{x}$')
ax[1].plot(optic_dat["S"]-Smin,optic_dat["BETY"]/1000.,"-g",label=r'$\beta_{y}$')
#ax[1].set_ylim(0,10000);
ax[1].set_xlabel(r"S [m]");
ax[1].set_ylabel(r"$\beta$ [km]");
#ax[1].ticklabel_format(style='sci',axis='y', scilimits=(0,0),useMathText = True)
ax[1].legend();



# - Plot Elements
axsize=1;
#ax_opt.plot([0,Smax-Smin],[0,0],'k')
#ax_opt.set_xlabel(r"S [m]");
 
ymin=0.
ymax=0.5

if name_optic=="HLLHC":
	#       - plot Quads
	plot_quad("MQXFA.B3L5..FL","MQXFA.B3L5..FR",optic_dat,ax_opt,ymin,ymax,cside="L")
	plot_quad("MQXFA.A3L5..FL","MQXFA.A3L5..FR",optic_dat,ax_opt,ymin,ymax,cside="R")
	Q3L5=optic_dat[optic_dat['NAME'].str.contains("MQXFA..3L5")]; Q3L5_S=np.mean(Q3L5.S.values)
	ax_opt.text(Q3L5_S-Smin-4.7,ymax+0.13," Q3 ", #IP["NAME"].values[0], 
		rotation='horizontal', va="center_baseline", fontsize="x-small",
		bbox={'facecolor':'w', 'alpha':1, 'pad':1})

	plot_quad("MQXFB.B2L5..FL","MQXFB.B2L5..FR",optic_dat,ax_opt,ymin,ymax,cside="R")
	plot_quad("MQXFB.A2L5..FL","MQXFB.A2L5..FR",optic_dat,ax_opt,ymin,ymax,cside="L")
	Q2L5=optic_dat[optic_dat['NAME'].str.contains("MQXFB..2L5")]; Q2L5_S=np.mean(Q2L5.S.values)
	ax_opt.text(Q2L5_S-Smin-4.7,ymax+0.13," Q2 ", #IP["NAME"].values[0], 
		rotation='horizontal', va="center_baseline", fontsize="x-small",
		bbox={'facecolor':'w', 'alpha':1, 'pad':1})

	plot_quad("MQXFA.B1L5..FL","MQXFA.B1L5..FR",optic_dat,ax_opt,ymin,ymax,cside="L")
	plot_quad("MQXFA.A1L5..FL","MQXFA.A1L5..FR",optic_dat,ax_opt,ymin,ymax,cside="R")
	Q1L5=optic_dat[optic_dat['NAME'].str.contains("MQXFA..1L5")]; Q1L5_S=np.mean(Q1L5.S.values)
	ax_opt.text(Q1L5_S-Smin-4.7,ymax+0.13," Q1 ", #IP["NAME"].values[0], 
		rotation='horizontal', va="center_baseline", fontsize="x-small",
		bbox={'facecolor':'w', 'alpha':1, 'pad':1})

	#plot_quad("MQXFA.B3L5..FL","MQXFA.B3L5..FR",optic_dat,ax_opt,ymin,ymax,cside="L")
	#plot_quad("MQXFA.A3L5..FL","MQXFA.A3L5..FR",optic_dat,ax_opt,ymin,ymax,cside="R")

	#plot_quad("MQXFB.B2L5..FL","MQXFB.B2L5..FR",optic_dat,ax_opt,ymin,ymax,cside="")
	#plot_quad("MQXFB.A2L5..FL","MQXFB.A2L5..FR",optic_dat,ax_opt,ymin,ymax,cside="")

	#plot_quad("MQXFA.B1L5..FL","MQXFA.B1L5..FR",optic_dat,ax_opt,ymin,ymax,cside="")
	#plot_quad("MQXFA.A1L5..FL","MQXFA.A1L5..FR",optic_dat,ax_opt,ymin,ymax,cside="")

	plot_quad("MQXFA.A3R5..FL","MQXFA.A3R5..FR",optic_dat,ax_opt,ymin,ymax,cside="L")
	plot_quad("MQXFA.B3R5..FL","MQXFA.B3R5..FR",optic_dat,ax_opt,ymin,ymax,cside="R")
	Q3R5=optic_dat[optic_dat['NAME'].str.contains("MQXFA..3R5")]; Q3R5_S=np.mean(Q3R5.S.values)
	ax_opt.text(Q3R5_S-Smin-4.7,ymax+0.13," Q3 ", #IP["NAME"].values[0], 
		rotation='horizontal', va="center_baseline", fontsize="x-small",
		bbox={'facecolor':'w', 'alpha':1, 'pad':1})

	plot_quad("MQXFB.A2R5..FL","MQXFB.A2R5..FR",optic_dat,ax_opt,ymin,ymax,cside="R")
	plot_quad("MQXFB.B2R5..FL","MQXFB.B2R5..FR",optic_dat,ax_opt,ymin,ymax,cside="L")
	Q2R5=optic_dat[optic_dat['NAME'].str.contains("MQXFB..2R5")]; Q2R5_S=np.mean(Q2R5.S.values)
	ax_opt.text(Q2R5_S-Smin-4.7,ymax+0.13," Q2 ", #IP["NAME"].values[0], 
		rotation='horizontal', va="center_baseline", fontsize="x-small",
		bbox={'facecolor':'w', 'alpha':1, 'pad':1})

	plot_quad("MQXFA.A1R5..FL","MQXFA.A1R5..FR",optic_dat,ax_opt,ymin,ymax,cside="L")
	plot_quad("MQXFA.B1R5..FL","MQXFA.B1R5..FR",optic_dat,ax_opt,ymin,ymax,cside="R")
	Q1R5=optic_dat[optic_dat['NAME'].str.contains("MQXFA..1R5")]; Q1R5_S=np.mean(Q1R5.S.values)
	ax_opt.text(Q1R5_S-Smin-4.7,ymax+0.13," Q1 ", #IP["NAME"].values[0], 
		rotation='horizontal', va="center_baseline", fontsize="x-small",
		bbox={'facecolor':'w', 'alpha':1, 'pad':1})

	#plot_quad("MQXFA.A3R5..FL","MQXFA.A3R5..FR",optic_dat,ax_opt,ymin,ymax,cside="")
	#plot_quad("MQXFA.B3R5..FL","MQXFA.B3R5..FR",optic_dat,ax_opt,ymin,ymax,cside="")

	#plot_quad("MQXFB.A2R5..FL","MQXFB.A2R5..FR",optic_dat,ax_opt,ymin,ymax,cside="")
	#plot_quad("MQXFB.B2R5..FL","MQXFB.B2R5..FR",optic_dat,ax_opt,ymin,ymax,cside="")

	#plot_quad("MQXFA.A1R5..FL","MQXFA.A1R5..FR",optic_dat,ax_opt,ymin,ymax,cside="")
	#plot_quad("MQXFA.B1R5..FL","MQXFA.B1R5..FR",optic_dat,ax_opt,ymin,ymax,cside="")

	#       - plot Dipole
	plot_dip("MBXF.4L5..1","MBXF.4L5..4",optic_dat,ax_opt,ymin,ymax)
	plot_dip("MBXF.4R5..1","MBXF.4R5..4",optic_dat,ax_opt,ymin,ymax)

	#       - plot Corrector
	plot_Cor( "MCSXF.3L5" ,1.,optic_dat,ax_opt,ymin,ymax,cside="")
	plot_Cor( "MCSSXF.3L5",1.,optic_dat,ax_opt,ymin,ymax,cside="")
	plot_Cor( "MCOXF.3L5" ,1.,optic_dat,ax_opt,ymin,ymax,cside="")
	plot_Cor( "MCOSXF.3L5",1.,optic_dat,ax_opt,ymin,ymax,cside="")
	plot_Cor( "MCDXF.3L5" ,1.,optic_dat,ax_opt,ymin,ymax,cside="")
	plot_Cor( "MCDSXF.3L5",1.,optic_dat,ax_opt,ymin,ymax,cside="")
	plot_Cor( "MCTXF.3L5" ,1.,optic_dat,ax_opt,ymin,ymax,cside="")
	plot_Cor( "MCTSXF.3L5",1.,optic_dat,ax_opt,ymin,ymax,cside="")

	plot_Cor( "MCSXF.3R5" ,1.,optic_dat,ax_opt,ymin,ymax,cside="")
	plot_Cor( "MCSSXF.3R5",1.,optic_dat,ax_opt,ymin,ymax,cside="")
	plot_Cor( "MCOXF.3R5" ,1.,optic_dat,ax_opt,ymin,ymax,cside="")
	plot_Cor( "MCDSXF.3R5",1.,optic_dat,ax_opt,ymin,ymax,cside="")
	plot_Cor( "MCDXF.3R5" ,1.,optic_dat,ax_opt,ymin,ymax,cside="")
	plot_Cor( "MCOSXF.3R5",1.,optic_dat,ax_opt,ymin,ymax,cside="")
	plot_Cor( "MCTXF.3R5" ,1.,optic_dat,ax_opt,ymin,ymax,cside="")
	plot_Cor( "MCTSXF.3R5",1.,optic_dat,ax_opt,ymin,ymax,cside="")

elif name_optic=="LHC":
	#       - plot Corrector
	plot_Cor( "MCSX.3L5" ,1.,optic_dat,ax_opt,ymin,ymax,cside="")
	plot_Cor( "MCSSX.3L5",1.,optic_dat,ax_opt,ymin,ymax,cside="")
	plot_Cor( "MCOX.3L5" ,1.,optic_dat,ax_opt,ymin,ymax,cside="")
	plot_Cor( "MCOSX.3L5",1.,optic_dat,ax_opt,ymin,ymax,cside="")
	#plot_Cor( "MCDX.3L5" ,1.,optic_dat,ax_opt,ymin,ymax,cside="")
	#plot_Cor( "MCDSX.3L5",1.,optic_dat,ax_opt,ymin,ymax,cside="")
	plot_Cor( "MCTX.3L5" ,1.,optic_dat,ax_opt,ymin,ymax,cside="")

	plot_Cor( "MCSX.3R5" ,1.,optic_dat,ax_opt,ymin,ymax,cside="")
	plot_Cor( "MCSSX.3R5",1.,optic_dat,ax_opt,ymin,ymax,cside="")
	plot_Cor( "MCOX.3R5" ,1.,optic_dat,ax_opt,ymin,ymax,cside="")
	#plot_Cor( "MCDSX.3R5",1.,optic_dat,ax_opt,ymin,ymax,cside="")
	#plot_Cor( "MCDX.3R5" ,1.,optic_dat,ax_opt,ymin,ymax,cside="")
	plot_Cor( "MCOSX.3R5",1.,optic_dat,ax_opt,ymin,ymax,cside="")
	plot_Cor( "MCTX.3R5" ,1.,optic_dat,ax_opt,ymin,ymax,cside="")

	#       - plot Quads
	plot_quad("MQXA.3L5..FL" ,"MQXA.3L5..FR" ,optic_dat,ax_opt,ymin,ymax,cside="L")
	Q3L5=optic_dat[optic_dat['NAME'].str.contains("MQXA.3L5")]; Q3L5_S=np.mean(Q3L5.S.values)
	ax_opt.text(Q3L5_S-Smin-4.7,ymax+0.13," Q3 ", #IP["NAME"].values[0], 
		rotation='horizontal', va="center_baseline", fontsize="x-small",
		bbox={'facecolor':'w', 'alpha':1, 'pad':1})

	plot_quad("MQXB.B2L5..FL","MQXB.B2L5..FR",optic_dat,ax_opt,ymin,ymax,cside="R")
	plot_quad("MQXB.A2L5..FL","MQXB.A2L5..FR",optic_dat,ax_opt,ymin,ymax,cside="R")
	Q2L5=optic_dat[optic_dat['NAME'].str.contains("MQXB..2L5")]; Q2L5_S=np.mean(Q2L5.S.values)
	ax_opt.text(Q2L5_S-Smin-4.7,ymax+0.13," Q2 ", #IP["NAME"].values[0], 
		rotation='horizontal', va="center_baseline", fontsize="x-small",
		bbox={'facecolor':'w', 'alpha':1, 'pad':1})

	plot_quad("MQXA.1L5..FL" ,"MQXA.1L5..FR" ,optic_dat,ax_opt,ymin,ymax,cside="R")
	Q1L5=optic_dat[optic_dat['NAME'].str.contains("MQXA.1L5")]; Q1L5_S=np.mean(Q1L5.S.values)
	ax_opt.text(Q1L5_S-Smin-4.7,ymax+0.13," Q1 ", #IP["NAME"].values[0], 
		rotation='horizontal', va="center_baseline", fontsize="x-small",
		bbox={'facecolor':'w', 'alpha':1, 'pad':1})

	plot_quad("MQXA.3R5..FL" ,"MQXA.3R5..FR" ,optic_dat,ax_opt,ymin,ymax,cside="R")
	Q3R5=optic_dat[optic_dat['NAME'].str.contains("MQXA.3R5")]; Q3R5_S=np.mean(Q3R5.S.values)
	ax_opt.text(Q3R5_S-Smin-4.7,ymax+0.13," Q3 ", #IP["NAME"].values[0], 
		rotation='horizontal', va="center_baseline", fontsize="x-small",
		bbox={'facecolor':'w', 'alpha':1, 'pad':1})

	plot_quad("MQXB.A2R5..FL","MQXB.A2R5..FR",optic_dat,ax_opt,ymin,ymax,cside="L")
	plot_quad("MQXB.B2R5..FL","MQXB.B2R5..FR",optic_dat,ax_opt,ymin,ymax,cside="L")
	Q2R5=optic_dat[optic_dat['NAME'].str.contains("MQXB..2R5")]; Q2R5_S=np.mean(Q2R5.S.values)
	ax_opt.text(Q2R5_S-Smin-4.7,ymax+0.13," Q2 ", #IP["NAME"].values[0], 
		rotation='horizontal', va="center_baseline", fontsize="x-small",
		bbox={'facecolor':'w', 'alpha':1, 'pad':1})

	plot_quad("MQXA.1R5..FL" ,"MQXA.1R5..FR" ,optic_dat,ax_opt,ymin,ymax,cside="L")
	Q1R5=optic_dat[optic_dat['NAME'].str.contains("MQXA.1R5")]; Q1R5_S=np.mean(Q1R5.S.values)
	ax_opt.text(Q1R5_S-Smin-4.7,ymax+0.13," Q1 ", #IP["NAME"].values[0], 
		rotation='horizontal', va="center_baseline", fontsize="x-small",
		bbox={'facecolor':'w', 'alpha':1, 'pad':1})

	#       - plot Dipole
	plot_dip("MBXW.F4L5","MBXW.A4L5",optic_dat,ax_opt,ymin,ymax)
	plot_dip("MBXW.A4R5","MBXW.F4R5",optic_dat,ax_opt,ymin,ymax)


#       - plot IP
IP=optic_dat[optic_dat['NAME'].str.contains("IP5")]
ax_opt.axvline(x=IP["S"].values[0]-Smin,ymin=-axsize,ymax=axsize,ls='-.',lw=1,c='k',alpha=.5)
ax_opt.text(IP["S"].values[0]-Smin-3.7,ymax+0.13," IP ", #IP["NAME"].values[0], 
        rotation='horizontal', va="center_baseline", fontsize="x-small",
        bbox={'facecolor':'w', 'alpha':1, 'pad':1})
ax_opt.yaxis.set_major_locator(plt.NullLocator())
#ax_opt.set_ylim(-axsize,axsize);



fig, plt.subplots_adjust(left=.11, right=.95, top=.95, bottom=.12)
fig.savefig('IR_plot_'+name_optic+'.pdf')
plt.show()
