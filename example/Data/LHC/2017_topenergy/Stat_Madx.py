import numpy  as np
import pandas as pd
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size':18})
plt.rc('xtick',labelsize=10)
plt.rc('ytick',labelsize=10)


path="/afs/cern.ch/user/t/thpugnat/public/2017_topenergy/"

nb_seed=60;
DQXD2JX_nom=[]; DQXD2JX_ric=[]; DQXD2JX_WISE=[];
DQYD2JY_nom=[]; DQYD2JY_ric=[]; DQYD2JY_WISE=[];

MCOX3L1_nom=[]; MCOX3L1_ric=[]; MCOX3L1_WISE=[];
MCOX3R1_nom=[]; MCOX3R1_ric=[]; MCOX3R1_WISE=[];
MCOX3L5_nom=[]; MCOX3L5_ric=[]; MCOX3L5_WISE=[];
MCOX3R5_nom=[]; MCOX3R5_ric=[]; MCOX3R5_WISE=[];
for i in range(nb_seed):
   print(i+1)

   # Read Detuning linear
   # -----------------------------------------------------------------------------
   with open(path+'output_{0}/ptc_normal_errors_nom.dat'.format(i+1), 'r') as fp:
     for _ in range(16):
        fp.readline()
     line = fp.readline(); 
     splt= line.split(); 
     DQXD2JX_nom.append(float(splt[5]))
     line = fp.readline(); 
     splt= line.split(); 
     DQYD2JY_nom.append(float(splt[5]))

   with open(path+'output_{0}/ptc_normal_errors_ric.dat'.format(i+1), 'r') as fp:
     for _ in range(16):
        fp.readline()
     line = fp.readline();
     splt= line.split(); 
     DQXD2JX_ric.append(float(splt[5]))
     line = fp.readline(); 
     splt= line.split(); 
     DQYD2JY_ric.append(float(splt[5]))


   with open(path+'output_{0}/ptc_normal_errors_WISE.dat'.format(i+1), 'r') as fp:
     for _ in range(16):
        fp.readline()
     line = fp.readline();
     splt= line.split(); 
     DQXD2JX_WISE.append(float(splt[5]))
     line = fp.readline(); 
     splt= line.split(); 
     DQYD2JY_WISE.append(float(splt[5]))


   # Read Strength corr
   # -----------------------------------------------------------------------------
   fp=pd.read_csv(path+'output_{0}/correctors_nom.tfs'.format(i+1),skiprows=48,sep='\s+',header=None)
   #fp=pd.read_csv('correctors_nom.tfs',skiprows=48,sep='\s+',header=None)
   fp.columns =["NAME","BETX","BETY","DX","MUX","MUY","K2L","K3L","K4L","K5L","K6L","K2SL","K3SL","K4SL","K5SL","K6SL"]
   fp=fp.set_index("NAME")
   MCOX3L1_nom.append(fp.loc["MCOX.3L1",'K3L']/0.137)
   MCOX3R1_nom.append(fp.loc["MCOX.3R1",'K3L']/0.137)
   MCOX3L5_nom.append(fp.loc["MCOX.3L5",'K3L']/0.137)
   MCOX3R5_nom.append(fp.loc["MCOX.3R5",'K3L']/0.137)


   fp=pd.read_csv(path+'output_{0}/correctors_ric.tfs'.format(i+1),skiprows=48,sep='\s+',header=None)
   fp.columns =["NAME","BETX","BETY","DX","MUX","MUY","K2L","K3L","K4L","K5L","K6L","K2SL","K3SL","K4SL","K5SL","K6SL"]
   fp=fp.set_index("NAME")
   MCOX3L1_ric.append(fp.loc["MCOX.3L1",'K3L']/0.137)
   MCOX3R1_ric.append(fp.loc["MCOX.3R1",'K3L']/0.137)
   MCOX3L5_ric.append(fp.loc["MCOX.3L5",'K3L']/0.137)
   MCOX3R5_ric.append(fp.loc["MCOX.3R5",'K3L']/0.137)


   fp=pd.read_csv(path+'output_{0}/correctors_WISE.tfs'.format(i+1),skiprows=48,sep='\s+',header=None)
   fp.columns =["NAME","BETX","BETY","DX","MUX","MUY","K2L","K3L","K4L","K5L","K6L","K2SL","K3SL","K4SL","K5SL","K6SL"]
   fp=fp.set_index("NAME")
   MCOX3L1_WISE.append(fp.loc["MCOX.3L1",'K3L']/0.137)
   MCOX3R1_WISE.append(fp.loc["MCOX.3R1",'K3L']/0.137)
   MCOX3L5_WISE.append(fp.loc["MCOX.3L5",'K3L']/0.137)
   MCOX3R5_WISE.append(fp.loc["MCOX.3R5",'K3L']/0.137)

fg1 = plt.figure()
fg1, plt.hist(DQXD2JX_nom,  color='green',label='HE',      alpha=0.7)
fg1, plt.hist(DQXD2JX_ric,  color='red',  label='HE+Heads',alpha=0.7)
fg1, plt.hist(DQXD2JX_WISE, color='blue', label='WISE',    alpha=0.7)
fg1, plt.xlabel(r'$\partial Q_X$ / $\partial$2J$_X$')
fg1, plt.legend()

fg2 = plt.figure()
fg2, plt.hist(DQYD2JY_nom,  color='green',label='HE',      alpha=0.7)
fg2, plt.hist(DQYD2JY_ric,  color='red',  label='HE+Heads',alpha=0.7)
fg2, plt.hist(DQYD2JY_WISE, color='blue', label='WISE',    alpha=0.7)
fg2, plt.xlabel(r'$\partial Q_Y$ / $\partial$2$J_Y$')
fg2, plt.legend()

fg3 = plt.figure()
fg3, plt.plot(MCOX3L1_nom,MCOX3R1_nom,   '+g', markeredgewidth=10,label='HE IP1')
fg3, plt.plot(MCOX3L5_nom,MCOX3R5_nom,   'xg', markeredgewidth=9 ,label='HE IP5')
fg3, plt.plot(MCOX3L1_ric,MCOX3R1_ric,   '+r', markeredgewidth=10,label='HE+Heads IP1')
fg3, plt.plot(MCOX3L5_ric,MCOX3R5_ric,   'xr', markeredgewidth=9 ,label='HE+Heads IP5')
fg3, plt.plot(MCOX3L1_WISE,MCOX3R1_WISE, '+b', markeredgewidth=10,label='WISE IP1')
fg3, plt.plot(MCOX3L5_WISE,MCOX3R5_WISE, 'xb', markeredgewidth=9 ,label='WISE IP5')
fg3, plt.xlabel('KCOX3 Left [m-4]')
fg3, plt.ylabel('KCOX3 Right [m-4]')
fg3, plt.xlim(0.0,1.8)
fg3, plt.ylim(-1.6,0.0)
fg3, plt.legend(loc='best',fontsize='small')

print('Shift DQX/D2JX = {0}'.format(DQXD2JX_ric[0]-DQXD2JX_nom[0]))
print('Shift DQY/D2JY = {0}'.format(DQYD2JY_ric[0]-DQYD2JY_nom[0]))

print('Shift MCOX3L1 = {0}'.format(MCOX3L1_ric[0]-MCOX3L1_nom[0]))
print('Shift MCOX3R1 = {0}'.format(MCOX3R1_ric[0]-MCOX3R1_nom[0]))
print('Shift MCOX3L5 = {0}'.format(MCOX3L5_ric[0]-MCOX3L5_nom[0]))
print('Shift MCOX3R5 = {0}'.format(MCOX3R5_ric[0]-MCOX3R5_nom[0]))
plt.show()
