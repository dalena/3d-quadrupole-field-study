import numpy as np
import math
import sys

chs=0  #0=Stat, 1=Fidel, 2=WISE 
addBS=False

# MQXA 1R1  3R1  3L5  1L5  1R5  3R5  3L1  1L1
if (chs==0):
    b4a = np.array([ 1.260, 1.380, 1.300, 1.270, 1.210, 1.390, 1.370, 1.130]) #Stat
    b6a = np.array([ 0.160, 0.160, 0.160, 0.160, 0.160, 0.160, 0.160, 0.160]) #Stat ???

elif (chs==1):
    b4a = np.array([ 1.260, 1.380, 1.300, 1.270, 1.210, 1.390, 1.370, 1.130]) #Fidel
    b6a = np.array([ 0.228, 0.213, 0.245, 0.268, 0.246, 0.206, 0.194, 0.322]) #Fidel ???

elif (chs==2):
    b4a = np.array([ 1.017, 1.270, 1.414, 1.512, 1.451, 1.503, 1.255, 0.882]) #Wise
    b6a = np.array([ 0.228, 0.213, 0.245, 0.268, 0.246, 0.206, 0.194, 0.322]) #Wise

if addBS:
    b4a+= np.array([-0.120,-0.120, 0.120, 0.120, 0.120, 0.120,-0.120,-0.120]) #Add Beam Screen
    #b4a+= np.array([-0.110,-0.110, 0.110, 0.110, 0.110, 0.110,-0.110,-0.110]) #Add Beam Screen
    b6a+= np.array([ 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000]) #Add Beam Screen


#MQXB A2R1 B2R1 B2L5 A2L5 A2R5 B2R5 B2L1 A2L1
if (chs==0):
    b4b = np.array([ 0.267, 0.042,-0.005, 0.168, 0.286, 0.126, 0.230, 0.350]) #Stat
    b6b = np.array([ 0.415,-0.231,-0.012, 0.398, 0.353, 0.163, 0.218, 0.224]) #Stat

elif (chs==1):
    b4b = np.array([ 0.267, 0.042,-0.005, 0.168, 0.286, 0.126, 0.230, 0.350]) #Fidel
    b6b = np.array([ 0.415,-0.231,-0.012, 0.398, 0.353, 0.163, 0.218, 0.224]) #Fidel

elif (chs==2):
    b4b = np.array([ 0.040, 0.040, 0.196, 0.196, 0.320, 0.320, 0.181, 0.181]) #Wise
    b6b = np.array([ 0.036, 0.036, 0.136, 0.136, 0.202, 0.202, 0.157, 0.157]) #Wise

if addBS:
    b4b+= np.array([-0.120,-0.120, 0.120, 0.120, 0.120, 0.120,-0.120,-0.120]) #Add Beam Screen
    #b4b+= np.array([-0.110,-0.110, 0.110, 0.110, 0.110, 0.110,-0.110,-0.110]) #Add Beam Screen
    b6b+= np.array([ 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000, 0.000]) #Add Beam Screen


Lmqxa=6.37
Lanc=0.35 #0.62
Lacs=0.35 #0.34
b4a_nc =  2.07
b6a_nc =  0.27 # 2.59
b4a_cs =  1.17
b6a_cs =  4.57 #-0.54

Lmqxb=5.5
Lbnc=0.35 #0.62
Lbcs=0.35 #0.34
b4b_nc = 0.# 2.07
b6b_nc = 0.# 2.59
b4b_cs = 0.# 1.17
b6b_cs = 0.#-0.54

b4_body_a = np.zeros((8))
b6_body_a = np.zeros((8))
b4_body_b = np.zeros((8))
b6_body_b = np.zeros((8))

for i in range(0,8,1):
    print(i)
    print(b6a[i])
    b4_body_a[i] =b4a[i] -(b4a_nc*Lanc + b4a_cs*Lacs)/(Lmqxa)
    b6_body_a[i] =b6a[i] -(b6a_nc*Lanc + b6a_cs*Lacs)/(Lmqxa)

    print(b4a[i])
    b4_body_b[i] =b4b[i] -(b4b_nc*Lbnc + b4b_cs*Lbcs)/(Lmqxb)
    b6_body_b[i] =b6b[i] -(b6b_nc*Lbnc + b6b_cs*Lbcs)/(Lmqxb)


print("*********************************************")
print("                   Result")
print("*********************************************")

if (chs==0):
    print(  "                    Stat")
elif (chs==1):
    print(  "                    Fidel")
elif (chs==2):
    print(  "                    WISE")
if addBS:
    print(  "                    WISE")
print("\n\nMQXA: 1R1  3R1  3L5  1L5  1R5  3R5  3L1  1L1")
print(f'  - Lbody:\n       {Lmqxa-Lanc-Lacs}')
print("  - b4:\n       * Total:")
print(b4a)
print("\n       * Body:")
print(b4_body_a)
print("\n  - b6:\n       * Total:")
print(b6a)
print("\n       * Body:")
print(b6_body_a)

print("\n\nMQXB: A2R1 B2R1 B2L5 A2L5 A2R5 B2R5 B2L1 A2L1")
print("  - b4:\n       * Total:")
print(b4b)
print("\n       * Body:")
print(b4_body_b)
print("\n  - b6:\n       * Total:")
print(b6b)
print("\n       * Body:")
print(b6_body_b)

print("\n\n   Pour le b4")
print("(A2R1+B2R1)/2:{}".format((b4b[0]+b4b[1])/2))
print("(B2L5+A2L5)/2:{}".format((b4b[2]+b4b[3])/2))
print("(A2R5+B2R5)/2:{}".format((b4b[4]+b4b[5])/2))
print("(B2L1+A2L1)/2:{}".format((b4b[6]+b4b[7])/2))

print("\n\n   Pour le b6")
print("(A2R1+B2R1)/2:{}".format((b6b[0]+b6b[1])/2))
print("(B2L5+A2L5)/2:{}".format((b6b[2]+b6b[3])/2))
print("(A2R5+B2R5)/2:{}".format((b6b[4]+b6b[5])/2))
print("(B2L1+A2L1)/2:{}".format((b6b[6]+b6b[7])/2))

sys.exit(0)
#********************************************************************************
## model for distributing b4 from beam screen
# difference wise -Fidel coming from beam screen
#MQXA 1R1  1L1  1R5  1L5  3R1  3L1  3R5  3L5
dWF_A = np.array([-0.243, -0.248, 0.241, 0.242,  -0.110,  -0.115, 0.113, 0.114])
#MQXB A2R1 A2L1 B2R1 B2L1 A2R5 A2L5 B2R5 B2L5    
dWF_B = np.array([-0.227, -0.169,  -0.002,  -0.049, 0.034, 0.028, 0.194, 0.201])

dWF_A_heads = dWF_A/2.
dWF_B_heads = dWF_B/2.

dWF_A_body = dWF_A -(dWF_A_heads*Lnc + dWF_A*Lcs)/Lmqxa
dWF_B_body = dWF_B -(dWF_B_heads*Lnc + dWF_B*Lcs)/Lmqxb
print("MQXA beam screen total")
print(dWF_A)
print(dWF_A_heads)
print("beam screen in the heads")
print(dWF_A_heads)
print("beam screen MQXA: 1R1  1L1  1R5  1L5  3R1  3L1  3R5  3L5")
print(dWF_A_body)

print("MQXB beam screen total")
print(dWF_B)
print(dWF_B_heads)
print("beam screen in the heads")
print(dWF_B_heads)
print("beam screen MQXB: A2R1 A2L1 B2R1 B2L1 A2R5 A2L5 B2R5 B2L5")
print(dWF_B_body)

# the new tables should be update with the values
# b4nc_tot_A(B) = b4nc + b4dWF_A(B)/2.
# b4cs_tot_A(B) = b4cs + b4dWF_A(B)/2.
# b4body_A(B) =  b4body + b4dWF_A(B)_body 
