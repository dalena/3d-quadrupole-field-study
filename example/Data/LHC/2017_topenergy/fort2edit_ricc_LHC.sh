#!/bin/bash
#args=("$@")
if [ $# -ge 1 ]; then
	fname=$1
else
	fname=fort.2_1
fi
if [ $# -ge 2 ]; then
	nbsub=$2
else
	nbsub=128
fi

# Deziper
gunzip $fname.gz


# Copy the file
cp $fname fc.2.tmp

# Set Quad name and get where to add the name in "SINGLE ELEMENT"
Quad1='mqxa.3l5..1';
Quad2='mqxa.1r5..1';
Quad3='mqxb.b2l5..1';
Quad4='mqxb.a2r5..1';



Q=($(awk '/mqxa.3l5..1/{print NR}' $fname))
Q_first=${Q[0]}





# Change name in the structure
#Quad1_lst=( 'mqxa.3l5..1' 'mqxa.1l5..1' 'mqxa.3l1..1' 'mqxa.1l1..1' )
awk '/mqxa.3l5..1/{
sv=$0
n=gsub(/mqxa.3l5..1/, "", $0)
$0=sv
for (i=0; i<n; i++)
{
  c++;
  if      ( int((c-2)/'$nbsub')%4==1 ) sub("mqxa.3l5..1","mqxa.1l5..1");
  else if ( int((c-2)/'$nbsub')%4==2 ) sub("mqxa.3l5..1","mqxa.3l1..1");
  else if ( int((c-2)/'$nbsub')%4==3 ) sub("mqxa.3l5..1","mqxa.1l1..1");
}
}1' fc.2.tmp > fc.2.tmp_1

#Quad2_lst=( 'mqxa.1r5..1' 'mqxa.3r5..1' 'mqxa.1r1..1' 'mqxa.3r1..1' )
awk '/mqxa.1r5..1/{
sv=$0
n=gsub(/mqxa.1r5..1/, "", $0)
$0=sv
for (i=0; i<n; i++)
{
  c++;
  if      ( int((c-2)/'$nbsub')%4==1 ) sub("mqxa.1r5..1","mqxa.3r5..1");
  else if ( int((c-2)/'$nbsub')%4==2 ) sub("mqxa.1r5..1","mqxa.1r1..1");
  else if ( int((c-2)/'$nbsub')%4==3 ) sub("mqxa.1r5..1","mqxa.3r1..1");
}
}1' fc.2.tmp_1 > fc.2.tmp_2
#cp fc.2.tmp_2   fc.2.tmp_3

##Quad3_lst=( 'mqxb.b2l5..1' 'mqxb.a2l5..1' 'mqxb.b2l1..1' 'mqxb.a2l1..1' )
#awk '/mqxb.b2l5..1/{
#sv=$0
#n=gsub(/mqxb.b2l5..1/, "", $0)
#$0=sv
#for (i=0; i<n; i++)
#{
#  c++;
#  if      ( int((c-2)/'$nbsub')%4==1 ) sub("mqxb.b2l5..1","mqxb.a2l5..1");
#  else if ( int((c-2)/'$nbsub')%4==2 ) sub("mqxb.b2l5..1","mqxb.b2l1..1");
#  else if ( int((c-2)/'$nbsub')%4==3 ) sub("mqxb.b2l5..1","mqxb.a2l1..1");
#}
#}1' fc.2.tmp_2 > fc.2.tmp_3
cp fc.2.tmp_2   fc.2.tmp_3

##Quad4_lst=( 'mqxb.a2r5..1' 'mqxb.b2r5..1' 'mqxb.a2r1..1' 'mqxb.b2r1..1' )
#awk '/mqxb.a2r5..1/{
#sv=$0
#n=gsub(/mqxb.a2r5..1/, "", $0)
#$0=sv
#for (i=0; i<n; i++)
#{
#  c++;
#  if      ( int((c-2)/'$nbsub')%4==1 ) sub("mqxb.a2r5..1","mqxb.b2r5..1");
#  else if ( int((c-2)/'$nbsub')%4==2 ) sub("mqxb.a2r5..1","mqxb.a2r1..1");
#  else if ( int((c-2)/'$nbsub')%4==3 ) sub("mqxb.a2r5..1","mqxb.b2r1..1");
#}
#}1' fc.2.tmp_3 > fc.2.tmp_4
cp fc.2.tmp_3   fc.2.tmp_4


# insert definition of the additional quadrupoles in the single element part
cp fc.2.tmp_4 fc.2.tmp0
sed -e $Q_first"imqxa.1l5..1        2   4.252783713e-04   0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00" fc.2.tmp0 > fc.2.tmp1
sed -e $Q_first"imqxa.3l1..1        2   4.252783713e-04   0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00" fc.2.tmp1 > fc.2.tmp2
sed -e $Q_first"imqxa.1l1..1        2   4.252783713e-04   0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00" fc.2.tmp2 > fc.2.tmp3
sed -e $Q_first"imqxa.3r5..1        2  -4.252783713e-04   0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00" fc.2.tmp3 > fc.2.tmp4
sed -e $Q_first"imqxa.1r1..1        2  -4.252783713e-04   0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00" fc.2.tmp4 > fc.2.tmp5
sed -e $Q_first"imqxa.3r1..1        2  -4.252783713e-04   0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00" fc.2.tmp5 > fc.2.tmp6
#sed -e $Q_first"imqxb.a2l5..1       2  -3.671948261e-04   0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00" fc.2.tmp6 > fc.2.tmp7
#sed -e $Q_first"imqxb.b2l1..1       2  -3.671948261e-04   0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00" fc.2.tmp7 > fc.2.tmp8
#sed -e $Q_first"imqxb.a2l1..1       2  -3.671948261e-04   0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00" fc.2.tmp8 > fc.2.tmp9
#sed -e $Q_first"imqxb.b2r5..1       2   3.671948261e-04   0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00" fc.2.tmp9 > fc.2.tmp10
#sed -e $Q_first"imqxb.a2r1..1       2   3.671948261e-04   0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00" fc.2.tmp10> fc.2.tmp11
#sed -e $Q_first"imqxb.b2r1..1       2   3.671948261e-04   0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00" fc.2.tmp11> fc.2.tmp12

# clean
mv fc.2.tmp6 $fname.new
#mv fc.2.tmp12 $fname.new
rm fc.2.tm*
