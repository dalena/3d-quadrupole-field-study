+------------------------------------------+
|                  README                  |
+------------------------------------------+
   * Compilation option
The Fringe Field module must be activated at the compilation by adding the flag FFIELD


   * Test
The ref case correspond to the reference case with strong multipole in the head of the IT's Quadrupole at ip1 and ip5. The C2_ND0 case check that the Fringe Field module doesn't add any Tune Shift (the output should be the same as ref).


   * Documentation
This method allows the user to use a longitudinal description of the quadrupole magnetic field, adapted for each magnet specifically selected for the study, without changing the reference optics of SixTrack.
This patch has been developed in order to study the impact on the single particle tracking of the fringe field in low beta insertions, like IP1 and IP5 in LHC.
This Fringe Field module is controlled by the FFIE block in fort.3 and is followed by 3 types of flag: FFQN, FFMS and FFFI. The FFQN flag select which quadrupole (name) has a longitudinal description in additional files that will be loaded in the study and link it to the type of Fringe Field that will be used (additional file).
The FFMS flag specify the name of the multipole to skip in the tracking, in order to not increase the integrated nonlinearities (if multipole kicks are already added for the quadrupole in the lattice). The FFFI flag is followed by the files where the different Fringe Field vector potential values are saved.

Keyword     FFIE
Data lines  Variable
Format      FFQN     Quadname       #in              #ex
            FFMS     Multiname
            FFFI     Filename       LengthInQuad     LengthTot     (Aperture)
            The last parameter of FFFI is optional, by defualt it is fixed as 0.080m.

Table: Input parameters for FFIE block
+----------+--------------+------+-----------------------------------------------------------------------------+
|Type line | Arguments    | Unit | Description                                                                 |
+----------+--------------+------+-----------------------------------------------------------------------------+
| FFQN                                                                                                         |
+----------+--------------+------+-----------------------------------------------------------------------------+
|          | Quadname     | -    | Name of quadrupole. Must be the same as in the list of single elements.     |
+----------+--------------+------+-----------------------------------------------------------------------------+
|          | #in          | -    | Index of the Fringe Field to use at the beginning of the Quadrupole.        |
+----------+--------------+------+-----------------------------------------------------------------------------+
|          | #ex          | -    | Index of the Fringe Field to use at the end of the Quadrupole.              |
+----------+--------------+------+-----------------------------------------------------------------------------+
| FFMS                                                                                                         |
+----------+--------------+------+-----------------------------------------------------------------------------+
|          | Multiname    | -    | Name of multipole that will be skipped in the main lattice. Must be the same|
|          |              |      | as in the list of single elements.                                          |
+----------+--------------+------+-----------------------------------------------------------------------------+
| FFFI                                                                                                         |
+----------+--------------+------+-----------------------------------------------------------------------------+
|          | Filename     | -    | Name of the Fringe Field profile file. See next table for proper formatting.|
+----------+--------------+------+-----------------------------------------------------------------------------+
|          | LengthInQuad | m    | Length inside the quadrupole, i.e. integrated length of the equivalent      |
|          |              |      | quadrupole for this file.                                                   |
+----------+--------------+------+-----------------------------------------------------------------------------+
|          | LengthTot    | m    | Total length of integration inside the file.                                |
+----------+--------------+------+-----------------------------------------------------------------------------+
|          | Aperture     | m    | The physical aperture of the quadrupole, or the maximum apeture at which you|

|          |              |      | trust the representation of the Vector Potential.                           |
+----------+--------------+------+-----------------------------------------------------------------------------+


The Fringe Field Vector Potential profile is saved using the following format:
Format z i j k Ax Ay Az

Table: Input parameter in the Fringe Field profile's file
+--------------+--------------+------------------------------------------------------------------------------+
| Arguments    | Unit         | Description                                                                  |
+--------------+--------------+------------------------------------------------------------------------------+
| z            | m            | Longitudinal position of the kick.                                           |
+--------------+--------------+------------------------------------------------------------------------------+
| i            | -            | Exponent of x in the Horner polynomial representation of the vector          |
|              |              | potential.                                                                   |
+--------------+--------------+------------------------------------------------------------------------------+
| j            | -            | Exponent of y in the Horner polynomial representation of the vector          |
|              |              | potential.                                                                   |
+--------------+--------------+------------------------------------------------------------------------------+
| k            | -            | Exponent of z in the Horner polynomial representation of the vector          |
|              |              | potential. Always 0 in case of magnetic quadrupole.                          |
+------------------------------------------------------------------------------------------------------------+
| Ax           | m^{-(1+i+j)} | Coefficient of the Horner polynomial of the vector potential for the axis x. |
+--------------+--------------+------------------------------------------------------------------------------+
| Ay           | m^{-(1+i+j)} | Coefficient of the Horner polynomial of the vector potential for the axis y. |
+--------------+--------------+------------------------------------------------------------------------------+
| Az           | m^{-(1+i+j)} | Coefficient of the Horner polynomial of the vector potential for the axis z. |
+--------------+--------------+------------------------------------------------------------------------------+
