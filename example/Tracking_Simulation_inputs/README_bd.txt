###################################
#        HOW TO INSTALL           #
###################################

--- REQUIRED LIBRARIES --- (I suppose, not check the minimal one)
   - libboost-all-dev
   - libeigen3-dev
   - libgnuplot-iostream-dev  (!!! See OSS 1 !!!)
   - FFTW3 libraries (libfftw3-dev ubuntu)
   - doxygen (Just for documentation
   - cmake
  
--- CMAKE ---
   To compile the code first you have to go to the directory "build" (cd "yourpath"/vector_potential_templ/build) and from there use the command "cmake .."
   This command will check if you have the right libraries (if not it should print comprensible messages that inform you where is the problem) and then generate a Makefiles
   
--- COMPILE ---
   At this point, if cmake ended succesfully, you just have to run the make file (using the command "make" or "make -jN" where N is the number of you thread, in my case only "make -j2" )
   

  
OSS 1) 
   gnuplot-iostream.h ha attualmente un errore, definisce al suo interno 2 funzioni e quindi questo genera errori in file di compilazione, nella cartella "gnuplot-iostream_cheFunziona" si trova un file corretto da sostituire (di solito nella cartella /usr/include)

###################################
#        HOW TO RUN               #
###################################
   
--- SET UP CONFIGURATION FILES ----
   In the folder "yourpath"/vector_potential_templ/config is present a text file called "config.pot", it contains all the parameters you can modify
--- RUN ---   
   In order to run the executable you just have to go in the directory "yourpath"/vector_potential_templ/build/bin and run it (of course after the compilation step)
   
###################################
#  HOW TO GENERATE DOCUMENTATION  #
###################################
   
--- GENERATE IT ---
   It's sufficent to go in the build directory and use the command "make doc" (of course if you have doxygen and you cmake stage was succesfully completed)
   
--- LATEX ---
   To generate the laTeX one you need another step: go in the directory "yourpath"/vector_potential_templ/build/doc/latex and generate the pdf from the file "refman.tex" OR AN EASIER WAY is to just use the makefile, so just use the command "make" in that directory
   
OSS 2)
   per generare un file di progetto per CodeBlocks utilizzando cmake è sufficiente andare nella cartella build e digitare il seguente comando 
      $ cmake .. -G "CodeBlocks - Unix Makefiles"
