#!/bin/bash
#args=("$@")
if [ $# -ge 1 ]; then
	fname=$1
else
	fname=fort.2_1
fi
if [ $# -ge 2 ]; then
	nbsub=$2
else
	nbsub=16
fi

# Deziper
gunzip $fname.gz


# Copy the file
cp $fname fc.2.tmp

# Set Quad name and get where to add the name in "SINGLE ELEMENT"
Quad1='mqxfb.b2l5..1';
Quad2='mqxfa.b3l5..1';
Quad3='mqxfa.a1r5..1';
Quad4='mqxfb.a2r5..1';


a1r5=($(awk '/mqxfa.a1r5..1/{print NR}' $fname))
a1r5_first=${a1r5[0]}





# Change name in the structure
#Quad1_lst=( 'mqxfb.b2l5..1' 'mqxfb.a2l5..1' )
awk '/mqxfb.b2l5..1/{
sv=$0
n=gsub(/mqxfb.b2l5..1/, "", $0)
$0=sv
for (i=0; i<n; i++)
{
  c++;
  if ( int((c-2)/'$nbsub')%2==1 ) sub("mqxfb.b2l5..1","mqxfb.a2l5..1");
}
}1' fc.2.tmp > fc.2.tmp_1

#Quad2_lst=( 'mqxfa.b3l5..1' 'mqxfa.a3l5..1' 'mqxfa.b1l5..1' 'mqxfa.a1l5..1' )
awk '/mqxfa.b3l5..1/{
sv=$0
n=gsub(/mqxfa.b3l5..1/, "", $0)
$0=sv
for (i=0; i<n; i++)
{
  c++;
  if ( int((c-2)/'$nbsub')%4==1 ) sub("mqxfa.b3l5..1","mqxfa.a3l5..1");
  else if ( int((c-2)/'$nbsub')%4==2 ) sub("mqxfa.b3l5..1","mqxfa.b1l5..1");
  else if ( int((c-2)/'$nbsub')%4==3 ) sub("mqxfa.b3l5..1","mqxfa.a1l5..1");
}
}1' fc.2.tmp_1 > fc.2.tmp_2

#Quad3_lst=( 'mqxfa.a1r5..1' 'mqxfa.b1r5..1' 'mqxfa.a3r5..1' 'mqxfa.b3r5..1' )
awk '/mqxfa.a1r5..1/{
sv=$0
n=gsub(/mqxfa.a1r5..1/, "", $0)
$0=sv
for (i=0; i<n; i++)
{
  c++;
  if ( int((c-2)/'$nbsub')%4==1 ) sub("mqxfa.a1r5..1","mqxfa.b1r5..1");
  else if ( int((c-2)/'$nbsub')%4==2 ) sub("mqxfa.a1r5..1","mqxfa.a3r5..1");
  else if ( int((c-2)/'$nbsub')%4==3 ) sub("mqxfa.a1r5..1","mqxfa.b3r5..1");
}
}1' fc.2.tmp_2 > fc.2.tmp_3

#Quad4_lst=( 'mqxfb.a2r5..1' 'mqxfb.b2r5..1' )
awk '/mqxfb.a2r5..1/{
sv=$0
n=gsub(/mqxfb.a2r5..1/, "", $0)
$0=sv
for (i=0; i<n; i++)
{
  c++;
  if ( int((c-2)/'$nbsub')%2==1 ) sub("mqxfb.a2r5..1","mqxfb.b2r5..1");
}
}1' fc.2.tmp_3 > fc.2.tmp_4


# insert definition of the additional quadrupoles in the single element part
sed -e $a1r5_first"imqxfa.b1r5..1      2  -1.499749500e-03   0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00" fc.2.tmp_4 > fc.2.tmp1
sed -e $a1r5_first"imqxfb.b2r5..1      2   2.545302000e-03   0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00" fc.2.tmp1 > fc.2.tmp2
sed -e $a1r5_first"imqxfa.a3r5..1      2  -1.499749500e-03   0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00" fc.2.tmp2 > fc.2.tmp3
sed -e $a1r5_first"imqxfa.b3r5..1      2  -1.499749500e-03   0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00" fc.2.tmp3 > fc.2.tmp4
sed -e $a1r5_first"imqxfa.a3l5..1      2   1.499749500e-03   0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00" fc.2.tmp4 > fc.2.tmp5
sed -e $a1r5_first"imqxfb.a2l5..1      2  -2.545302000e-03   0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00" fc.2.tmp5 > fc.2.tmp6
sed -e $a1r5_first"imqxfa.a1l5..1      2   1.499749500e-03   0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00" fc.2.tmp6 > fc.2.tmp7
sed -e $a1r5_first"imqxfa.b1l5..1      2   1.499749500e-03   0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00    0.000000000e+00" fc.2.tmp7 > fc.2.tmp8

# clean
mv fc.2.tmp8 $fname.new
rm fc.2.tm*
