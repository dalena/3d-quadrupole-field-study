#!/bin/bash
args=("$@")
# cible = folder where the Lie2 files will be
cible=${args[1]}
# origin1 = folder where the sixtrack original files are
origi1=${args[0]}
path=$(pwd)
#cible="six_inp_flatbeam_correct_edit_harmo_corb6_Lie2"
#origi1="six_inp_flatbeam_correct_edit_harmo_corb6"

seed_b=1
seed_e=60

# check repository
echo ''; echo "     * check repository"
if [ -d $origi1 ]; then 
	echo "           - 1st repository of origin found!"
else
	echo "           - Error run_fortxedit_new.sh: 1st repository of origin not found!"
	exit
fi

#if [ "" = "$origi2" ]; then
#	echo "           - 2st repository of origin not used!"
#else
if [ "" != "$origi2" ]; then
	if [ -d $origi2 ]; then 
		echo "           - 2st repository of origin found!"
	else
		echo "           - Error run_fortxedit_new.sh: 2st repository of origin not found!"
		exit
	fi
fi


if [ -d $cible ]; then 
	rm $cible/*
	echo "           - Repository for new file found and cleaned!"
else
	mkdir $cible
	echo "           - Repository for new file made!"
fi


# create new file by changing the name of some quadrupole
echo ''; echo "     * Create new file by changing the name of some quadrupole"
for fnum in `seq $seed_b $seed_e`; do
	echo "        > Seed "$fnum
	echo "           - Modification of fort.2!"
#	./fort2edit_new.sh "$origi/fort.2_$fnum" 16
	./fort2edit_ricc.sh "$origi1/fort.2_$fnum" 16
	
	echo "           - Copy of fort.8!"
	cp $origi1/fort.8_$fnum.gz $cible/fort.8_$fnum.gz

	
	echo "           - Copy of fort.16!"
	cp $origi1/fort.16_$fnum.gz $cible/fort.16_$fnum.gz
done


# transfert new file to cible, rename them and zip them
echo ''; echo "     * transfert new file to $cible, rename them and zip them"
mv $origi1/fort.2_*.new $cible/
for fnum in `seq $seed_b $seed_e`; do
    mv "$cible/fort.2_$fnum.new"  "$cible/fort.2_$fnum"
done


gzip $origi1/fort.2*
gzip $cible/fort.2*



